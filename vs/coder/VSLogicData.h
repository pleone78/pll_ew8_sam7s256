/*
 * Version:   50
 * Signature: eb1b 13af 7a0e 2056 ded0 ade0
 *
 * Id:        VSLogicData.h
 *
 * Function:  VS System Header File.
 *
 * Generated: Thu May 30 15:27:04 2019
 *
 * Coder 5, 4, 0, 1273
 * 
 * This is an automatically generated file. It will be overwritten by the Coder. 
 * 
 * DO NOT EDIT THE FILE! 
 */


#ifndef __VSLOGICDATA_H
#define __VSLOGICDATA_H


/*
 * Number of Identifiers.
 */
#define VS_NOF_ACTION_EXPRESSIONS        0X004d0  /*  1232 */
#define VS_NOF_ACTION_FUNCTIONS          0X080  /* 128 */
#define VS_NOF_EVENT_GROUPS              0X004  /*   4 */
#define VS_NOF_EVENTS                    0X07e  /* 126 */
#define VS_NOF_EXTERNAL_VARIABLES        0X087  /* 135 */
#define VS_NOF_GUARD_EXPRESSIONS         0X005fc  /*  1532 */
#define VS_NOF_INSTANCES                 0X001  /*   1 */
#define VS_NOF_INTERNAL_VARIABLES        0X028  /*  40 */
#define VS_NOF_SIGNALS                   0X06d  /* 109 */
#define VS_NOF_STATE_MACHINES            0X064  /* 100 */
#define VS_NOF_STATES                    0X00253  /*   595 */


/*
 * Include SEM Library Header File.
 */
#include "SEMLibE.h"


#if (VS_CODER_GUID != 0X015e8c916)
#error The generated file does not match the SEMTypes.h header file.
#endif


/*
 * Include Global Events Header File.
 */
#include "PLL_PEvent.h"


/*
 * External Declarations for Guard Expressions Function Pointer Table.
 */
extern VS_GUARDEXPR_TYPE const VSLogicVSGuard[1532];


/*
 * External Declaration for SEM Deduct Function.
 */
extern unsigned char VSLogicSMP_Deduct (SEM_CONTEXT VS_TQ_CONTEXT * Context, SEM_EVENT_TYPE EventNo);


/*
 * External Declaration for SEM Signal Doubble Buffer Function.
 */
extern VS_VOID VSLogicSMP_SignalDB (SEM_CONTEXT VS_TQ_CONTEXT * Context, SEM_EVENT_TYPE SignalNo);


/*
 * External Declaration for wrapper function for all initializing functions.
 */
extern unsigned char VSLogicSMP_InitAll (SEM_CONTEXT VS_TQ_CONTEXT * * Context);


/*
 * Conditional Compilation Definitions.
 */
#define SEM_DEDUCT_FUNC                  1
#define SEM_DEDUCT_WITH_VA_LIST          0


/*
 * Event Identifier Definitions.
 */
#define eABT1Off                         0X001  /*   1 */
#define eABT1On                          0X002  /*   2 */
#define eABT2Off                         0X003  /*   3 */
#define eABT2On                          0X004  /*   4 */
#define eAfterPanicBtn                   0X005  /*   5 */
#define eAuto                            0X006  /*   6 */
#define eBattLow                         0X007  /*   7 */
#define eBioNotOk                        0X008  /*   8 */
#define eBioOk                           0X009  /*   9 */
#define eBlankKey                        0X00A  /*  10 */
#define eBtn1                            0X00B  /*  11 */
#define eBtn2                            0X00C  /*  12 */
#define eCfgLoad                         0X00D  /*  13 */
#define eClosed1                         0X00E  /*  14 */
#define eClosed2                         0X00F  /*  15 */
#define eCmdOpen1                        0X010  /*  16 */
#define eCmdOpen2                        0X011  /*  17 */
#define eCns1                            0X012  /*  18 */
#define eCns1NotOk                       0X013  /*  19 */
#define eCns1Ok                          0X014  /*  20 */
#define eCns2                            0X015  /*  21 */
#define eCns2NotOk                       0X016  /*  22 */
#define eCns2Ok                          0X017  /*  23 */
#define eCns3                            0X018  /*  24 */
#define eCns3NotOk                       0X019  /*  25 */
#define eCns3Ok                          0X01A  /*  26 */
#define eCommClr                         0X01B  /*  27 */
#define eCommFid                         0X01C  /*  28 */
#define eCommFrun                        0X01D  /*  29 */
#define eCommKey                         0X01E  /*  30 */
#define eCommRid                         0X01F  /*  31 */
#define eCommRun                         0X020  /*  32 */
#define eCommSid                         0X021  /*  33 */
#define eCountEnd                        0X022  /*  34 */
#define eDeadLine                        0X023  /*  35 */
#define eDelayExit                       0X024  /*  36 */
#define eEmergency                       0X025  /*  37 */
#define eEndPlay                         0X026  /*  38 */
#define eEndPlayBtn                      0X027  /*  39 */
#define eEndTPolTr                       0X028  /*  40 */
#define eEndTService                     0X029  /*  41 */
#define eEndTSession                     0X02A  /*  42 */
#define eEndTvisEx                       0X02B  /*  43 */
#define eEnter                           0X02C  /*  44 */
#define eExit                            0X02D  /*  45 */
#define eF1                              0X02E  /*  46 */
#define eF2                              0X02F  /*  47 */
#define eF3                              0X030  /*  48 */
#define eFingNotPres                     0X031  /*  49 */
#define eFingPres                        0X032  /*  50 */
#define eFireAlm                         0X033  /*  51 */
#define eFirstIn                         0X034  /*  52 */
#define eFreeSession                     0X035  /*  53 */
#define eInSession                       0X036  /*  54 */
#define eInhibit                         0X037  /*  55 */
#define eLastOut                         0X038  /*  56 */
#define eMBtn1                           0X039  /*  57 */
#define eMBtn2                           0X03A  /*  58 */
#define eMetal                           0X03B  /*  59 */
#define eMetalOff                        0X03C  /*  60 */
#define eMetalOn                         0X03D  /*  61 */
#define eMode                            0X03E  /*  62 */
#define eMore                            0X03F  /*  63 */
#define eMorning                         0X040  /*  64 */
#define eNight                           0X041  /*  65 */
#define eNormal                          0X042  /*  66 */
#define eNotClosed1                      0X043  /*  67 */
#define eNotClosed2                      0X044  /*  68 */
#define eNotMore                         0X045  /*  69 */
#define eNotPresent                      0X046  /*  70 */
#define eObject                          0X047  /*  71 */
#define eOnOff                           0X048  /*  72 */
#define eOpened1                         0X049  /*  73 */
#define eOpened2                         0X04A  /*  74 */
#define eOutSession                      0X04B  /*  75 */
#define ePCBioAmpOff                     0X04C  /*  76 */
#define ePCBioAmpOn                      0X04D  /*  77 */
#define ePCBioExit                       0X04E  /*  78 */
#define ePCBioOff                        0X04F  /*  79 */
#define ePCBioOn                         0X050  /*  80 */
#define ePCBioPolling                    0X051  /*  81 */
#define ePCBioRecStop                    0X052  /*  82 */
#define ePCBioStart                      0X053  /*  83 */
#define ePCBioStop                       0X054  /*  84 */
#define ePanicBtn                        0X055  /*  85 */
#define ePlayBtn                         0X056  /*  86 */
#define ePresent                         0X057  /*  87 */
#define eProgOn                          0X058  /*  88 */
#define eProgStart                       0X059  /*  89 */
#define eProxDis1                        0X05A  /*  90 */
#define eProxDis2                        0X05B  /*  91 */
#define eProxNorm1                       0X05C  /*  92 */
#define eProxNorm2                       0X05D  /*  93 */
#define eQ5ReadOk                        0X05E  /*  94 */
#define eRecBtn                          0X05F  /*  95 */
#define eResAftLowBStat                  0X060  /*  96 */
#define eResBtn1                         0X061  /*  97 */
#define eResBtn2                         0X062  /*  98 */
#define eResMBtn1                        0X063  /*  99 */
#define eResMBtn2                        0X064  /* 100 */
#define eReset                           0X065  /* 101 */
#define eResetSod                        0X066  /* 102 */
#define eServiceOff                      0X067  /* 103 */
#define eServiceOn                       0X068  /* 104 */
#define eSetAftLowBStat                  0X069  /* 105 */
#define eStart                           0X06A  /* 106 */
#define eT1End                           0X06B  /* 107 */
#define eT2End                           0X06C  /* 108 */
#define eT3End                           0X06D  /* 109 */
#define eTBioEnd                         0X06E  /* 110 */
#define eTBtn1End                        0X06F  /* 111 */
#define eTBtn2End                        0X070  /* 112 */
#define eTMBtn2End                       0X071  /* 113 */
#define eTOnePassEnd                     0X072  /* 114 */
#define eTReadyEnd                       0X073  /* 115 */
#define eTReservEnd                      0X074  /* 116 */
#define eTSwALBEnd                       0X075  /* 117 */
#define eTVipEnd                         0X076  /* 118 */
#define eTic                             0X077  /* 119 */
#define eToProgOn                        0X078  /* 120 */
#define eVacOff                          0X079  /* 121 */
#define eVacOn                           0X07A  /* 122 */
#define eViolateP1                       0X07B  /* 123 */
#define eViolateP2                       0X07C  /* 124 */
#define eVip                             0X07D  /* 125 */


/*
 * Constants.
 */
#define PassCountMax                     ((VS_UCHAR) 10)
#define T48Ore                           ((VS_INT) 172800)
#define T4Ore                            ((VS_INT) 14400)
#define TClear                           ((VS_INT) 1)
#define THoly                            ((VS_INT) 70)
#define TWork                            ((VS_INT) 76)
#define aABT1                            ((VS_UCHAR) 58)
#define aABT2                            ((VS_UCHAR) 59)
#define aAccessCtrl                      ((VS_UCHAR) 48)
#define aAdvanceConf                     ((VS_UCHAR) 52)
#define aAfterLowBatStat                 ((VS_UCHAR) 67)
#define aBioCnbTOut                      ((VS_UCHAR) 41)
#define aBioIn                           ((VS_UCHAR) 18)
#define aBioMode                         ((VS_UCHAR) 24)
#define aBioOut                          ((VS_UCHAR) 40)
#define aBlkAlarmRed                     ((VS_UCHAR) 33)
#define aCAM                             ((VS_UCHAR) 10)
#define aCN93_4AsIntTrLight              ((VS_UCHAR) 64)
#define aDelayVisEx                      ((VS_UCHAR) 60)
#define aDoorsOff                        ((VS_UCHAR) 28)
#define aEasyTrans                       ((VS_UCHAR) 51)
#define aEmeMode                         ((VS_UCHAR) 21)
#define aEnNightRSOD                     ((VS_UCHAR) 46)
#define aEnPanicBtn                      ((VS_UCHAR) 45)
#define aEnReady                         ((VS_SCHAR) 61)
#define aEnSynHall                       ((VS_UCHAR) 44)
#define aF1Sel                           ((VS_UCHAR) 2)
#define aF2Sel                           ((VS_UCHAR) 3)
#define aF3Sel                           ((VS_UCHAR) 4)
#define aFInMetal                        ((VS_UCHAR) 31)
#define aFInMore                         ((VS_UCHAR) 43)
#define aFri                             ((VS_UCHAR) 16)
#define aHHDay                           ((VS_UCHAR) 8)
#define aHHNight                         ((VS_UCHAR) 6)
#define aIntraSOD                        ((VS_UCHAR) 37)
#define aLampOn                          ((VS_UCHAR) 30)
#define aLanguage                        ((VS_UCHAR) 35)
#define aLastOnOff                       ((VS_UCHAR) 53)
#define aLastOutEm                       ((VS_UCHAR) 32)
#define aMMDay                           ((VS_UCHAR) 9)
#define aMMNight                         ((VS_UCHAR) 7)
#define aMOutAuto                        ((VS_UCHAR) 29)
#define aMail                            ((VS_UCHAR) 27)
#define aMemBioRes                       ((VS_UCHAR) 50)
#define aMemoReq                         ((VS_UCHAR) 38)
#define aMon                             ((VS_UCHAR) 12)
#define aMoreIn                          ((VS_UCHAR) 20)
#define aMoreMode                        ((VS_UCHAR) 68)
#define aMoreOut                         ((VS_UCHAR) 34)
#define aNhtFireAlm                      ((VS_UCHAR) 55)
#define aNightNc                         ((VS_UCHAR) 19)
#define aPresentMode                     ((VS_UCHAR) 56)
#define aProxIn                          ((VS_UCHAR) 39)
#define aProxOut                         ((VS_UCHAR) 42)
#define aRestoreMode                     ((VS_UCHAR) 49)
#define aSat                             ((VS_UCHAR) 17)
#define aServiceMode                     ((VS_UCHAR) 47)
#define aSun                             ((VS_UCHAR) 11)
#define aTOutObjVfyCfg                   ((VS_UCHAR) 65)
#define aTOutOpDrCfg                     ((VS_UCHAR) 63)
#define aTResSOD                         ((VS_UCHAR) 36)
#define aThu                             ((VS_UCHAR) 15)
#define aTue                             ((VS_UCHAR) 13)
#define aTunMode                         ((VS_UCHAR) 5)
#define aVfy                             ((VS_UCHAR) 25)
#define aVfyIn                           ((VS_UCHAR) 54)
#define aVfyOut                          ((VS_UCHAR) 26)
#define aVfyRepeat                       ((VS_UCHAR) 66)
#define aViewMode                        ((VS_UCHAR) 57)
#define aViolateNtfy                     ((VS_UCHAR) 62)
#define aWed                             ((VS_UCHAR) 14)
#define aXChgIn                          ((VS_UCHAR) 22)
#define aXChgOut                         ((VS_UCHAR) 23)
#define addDate                          ((VS_UCHAR) 4)
#define addDay                           ((VS_UCHAR) 3)
#define addHour                          ((VS_UCHAR) 2)
#define addMin                           ((VS_UCHAR) 1)
#define addMonth                         ((VS_UCHAR) 5)
#define addSec                           ((VS_UCHAR) 61)
#define addYear                          ((VS_UCHAR) 6)
#define mABT1Off                         ((VS_INT) 260)
#define mABT1On                          ((VS_INT) 261)
#define mABT2Off                         ((VS_INT) 262)
#define mABT2On                          ((VS_INT) 263)
#define mAccessCtrlOff                   ((VS_INT) 242)
#define mAccessCtrlOn                    ((VS_INT) 241)
#define mAccessMode                      ((VS_INT) 160)
#define mAdvanceConfOff                  ((VS_INT) 251)
#define mAdvanceConfOn                   ((VS_INT) 250)
#define mAfterLowBatStatEme              ((VS_INT) 282)
#define mAfterLowBatStatEmeAlw           ((VS_INT) 284)
#define mAfterLowBatStatForced           ((VS_INT) 281)
#define mAlertChangAftLowBStat           ((VS_INT) 283)
#define mBackUpCfg                       ((VS_INT) 227)
#define mBid                             ((VS_INT) 136)
#define mBioCheckIn                      ((VS_INT) 0)
#define mBioCheckOut                     ((VS_INT) 98)
#define mBioCnb                          ((VS_INT) 165)
#define mBioCnbTOut15                    ((VS_INT) 170)
#define mBioCnbTOut1m                    ((VS_INT) 236)
#define mBioCnbTOut2m                    ((VS_INT) 237)
#define mBioCnbTOut3m                    ((VS_INT) 238)
#define mBioCnbTOut45                    ((VS_INT) 171)
#define mBioCnbTOut4m                    ((VS_INT) 239)
#define mBioCnbTOut5m                    ((VS_INT) 240)
#define mBioFace                         ((VS_INT) 102)
#define mBioFing                         ((VS_INT) 101)
#define mBioFingFace                     ((VS_INT) 103)
#define mBioInDisOff                     ((VS_INT) 78)
#define mBioInNone                       ((VS_INT) 92)
#define mBioInOff                        ((VS_INT) 90)
#define mBioInOn                         ((VS_INT) 91)
#define mBioNok                          ((VS_INT) 1)
#define mBioNone                         ((VS_INT) 100)
#define mBioOff                          ((VS_INT) 208)
#define mBioOn                           ((VS_INT) 209)
#define mBioOutDisOff                    ((VS_INT) 79)
#define mBioOutNone                      ((VS_INT) 166)
#define mBioOutOff                       ((VS_INT) 168)
#define mBioOutOn                        ((VS_INT) 167)
#define mBiometric                       ((VS_INT) 164)
#define mBlkAlarmRedOff                  ((VS_INT) 126)
#define mBlkAlarmRedOn                   ((VS_INT) 127)
#define mCAM                             ((VS_INT) 68)
#define mCN93_4AsIntTrLight              ((VS_INT) 276)
#define mCN93_4AsTransOk                 ((VS_INT) 277)
#define mClosingBeforeObjChk             ((VS_INT) 278)
#define mDate                            ((VS_INT) 73)
#define mDateT                           ((VS_INT) 162)
#define mDateTime                        ((VS_INT) 161)
#define mDay                             ((VS_INT) 74)
#define mDelayVisEx                      ((VS_INT) 270)
#define mDoorOpnN                        ((VS_INT) 143)
#define mDoorOpnP1                       ((VS_INT) 139)
#define mDoorOpnP2                       ((VS_INT) 140)
#define mDoorsClsd                       ((VS_INT) 141)
#define mDoorsForced                     ((VS_INT) 52)
#define mEasyTransOff                    ((VS_INT) 249)
#define mEasyTransOn                     ((VS_INT) 248)
#define mEmeP1                           ((VS_INT) 213)
#define mEmeP1P2                         ((VS_INT) 212)
#define mEmeP2                           ((VS_INT) 214)
#define mEmergency                       ((VS_INT) 2)
#define mEnProgOn                        ((VS_INT) 221)
#define mEnReadyOff                      ((VS_INT) 272)
#define mEnReadyOn                       ((VS_INT) 271)
#define mEnSynHallOff                    ((VS_INT) 223)
#define mEnSynHallOn                     ((VS_INT) 222)
#define mEnglish                         ((VS_INT) 173)
#define mF1Bio                           ((VS_INT) 111)
#define mF1Emergency                     ((VS_INT) 112)
#define mF1In                            ((VS_INT) 113)
#define mF1Metal                         ((VS_INT) 114)
#define mF1More                          ((VS_INT) 115)
#define mF1Off                           ((VS_INT) 110)
#define mF1VisEx                         ((VS_INT) 267)
#define mF2Bio                           ((VS_INT) 121)
#define mF2Emergency                     ((VS_INT) 122)
#define mF2In                            ((VS_INT) 123)
#define mF2Metal                         ((VS_INT) 124)
#define mF2More                          ((VS_INT) 125)
#define mF2Off                           ((VS_INT) 120)
#define mF2VisEx                         ((VS_INT) 268)
#define mF3Bio                           ((VS_INT) 131)
#define mF3Emergency                     ((VS_INT) 132)
#define mF3In                            ((VS_INT) 133)
#define mF3Metal                         ((VS_INT) 134)
#define mF3More                          ((VS_INT) 135)
#define mF3Off                           ((VS_INT) 130)
#define mF3VisEx                         ((VS_INT) 269)
#define mFInMetalOff                     ((VS_INT) 116)
#define mFInMetalOn                      ((VS_INT) 117)
#define mFInMoreOff                      ((VS_INT) 177)
#define mFInMoreOn                       ((VS_INT) 176)
#define mFri                             ((VS_INT) 85)
#define mFuncBtn                         ((VS_INT) 169)
#define mHHDay                           ((VS_INT) 62)
#define mHHMMDay                         ((VS_INT) 66)
#define mHHMMNight                       ((VS_INT) 67)
#define mHHNight                         ((VS_INT) 63)
#define mHour                            ((VS_INT) 75)
#define mIdle                            ((VS_INT) 3)
#define mInAbort                         ((VS_INT) 8)
#define mInAbortBioNotPresent            ((VS_INT) 218)
#define mInAbortBioPresent               ((VS_INT) 217)
#define mInAbortNotPresent               ((VS_INT) 216)
#define mInAbortPresent                  ((VS_INT) 215)
#define mInAbortVisExNotPres             ((VS_INT) 266)
#define mInAbortVisExPres                ((VS_INT) 265)
#define mInClose1                        ((VS_INT) 4)
#define mInClose2                        ((VS_INT) 5)
#define mInControl                       ((VS_INT) 6)
#define mInMoreAlarm                     ((VS_INT) 7)
#define mInOpen1                         ((VS_INT) 9)
#define mInOpen2                         ((VS_INT) 10)
#define mInPause                         ((VS_INT) 11)
#define mInVisEx                         ((VS_INT) 264)
#define mInWait2                         ((VS_INT) 12)
#define mIntraSOD15                      ((VS_INT) 152)
#define mIntraSOD240                     ((VS_INT) 154)
#define mIntraSOD45                      ((VS_INT) 153)
#define mItaliano                        ((VS_INT) 172)
#define mLOutEmOff                       ((VS_INT) 118)
#define mLOutEmOn                        ((VS_INT) 119)
#define mLampOff                         ((VS_INT) 181)
#define mLampOn                          ((VS_INT) 180)
#define mMChangeDir1                     ((VS_INT) 13)
#define mMChangeDir2                     ((VS_INT) 14)
#define mMIdle                           ((VS_INT) 15)
#define mMIn                             ((VS_INT) 93)
#define mMInClose1                       ((VS_INT) 16)
#define mMInClose2                       ((VS_INT) 17)
#define mMInControl                      ((VS_INT) 60)
#define mMInMoreAlarm                    ((VS_INT) 61)
#define mMInOpen1                        ((VS_INT) 18)
#define mMInOpen2                        ((VS_INT) 19)
#define mMInPause                        ((VS_INT) 20)
#define mMInWait2                        ((VS_INT) 21)
#define mMMDay                           ((VS_INT) 64)
#define mMMNight                         ((VS_INT) 65)
#define mMOut                            ((VS_INT) 224)
#define mMOutAutoOff                     ((VS_INT) 220)
#define mMOutAutoOn                      ((VS_INT) 219)
#define mMOutClose1                      ((VS_INT) 22)
#define mMOutClose2                      ((VS_INT) 23)
#define mMOutControl                     ((VS_INT) 59)
#define mMOutMoreAlarm                   ((VS_INT) 58)
#define mMOutOpen1                       ((VS_INT) 24)
#define mMOutOpen2                       ((VS_INT) 25)
#define mMOutPause                       ((VS_INT) 26)
#define mMOutWait1                       ((VS_INT) 27)
#define mMailOff                         ((VS_INT) 108)
#define mMailOn                          ((VS_INT) 109)
#define mMemBioResOff                    ((VS_INT) 247)
#define mMemBioResOn                     ((VS_INT) 246)
#define mMemoOff                         ((VS_INT) 157)
#define mMemoOn1                         ((VS_INT) 158)
#define mMemoOn2                         ((VS_INT) 159)
#define mMetalAlarm                      ((VS_INT) 53)
#define mMetalNotPresent                 ((VS_INT) 28)
#define mMetalOff                        ((VS_INT) 206)
#define mMetalOn                         ((VS_INT) 207)
#define mMetalPresent                    ((VS_INT) 29)
#define mMin                             ((VS_INT) 76)
#define mModeWeek                        ((VS_INT) 175)
#define mMon                             ((VS_INT) 81)
#define mMonth                           ((VS_INT) 72)
#define mMoreInOff                       ((VS_INT) 145)
#define mMoreInOn                        ((VS_INT) 144)
#define mMoreModeRewind                  ((VS_INT) 285)
#define mMoreModeStopStart               ((VS_INT) 286)
#define mMoreOff                         ((VS_INT) 210)
#define mMoreOn                          ((VS_INT) 211)
#define mMoreOutOff                      ((VS_INT) 147)
#define mMoreOutOn                       ((VS_INT) 146)
#define mMorningError                    ((VS_INT) 32)
#define mMorningIdle                     ((VS_INT) 33)
#define mMorningPresent                  ((VS_INT) 34)
#define mNhtFireAlmOff                   ((VS_INT) 255)
#define mNhtFireAlmOn                    ((VS_INT) 254)
#define mNightChkObject                  ((VS_INT) 49)
#define mNightClose1                     ((VS_INT) 50)
#define mNightClose2                     ((VS_INT) 48)
#define mNightError                      ((VS_INT) 149)
#define mNightIdle                       ((VS_INT) 47)
#define mNightNc                         ((VS_INT) 142)
#define mNightOpenAll                    ((VS_INT) 51)
#define mNightPresent                    ((VS_INT) 148)
#define mNightRSODOff                    ((VS_INT) 232)
#define mNightRSODOn                     ((VS_INT) 233)
#define mObjectAlarm                     ((VS_INT) 30)
#define mObjectCheck                     ((VS_INT) 31)
#define mOnlyIn                          ((VS_INT) 137)
#define mOnlyOut                         ((VS_INT) 138)
#define mOutAbort                        ((VS_INT) 54)
#define mOutAbortPresent                 ((VS_INT) 243)
#define mOutClose1                       ((VS_INT) 35)
#define mOutClose2                       ((VS_INT) 36)
#define mOutControl                      ((VS_INT) 37)
#define mOutMoreAlarm                    ((VS_INT) 38)
#define mOutOpen1                        ((VS_INT) 40)
#define mOutOpen2                        ((VS_INT) 41)
#define mOutPause                        ((VS_INT) 42)
#define mOutWait1                        ((VS_INT) 43)
#define mPCBioOffLine                    ((VS_INT) 205)
#define mPCBioOn                         ((VS_INT) 201)
#define mPCBioOnLine                     ((VS_INT) 204)
#define mPCBioWaitOff                    ((VS_INT) 203)
#define mPCBioWaitOn                     ((VS_INT) 202)
#define mPanicBtnOff                     ((VS_INT) 225)
#define mPanicBtnOn                      ((VS_INT) 226)
#define mPresentModeOff                  ((VS_INT) 257)
#define mPresentModeOn                   ((VS_INT) 256)
#define mProgOn1                         ((VS_INT) 56)
#define mProxInOff                       ((VS_INT) 156)
#define mProxInOn                        ((VS_INT) 155)
#define mProxOutOff                      ((VS_INT) 179)
#define mProxOutOn                       ((VS_INT) 178)
#define mRestoreCfg                      ((VS_INT) 231)
#define mRestoreModeOff                  ((VS_INT) 245)
#define mRestoreModeOn                   ((VS_INT) 244)
#define mRestoredCfg                     ((VS_INT) 230)
#define mSOD3                            ((VS_INT) 150)
#define mSOD6                            ((VS_INT) 151)
#define mSat                             ((VS_INT) 86)
#define mSaveCfg                         ((VS_INT) 228)
#define mSavedCfg                        ((VS_INT) 229)
#define mSec                             ((VS_INT) 77)
#define mServiceCfg                      ((VS_INT) 235)
#define mServiceMode                     ((VS_INT) 234)
#define mSodCheck                        ((VS_INT) 44)
#define mSodPause                        ((VS_INT) 45)
#define mSodReset                        ((VS_INT) 46)
#define mSun                             ((VS_INT) 87)
#define mTOutObjVfyCfg                   ((VS_INT) 279)
#define mTOutOpDrCfg                     ((VS_INT) 275)
#define mThu                             ((VS_INT) 84)
#define mTime                            ((VS_INT) 163)
#define mTue                             ((VS_INT) 82)
#define mTunnel                          ((VS_INT) 174)
#define mUnavailable                     ((VS_INT) 88)
#define mVfyInOff                        ((VS_INT) 253)
#define mVfyInOn                         ((VS_INT) 252)
#define mVfyOff                          ((VS_INT) 104)
#define mVfyOn                           ((VS_INT) 105)
#define mVfyOutOff                       ((VS_INT) 106)
#define mVfyOutOn                        ((VS_INT) 107)
#define mVfyRepeat                       ((VS_INT) 280)
#define mViewModeEU                      ((VS_INT) 258)
#define mViewModeUK                      ((VS_INT) 259)
#define mViolateNtfyOff                  ((VS_INT) 274)
#define mViolateNtfyOn                   ((VS_INT) 273)
#define mWed                             ((VS_INT) 83)
#define mWeek                            ((VS_INT) 80)
#define mXChgInOff                       ((VS_INT) 94)
#define mXChgInOn                        ((VS_INT) 95)
#define mXChgOutOff                      ((VS_INT) 96)
#define mXChgOutOn                       ((VS_INT) 97)
#define mYear                            ((VS_INT) 71)


/*
 * VS System External Variable Declarations.
 */
extern VS_UCHAR ABT1;

extern VS_UCHAR ABT2;

extern VS_UCHAR AIn;

extern VS_UCHAR AInBio;

extern VS_UCHAR AInChange;

extern VS_UCHAR AInMetal;

extern VS_UCHAR AInMore;

extern VS_UCHAR AInNc;

extern VS_UCHAR AOut;

extern VS_UCHAR AOutBio;

extern VS_UCHAR AOutChange;

extern VS_UCHAR AOutMetal;

extern VS_UCHAR AOutMore;

extern VS_UCHAR AOutNc;

extern VS_UCHAR AVerify;

extern VS_UCHAR AVerifyIn;

extern VS_UCHAR AVerifyOut;

extern VS_UCHAR AccessCtrl;

extern VS_UCHAR AdvanceConf;

extern VS_UCHAR AfterLowBatStat;

extern VS_UCHAR BattLow;

extern VS_UCHAR BioCnbTOut;

extern VS_UCHAR BioMode;

extern VS_UCHAR BlkAlarmRed;

extern VS_UCHAR CN93_4AsIntTrLight;

extern VS_UCHAR CfgCAM;

extern VS_UCHAR CfgFri;

extern VS_UCHAR CfgMon;

extern VS_UCHAR CfgSat;

extern VS_UCHAR CfgSun;

extern VS_UCHAR CfgThu;

extern VS_UCHAR CfgTue;

extern VS_UCHAR CfgV;

extern VS_UCHAR CfgWed;

extern VS_UCHAR Closed1;

extern VS_UCHAR Closed2;

extern VS_UCHAR Cns1Disp;

extern VS_UCHAR Cns1On;

extern VS_UCHAR Cns2Disp;

extern VS_UCHAR Cns2On;

extern VS_UCHAR Cns3Disp;

extern VS_UCHAR Cns3On;

extern VS_UCHAR DelayVisEx;

extern VS_UCHAR DoorsMode;

extern VS_UCHAR EasyTrans;

extern VS_UCHAR EmeMode;

extern VS_UCHAR EnNightRSOD;

extern VS_UCHAR EnPanicBtn;

extern VS_UCHAR EnReady;

extern VS_UCHAR EnSynHall;

extern VS_UCHAR EnablePCBioOnOff;

extern VS_UCHAR F1Sel;

extern VS_UCHAR F2Sel;

extern VS_UCHAR F3Sel;

extern VS_UCHAR FInBio;

extern VS_UCHAR FInMetal;

extern VS_UCHAR FInMore;

extern VS_UCHAR FmwV1;

extern VS_UCHAR FmwV2;

extern VS_UCHAR FullStroke;

extern VS_UCHAR HHDay;

extern VS_UCHAR HHNight;

extern VS_UCHAR IntraSOD;

extern VS_UCHAR LOutBio;

extern VS_UCHAR LOutMore;

extern VS_UCHAR LampOn;

extern VS_UCHAR Language;

extern VS_UINT LastDoorClosed;

extern VS_UCHAR LastOnOff;

extern VS_UCHAR LastOutEm;

extern VS_UINT LeftTimeSession;

extern VS_INT LogicMsgId;

extern VS_UCHAR MIn;

extern VS_UCHAR MInBio;

extern VS_UCHAR MInChange;

extern VS_UCHAR MInMetal;

extern VS_UCHAR MInMore;

extern VS_UCHAR MInNc;

extern VS_UCHAR MMDay;

extern VS_UCHAR MMNight;

extern VS_UCHAR MOut;

extern VS_UCHAR MOutAuto;

extern VS_UCHAR MOutBio;

extern VS_UCHAR MOutChange;

extern VS_UCHAR MOutMetal;

extern VS_UCHAR MOutMore;

extern VS_UCHAR MOutNc;

extern VS_UCHAR MVerify;

extern VS_UCHAR MVerifyIn;

extern VS_UCHAR MVerifyOut;

extern VS_UCHAR Mail;

extern VS_UINT MaxBioCount;

extern VS_UCHAR MaxUsersIn;

extern VS_UCHAR MemBioRes;

extern VS_UCHAR MemoReq;

extern VS_UCHAR MoreMode;

extern VS_INT NMsg;

extern VS_UCHAR NextOnOff;

extern VS_UCHAR NightFireAlarm;

extern VS_UCHAR NightNc;

extern VS_UCHAR OStInCng;

extern VS_UCHAR OStOutCng;

extern VS_UCHAR OnlyM2Off;

extern VS_UCHAR Opened1;

extern VS_UCHAR Opened2;

extern VS_UCHAR PCBioStatus;

extern VS_UCHAR PassCount;

extern VS_UCHAR Present;

extern VS_UCHAR PresentMode;

extern VS_UCHAR ProxIn;

extern VS_UCHAR ProxOut;

extern VS_UCHAR RestoreMode;

extern VS_UCHAR ServMode;

extern VS_UCHAR SwAfterLowBatSt;

extern VS_UCHAR THolyD;

extern VS_UCHAR TOutObjVfyCfg;

extern VS_UCHAR TOutOpDrCfg;

extern VS_UCHAR TResSOD;

extern VS_UCHAR TWorkD;

extern VS_UCHAR TunMode;

extern VS_UCHAR UsersIn;

extern VS_UCHAR Vac;

extern VS_INT VfyRepeat;

extern VS_UCHAR ViewMode;

extern VS_UCHAR ViolateNtfy;

extern VS_UCHAR VisExEn;

extern VS_UCHAR bBioFingPres;

extern VS_UCHAR bCfg;

extern VS_UCHAR bCheckIDKey;

extern VS_UCHAR bDisPrg;

extern VS_UCHAR bInNc;

extern VS_UCHAR bManu;

extern VS_UCHAR bOutNc;

extern VS_UCHAR bReady;

extern VS_UCHAR bVip;


#endif
