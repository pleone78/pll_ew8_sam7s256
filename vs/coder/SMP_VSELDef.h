/*
 * Version:   50
 * Signature: eb1b 13af 7a0e 2056 ded0 ade0
 *
 * Id:        SMP_VSELDef.h
 *
 * Function:  VS Project Definitions Header File.
 *
 * Generated: Thu May 30 15:27:04 2019
 *
 * Coder 5, 4, 0, 1273
 * 
 * This is an automatically generated file. It will be overwritten by the Coder. 
 * 
 * DO NOT EDIT THE FILE! 
 */


#ifndef __SMP_VSELDEF_H
#define __SMP_VSELDEF_H


/*
 * Include SEM Defines Header File.
 */
#include "SEMEDef.h"



#include "SEMLibE.h"


#define VS_GDEF 1


typedef VS_UINT8 (** VSEL_GUARD_FUNCTION_TYPE) (struct SEM_CONTEXT*);
typedef VS_VOID (** VSEL_ACTION_FUNCTION_TYPE) (struct SEM_CONTEXT*);


#include "PLL_PEvent.h"
#include "PLL_PConstant.h"



#define VSDoors_STM_DEDUCT_FUNC SMP_Deduct
extern unsigned char SMP_Deduct (struct SEM_CONTEXT *Context, SEM_EVENT_TYPE EventNo);
#define VSDoors_STM_ACTION_FUNC VSDoorsVSAction
extern VS_VOID (* VSDoorsVSAction[]) (struct SEM_CONTEXT*);
#define VSDoors_STM_RULE_BASE_DATA VSDoors
#include "C:\IAR_EW8_Works\arm_sam7_256\PLL\vs\coder\VSDoors.h"
#define VSDoors_STM_GUARD_FUNC VSDoorsVSGuard
extern unsigned char (* VSDoorsVSGuard[]) (struct SEM_CONTEXT*);
#define VSDoors_SE_RESET 0

#define VSLogic_STM_DEDUCT_FUNC VSLogicSMP_Deduct
extern unsigned char VSLogicSMP_Deduct (struct SEM_CONTEXT *Context, SEM_EVENT_TYPE EventNo);
#define VSLogic_STM_ACTION_FUNC VSLogicVSAction
extern VS_VOID (* VSLogicVSAction[]) (struct SEM_CONTEXT*);
#define VSLogic_STM_RULE_BASE_DATA VSLogic
#include "C:\IAR_EW8_Works\arm_sam7_256\PLL\vs\coder\VSLogic.h"
#define VSLogic_STM_GUARD_FUNC VSLogicVSGuard
extern unsigned char (* VSLogicVSGuard[]) (struct SEM_CONTEXT*);
#define VSLogic_SE_RESET 0



#undef VS_GDEF



#define PROJECT_ANY_DEDUCT_WITH_VA_LIST 0



#endif
