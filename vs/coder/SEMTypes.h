/*
 * Version:   50
 * Signature: eb1b 13af 7a0e 2056 ded0 ade0
 *
 * Id:        SEMTypes.h
 *
 * Function:  SEM Types Header File.
 *
 * Generated: Thu May 30 15:27:04 2019
 *
 * Coder 5, 4, 0, 1273
 * 
 * This is an automatically generated file. It will be overwritten by the Coder. 
 * 
 * DO NOT EDIT THE FILE! 
 */


#ifndef __SEMTYPES_H
#define __SEMTYPES_H


#define VS_CODER_GUID 0X015e8c916


#include <limits.h>


#ifdef SE_EXPERTDLL
#define VS_FILE_TYPE FILE
#else
#define VS_FILE_TYPE void
#endif


typedef unsigned char      VS_BOOL;
typedef unsigned char      VS_UCHAR;
typedef signed char        VS_SCHAR;
typedef unsigned int       VS_UINT;
typedef signed int         VS_INT;
typedef float              VS_FLOAT;
typedef double             VS_DOUBLE;
#define VS_VOID            void               
typedef void*              VS_VOIDPTR;

#if (UCHAR_MAX >= 0x0ff)
typedef unsigned char      VS_UINT8;
typedef signed char        VS_INT8;
#elif (USHRT_MAX >= 0x0ff)
typedef unsigned short     VS_UINT8;
typedef signed short       VS_INT8;
#elif (UINT_MAX >= 0x0ff)
typedef unsigned int       VS_UINT8;
typedef signed int         VS_INT8;
#elif (ULONG_MAX >= 0x0ff)
typedef unsigned long      VS_UINT8;
typedef signed long        VS_INT8;
#else
#define VS_UINT8           (unsupported data type)
#define VS_INT8            (unsupported data type)
#endif

#define VS_UINT16_VAARG    VS_INT
#define VS_INT16_VAARG     VS_INT
#if (UCHAR_MAX >= 0x0ffff)
typedef unsigned char      VS_UINT16;
typedef signed char        VS_INT16;
#elif (USHRT_MAX >= 0x0ffff)
typedef unsigned short     VS_UINT16;
typedef signed short       VS_INT16;
#elif (UINT_MAX >= 0x0ffff)
typedef unsigned int       VS_UINT16;
typedef signed int         VS_INT16;
#elif (ULONG_MAX >= 0x0ffff)
typedef unsigned long      VS_UINT16;
typedef signed long        VS_INT16;
#undef VS_UINT16_VAARG
#undef VS_INT16_VAARG
#define VS_UINT16_VAARG    VS_INT16
#define VS_INT16_VAARG     VS_INT16
#else
#define VS_UINT16          (unsupported data type)
#define VS_INT16           (unsupported data type)
#endif

#define VS_UINT32_VAARG    VS_INT
#define VS_INT32_VAARG     VS_INT
#if (UCHAR_MAX >= 0x0ffffffffUL)
typedef unsigned char      VS_UINT32;
typedef signed char        VS_INT32;
#elif (USHRT_MAX >= 0x0ffffffffUL)
typedef unsigned short     VS_UINT32;
typedef signed short       VS_INT32;
#elif (UINT_MAX >= 0x0ffffffffUL)
typedef unsigned int       VS_UINT32;
typedef signed int         VS_INT32;
#elif (ULONG_MAX >= 0x0ffffffffUL)
typedef unsigned long      VS_UINT32;
typedef signed long        VS_INT32;
#undef VS_UINT32_VAARG
#undef VS_INT32_VAARG
#define VS_UINT32_VAARG    VS_INT32
#define VS_INT32_VAARG     VS_INT32
#else
#define VS_UINT32          (unsupported data type)
#define VS_INT32           (unsupported data type)
#endif


/*
 * SEM Variable Types.
 */
typedef VS_UINT8                         SEM_EVENT_TYPE;
typedef VS_UINT16                        SEM_ACTION_EXPRESSION_TYPE;
typedef VS_UINT16                        SEM_GUARD_EXPRESSION_TYPE;
typedef VS_UINT16                        SEM_EXPLANATION_TYPE;
typedef VS_UINT16                        SEM_STATE_TYPE;
typedef VS_UINT16                        SEM_STATE_MACHINE_TYPE;
typedef VS_UINT8                         SEM_INSTANCE_TYPE;
typedef VS_UINT16                        SEM_RULE_INDEX_TYPE;
typedef VS_UINT16                        SEM_INTERNAL_TYPE;
typedef VS_UINT8                         SEM_SIGNAL_QUEUE_TYPE;
typedef VS_UINT8                         SEM_ACTION_FUNCTION_TYPE;
typedef VS_UINT16                        SEM_EVENT_GROUP_TYPE;
typedef VS_UINT16                        SEM_EGTI_TYPE;
typedef VS_UINT16                        SEM_RULE_TABLE_INDEX_TYPE;


#endif
