/*
 * Version:   50
 * Signature: eb1b 13af 7a0e 2056 ded0 ade0
 *
 * Id:        SEMEDef.h
 *
 * Function:  SEM Defines Header File.
 *
 * Generated: Thu May 30 15:27:04 2019
 *
 * Coder 5, 4, 0, 1273
 * 
 * This is an automatically generated file. It will be overwritten by the Coder. 
 * 
 * DO NOT EDIT THE FILE! 
 */


#ifndef __SEMEDEF_H
#define __SEMEDEF_H


/*
 * Include SEM Types Header File.
 */
#include "SEMTypes.h"


#if (VS_CODER_GUID != 0X015e8c916)
#error The generated file does not match the SEMTypes.h header file.
#endif


/*
 * Conditional Compilation Definitions for the API only.
 */
#define SEM_RDHW_TYPE_1                    0
#define SEM_RDHW_TYPE_2                    0
#define SEM_RDHW_TYPE_3                    1
#define SEM_RDHW_WIDTH_16_BIT              0
#define SEM_RDHW_WIDTH_24_BIT              0
#define SEM_RDHW_WIDTH_32_BIT              1
#define SEM_RDHW_WIDTH_48_BIT              0
#define SEM_RDHW_WIDTH_64_BIT              0
#define SEM_RD_WIDTH_8_BIT                 0
#define SEM_RD_WIDTH_16_BIT                1
#define SEM_RD_WIDTH_32_BIT                0
#define SEM_RDFM_NUMBER                    5
#define SEM_EVENT_GROUP_INDEX              0
#define SEM_EVENT_GROUP_TABLE_INDEX        1
#define SEM_SIGNAL_QUEUE_ERROR_IF_FULL     1
#define SEM_SIGNAL_QUEUE_NO_ERROR_IF_FULL  0
#define SEM_RMN_ACTIONS                    15
#define SEM_RMN_GUARDS                     7
#define SEM_RMN_NEGATIVE_STATE_SYNCS       3
#define SEM_RMN_NEXT_STATES                15
#define SEM_RMN_POSITIVE_STATE_SYNCS       7
#define SEM_RMN_SIGNALS                    12
#define SEM_SIGNAL                         1


/*
 * Conditional Compilation Definitions.
 */
#define SEM_FUNCEXPHANDLING              0
#define SEM_GUARD_EXPRESSION             1
#define SEM_SIGNAL_DB                    1
#define VS_REALLINKMODE                  0
#define VS_USE_HEAP                      0


/*
 * Undefined State.
 */
#define STATE_UNDEFINED                  0X0FFFF  /* 65535 */


/*
 * Undefined Event.
 */
#define EVENT_UNDEFINED                  0X0FF  /* 255 */


/*
 * Undefined Event Group.
 */
#define EVENT_GROUP_UNDEFINED            0X0FFFF  /* 65535 */


/*
 * Event Termination ID.
 */
#define EVENT_TERMINATION_ID             0X0FF  /* 255 */


/*
 * Action Expression Termination ID.
 */
#define ACTION_EXPRESSION_TERMINATION_ID 0X0FFFF  /* 65535 */


#ifdef SE_EXPERTDLL
#include <stdio.h>
#endif


#if (SEM_RD_WIDTH_8_BIT)
#define SEM_RULE_DATA_TYPE VS_UINT8
#elif (SEM_RD_WIDTH_16_BIT)
#define SEM_RULE_DATA_TYPE VS_UINT16
#elif (SEM_RD_WIDTH_32_BIT)
#define SEM_RULE_DATA_TYPE VS_UINT32
#else
#error Invalid files, contact the vendor
#endif


/*
 * Type qualifier macros
 */
#define VS_TQ_RULEBASE                 const 
#define VS_TQ_GUARDEXPRCOL             const 
#define VS_TQ_ACTIONEXPRCOL            const 
#define VS_TQ_CONTEXT                  


/*
 * Functional expression type definitions
 */
struct SEM_CONTEXT;
typedef VS_BOOL (* VS_GUARDEXPR_TYPE) (struct SEM_CONTEXT VS_TQ_CONTEXT * );
typedef VS_VOID (* VS_ACTIONEXPR_TYPE) (struct SEM_CONTEXT VS_TQ_CONTEXT * );


/*
 * SEM Library Datatype Definition.
 */
typedef struct SEM_CONTEXT
{
  SEM_STATE_TYPE VS_TQ_CONTEXT *                pCSV;
  SEM_STATE_TYPE VS_TQ_CONTEXT *                pWSV;
  SEM_STATE_TYPE VS_TQ_CONTEXT *                pIns;
  SEM_EVENT_TYPE VS_TQ_CONTEXT *                pSQueue;
  VS_UINT8                                      Status;
  VS_UINT8                                      EventGroupType;
  VS_UINT8                                      State;
  VS_UINT8                                      DIt;
  VS_UINT8                                      InqAct;
  VS_UINT8                                      StateChange;
  VS_UINT8                                      nNofActions;
  VS_UINT8                                      SignalStateChg;
  SEM_EVENT_TYPE                                nNofEvents;
  SEM_EVENT_TYPE                                nNofSignals;
  SEM_STATE_TYPE                                nNofStates;
  SEM_STATE_MACHINE_TYPE                        nNofStateMachines;
  SEM_ACTION_FUNCTION_TYPE                      nNofActionFunctions;
  SEM_INSTANCE_TYPE                             nNofInstances;
  SEM_INSTANCE_TYPE                             ActIns;
  SEM_EVENT_TYPE                                EventNo;
  SEM_EGTI_TYPE                                 iFirstEgi;
  SEM_EGTI_TYPE                                 iLastEgi;
  SEM_RULE_INDEX_TYPE                           iRI;
  SEM_RULE_TABLE_INDEX_TYPE                     iFirstR;
  SEM_RULE_TABLE_INDEX_TYPE                     iLastR;
  SEM_RULE_INDEX_TYPE                           iInqRI;
  SEM_EVENT_TYPE                                InqEventNo;
  SEM_EVENT_TYPE                                InqCurrentEventNo;
  SEM_EVENT_TYPE                                InqFoundEventNo;
  SEM_EGTI_TYPE                                 iInqFirstEgi;
  SEM_EGTI_TYPE                                 iInqLastEgi;
  VS_FILE_TYPE *                                pVSFile;
  void VS_TQ_RULEBASE *                         pVSData;
  SEM_STATE_MACHINE_TYPE VS_TQ_RULEBASE *       pSMI;
  SEM_EVENT_GROUP_TYPE VS_TQ_RULEBASE *         pEGT;
  SEM_EGTI_TYPE VS_TQ_RULEBASE *                pEGTI;
  SEM_RULE_DATA_TYPE VS_TQ_RULEBASE *           pRD;
  SEM_RULE_INDEX_TYPE VS_TQ_RULEBASE *          pRI;
  SEM_RULE_TABLE_INDEX_TYPE VS_TQ_RULEBASE *    pRTI;
  SEM_SIGNAL_QUEUE_TYPE                         SPut;
  SEM_SIGNAL_QUEUE_TYPE                         SGet;
  SEM_SIGNAL_QUEUE_TYPE                         SUsed;
  SEM_SIGNAL_QUEUE_TYPE                         SSize;
  VS_GUARDEXPR_TYPE VS_TQ_GUARDEXPRCOL *        pGuard;
   void (*pSEM_SignalDB)(struct SEM_CONTEXT VS_TQ_CONTEXT *Context, SEM_EVENT_TYPE SignalNo);
  unsigned long VS_TQ_RULEBASE *                pENIPos;
  unsigned long VS_TQ_RULEBASE *                pSNIPos;
  unsigned long VS_TQ_RULEBASE *                pANIPos;
  unsigned long VS_TQ_RULEBASE *                pEEIPos;
  unsigned long VS_TQ_RULEBASE *                pSEIPos;
  unsigned long VS_TQ_RULEBASE *                pAEIPos;
} SEM_CONTEXT;


/*
 * VS Run Time Info
 */
#define VS_RUNTIME_INFO                1
#define VS_RUNTIME_INFO_EXTKW          
#define VS_SIGNATURE_VERSION           "50"
#define VS_SIGNATURE_CONTENT           "eb1b 13af 7a0e 2056 ded0 ade0"
#define VS_SIGNATURE_VERSION_LENGTH    3
#define VS_SIGNATURE_CONTENT_LENGTH    30


#endif
