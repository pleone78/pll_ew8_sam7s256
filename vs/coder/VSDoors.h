/*
 * Version:   50
 * Signature: eb1b 13af 7a0e 2056 ded0 ade0
 *
 * Id:        VSDoors.h
 *
 * Function:  Include VS System Data Header File.
 *
 * Generated: Thu May 30 15:27:04 2019
 *
 * Coder 5, 4, 0, 1273
 * 
 * This is an automatically generated file. It will be overwritten by the Coder. 
 * 
 * DO NOT EDIT THE FILE! 
 */


#ifndef __VSDOORS_H
#define __VSDOORS_H


/*
 * Include SEM Defines Header File.
 */
#include "SEMEDef.h"


#if (VS_CODER_GUID != 0X015e8c916)
#error The generated file does not match the SEMTypes.h header file.
#endif


/*
 * VS System Datatype Definition.
 */
typedef struct 
{
  VS_UINT32      S0[0X000000016];
  VS_UINT16      S1[0X01534];
  SEM_CONTEXT * pContext;
} VSDATAVSDoors;


/*
 * External Declaration for VS System Data.
 */
extern VSDATAVSDoors const VSDoors;


#endif
