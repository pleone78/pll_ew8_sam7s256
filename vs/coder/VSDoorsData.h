/*
 * Version:   50
 * Signature: eb1b 13af 7a0e 2056 ded0 ade0
 *
 * Id:        VSDoorsData.h
 *
 * Function:  VS System Header File.
 *
 * Generated: Thu May 30 15:27:04 2019
 *
 * Coder 5, 4, 0, 1273
 * 
 * This is an automatically generated file. It will be overwritten by the Coder. 
 * 
 * DO NOT EDIT THE FILE! 
 */


#ifndef __VSDOORSDATA_H
#define __VSDOORSDATA_H


/*
 * Number of Identifiers.
 */
#define VS_NOF_ACTION_EXPRESSIONS        0X0cf  /* 207 */
#define VS_NOF_ACTION_FUNCTIONS          0X067  /* 103 */
#define VS_NOF_EVENT_GROUPS              0X000  /*   0 */
#define VS_NOF_EVENTS                    0X037  /*  55 */
#define VS_NOF_EXTERNAL_VARIABLES        0X020  /*  32 */
#define VS_NOF_GUARD_EXPRESSIONS         0X0b7  /* 183 */
#define VS_NOF_INSTANCES                 0X002  /*   2 */
#define VS_NOF_INTERNAL_VARIABLES        0X000  /*   0 */
#define VS_NOF_SIGNALS                   0X005  /*   5 */
#define VS_NOF_STATE_MACHINES            0X007  /*   7 */
#define VS_NOF_STATES                    0X040  /*  64 */


/*
 * Include SEM Library Header File.
 */
#include "SEMLibE.h"


#if (VS_CODER_GUID != 0X015e8c916)
#error The generated file does not match the SEMTypes.h header file.
#endif


/*
 * Include Global Events Header File.
 */
#include "PLL_PEvent.h"


/*
 * External Declarations for Guard Expressions Function Pointer Table.
 */
extern VS_GUARDEXPR_TYPE const VSDoorsVSGuard[183];


/*
 * External Declaration for wrapper function for all initializing functions.
 */
extern unsigned char VSDoorsSMP_InitAll (SEM_CONTEXT VS_TQ_CONTEXT * * Context);


/*
 * Conditional Compilation Definitions.
 */
#define SEM_DEDUCT_FUNC                  0
#define SEM_DEDUCT_WITH_VA_LIST          0


/*
 * Event Identifier Definitions.
 */
#define eBreakThrOff                     0X001  /*   1 */
#define eBreakThrOn                      0X002  /*   2 */
#define eCheckDoor                       0X003  /*   3 */
#define eClose                           0X004  /*   4 */
#define eClosed                          0X005  /*   5 */
#define eExtProt                         0X006  /*   6 */
#define eExtProtLev                      0X007  /*   7 */
#define eHookup                          0X008  /*   8 */
#define eLocked                          0X009  /*   9 */
#define eMovClose                        0X00A  /*  10 */
#define eMovOpen                         0X00B  /*  11 */
#define eOpen                            0X00C  /*  12 */
#define eOpened                          0X00D  /*  13 */
#define eOverCur                         0X00E  /*  14 */
#define eResultCfg                       0X00F  /*  15 */
#define eTick                            0X010  /*  16 */
#define eUnLocked                        0X011  /*  17 */
#define eUnViolateDoor                   0X012  /*  18 */
#define eUnderCur                        0X013  /*  19 */
#define eUnhook                          0X014  /*  20 */
#define eViolateDoor                     0X015  /*  21 */
#define eWriteError                      0X016  /*  22 */
#define etDelay                          0X017  /*  23 */
#define etDelay10                        0X018  /*  24 */
#define etDelay100                       0X019  /*  25 */
#define etDelay20                        0X01A  /*  26 */
#define etDelay30                        0X01B  /*  27 */
#define etDelay40                        0X01C  /*  28 */
#define etDelay50                        0X01D  /*  29 */
#define etDelay60                        0X01E  /*  30 */
#define etDelay70                        0X01F  /*  31 */
#define etDelay80                        0X020  /*  32 */
#define etDelay90                        0X021  /*  33 */
#define etExit                           0X022  /*  34 */
#define etExit2                          0X023  /*  35 */
#define etFail                           0X024  /*  36 */
#define etFinish                         0X025  /*  37 */
#define etSetLock                        0X026  /*  38 */
#define etStart                          0X027  /*  39 */
#define etWait                           0X028  /*  40 */
#define euClearCount                     0X029  /*  41 */
#define euClearE2                        0X02A  /*  42 */
#define euDefault                        0X02B  /*  43 */
#define euDown                           0X02C  /*  44 */
#define euEnter                          0X02D  /*  45 */
#define euEsc                            0X02E  /*  46 */
#define euInit                           0X02F  /*  47 */
#define euLoad                           0X030  /*  48 */
#define euNext                           0X031  /*  49 */
#define euPrior                          0X032  /*  50 */
#define euRead                           0X033  /*  51 */
#define euRun                            0X034  /*  52 */
#define euUp                             0X035  /*  53 */
#define euUpMask                         0X036  /*  54 */


/*
 * Constants.
 */
#define D002                             ((VS_UINT16) 20)
#define D005                             ((VS_UINT16) 1000)
#define D010                             ((VS_UINT16) 100)
#define D015                             ((VS_UINT16) 600)
#define D020                             ((VS_UINT16) 1300)
#define D100                             ((VS_UINT16) 4000)
#define D200                             ((VS_UINT16) 8000)
#define D300                             ((VS_UINT16) 18000)
#define DFail                            ((VS_UINT16) 60000)
#define DFindWait                        ((VS_UINT16) 600)
#define DManual                          ((VS_UINT16) 60000)
#define DMaskUnderClose                  ((VS_UINT16) 600)
#define DMaskUnderOpen                   ((VS_UINT16) 600)
#define DMotStart                        ((VS_UINT16) 300)
#define DNearExit                        ((VS_UINT16) 600)
#define DWaitPos                         ((VS_UINT16) 800)
#define DWaitUnderClose                  ((VS_UINT16) 1000)
#define DWaitUnderOpen                   ((VS_UINT16) 1000)
#define MsgBrakeClosing                  ((VS_UINT16) 26)
#define MsgBrakeOpening                  ((VS_UINT16) 25)
#define MsgBreakThrough                  ((VS_UINT16) 27)
#define MsgCheck                         ((VS_UINT16) 21)
#define MsgCheckDoor                     ((VS_UINT16) 22)
#define MsgChkLoked                      ((VS_UINT16) 29)
#define MsgCloseToOpen                   ((VS_UINT16) 28)
#define MsgClosed                        ((VS_UINT16) 12)
#define MsgClosing                       ((VS_UINT16) 9)
#define MsgDefault                       ((VS_UINT16) 20)
#define MsgFail                          ((VS_UINT16) 13)
#define MsgFindClosed                    ((VS_UINT16) 11)
#define MsgFindOpened                    ((VS_UINT16) 5)
#define MsgFreewheel                     ((VS_UINT16) 24)
#define MsgInitClosed                    ((VS_UINT16) 15)
#define MsgInitOpened                    ((VS_UINT16) 14)
#define MsgNearClosed                    ((VS_UINT16) 10)
#define MsgNearOpened                    ((VS_UINT16) 4)
#define MsgOldParLoad                    ((VS_UINT16) 23)
#define MsgOpened                        ((VS_UINT16) 6)
#define MsgOpening                       ((VS_UINT16) 3)
#define MsgRead                          ((VS_UINT16) 17)
#define MsgStartClosing                  ((VS_UINT16) 7)
#define MsgStartOpening                  ((VS_UINT16) 1)
#define MsgUpMask                        ((VS_UINT16) 19)
#define MsgWaitClosing                   ((VS_UINT16) 8)
#define MsgWaitCommand                   ((VS_UINT16) 16)
#define MsgWaitOpening                   ((VS_UINT16) 2)
#define MsgWrite                         ((VS_UINT16) 18)
#define PosMid                           ((VS_UINT16) 512)
#define PosMin                           ((VS_UINT16) 50)
#define PosRangeMin                      ((VS_UINT16) 20)


/*
 * VS System External Variable Declarations.
 */
extern VS_UINT BrakeOnOff;

extern VS_UINT CloseOnOff;

extern VS_UINT CurStep;

extern VS_UINT CurThrOverClose;

extern VS_UINT CurThrOverOpen;

extern VS_UINT Motor;

extern VS_UINT MovCount;

extern VS_UINT ParNumCounter;

extern VS_UINT ParSel;

extern VS_UINT Pos;

extern VS_UINT PosClose;

extern VS_UINT PosInvClose;

extern VS_UINT PosInvOpen;

extern VS_UINT PosInvOpenShk;

extern VS_UINT PosMaxClose;

extern VS_UINT PosMinOpen;

extern VS_UINT PosMinOpenShk;

extern VS_UINT PosNearClose;

extern VS_UINT PosNearOpen;

extern VS_UINT PosNearOpenShk;

extern VS_UINT PosOffsetAC;

extern VS_UINT PosOffsetAO;

extern VS_UINT PosOpen;

extern VS_UINT PosOpenShk;

extern VS_UINT SPCfg;

extern VS_UINT SSCfg;

extern VS_UINT STCfg;

extern VS_BOOL bEmergency;

extern VS_BOOL bEnExtProt;

extern VS_BOOL bLowBatt;

extern VS_BOOL bParReread;

extern VS_BOOL bPassiveMov;


#endif
