/*
 * Version:   50
 * Signature: eb1b 13af 7a0e 2056 ded0 ade0
 *
 * Id:        PLL_PEvent.h
 *
 * Function:  VS Project Event Header File.
 *
 * Generated: Thu May 30 15:27:04 2019
 *
 * Coder 5, 4, 0, 1273
 * 
 * This is an automatically generated file. It will be overwritten by the Coder. 
 * 
 * DO NOT EDIT THE FILE! 
 */


#ifndef __PLL_PEVENT_H
#define __PLL_PEVENT_H


/*
 * Global Event Identifier Definitions.
 */
#define SE_RESET                         0X000  /*   0 */


#endif
