/*
 * Version:   50
 * Signature: 72af f7cb a6bf a334 6b90 c26a
 *
 * Id:        PLL_PExtVar.c
 *
 * Function:  VS Project External Variable Source File.
 *
 * Generated: Tue Jul 17 13:01:51 2007
 *
 * Coder 5, 2, 0, 230 
 * 
 * This is an automatically generated file. It will be overwritten by the Coder. 
 * 
 * DO NOT EDIT THE FILE! 
 */


/*
 * Include VS Project External Variable Header File.
 */
#include "PLL_PExtVar.h"


/*
 * There is no VS Project external variables, so this section is intentionally empty!
 */
