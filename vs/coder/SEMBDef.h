/*
 * Version:   50
 * Signature: 72af f7cb a6bf a334 6b90 c26a
 *
 * Id:        SEMBDef.h
 *
 * Function:  SEM Defines Header File.
 *
 * Generated: Wed Jul 18 14:51:39 2007
 *
 * Coder 5, 4, 0, 1273
 * 
 * This is an automatically generated file. It will be overwritten by the Coder. 
 * 
 * DO NOT EDIT THE FILE! 
 */


#ifndef __SEMBDEF_H
#define __SEMBDEF_H


/*
 * Include SEM Types Header File.
 */
#include "SEMTypes.h"


#if (VS_CODER_GUID != 0X003dfd880)
#error The generated file does not match the SEMTypes.h header file.
#endif


/*
 * Number of Identifiers.
 */
#define VS_NOF_ACTION_EXPRESSIONS        0X002cb  /*   715 */
#define VS_NOF_ACTION_FUNCTIONS          0X05b  /*  91 */
#define VS_NOF_EVENT_GROUPS              0X000  /*   0 */
#define VS_NOF_EVENTS                    0X04e  /*  78 */
#define VS_NOF_EXTERNAL_VARIABLES        0X05b  /*  91 */
#define VS_NOF_GUARD_EXPRESSIONS         0X00315  /*   789 */
#define VS_NOF_INSTANCES                 0X001  /*   1 */
#define VS_NOF_INTERNAL_VARIABLES        0X01a  /*  26 */
#define VS_NOF_SIGNALS                   0X045  /*  69 */
#define VS_NOF_STATE_MACHINES            0X048  /*  72 */
#define VS_NOF_STATES                    0X0018e  /*   398 */


/*
 * Undefined State.
 */
#define STATE_UNDEFINED                  0X0FFFF  /* 65535 */


/*
 * Undefined Event.
 */
#define EVENT_UNDEFINED                  0X0FF  /* 255 */


/*
 * Undefined Event Group.
 */
#define EVENT_GROUP_UNDEFINED            0X0FF  /* 255 */


/*
 * Event Termination ID.
 */
#define EVENT_TERMINATION_ID             0X0FF  /* 255 */


/*
 * Action Expression Termination ID.
 */
#define ACTION_EXPRESSION_TERMINATION_ID 0X0FFFF  /* 65535 */
#define ACTION_FPT_NAME VSLogicVSAction


/*
 * Functional expression type definitions
 */
typedef VS_BOOL (* VS_GUARDEXPR_TYPE) (VS_VOID);
typedef VS_VOID (* VS_ACTIONEXPR_TYPE) (VS_VOID);


/*
 * SEM Library Datatype Definition.
 */
typedef struct SEMDATAVSLogic
{
  VS_UINT8                                      Status;
  VS_UINT8                                      State;
  VS_UINT8                                      DIt;
  VS_UINT8                                      nAction;
  SEM_EVENT_TYPE                                EventNo;
  SEM_RULE_INDEX_TYPE                           _iRI;
  SEM_RULE_TABLE_INDEX_TYPE                     iFirstR;
  SEM_RULE_TABLE_INDEX_TYPE                     iLastR;
  SEM_STATE_TYPE                                CSV[VS_NOF_STATE_MACHINES];
  SEM_STATE_TYPE                                WSV[VS_NOF_STATE_MACHINES];
  SEM_EVENT_TYPE                                SQueue[24];
  SEM_SIGNAL_QUEUE_TYPE                         SPut;
  SEM_SIGNAL_QUEUE_TYPE                         SGet;
  SEM_SIGNAL_QUEUE_TYPE                         SUsed;
} SEMDATAVSLogic;


/*
 * VS System Datatype Definition.
 */
typedef struct
{
  VS_UINT16      SMI[0X0018e];
  VS_UINT16      RD[0X088a6];
  VS_UINT16      RI[0X00c58];
  VS_UINT16      RTI[0X00094];
} VSDATAVSLogic;


/*
 * Data External Declaration.
 */
extern VSDATAVSLogic const VSLogic;

extern SEMDATAVSLogic SEMVSLogic;


/*
 * External Declarations for Guard Expressions Function Pointer Table.
 */
extern VS_GUARDEXPR_TYPE const VSLogicVSGuard[789];


/*
 * Action Expression Collection Macro.
 */
#define VSAction                       VSLogicVSAction


/*
 * VS Run Time Info
 */
#define VS_RUNTIME_INFO                1
#define VS_RUNTIME_INFO_EXTKW          
#define VS_SIGNATURE_VERSION           "50"
#define VS_SIGNATURE_CONTENT           "72af f7cb a6bf a334 6b90 c26a"
#define VS_SIGNATURE_VERSION_LENGTH    3
#define VS_SIGNATURE_CONTENT_LENGTH    30


#endif
