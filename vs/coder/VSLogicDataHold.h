/*
 * Version:   50
 * Signature: f58e bd71 dc7f bc14 a178 f1ba
 *
 * Id:        VSLogicData.h
 *
 * Function:  VS System Header File.
 *
 * Generated: Wed Sep 26 12:44:07 2007
 *
 * Coder 5, 4, 0, 1273
 * 
 * This is an automatically generated file. It will be overwritten by the Coder. 
 * 
 * DO NOT EDIT THE FILE! 
 */


#ifndef __VSLOGICDATA_H
#define __VSLOGICDATA_H


/*
 * Number of Identifiers.
 */
#define VS_NOF_ACTION_EXPRESSIONS        0X0034e  /*   846 */
#define VS_NOF_ACTION_FUNCTIONS          0X056  /*  86 */
#define VS_NOF_EVENT_GROUPS              0X003  /*   3 */
#define VS_NOF_EVENTS                    0X054  /*  84 */
#define VS_NOF_EXTERNAL_VARIABLES        0X05d  /*  93 */
#define VS_NOF_GUARD_EXPRESSIONS         0X003dd  /*   989 */
#define VS_NOF_INSTANCES                 0X001  /*   1 */
#define VS_NOF_INTERNAL_VARIABLES        0X01f  /*  31 */
#define VS_NOF_SIGNALS                   0X046  /*  70 */
#define VS_NOF_STATE_MACHINES            0X04f  /*  79 */
#define VS_NOF_STATES                    0X001b4  /*   436 */


/*
 * Include SEM Library Header File.
 */
#include "SEMLibE.h"


#if (VS_CODER_GUID != 0X01e94830e)
#error The generated file does not match the SEMTypes.h header file.
#endif


/*
 * Include Global Events Header File.
 */
#include "PLL_PEvent.h"


/*
 * External Declarations for Guard Expressions Function Pointer Table.
 */
extern VS_GUARDEXPR_TYPE const VSLogicVSGuard[989];


/*
 * External Declaration for SEM Deduct Function.
 */
extern unsigned char VSLogicSMP_Deduct (SEM_CONTEXT VS_TQ_CONTEXT * Context, SEM_EVENT_TYPE EventNo);


/*
 * External Declaration for SEM Signal Doubble Buffer Function.
 */
extern VS_VOID VSLogicSMP_SignalDB (SEM_CONTEXT VS_TQ_CONTEXT * Context, SEM_EVENT_TYPE SignalNo);


/*
 * External Declaration for wrapper function for all initializing functions.
 */
extern unsigned char VSLogicSMP_InitAll (SEM_CONTEXT VS_TQ_CONTEXT * * Context);


/*
 * Conditional Compilation Definitions.
 */
#define SEM_DEDUCT_FUNC                  1
#define SEM_DEDUCT_WITH_VA_LIST          0


/*
 * Event Identifier Definitions.
 */
#define eAuto                            0X001  /*   1 */
#define eBattLow                         0X002  /*   2 */
#define eBioNotOk                        0X003  /*   3 */
#define eBioOk                           0X004  /*   4 */
#define eBtn1                            0X005  /*   5 */
#define eBtn2                            0X006  /*   6 */
#define eCfgLoad                         0X007  /*   7 */
#define eClosed1                         0X008  /*   8 */
#define eClosed2                         0X009  /*   9 */
#define eCns1                            0X00A  /*  10 */
#define eCns1NotOk                       0X00B  /*  11 */
#define eCns1Ok                          0X00C  /*  12 */
#define eCns2                            0X00D  /*  13 */
#define eCns2NotOk                       0X00E  /*  14 */
#define eCns2Ok                          0X00F  /*  15 */
#define eCns3                            0X010  /*  16 */
#define eCns3NotOk                       0X011  /*  17 */
#define eCns3Ok                          0X012  /*  18 */
#define eCountEnd                        0X013  /*  19 */
#define eDeadLine                        0X014  /*  20 */
#define eEmergency                       0X015  /*  21 */
#define eEndPlay                         0X016  /*  22 */
#define eEndPlayBtn                      0X017  /*  23 */
#define eEnter                           0X018  /*  24 */
#define eExit                            0X019  /*  25 */
#define eF1                              0X01A  /*  26 */
#define eF2                              0X01B  /*  27 */
#define eF3                              0X01C  /*  28 */
#define eFirstIn                         0X01D  /*  29 */
#define eLastOut                         0X01E  /*  30 */
#define eMBtn1                           0X01F  /*  31 */
#define eMBtn2                           0X020  /*  32 */
#define eMetal                           0X021  /*  33 */
#define eMetalOff                        0X022  /*  34 */
#define eMetalOn                         0X023  /*  35 */
#define eMode                            0X024  /*  36 */
#define eMore                            0X025  /*  37 */
#define eMorning                         0X026  /*  38 */
#define eNight                           0X027  /*  39 */
#define eNormal                          0X028  /*  40 */
#define eNotClosed1                      0X029  /*  41 */
#define eNotClosed2                      0X02A  /*  42 */
#define eNotMore                         0X02B  /*  43 */
#define eNotPresent                      0X02C  /*  44 */
#define eObject                          0X02D  /*  45 */
#define eOnOff                           0X02E  /*  46 */
#define eOpened1                         0X02F  /*  47 */
#define eOpened2                         0X030  /*  48 */
#define ePCBioAmpOff                     0X031  /*  49 */
#define ePCBioAmpOn                      0X032  /*  50 */
#define ePCBioExit                       0X033  /*  51 */
#define ePCBioOff                        0X034  /*  52 */
#define ePCBioOn                         0X035  /*  53 */
#define ePCBioPolling                    0X036  /*  54 */
#define ePCBioRecStop                    0X037  /*  55 */
#define ePCBioStart                      0X038  /*  56 */
#define ePCBioStop                       0X039  /*  57 */
#define ePlayBtn                         0X03A  /*  58 */
#define ePresent                         0X03B  /*  59 */
#define eProgStart                       0X03C  /*  60 */
#define eProxDis1                        0X03D  /*  61 */
#define eProxDis2                        0X03E  /*  62 */
#define eProxNorm1                       0X03F  /*  63 */
#define eProxNorm2                       0X040  /*  64 */
#define eQ5ReadOk                        0X041  /*  65 */
#define eRecBtn                          0X042  /*  66 */
#define eResBtn1                         0X043  /*  67 */
#define eResBtn2                         0X044  /*  68 */
#define eResMBtn1                        0X045  /*  69 */
#define eResMBtn2                        0X046  /*  70 */
#define eReset                           0X047  /*  71 */
#define eResetSod                        0X048  /*  72 */
#define eStart                           0X049  /*  73 */
#define eT2End                           0X04A  /*  74 */
#define eTBioEnd                         0X04B  /*  75 */
#define eTBtn1End                        0X04C  /*  76 */
#define eTBtn2End                        0X04D  /*  77 */
#define eTMBtn2End                       0X04E  /*  78 */
#define eTVipEnd                         0X04F  /*  79 */
#define eTic                             0X050  /*  80 */
#define eVacOff                          0X051  /*  81 */
#define eVacOn                           0X052  /*  82 */
#define eVip                             0X053  /*  83 */


/*
 * Constants.
 */
#define PassCountMax                     ((VS_UCHAR) 10)
#define THoly                            ((VS_INT) 70)
#define TWork                            ((VS_INT) 76)
#define aBioCnbTOut                      ((VS_UCHAR) 41)
#define aBioIn                           ((VS_UCHAR) 18)
#define aBioMode                         ((VS_UCHAR) 24)
#define aBioOut                          ((VS_UCHAR) 40)
#define aBlkAlarmRed                     ((VS_UCHAR) 33)
#define aCAM                             ((VS_UCHAR) 10)
#define aDoorsOff                        ((VS_UCHAR) 28)
#define aEmeMode                         ((VS_UCHAR) 21)
#define aF1Sel                           ((VS_UCHAR) 2)
#define aF2Sel                           ((VS_UCHAR) 3)
#define aF3Sel                           ((VS_UCHAR) 4)
#define aFInMetal                        ((VS_UCHAR) 31)
#define aFInMore                         ((VS_UCHAR) 43)
#define aFri                             ((VS_UCHAR) 16)
#define aHHDay                           ((VS_UCHAR) 8)
#define aHHNight                         ((VS_UCHAR) 6)
#define aIntraSOD                        ((VS_UCHAR) 37)
#define aLampOn                          ((VS_UCHAR) 30)
#define aLanguage                        ((VS_UCHAR) 35)
#define aLastOutEm                       ((VS_UCHAR) 32)
#define aMMDay                           ((VS_UCHAR) 9)
#define aMMNight                         ((VS_UCHAR) 7)
#define aMOutAuto                        ((VS_UCHAR) 29)
#define aMail                            ((VS_UCHAR) 27)
#define aMemoReq                         ((VS_UCHAR) 38)
#define aMon                             ((VS_UCHAR) 12)
#define aMoreIn                          ((VS_UCHAR) 20)
#define aMoreOut                         ((VS_UCHAR) 34)
#define aNightNc                         ((VS_UCHAR) 19)
#define aProxIn                          ((VS_UCHAR) 39)
#define aProxOut                         ((VS_UCHAR) 42)
#define aSat                             ((VS_UCHAR) 17)
#define aSun                             ((VS_UCHAR) 11)
#define aTResSOD                         ((VS_UCHAR) 36)
#define aThu                             ((VS_UCHAR) 15)
#define aTue                             ((VS_UCHAR) 13)
#define aTunMode                         ((VS_UCHAR) 5)
#define aVfy                             ((VS_UCHAR) 25)
#define aVfyOut                          ((VS_UCHAR) 26)
#define aWed                             ((VS_UCHAR) 14)
#define aXChgIn                          ((VS_UCHAR) 22)
#define aXChgOut                         ((VS_UCHAR) 23)
#define addDate                          ((VS_UCHAR) 4)
#define addDay                           ((VS_UCHAR) 3)
#define addHour                          ((VS_UCHAR) 2)
#define addMin                           ((VS_UCHAR) 1)
#define addMonth                         ((VS_UCHAR) 5)
#define addSec                           ((VS_UCHAR) 0)
#define addYear                          ((VS_UCHAR) 6)
#define mAccessMode                      ((VS_INT) 160)
#define mBid                             ((VS_INT) 136)
#define mBioCheckIn                      ((VS_INT) 0)
#define mBioCheckOut                     ((VS_INT) 98)
#define mBioCnb                          ((VS_INT) 165)
#define mBioCnbTOut15                    ((VS_INT) 170)
#define mBioCnbTOut45                    ((VS_INT) 171)
#define mBioFace                         ((VS_INT) 102)
#define mBioFing                         ((VS_INT) 101)
#define mBioFingFace                     ((VS_INT) 103)
#define mBioInDisOff                     ((VS_INT) 78)
#define mBioInNone                       ((VS_INT) 92)
#define mBioInOff                        ((VS_INT) 90)
#define mBioInOn                         ((VS_INT) 91)
#define mBioNok                          ((VS_INT) 1)
#define mBioNone                         ((VS_INT) 100)
#define mBioOff                          ((VS_INT) 208)
#define mBioOn                           ((VS_INT) 209)
#define mBioOutDisOff                    ((VS_INT) 79)
#define mBioOutNone                      ((VS_INT) 166)
#define mBioOutOff                       ((VS_INT) 168)
#define mBioOutOn                        ((VS_INT) 167)
#define mBiometric                       ((VS_INT) 164)
#define mBlkAlarmRedOff                  ((VS_INT) 126)
#define mBlkAlarmRedOn                   ((VS_INT) 127)
#define mCAM                             ((VS_INT) 68)
#define mDate                            ((VS_INT) 73)
#define mDateT                           ((VS_INT) 162)
#define mDateTime                        ((VS_INT) 161)
#define mDay                             ((VS_INT) 74)
#define mDoorOpnN                        ((VS_INT) 143)
#define mDoorOpnP1                       ((VS_INT) 139)
#define mDoorOpnP2                       ((VS_INT) 140)
#define mDoors                           ((VS_INT) 176)
#define mDoorsClsd                       ((VS_INT) 141)
#define mDoorsForced                     ((VS_INT) 52)
#define mEmeP1                           ((VS_INT) 213)
#define mEmeP1P2                         ((VS_INT) 212)
#define mEmeP2                           ((VS_INT) 214)
#define mEmergency                       ((VS_INT) 2)
#define mEnglish                         ((VS_INT) 173)
#define mF1Bio                           ((VS_INT) 111)
#define mF1Emergency                     ((VS_INT) 112)
#define mF1In                            ((VS_INT) 113)
#define mF1Metal                         ((VS_INT) 114)
#define mF1More                          ((VS_INT) 115)
#define mF1Off                           ((VS_INT) 110)
#define mF2Bio                           ((VS_INT) 121)
#define mF2Emergency                     ((VS_INT) 122)
#define mF2In                            ((VS_INT) 123)
#define mF2Metal                         ((VS_INT) 124)
#define mF2More                          ((VS_INT) 125)
#define mF2Off                           ((VS_INT) 120)
#define mF3Bio                           ((VS_INT) 131)
#define mF3Emergency                     ((VS_INT) 132)
#define mF3In                            ((VS_INT) 133)
#define mF3Metal                         ((VS_INT) 134)
#define mF3More                          ((VS_INT) 135)
#define mF3Off                           ((VS_INT) 130)
#define mFInMetalOff                     ((VS_INT) 116)
#define mFInMetalOn                      ((VS_INT) 117)
#define mFInMoreOff                      ((VS_INT) 177)
#define mFInMoreOn                       ((VS_INT) 176)
#define mFri                             ((VS_INT) 85)
#define mFuncBtn                         ((VS_INT) 169)
#define mHHDay                           ((VS_INT) 62)
#define mHHMMDay                         ((VS_INT) 66)
#define mHHMMNight                       ((VS_INT) 67)
#define mHHNight                         ((VS_INT) 63)
#define mHour                            ((VS_INT) 75)
#define mIdle                            ((VS_INT) 3)
#define mInAbort                         ((VS_INT) 8)
#define mInAbortBioNotPresent            ((VS_INT) 218)
#define mInAbortBioPresent               ((VS_INT) 217)
#define mInAbortNotPresent               ((VS_INT) 216)
#define mInAbortPresent                  ((VS_INT) 215)
#define mInClose1                        ((VS_INT) 4)
#define mInClose2                        ((VS_INT) 5)
#define mInControl                       ((VS_INT) 6)
#define mInMoreAlarm                     ((VS_INT) 7)
#define mInOpen1                         ((VS_INT) 9)
#define mInOpen2                         ((VS_INT) 10)
#define mInPause                         ((VS_INT) 11)
#define mInWait2                         ((VS_INT) 12)
#define mIntraSOD15                      ((VS_INT) 152)
#define mIntraSOD240                     ((VS_INT) 154)
#define mIntraSOD45                      ((VS_INT) 153)
#define mItaliano                        ((VS_INT) 172)
#define mLOutEmOff                       ((VS_INT) 118)
#define mLOutEmOn                        ((VS_INT) 119)
#define mLampOff                         ((VS_INT) 181)
#define mLampOn                          ((VS_INT) 180)
#define mMChangeDir1                     ((VS_INT) 13)
#define mMChangeDir2                     ((VS_INT) 14)
#define mMIdle                           ((VS_INT) 15)
#define mMIn                             ((VS_INT) 93)
#define mMInClose1                       ((VS_INT) 16)
#define mMInClose2                       ((VS_INT) 17)
#define mMInControl                      ((VS_INT) 60)
#define mMInMoreAlarm                    ((VS_INT) 61)
#define mMInOpen1                        ((VS_INT) 18)
#define mMInOpen2                        ((VS_INT) 19)
#define mMInPause                        ((VS_INT) 20)
#define mMInWait2                        ((VS_INT) 21)
#define mMMDay                           ((VS_INT) 64)
#define mMMNight                         ((VS_INT) 65)
#define mMOutAutoOff                     ((VS_INT) 220)
#define mMOutAutoOn                      ((VS_INT) 219)
#define mMOutClose1                      ((VS_INT) 22)
#define mMOutClose2                      ((VS_INT) 23)
#define mMOutControl                     ((VS_INT) 59)
#define mMOutMoreAlarm                   ((VS_INT) 58)
#define mMOutOpen1                       ((VS_INT) 24)
#define mMOutOpen2                       ((VS_INT) 25)
#define mMOutPause                       ((VS_INT) 26)
#define mMOutWait1                       ((VS_INT) 27)
#define mMailOff                         ((VS_INT) 108)
#define mMailOn                          ((VS_INT) 109)
#define mMemoOff                         ((VS_INT) 157)
#define mMemoOn1                         ((VS_INT) 158)
#define mMemoOn2                         ((VS_INT) 159)
#define mMetalAlarm                      ((VS_INT) 53)
#define mMetalNotPresent                 ((VS_INT) 28)
#define mMetalOff                        ((VS_INT) 206)
#define mMetalOn                         ((VS_INT) 207)
#define mMetalPresent                    ((VS_INT) 29)
#define mMin                             ((VS_INT) 76)
#define mModeWeek                        ((VS_INT) 175)
#define mMon                             ((VS_INT) 81)
#define mMonth                           ((VS_INT) 72)
#define mMoreInOff                       ((VS_INT) 145)
#define mMoreInOn                        ((VS_INT) 144)
#define mMoreOff                         ((VS_INT) 210)
#define mMoreOn                          ((VS_INT) 211)
#define mMoreOutOff                      ((VS_INT) 147)
#define mMoreOutOn                       ((VS_INT) 146)
#define mMorningError                    ((VS_INT) 32)
#define mMorningIdle                     ((VS_INT) 33)
#define mMorningPresent                  ((VS_INT) 34)
#define mNightChkObject                  ((VS_INT) 49)
#define mNightClose1                     ((VS_INT) 50)
#define mNightClose2                     ((VS_INT) 48)
#define mNightError                      ((VS_INT) 149)
#define mNightIdle                       ((VS_INT) 47)
#define mNightNc                         ((VS_INT) 142)
#define mNightOpenAll                    ((VS_INT) 51)
#define mNightPresent                    ((VS_INT) 148)
#define mObjectAlarm                     ((VS_INT) 30)
#define mObjectCheck                     ((VS_INT) 31)
#define mOnlyIn                          ((VS_INT) 137)
#define mOnlyOut                         ((VS_INT) 138)
#define mOutAbort                        ((VS_INT) 54)
#define mOutClose1                       ((VS_INT) 35)
#define mOutClose2                       ((VS_INT) 36)
#define mOutControl                      ((VS_INT) 37)
#define mOutMoreAlarm                    ((VS_INT) 38)
#define mOutOpen1                        ((VS_INT) 40)
#define mOutOpen2                        ((VS_INT) 41)
#define mOutPause                        ((VS_INT) 42)
#define mOutWait1                        ((VS_INT) 43)
#define mPCBioOffLine                    ((VS_INT) 205)
#define mPCBioOn                         ((VS_INT) 201)
#define mPCBioOnLine                     ((VS_INT) 204)
#define mPCBioWaitOff                    ((VS_INT) 203)
#define mPCBioWaitOn                     ((VS_INT) 202)
#define mProgOn1                         ((VS_INT) 56)
#define mProxInOff                       ((VS_INT) 156)
#define mProxInOn                        ((VS_INT) 155)
#define mProxOutOff                      ((VS_INT) 179)
#define mProxOutOn                       ((VS_INT) 178)
#define mSOD3                            ((VS_INT) 150)
#define mSOD6                            ((VS_INT) 151)
#define mSat                             ((VS_INT) 86)
#define mSec                             ((VS_INT) 77)
#define mSodCheck                        ((VS_INT) 44)
#define mSodPause                        ((VS_INT) 45)
#define mSodReset                        ((VS_INT) 46)
#define mSun                             ((VS_INT) 87)
#define mThu                             ((VS_INT) 84)
#define mTime                            ((VS_INT) 163)
#define mTue                             ((VS_INT) 82)
#define mTunnel                          ((VS_INT) 174)
#define mUnavailable                     ((VS_INT) 88)
#define mVfyOff                          ((VS_INT) 104)
#define mVfyOn                           ((VS_INT) 105)
#define mVfyOutOff                       ((VS_INT) 106)
#define mVfyOutOn                        ((VS_INT) 107)
#define mWed                             ((VS_INT) 83)
#define mWeek                            ((VS_INT) 80)
#define mXChgInOff                       ((VS_INT) 94)
#define mXChgInOn                        ((VS_INT) 95)
#define mXChgOutOff                      ((VS_INT) 96)
#define mXChgOutOn                       ((VS_INT) 97)
#define mYear                            ((VS_INT) 71)


/*
 * VS System External Variable Declarations.
 */
extern VS_UCHAR AIn;

extern VS_UCHAR AInBio;

extern VS_UCHAR AInChange;

extern VS_UCHAR AInMetal;

extern VS_UCHAR AInMore;

extern VS_UCHAR AInNc;

extern VS_UCHAR AOut;

extern VS_UCHAR AOutBio;

extern VS_UCHAR AOutChange;

extern VS_UCHAR AOutMetal;

extern VS_UCHAR AOutMore;

extern VS_UCHAR AOutNc;

extern VS_UCHAR AVerify;

extern VS_UCHAR AVerifyOut;

extern VS_UCHAR BattLow;

extern VS_UCHAR BioCnbTOut;

extern VS_UCHAR BioMode;

extern VS_UCHAR BlkAlarmRed;

extern VS_UCHAR CfgCAM;

extern VS_UCHAR CfgFri;

extern VS_UCHAR CfgMon;

extern VS_UCHAR CfgSat;

extern VS_UCHAR CfgSun;

extern VS_UCHAR CfgThu;

extern VS_UCHAR CfgTue;

extern VS_UCHAR CfgV;

extern VS_UCHAR CfgWed;

extern VS_UCHAR Closed1;

extern VS_UCHAR Closed2;

extern VS_UCHAR Cns1Disp;

extern VS_UCHAR Cns1On;

extern VS_UCHAR Cns2Disp;

extern VS_UCHAR Cns2On;

extern VS_UCHAR Cns3Disp;

extern VS_UCHAR Cns3On;

extern VS_UCHAR DoorsMode;

extern VS_UCHAR EmeMode;

extern VS_UCHAR EnablePCBioOnOff;

extern VS_UCHAR F1Sel;

extern VS_UCHAR F2Sel;

extern VS_UCHAR F3Sel;

extern VS_UCHAR FInBio;

extern VS_UCHAR FInMetal;

extern VS_UCHAR FInMore;

extern VS_UCHAR FmwV1;

extern VS_UCHAR FmwV2;

extern VS_UCHAR HHDay;

extern VS_UCHAR HHNight;

extern VS_UCHAR IntraSOD;

extern VS_UCHAR LOutBio;

extern VS_UCHAR LOutMore;

extern VS_UCHAR LampOn;

extern VS_UCHAR Language;

extern VS_UCHAR LastOutEm;

extern VS_INT LogicMsgId;

extern VS_UCHAR MIn;

extern VS_UCHAR MInBio;

extern VS_UCHAR MInChange;

extern VS_UCHAR MInMetal;

extern VS_UCHAR MInMore;

extern VS_UCHAR MInNc;

extern VS_UCHAR MMDay;

extern VS_UCHAR MMNight;

extern VS_UCHAR MOut;

extern VS_UCHAR MOutAuto;

extern VS_UCHAR MOutBio;

extern VS_UCHAR MOutChange;

extern VS_UCHAR MOutMetal;

extern VS_UCHAR MOutMore;

extern VS_UCHAR MOutNc;

extern VS_UCHAR MVerify;

extern VS_UCHAR MVerifyOut;

extern VS_UCHAR Mail;

extern VS_UINT MaxBioCount;

extern VS_UCHAR MaxUsersIn;

extern VS_UCHAR MemoReq;

extern VS_INT NMsg;

extern VS_UCHAR NightNc;

extern VS_UCHAR Opened1;

extern VS_UCHAR Opened2;

extern VS_UCHAR PCBioStatus;

extern VS_UCHAR PassCount;

extern VS_UCHAR Present;

extern VS_UCHAR ProxIn;

extern VS_INT ProxOut;

extern VS_UCHAR TResSOD;

extern VS_UCHAR TunMode;

extern VS_UCHAR UsersIn;

extern VS_UCHAR Vac;

extern VS_UCHAR bCfg;

extern VS_UCHAR bDisPrg;

extern VS_UCHAR bManu;

extern VS_UCHAR bVip;


#endif
