/*
 * Version:   50
 * Signature: eb1b 13af 7a0e 2056 ded0 ade0
 *
 * Id:        VSDoorsData.c
 *
 * Function:  VS System Data Source File.
 *
 * Generated: Thu May 30 15:27:04 2019
 *
 * Coder 5, 4, 0, 1273
 * 
 * This is an automatically generated file. It will be overwritten by the Coder. 
 * 
 * DO NOT EDIT THE FILE! 
 */


/*
 * Include VS System Data Header File.
 */
#include "VSDoorsData.h"


#if (VS_CODER_GUID != 0X015e8c916)
#error The generated file does not match the SEMTypes.h header file.
#endif


/*
 * Include VS System Header File.
 */
#include "VSDoors.h"


/*
 * Include VS Project Constants Header file.
 */
#include "PLL_PConstant.h"


/*
 * Include VS Project Event Header File.
 */
#include "PLL_PEvent.h"


/*
 * Include VS System Action Expression Pointer Table File.
 */
#include "VSDoorsAction.h"


#include <stdarg.h>


/*
 * VS System External Variable Definitions.
 */
VS_UINT BrakeOnOff;

VS_UINT CloseOnOff;

VS_UINT CurStep;

VS_UINT CurThrOverClose;

VS_UINT CurThrOverOpen;

VS_UINT Motor;

VS_UINT MovCount;

VS_UINT ParNumCounter;

VS_UINT ParSel;

VS_UINT Pos;

VS_UINT PosClose;

VS_UINT PosInvClose;

VS_UINT PosInvOpen;

VS_UINT PosInvOpenShk;

VS_UINT PosMaxClose;

VS_UINT PosMinOpen;

VS_UINT PosMinOpenShk;

VS_UINT PosNearClose;

VS_UINT PosNearOpen;

VS_UINT PosNearOpenShk;

VS_UINT PosOffsetAC;

VS_UINT PosOffsetAO;

VS_UINT PosOpen;

VS_UINT PosOpenShk;

VS_UINT SPCfg;

VS_UINT SSCfg;

VS_UINT STCfg;

VS_BOOL bEmergency;

VS_BOOL bEnExtProt;

VS_BOOL bLowBatt;

VS_BOOL bParReread;

VS_BOOL bPassiveMov;


/*
 * Guard Expression Functions.
 */
static VS_BOOL VSDoorsVSGuard_0 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (ParSel == ParNumCounter);
}
static VS_BOOL VSDoorsVSGuard_1 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Motor != 0);
}
static VS_BOOL VSDoorsVSGuard_2 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Motor != 0);
}
static VS_BOOL VSDoorsVSGuard_3 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Motor != 0);
}
static VS_BOOL VSDoorsVSGuard_4 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos > PosMaxClose);
}
static VS_BOOL VSDoorsVSGuard_5 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Motor != 0);
}
static VS_BOOL VSDoorsVSGuard_6 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (BrakeOnOff == 0);
}
static VS_BOOL VSDoorsVSGuard_7 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos < PosMinOpen);
}
static VS_BOOL VSDoorsVSGuard_8 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Motor != 0);
}
static VS_BOOL VSDoorsVSGuard_9 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Motor != 0);
}
static VS_BOOL VSDoorsVSGuard_10 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos < PosMinOpen);
}
static VS_BOOL VSDoorsVSGuard_11 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (BrakeOnOff == 1);
}
static VS_BOOL VSDoorsVSGuard_12 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Motor == 0);
}
static VS_BOOL VSDoorsVSGuard_13 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (SSCfg == 1);
}
static VS_BOOL VSDoorsVSGuard_14 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (SPCfg == 1);
}
static VS_BOOL VSDoorsVSGuard_15 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Motor == 0);
}
static VS_BOOL VSDoorsVSGuard_16 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (SSCfg == 0);
}
static VS_BOOL VSDoorsVSGuard_17 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (SPCfg == 1);
}
static VS_BOOL VSDoorsVSGuard_18 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Motor == 0);
}
static VS_BOOL VSDoorsVSGuard_19 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (SSCfg == 1);
}
static VS_BOOL VSDoorsVSGuard_20 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (SPCfg == 0);
}
static VS_BOOL VSDoorsVSGuard_21 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Motor == 0);
}
static VS_BOOL VSDoorsVSGuard_22 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (SSCfg == 0);
}
static VS_BOOL VSDoorsVSGuard_23 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (SPCfg == 0);
}
static VS_BOOL VSDoorsVSGuard_24 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Motor == 0);
}
static VS_BOOL VSDoorsVSGuard_25 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (SSCfg == 0);
}
static VS_BOOL VSDoorsVSGuard_26 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (SPCfg == 1);
}
static VS_BOOL VSDoorsVSGuard_27 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Motor == 0);
}
static VS_BOOL VSDoorsVSGuard_28 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (SSCfg == 0);
}
static VS_BOOL VSDoorsVSGuard_29 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (SPCfg == 1);
}
static VS_BOOL VSDoorsVSGuard_30 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Motor == 0);
}
static VS_BOOL VSDoorsVSGuard_31 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (SSCfg == 1);
}
static VS_BOOL VSDoorsVSGuard_32 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (SPCfg == 1);
}
static VS_BOOL VSDoorsVSGuard_33 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Motor == 0);
}
static VS_BOOL VSDoorsVSGuard_34 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (SSCfg == 1);
}
static VS_BOOL VSDoorsVSGuard_35 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (SPCfg == 0);
}
static VS_BOOL VSDoorsVSGuard_36 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Motor == 0);
}
static VS_BOOL VSDoorsVSGuard_37 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (SSCfg == 1);
}
static VS_BOOL VSDoorsVSGuard_38 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (SPCfg == 0);
}
static VS_BOOL VSDoorsVSGuard_39 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Motor == 0);
}
static VS_BOOL VSDoorsVSGuard_40 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (SSCfg == 1);
}
static VS_BOOL VSDoorsVSGuard_41 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (SPCfg == 1);
}
static VS_BOOL VSDoorsVSGuard_42 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos > PosMaxClose);
}
static VS_BOOL VSDoorsVSGuard_43 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (bEmergency == 0);
}
static VS_BOOL VSDoorsVSGuard_44 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (CloseOnOff == 0);
}
static VS_BOOL VSDoorsVSGuard_45 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (BrakeOnOff == 1);
}
static VS_BOOL VSDoorsVSGuard_46 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (CloseOnOff == 1);
}
static VS_BOOL VSDoorsVSGuard_47 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (bEmergency == 0);
}
static VS_BOOL VSDoorsVSGuard_48 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (CloseOnOff == 0);
}
static VS_BOOL VSDoorsVSGuard_49 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (BrakeOnOff == 0);
}
static VS_BOOL VSDoorsVSGuard_50 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (bEmergency == 1);
}
static VS_BOOL VSDoorsVSGuard_51 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (CloseOnOff == 0);
}
static VS_BOOL VSDoorsVSGuard_52 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (BrakeOnOff == 0);
}
static VS_BOOL VSDoorsVSGuard_53 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (bEmergency == 1);
}
static VS_BOOL VSDoorsVSGuard_54 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (CloseOnOff == 0);
}
static VS_BOOL VSDoorsVSGuard_55 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (BrakeOnOff == 1);
}
static VS_BOOL VSDoorsVSGuard_56 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos < PosMinOpen);
}
static VS_BOOL VSDoorsVSGuard_57 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos > PosNearOpen);
}
static VS_BOOL VSDoorsVSGuard_58 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos > (PosOpen - ((PosOpen - PosClose) * 8) / 100));
}
static VS_BOOL VSDoorsVSGuard_59 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos < (PosClose + ((PosOpen - PosClose) * 8) / 100));
}
static VS_BOOL VSDoorsVSGuard_60 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos < PosMinOpen);
}
static VS_BOOL VSDoorsVSGuard_61 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos > PosMinOpen);
}
static VS_BOOL VSDoorsVSGuard_62 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos > PosMinOpen);
}
static VS_BOOL VSDoorsVSGuard_63 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos > (PosMinOpen + PosOffsetAO));
}
static VS_BOOL VSDoorsVSGuard_64 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (bEmergency == 0);
}
static VS_BOOL VSDoorsVSGuard_65 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos < PosMaxClose);
}
static VS_BOOL VSDoorsVSGuard_66 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos > PosMaxClose + 2);
}
static VS_BOOL VSDoorsVSGuard_67 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos > PosMaxClose + 2);
}
static VS_BOOL VSDoorsVSGuard_68 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (bEmergency == 1);
}
static VS_BOOL VSDoorsVSGuard_69 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (bEmergency == 0);
}
static VS_BOOL VSDoorsVSGuard_70 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos < PosMaxClose);
}
static VS_BOOL VSDoorsVSGuard_71 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos > PosMaxClose + 2);
}
static VS_BOOL VSDoorsVSGuard_72 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (bEnExtProt == 1);
}
static VS_BOOL VSDoorsVSGuard_73 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos < PosMaxClose - PosOffsetAC);
}
static VS_BOOL VSDoorsVSGuard_74 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos > PosMaxClose + 2);
}
static VS_BOOL VSDoorsVSGuard_75 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (bEnExtProt == 1);
}
static VS_BOOL VSDoorsVSGuard_76 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (bEmergency == 1);
}
static VS_BOOL VSDoorsVSGuard_77 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (bEmergency == 0);
}
static VS_BOOL VSDoorsVSGuard_78 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos < PosNearClose);
}
static VS_BOOL VSDoorsVSGuard_79 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos > PosMaxClose);
}
static VS_BOOL VSDoorsVSGuard_80 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (bEmergency == 1);
}
static VS_BOOL VSDoorsVSGuard_81 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos > PosNearOpen);
}
static VS_BOOL VSDoorsVSGuard_82 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos < PosNearClose);
}
static VS_BOOL VSDoorsVSGuard_83 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (PosOpen >= 20 + Pos);
}
static VS_BOOL VSDoorsVSGuard_84 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (PosOpen < 20 + Pos);
}
static VS_BOOL VSDoorsVSGuard_85 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos > 50);
}
static VS_BOOL VSDoorsVSGuard_86 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos <= 50);
}
static VS_BOOL VSDoorsVSGuard_87 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos < PosMaxClose);
}
static VS_BOOL VSDoorsVSGuard_88 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos >= PosMaxClose);
}
static VS_BOOL VSDoorsVSGuard_89 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos < (((PosOpen - PosClose) / 2) + PosClose));
}
static VS_BOOL VSDoorsVSGuard_90 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos > PosMinOpen);
}
static VS_BOOL VSDoorsVSGuard_91 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos <= PosMinOpen);
}
static VS_BOOL VSDoorsVSGuard_92 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos >= (((PosOpen - PosClose) / 2) + PosClose));
}
static VS_BOOL VSDoorsVSGuard_93 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos >= PosMaxClose);
}
static VS_BOOL VSDoorsVSGuard_94 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos >= (((PosOpen - PosClose) / 2) + PosClose));
}
static VS_BOOL VSDoorsVSGuard_95 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos <= PosMinOpen);
}
static VS_BOOL VSDoorsVSGuard_96 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos < (((PosOpen - PosClose) / 2) + PosClose));
}
static VS_BOOL VSDoorsVSGuard_97 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (bEmergency == 0);
}
static VS_BOOL VSDoorsVSGuard_98 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (bEmergency == 1);
}
static VS_BOOL VSDoorsVSGuard_99 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ( ! ((bEmergency == 1) && (bLowBatt == 1)));
}
static VS_BOOL VSDoorsVSGuard_100 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (bParReread == 0);
}
static VS_BOOL VSDoorsVSGuard_101 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ( ! ((bEmergency == 1) && (bLowBatt == 1)));
}
static VS_BOOL VSDoorsVSGuard_102 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (bParReread == 1);
}
static VS_BOOL VSDoorsVSGuard_103 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (((bEmergency == 1) && (bLowBatt == 1)));
}
static VS_BOOL VSDoorsVSGuard_104 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (SSCfg == 1);
}
static VS_BOOL VSDoorsVSGuard_105 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos > PosNearOpenShk);
}
static VS_BOOL VSDoorsVSGuard_106 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos < PosMinOpenShk);
}
static VS_BOOL VSDoorsVSGuard_107 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos > (PosOpenShk - ((PosOpenShk - PosClose) * 8) / 100));
}
static VS_BOOL VSDoorsVSGuard_108 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos < PosMinOpenShk);
}
static VS_BOOL VSDoorsVSGuard_109 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos > PosMinOpenShk);
}
static VS_BOOL VSDoorsVSGuard_110 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos > PosMinOpenShk);
}
static VS_BOOL VSDoorsVSGuard_111 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos > (PosMinOpenShk + PosOffsetAO));
}
static VS_BOOL VSDoorsVSGuard_112 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos > PosNearOpenShk);
}
static VS_BOOL VSDoorsVSGuard_113 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (BrakeOnOff == 1);
}
static VS_BOOL VSDoorsVSGuard_114 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos > PosInvOpen);
}
static VS_BOOL VSDoorsVSGuard_115 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (CurThrOverClose == 0);
}
static VS_BOOL VSDoorsVSGuard_116 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos > PosInvOpen);
}
static VS_BOOL VSDoorsVSGuard_117 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (CurThrOverClose == 0);
}
static VS_BOOL VSDoorsVSGuard_118 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (bPassiveMov == 0);
}
static VS_BOOL VSDoorsVSGuard_119 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (MovCount >= 250);
}
static VS_BOOL VSDoorsVSGuard_120 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (bPassiveMov == 0);
}
static VS_BOOL VSDoorsVSGuard_121 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (MovCount < 250);
}
static VS_BOOL VSDoorsVSGuard_122 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (bPassiveMov == 0);
}
static VS_BOOL VSDoorsVSGuard_123 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (MovCount >= 250);
}
static VS_BOOL VSDoorsVSGuard_124 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (bPassiveMov == 0);
}
static VS_BOOL VSDoorsVSGuard_125 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (MovCount < 250);
}
static VS_BOOL VSDoorsVSGuard_126 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (BrakeOnOff == 0);
}
static VS_BOOL VSDoorsVSGuard_127 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (BrakeOnOff == 1);
}
static VS_BOOL VSDoorsVSGuard_128 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos <= PosMaxClose);
}
static VS_BOOL VSDoorsVSGuard_129 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos > PosNearClose);
}
static VS_BOOL VSDoorsVSGuard_130 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (bEnExtProt == 0);
}
static VS_BOOL VSDoorsVSGuard_131 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos < PosInvClose);
}
static VS_BOOL VSDoorsVSGuard_132 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (CurThrOverOpen == 0);
}
static VS_BOOL VSDoorsVSGuard_133 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos < PosInvClose);
}
static VS_BOOL VSDoorsVSGuard_134 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (CurThrOverOpen == 0);
}
static VS_BOOL VSDoorsVSGuard_135 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (BrakeOnOff == 0);
}
static VS_BOOL VSDoorsVSGuard_136 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((PosClose > 50) && (PosOpen >= 20 + PosClose));
}
static VS_BOOL VSDoorsVSGuard_137 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ( ! ((PosClose > 50) && (PosOpen >= 20 + PosClose)));
}
static VS_BOOL VSDoorsVSGuard_138 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (BrakeOnOff == 0);
}
static VS_BOOL VSDoorsVSGuard_139 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (BrakeOnOff == 1);
}
static VS_BOOL VSDoorsVSGuard_140 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (SSCfg == 0);
}
static VS_BOOL VSDoorsVSGuard_141 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (SSCfg == 1);
}
static VS_BOOL VSDoorsVSGuard_142 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos > PosInvOpenShk);
}
static VS_BOOL VSDoorsVSGuard_143 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (CurThrOverClose == 0);
}
static VS_BOOL VSDoorsVSGuard_144 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (Pos > PosInvOpenShk);
}
static VS_BOOL VSDoorsVSGuard_145 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (CurThrOverClose == 0);
}
static VS_BOOL VSDoorsVSGuard_146 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (bPassiveMov == 0);
}
static VS_BOOL VSDoorsVSGuard_147 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (MovCount >= 250);
}
static VS_BOOL VSDoorsVSGuard_148 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (bPassiveMov == 0);
}
static VS_BOOL VSDoorsVSGuard_149 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (MovCount < 250);
}
static VS_BOOL VSDoorsVSGuard_150 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((STCfg == 0) || (bEmergency == 1));
}
static VS_BOOL VSDoorsVSGuard_151 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((STCfg != 0) && (bEmergency == 0));
}
static VS_BOOL VSDoorsVSGuard_152 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((STCfg == 0) || (bEmergency == 1));
}
static VS_BOOL VSDoorsVSGuard_153 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((STCfg != 0) && (bEmergency == 0));
}
static VS_BOOL VSDoorsVSGuard_154 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((STCfg == 0) || (bEmergency == 1));
}
static VS_BOOL VSDoorsVSGuard_155 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((STCfg != 0) && (bEmergency == 0));
}
static VS_BOOL VSDoorsVSGuard_156 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((STCfg == 0) || (bEmergency == 1));
}
static VS_BOOL VSDoorsVSGuard_157 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((STCfg != 0) && (bEmergency == 0));
}
static VS_BOOL VSDoorsVSGuard_158 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((STCfg != 0) && (bEmergency == 0));
}
static VS_BOOL VSDoorsVSGuard_159 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((STCfg == 0) || (bEmergency == 1));
}
static VS_BOOL VSDoorsVSGuard_160 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((STCfg == 0) || (bEmergency == 1));
}
static VS_BOOL VSDoorsVSGuard_161 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((STCfg != 0) && (bEmergency == 0));
}
static VS_BOOL VSDoorsVSGuard_162 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((STCfg == 0) || (bEmergency == 1));
}
static VS_BOOL VSDoorsVSGuard_163 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((STCfg != 0) && (bEmergency == 0));
}
static VS_BOOL VSDoorsVSGuard_164 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((STCfg != 0) && (bEmergency == 1));
}
static VS_BOOL VSDoorsVSGuard_165 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((STCfg != 0) && (bEmergency == 0));
}
static VS_BOOL VSDoorsVSGuard_166 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (STCfg != 0);
}
static VS_BOOL VSDoorsVSGuard_167 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((STCfg == 0) || (bEmergency == 1));
}
static VS_BOOL VSDoorsVSGuard_168 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((STCfg != 0) && (bEmergency == 0));
}
static VS_BOOL VSDoorsVSGuard_169 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (STCfg != 0);
}
static VS_BOOL VSDoorsVSGuard_170 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((STCfg == 0) || (bEmergency == 1));
}
static VS_BOOL VSDoorsVSGuard_171 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((STCfg != 0) && (bEmergency == 0));
}
static VS_BOOL VSDoorsVSGuard_172 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (STCfg != 0);
}
static VS_BOOL VSDoorsVSGuard_173 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((STCfg != 0) && (bEmergency == 1));
}
static VS_BOOL VSDoorsVSGuard_174 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((STCfg != 0) && (bEmergency == 0));
}
static VS_BOOL VSDoorsVSGuard_175 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((STCfg == 0) || (bEmergency == 1));
}
static VS_BOOL VSDoorsVSGuard_176 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((STCfg != 0) && (bEmergency == 0));
}
static VS_BOOL VSDoorsVSGuard_177 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((STCfg != 0) && (bEmergency == 0));
}
static VS_BOOL VSDoorsVSGuard_178 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((STCfg == 0) || (bEmergency == 1));
}
static VS_BOOL VSDoorsVSGuard_179 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((STCfg != 0) && (bEmergency == 0));
}
static VS_BOOL VSDoorsVSGuard_180 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (STCfg != 0);
}
static VS_BOOL VSDoorsVSGuard_181 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return (STCfg != 0);
}
static VS_BOOL VSDoorsVSGuard_182 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  return ((STCfg != 0));
}


/*
 * Guard Expression Pointer Table.
 */
VS_GUARDEXPR_TYPE const VSDoorsVSGuard[183] = 
{
  VSDoorsVSGuard_0,
  VSDoorsVSGuard_1,
  VSDoorsVSGuard_2,
  VSDoorsVSGuard_3,
  VSDoorsVSGuard_4,
  VSDoorsVSGuard_5,
  VSDoorsVSGuard_6,
  VSDoorsVSGuard_7,
  VSDoorsVSGuard_8,
  VSDoorsVSGuard_9,
  VSDoorsVSGuard_10,
  VSDoorsVSGuard_11,
  VSDoorsVSGuard_12,
  VSDoorsVSGuard_13,
  VSDoorsVSGuard_14,
  VSDoorsVSGuard_15,
  VSDoorsVSGuard_16,
  VSDoorsVSGuard_17,
  VSDoorsVSGuard_18,
  VSDoorsVSGuard_19,
  VSDoorsVSGuard_20,
  VSDoorsVSGuard_21,
  VSDoorsVSGuard_22,
  VSDoorsVSGuard_23,
  VSDoorsVSGuard_24,
  VSDoorsVSGuard_25,
  VSDoorsVSGuard_26,
  VSDoorsVSGuard_27,
  VSDoorsVSGuard_28,
  VSDoorsVSGuard_29,
  VSDoorsVSGuard_30,
  VSDoorsVSGuard_31,
  VSDoorsVSGuard_32,
  VSDoorsVSGuard_33,
  VSDoorsVSGuard_34,
  VSDoorsVSGuard_35,
  VSDoorsVSGuard_36,
  VSDoorsVSGuard_37,
  VSDoorsVSGuard_38,
  VSDoorsVSGuard_39,
  VSDoorsVSGuard_40,
  VSDoorsVSGuard_41,
  VSDoorsVSGuard_42,
  VSDoorsVSGuard_43,
  VSDoorsVSGuard_44,
  VSDoorsVSGuard_45,
  VSDoorsVSGuard_46,
  VSDoorsVSGuard_47,
  VSDoorsVSGuard_48,
  VSDoorsVSGuard_49,
  VSDoorsVSGuard_50,
  VSDoorsVSGuard_51,
  VSDoorsVSGuard_52,
  VSDoorsVSGuard_53,
  VSDoorsVSGuard_54,
  VSDoorsVSGuard_55,
  VSDoorsVSGuard_56,
  VSDoorsVSGuard_57,
  VSDoorsVSGuard_58,
  VSDoorsVSGuard_59,
  VSDoorsVSGuard_60,
  VSDoorsVSGuard_61,
  VSDoorsVSGuard_62,
  VSDoorsVSGuard_63,
  VSDoorsVSGuard_64,
  VSDoorsVSGuard_65,
  VSDoorsVSGuard_66,
  VSDoorsVSGuard_67,
  VSDoorsVSGuard_68,
  VSDoorsVSGuard_69,
  VSDoorsVSGuard_70,
  VSDoorsVSGuard_71,
  VSDoorsVSGuard_72,
  VSDoorsVSGuard_73,
  VSDoorsVSGuard_74,
  VSDoorsVSGuard_75,
  VSDoorsVSGuard_76,
  VSDoorsVSGuard_77,
  VSDoorsVSGuard_78,
  VSDoorsVSGuard_79,
  VSDoorsVSGuard_80,
  VSDoorsVSGuard_81,
  VSDoorsVSGuard_82,
  VSDoorsVSGuard_83,
  VSDoorsVSGuard_84,
  VSDoorsVSGuard_85,
  VSDoorsVSGuard_86,
  VSDoorsVSGuard_87,
  VSDoorsVSGuard_88,
  VSDoorsVSGuard_89,
  VSDoorsVSGuard_90,
  VSDoorsVSGuard_91,
  VSDoorsVSGuard_92,
  VSDoorsVSGuard_93,
  VSDoorsVSGuard_94,
  VSDoorsVSGuard_95,
  VSDoorsVSGuard_96,
  VSDoorsVSGuard_97,
  VSDoorsVSGuard_98,
  VSDoorsVSGuard_99,
  VSDoorsVSGuard_100,
  VSDoorsVSGuard_101,
  VSDoorsVSGuard_102,
  VSDoorsVSGuard_103,
  VSDoorsVSGuard_104,
  VSDoorsVSGuard_105,
  VSDoorsVSGuard_106,
  VSDoorsVSGuard_107,
  VSDoorsVSGuard_108,
  VSDoorsVSGuard_109,
  VSDoorsVSGuard_110,
  VSDoorsVSGuard_111,
  VSDoorsVSGuard_112,
  VSDoorsVSGuard_113,
  VSDoorsVSGuard_114,
  VSDoorsVSGuard_115,
  VSDoorsVSGuard_116,
  VSDoorsVSGuard_117,
  VSDoorsVSGuard_118,
  VSDoorsVSGuard_119,
  VSDoorsVSGuard_120,
  VSDoorsVSGuard_121,
  VSDoorsVSGuard_122,
  VSDoorsVSGuard_123,
  VSDoorsVSGuard_124,
  VSDoorsVSGuard_125,
  VSDoorsVSGuard_126,
  VSDoorsVSGuard_127,
  VSDoorsVSGuard_128,
  VSDoorsVSGuard_129,
  VSDoorsVSGuard_130,
  VSDoorsVSGuard_131,
  VSDoorsVSGuard_132,
  VSDoorsVSGuard_133,
  VSDoorsVSGuard_134,
  VSDoorsVSGuard_135,
  VSDoorsVSGuard_136,
  VSDoorsVSGuard_137,
  VSDoorsVSGuard_138,
  VSDoorsVSGuard_139,
  VSDoorsVSGuard_140,
  VSDoorsVSGuard_141,
  VSDoorsVSGuard_142,
  VSDoorsVSGuard_143,
  VSDoorsVSGuard_144,
  VSDoorsVSGuard_145,
  VSDoorsVSGuard_146,
  VSDoorsVSGuard_147,
  VSDoorsVSGuard_148,
  VSDoorsVSGuard_149,
  VSDoorsVSGuard_150,
  VSDoorsVSGuard_151,
  VSDoorsVSGuard_152,
  VSDoorsVSGuard_153,
  VSDoorsVSGuard_154,
  VSDoorsVSGuard_155,
  VSDoorsVSGuard_156,
  VSDoorsVSGuard_157,
  VSDoorsVSGuard_158,
  VSDoorsVSGuard_159,
  VSDoorsVSGuard_160,
  VSDoorsVSGuard_161,
  VSDoorsVSGuard_162,
  VSDoorsVSGuard_163,
  VSDoorsVSGuard_164,
  VSDoorsVSGuard_165,
  VSDoorsVSGuard_166,
  VSDoorsVSGuard_167,
  VSDoorsVSGuard_168,
  VSDoorsVSGuard_169,
  VSDoorsVSGuard_170,
  VSDoorsVSGuard_171,
  VSDoorsVSGuard_172,
  VSDoorsVSGuard_173,
  VSDoorsVSGuard_174,
  VSDoorsVSGuard_175,
  VSDoorsVSGuard_176,
  VSDoorsVSGuard_177,
  VSDoorsVSGuard_178,
  VSDoorsVSGuard_179,
  VSDoorsVSGuard_180,
  VSDoorsVSGuard_181,
  VSDoorsVSGuard_182
};


/*
 * Action Expression Functions.
 */
VS_VOID VSDoorsVSAction_1 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  CheckEsc();
}
VS_VOID VSDoorsVSAction_2 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  CheckExtProt();
}
VS_VOID VSDoorsVSAction_3 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  CheckManual();
}
VS_VOID VSDoorsVSAction_4 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  CheckRun();
}
VS_VOID VSDoorsVSAction_5 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  CheckSP();
}
VS_VOID VSDoorsVSAction_6 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  CheckSS();
}
VS_VOID VSDoorsVSAction_7 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  ClearCount();
}
VS_VOID VSDoorsVSAction_8 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  ClearE2();
}
VS_VOID VSDoorsVSAction_9 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  ClearLastClose();
}
VS_VOID VSDoorsVSAction_10 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  ClearLastOpen();
}
VS_VOID VSDoorsVSAction_11 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  CloseDisable();
}
VS_VOID VSDoorsVSAction_12 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  CloseEnable();
}
VS_VOID VSDoorsVSAction_13 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  ClosedNotify();
}
VS_VOID VSDoorsVSAction_14 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  CounterUp();
}
VS_VOID VSDoorsVSAction_15 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  DecSel();
}
VS_VOID VSDoorsVSAction_16 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  DefaultValues();
}
VS_VOID VSDoorsVSAction_17 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  DeltaBrake();
}
VS_VOID VSDoorsVSAction_18 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  DeltaFail();
}
VS_VOID VSDoorsVSAction_19 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  DeltaInit();
}
VS_VOID VSDoorsVSAction_20 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  DeltaRun();
}
VS_VOID VSDoorsVSAction_21 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  DeltaStart();
}
VS_VOID VSDoorsVSAction_22 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  ErrorNotify();
}
VS_VOID VSDoorsVSAction_23 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  ErrorOff();
}
VS_VOID VSDoorsVSAction_24 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  ExitProg();
}
VS_VOID VSDoorsVSAction_25 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  FailMotNotify();
}
VS_VOID VSDoorsVSAction_26 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  FailMotOff();
}
VS_VOID VSDoorsVSAction_27 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  FmOff();
}
VS_VOID VSDoorsVSAction_28 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  FmOn();
}
VS_VOID VSDoorsVSAction_29 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  HookupMot();
}
VS_VOID VSDoorsVSAction_30 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  IncSel();
}
VS_VOID VSDoorsVSAction_31 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  InitProg();
}
VS_VOID VSDoorsVSAction_32 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  InitPwm();
}
VS_VOID VSDoorsVSAction_33 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  ManualOff();
}
VS_VOID VSDoorsVSAction_34 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  ManualOn();
}
VS_VOID VSDoorsVSAction_35 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MaskRead();
}
VS_VOID VSDoorsVSAction_36 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MaskWrite();
}
VS_VOID VSDoorsVSAction_37 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MotCloseBrake();
}
VS_VOID VSDoorsVSAction_38 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MotCloseFind();
}
VS_VOID VSDoorsVSAction_39 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MotCloseFull00();
}
VS_VOID VSDoorsVSAction_40 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MotCloseFull10();
}
VS_VOID VSDoorsVSAction_41 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MotCloseFull100();
}
VS_VOID VSDoorsVSAction_42 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MotCloseFull20();
}
VS_VOID VSDoorsVSAction_43 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MotCloseFull30();
}
VS_VOID VSDoorsVSAction_44 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MotCloseFull40();
}
VS_VOID VSDoorsVSAction_45 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MotCloseFull50();
}
VS_VOID VSDoorsVSAction_46 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MotCloseFull60();
}
VS_VOID VSDoorsVSAction_47 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MotCloseFull70();
}
VS_VOID VSDoorsVSAction_48 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MotCloseFull80();
}
VS_VOID VSDoorsVSAction_49 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MotCloseFull90();
}
VS_VOID VSDoorsVSAction_50 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MotCloseGoOn();
}
VS_VOID VSDoorsVSAction_51 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MotCloseInit();
}
VS_VOID VSDoorsVSAction_52 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MotCloseNear();
}
VS_VOID VSDoorsVSAction_53 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MotClosePos();
}
VS_VOID VSDoorsVSAction_54 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MotCloseStart();
}
VS_VOID VSDoorsVSAction_55 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MotOpenBrake();
}
VS_VOID VSDoorsVSAction_56 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MotOpenFind();
}
VS_VOID VSDoorsVSAction_57 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MotOpenGoOn();
}
VS_VOID VSDoorsVSAction_58 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MotOpenInit();
}
VS_VOID VSDoorsVSAction_59 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MotOpenNear();
}
VS_VOID VSDoorsVSAction_60 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MotOpenPos();
}
VS_VOID VSDoorsVSAction_61 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MotOpenPosShk();
}
VS_VOID VSDoorsVSAction_62 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MotOpenStart();
}
VS_VOID VSDoorsVSAction_63 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MotOpenStartShk();
}
VS_VOID VSDoorsVSAction_64 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MotStop();
}
VS_VOID VSDoorsVSAction_65 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MovNotify();
}
VS_VOID VSDoorsVSAction_66 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  OldMaskLoad();
}
VS_VOID VSDoorsVSAction_67 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  OldParLoad();
}
VS_VOID VSDoorsVSAction_68 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  OpenDisable();
}
VS_VOID VSDoorsVSAction_69 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  OpenEnable();
}
VS_VOID VSDoorsVSAction_70 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  OpenedNotify();
}
VS_VOID VSDoorsVSAction_71 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  OverCurDisable();
}
VS_VOID VSDoorsVSAction_72 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  OverCurEnable();
}
VS_VOID VSDoorsVSAction_73 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  ParRead();
}
VS_VOID VSDoorsVSAction_74 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  ParWrite();
}
VS_VOID VSDoorsVSAction_75 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  PosCloseSet();
}
VS_VOID VSDoorsVSAction_76 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  PosInit();
}
VS_VOID VSDoorsVSAction_77 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  PosNotifyDisable();
}
VS_VOID VSDoorsVSAction_78 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  PosNotifyEnable();
}
VS_VOID VSDoorsVSAction_79 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  PosOpenSet();
}
VS_VOID VSDoorsVSAction_80 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  PosReset();
}
VS_VOID VSDoorsVSAction_81 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  PrgOff();
}
VS_VOID VSDoorsVSAction_82 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  PrgOn();
}
VS_VOID VSDoorsVSAction_83 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  ResLock();
}
VS_VOID VSDoorsVSAction_84 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  ResPOffsetAC();
}
VS_VOID VSDoorsVSAction_85 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  ResPOffsetAO();
}
VS_VOID VSDoorsVSAction_86 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Reset();
}
VS_VOID VSDoorsVSAction_87 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  STLock();
}
VS_VOID VSDoorsVSAction_88 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  STTick();
}
VS_VOID VSDoorsVSAction_89 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  SetLock();
}
VS_VOID VSDoorsVSAction_90 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  SetPOffsetAC();
}
VS_VOID VSDoorsVSAction_91 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  SetPOffsetAO();
}
VS_VOID VSDoorsVSAction_92 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  StopAcqFull();
}
VS_VOID VSDoorsVSAction_93 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  UnViolateNotify();
}
VS_VOID VSDoorsVSAction_94 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  UnderCurDisable();
}
VS_VOID VSDoorsVSAction_95 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  UnderCurEnable();
}
VS_VOID VSDoorsVSAction_96 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  UnhookMot();
}
VS_VOID VSDoorsVSAction_97 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  UpMask();
}
VS_VOID VSDoorsVSAction_98 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  UpdateMaskClose();
}
VS_VOID VSDoorsVSAction_99 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  UpdateMaskOpen();
}
VS_VOID VSDoorsVSAction_100 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  UpdatePosClose();
}
VS_VOID VSDoorsVSAction_101 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  UpdatePosOpen();
}
VS_VOID VSDoorsVSAction_102 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  ViolateNotify();
}
VS_VOID VSDoorsVSAction_103 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  bParReread = 0;
}
VS_VOID VSDoorsVSAction_104 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  PosClose = 0;
}
VS_VOID VSDoorsVSAction_105 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  PosOpen = 0;
}
VS_VOID VSDoorsVSAction_106 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  SSCfg = 1;
}
VS_VOID VSDoorsVSAction_107 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  SPCfg = 1;
}
VS_VOID VSDoorsVSAction_108 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  CurThrOverClose = 0;
}
VS_VOID VSDoorsVSAction_109 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  CurThrOverOpen = 0;
}
VS_VOID VSDoorsVSAction_110 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  bEnExtProt = 0;
}
VS_VOID VSDoorsVSAction_111 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  CurThrOverOpen = CurThrOverOpen + CurStep;
}
VS_VOID VSDoorsVSAction_112 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  bEnExtProt = 1;
}
VS_VOID VSDoorsVSAction_113 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  CurThrOverClose = CurThrOverClose + CurStep;
}
VS_VOID VSDoorsVSAction_114 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  bPassiveMov = 1;
}
VS_VOID VSDoorsVSAction_115 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MovCount = 50;
}
VS_VOID VSDoorsVSAction_116 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  MovCount = MovCount + 100;
}
VS_VOID VSDoorsVSAction_117 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  bPassiveMov = 0;
}
VS_VOID VSDoorsVSAction_118 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Pos = 512;
}
VS_VOID VSDoorsVSAction_119 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  bParReread = 1;
}
VS_VOID VSDoorsVSAction_120 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(17);
}
VS_VOID VSDoorsVSAction_121 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(40 /* etWait */, 600);
}
VS_VOID VSDoorsVSAction_122 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(34 /* etExit */, 60000);
}
VS_VOID VSDoorsVSAction_123 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  DisplayPar(ParSel);
}
VS_VOID VSDoorsVSAction_124 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(18);
}
VS_VOID VSDoorsVSAction_125 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(13);
}
VS_VOID VSDoorsVSAction_126 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(23 /* etDelay */, 60000);
}
VS_VOID VSDoorsVSAction_127 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(14);
}
VS_VOID VSDoorsVSAction_128 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(23 /* etDelay */, 600);
}
VS_VOID VSDoorsVSAction_129 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(19);
}
VS_VOID VSDoorsVSAction_130 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(23 /* etDelay */, 20);
}
VS_VOID VSDoorsVSAction_131 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(7);
}
VS_VOID VSDoorsVSAction_132 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(1);
}
VS_VOID VSDoorsVSAction_133 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(20);
}
VS_VOID VSDoorsVSAction_134 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(23 /* etDelay */, 1000);
}
VS_VOID VSDoorsVSAction_135 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(21);
}
VS_VOID VSDoorsVSAction_136 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(23 /* etDelay */, 200);
}
VS_VOID VSDoorsVSAction_137 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(22);
}
VS_VOID VSDoorsVSAction_138 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(35 /* etExit2 */, 15000);
}
VS_VOID VSDoorsVSAction_139 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(4);
}
VS_VOID VSDoorsVSAction_140 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  TLock(38 /* etSetLock */, STCfg);
}
VS_VOID VSDoorsVSAction_141 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(12);
}
VS_VOID VSDoorsVSAction_142 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(34 /* etExit */, 500);
}
VS_VOID VSDoorsVSAction_143 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(3);
}
VS_VOID VSDoorsVSAction_144 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(9);
}
VS_VOID VSDoorsVSAction_145 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(34 /* etExit */, 30000);
}
VS_VOID VSDoorsVSAction_146 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(39 /* etStart */, 600);
}
VS_VOID VSDoorsVSAction_147 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(23);
}
VS_VOID VSDoorsVSAction_148 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(16);
}
VS_VOID VSDoorsVSAction_149 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  IncPar(ParSel);
}
VS_VOID VSDoorsVSAction_150 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  DecPar(ParSel);
}
VS_VOID VSDoorsVSAction_151 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(11);
}
VS_VOID VSDoorsVSAction_152 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(40 /* etWait */, 600);
}
VS_VOID VSDoorsVSAction_153 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(28);
}
VS_VOID VSDoorsVSAction_154 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(27);
}
VS_VOID VSDoorsVSAction_155 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(39 /* etStart */, 200);
}
VS_VOID VSDoorsVSAction_156 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(40 /* etWait */, 800);
}
VS_VOID VSDoorsVSAction_157 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(25);
}
VS_VOID VSDoorsVSAction_158 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(23 /* etDelay */, 800);
}
VS_VOID VSDoorsVSAction_159 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(2);
}
VS_VOID VSDoorsVSAction_160 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(39 /* etStart */, 600);
}
VS_VOID VSDoorsVSAction_161 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(8);
}
VS_VOID VSDoorsVSAction_162 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(39 /* etStart */, 600);
}
VS_VOID VSDoorsVSAction_163 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(26);
}
VS_VOID VSDoorsVSAction_164 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(5);
}
VS_VOID VSDoorsVSAction_165 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(23 /* etDelay */, 750);
}
VS_VOID VSDoorsVSAction_166 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(24);
}
VS_VOID VSDoorsVSAction_167 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(6);
}
VS_VOID VSDoorsVSAction_168 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(39 /* etStart */, 1000);
}
VS_VOID VSDoorsVSAction_169 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(29);
}
VS_VOID VSDoorsVSAction_170 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(40 /* etWait */, 5);
}
VS_VOID VSDoorsVSAction_171 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(10);
}
VS_VOID VSDoorsVSAction_172 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(36 /* etFail */, 18000);
}
VS_VOID VSDoorsVSAction_173 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Display(15);
}
VS_VOID VSDoorsVSAction_174 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(23 /* etDelay */, 400);
}
VS_VOID VSDoorsVSAction_175 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(23 /* etDelay */, 100);
}
VS_VOID VSDoorsVSAction_176 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(39 /* etStart */, 1300);
}
VS_VOID VSDoorsVSAction_177 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  TTick(16 /* eTick */, MovCount);
}
VS_VOID VSDoorsVSAction_178 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  TTick(16 /* eTick */, 100);
}
VS_VOID VSDoorsVSAction_179 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(34 /* etExit */, 600);
}
VS_VOID VSDoorsVSAction_180 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(34 /* etExit */, 1000);
}
VS_VOID VSDoorsVSAction_181 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(34 /* etExit */, 1000);
}
VS_VOID VSDoorsVSAction_182 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(40 /* etWait */, 4000);
}
VS_VOID VSDoorsVSAction_183 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(40 /* etWait */, 8000);
}
VS_VOID VSDoorsVSAction_184 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(34 /* etExit */, 4000);
}
VS_VOID VSDoorsVSAction_185 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(36 /* etFail */, 8000);
}
VS_VOID VSDoorsVSAction_186 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(36 /* etFail */, 20);
}
VS_VOID VSDoorsVSAction_187 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(24 /* etDelay10 */, 2000);
}
VS_VOID VSDoorsVSAction_188 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(26 /* etDelay20 */, 800);
}
VS_VOID VSDoorsVSAction_189 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(27 /* etDelay30 */, 800);
}
VS_VOID VSDoorsVSAction_190 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(28 /* etDelay40 */, 800);
}
VS_VOID VSDoorsVSAction_191 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(29 /* etDelay50 */, 800);
}
VS_VOID VSDoorsVSAction_192 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(30 /* etDelay60 */, 800);
}
VS_VOID VSDoorsVSAction_193 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(31 /* etDelay70 */, 800);
}
VS_VOID VSDoorsVSAction_194 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(32 /* etDelay80 */, 800);
}
VS_VOID VSDoorsVSAction_195 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(33 /* etDelay90 */, 800);
}
VS_VOID VSDoorsVSAction_196 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(25 /* etDelay100 */, 800);
}
VS_VOID VSDoorsVSAction_197 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(34 /* etExit */, 1500);
}
VS_VOID VSDoorsVSAction_198 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(36 /* etFail */, 5000);
}
VS_VOID VSDoorsVSAction_199 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  TViolate(23 /* etDelay */, 600);
}
VS_VOID VSDoorsVSAction_200 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(34 /* etExit */, 10000);
}
VS_VOID VSDoorsVSAction_201 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  TLock(16 /* eTick */, STCfg);
}
VS_VOID VSDoorsVSAction_202 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  TLock(16 /* eTick */, 8);
}
VS_VOID VSDoorsVSAction_203 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(39 /* etStart */, 5000);
}
VS_VOID VSDoorsVSAction_204 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(23 /* etDelay */, 600);
}
VS_VOID VSDoorsVSAction_205 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  Timer(23 /* etDelay */, 5000);
}
VS_VOID VSDoorsVSAction_206 (SEM_CONTEXT VS_TQ_CONTEXT * Context)
{
  TTick(16 /* eTick */, 5000);
}


/*
 * Action Expression Pointer Table.
 */
VS_ACTIONEXPR_TYPE const VSDoorsVSAction[207] = 
{
  (VS_ACTIONEXPR_TYPE) NULL /* declared action function is never used */,
  VSDoorsVSAction_1,
  VSDoorsVSAction_2,
  VSDoorsVSAction_3,
  VSDoorsVSAction_4,
  VSDoorsVSAction_5,
  VSDoorsVSAction_6,
  VSDoorsVSAction_7,
  VSDoorsVSAction_8,
  VSDoorsVSAction_9,
  VSDoorsVSAction_10,
  VSDoorsVSAction_11,
  VSDoorsVSAction_12,
  VSDoorsVSAction_13,
  VSDoorsVSAction_14,
  VSDoorsVSAction_15,
  VSDoorsVSAction_16,
  VSDoorsVSAction_17,
  VSDoorsVSAction_18,
  VSDoorsVSAction_19,
  VSDoorsVSAction_20,
  VSDoorsVSAction_21,
  VSDoorsVSAction_22,
  VSDoorsVSAction_23,
  VSDoorsVSAction_24,
  VSDoorsVSAction_25,
  VSDoorsVSAction_26,
  VSDoorsVSAction_27,
  VSDoorsVSAction_28,
  VSDoorsVSAction_29,
  VSDoorsVSAction_30,
  VSDoorsVSAction_31,
  VSDoorsVSAction_32,
  VSDoorsVSAction_33,
  VSDoorsVSAction_34,
  VSDoorsVSAction_35,
  VSDoorsVSAction_36,
  VSDoorsVSAction_37,
  VSDoorsVSAction_38,
  VSDoorsVSAction_39,
  VSDoorsVSAction_40,
  VSDoorsVSAction_41,
  VSDoorsVSAction_42,
  VSDoorsVSAction_43,
  VSDoorsVSAction_44,
  VSDoorsVSAction_45,
  VSDoorsVSAction_46,
  VSDoorsVSAction_47,
  VSDoorsVSAction_48,
  VSDoorsVSAction_49,
  VSDoorsVSAction_50,
  VSDoorsVSAction_51,
  VSDoorsVSAction_52,
  VSDoorsVSAction_53,
  VSDoorsVSAction_54,
  VSDoorsVSAction_55,
  VSDoorsVSAction_56,
  VSDoorsVSAction_57,
  VSDoorsVSAction_58,
  VSDoorsVSAction_59,
  VSDoorsVSAction_60,
  VSDoorsVSAction_61,
  VSDoorsVSAction_62,
  VSDoorsVSAction_63,
  VSDoorsVSAction_64,
  VSDoorsVSAction_65,
  VSDoorsVSAction_66,
  VSDoorsVSAction_67,
  VSDoorsVSAction_68,
  VSDoorsVSAction_69,
  VSDoorsVSAction_70,
  VSDoorsVSAction_71,
  VSDoorsVSAction_72,
  VSDoorsVSAction_73,
  VSDoorsVSAction_74,
  VSDoorsVSAction_75,
  VSDoorsVSAction_76,
  VSDoorsVSAction_77,
  VSDoorsVSAction_78,
  VSDoorsVSAction_79,
  VSDoorsVSAction_80,
  VSDoorsVSAction_81,
  VSDoorsVSAction_82,
  VSDoorsVSAction_83,
  VSDoorsVSAction_84,
  VSDoorsVSAction_85,
  VSDoorsVSAction_86,
  VSDoorsVSAction_87,
  VSDoorsVSAction_88,
  VSDoorsVSAction_89,
  VSDoorsVSAction_90,
  VSDoorsVSAction_91,
  VSDoorsVSAction_92,
  VSDoorsVSAction_93,
  VSDoorsVSAction_94,
  VSDoorsVSAction_95,
  VSDoorsVSAction_96,
  VSDoorsVSAction_97,
  VSDoorsVSAction_98,
  VSDoorsVSAction_99,
  VSDoorsVSAction_100,
  VSDoorsVSAction_101,
  VSDoorsVSAction_102,
  VSDoorsVSAction_103,
  VSDoorsVSAction_104,
  VSDoorsVSAction_105,
  VSDoorsVSAction_106,
  VSDoorsVSAction_107,
  VSDoorsVSAction_108,
  VSDoorsVSAction_109,
  VSDoorsVSAction_110,
  VSDoorsVSAction_111,
  VSDoorsVSAction_112,
  VSDoorsVSAction_113,
  VSDoorsVSAction_114,
  VSDoorsVSAction_115,
  VSDoorsVSAction_116,
  VSDoorsVSAction_117,
  VSDoorsVSAction_118,
  VSDoorsVSAction_119,
  VSDoorsVSAction_120,
  VSDoorsVSAction_121,
  VSDoorsVSAction_122,
  VSDoorsVSAction_123,
  VSDoorsVSAction_124,
  VSDoorsVSAction_125,
  VSDoorsVSAction_126,
  VSDoorsVSAction_127,
  VSDoorsVSAction_128,
  VSDoorsVSAction_129,
  VSDoorsVSAction_130,
  VSDoorsVSAction_131,
  VSDoorsVSAction_132,
  VSDoorsVSAction_133,
  VSDoorsVSAction_134,
  VSDoorsVSAction_135,
  VSDoorsVSAction_136,
  VSDoorsVSAction_137,
  VSDoorsVSAction_138,
  VSDoorsVSAction_139,
  VSDoorsVSAction_140,
  VSDoorsVSAction_141,
  VSDoorsVSAction_142,
  VSDoorsVSAction_143,
  VSDoorsVSAction_144,
  VSDoorsVSAction_145,
  VSDoorsVSAction_146,
  VSDoorsVSAction_147,
  VSDoorsVSAction_148,
  VSDoorsVSAction_149,
  VSDoorsVSAction_150,
  VSDoorsVSAction_151,
  VSDoorsVSAction_152,
  VSDoorsVSAction_153,
  VSDoorsVSAction_154,
  VSDoorsVSAction_155,
  VSDoorsVSAction_156,
  VSDoorsVSAction_157,
  VSDoorsVSAction_158,
  VSDoorsVSAction_159,
  VSDoorsVSAction_160,
  VSDoorsVSAction_161,
  VSDoorsVSAction_162,
  VSDoorsVSAction_163,
  VSDoorsVSAction_164,
  VSDoorsVSAction_165,
  VSDoorsVSAction_166,
  VSDoorsVSAction_167,
  VSDoorsVSAction_168,
  VSDoorsVSAction_169,
  VSDoorsVSAction_170,
  VSDoorsVSAction_171,
  VSDoorsVSAction_172,
  VSDoorsVSAction_173,
  VSDoorsVSAction_174,
  VSDoorsVSAction_175,
  VSDoorsVSAction_176,
  VSDoorsVSAction_177,
  VSDoorsVSAction_178,
  VSDoorsVSAction_179,
  VSDoorsVSAction_180,
  VSDoorsVSAction_181,
  VSDoorsVSAction_182,
  VSDoorsVSAction_183,
  VSDoorsVSAction_184,
  VSDoorsVSAction_185,
  VSDoorsVSAction_186,
  VSDoorsVSAction_187,
  VSDoorsVSAction_188,
  VSDoorsVSAction_189,
  VSDoorsVSAction_190,
  VSDoorsVSAction_191,
  VSDoorsVSAction_192,
  VSDoorsVSAction_193,
  VSDoorsVSAction_194,
  VSDoorsVSAction_195,
  VSDoorsVSAction_196,
  VSDoorsVSAction_197,
  VSDoorsVSAction_198,
  VSDoorsVSAction_199,
  VSDoorsVSAction_200,
  VSDoorsVSAction_201,
  VSDoorsVSAction_202,
  VSDoorsVSAction_203,
  VSDoorsVSAction_204,
  VSDoorsVSAction_205,
  VSDoorsVSAction_206
};


/*
 * Wrapper function for all initializing functions.
 */
unsigned char VSDoorsSMP_InitAll (SEM_CONTEXT VS_TQ_CONTEXT * * Context)
{
  unsigned char CC;

  if ((CC = SMP_Connect(Context, &VSDoors)) != SES_OKAY)
    return CC;

  SMP_Init(*Context);

  SMP_InitSignalQueue(*Context);

  if ((CC = SMP_InitInstances(*Context)) != SES_OKAY)
    return CC;

  SMP_InitGuardCallBack(*Context, VSDoorsVSGuard);

  return SES_OKAY;
}
