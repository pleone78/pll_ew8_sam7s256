/*
 * Version:   50
 * Signature: eb1b 13af 7a0e 2056 ded0 ade0
 *
 * Id:        PLL_PConstant.h
 *
 * Function:  VS Project Constant File.
 *
 * Generated: Thu May 30 15:27:04 2019
 *
 * Coder 5, 4, 0, 1273
 * 
 * This is an automatically generated file. It will be overwritten by the Coder. 
 * 
 * DO NOT EDIT THE FILE! 
 */


#ifndef __PLL_PCONSTANT_H
#define __PLL_PCONSTANT_H


/*
 * There are no VS Project constants, so this section is intentionally empty!
 */


#endif
