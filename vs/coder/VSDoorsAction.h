/*
 * Version:   50
 * Signature: eb1b 13af 7a0e 2056 ded0 ade0
 *
 * Id:        VSDoorsAction.h
 *
 * Function:  VS System Action Expression Pointer Table Header File.
 *
 * Generated: Thu May 30 15:27:04 2019
 *
 * Coder 5, 4, 0, 1273
 * 
 * This is an automatically generated file. It will be overwritten by the Coder. 
 * 
 * DO NOT EDIT THE FILE! 
 */


#ifndef __VSDOORSACTION_H
#define __VSDOORSACTION_H


/*
 * Include SEM Defines Header File.
 */
#include "SEMEDef.h"


#if (VS_CODER_GUID != 0X015e8c916)
#error The generated file does not match the SEMTypes.h header file.
#endif


/*
 * Action Function Prototypes.
 */
extern VS_VOID CheckCfgDoor (VS_VOID);
extern VS_VOID CheckEsc (VS_VOID);
extern VS_VOID CheckExtProt (VS_VOID);
extern VS_VOID CheckManual (VS_VOID);
extern VS_VOID CheckRun (VS_VOID);
extern VS_VOID CheckSP (VS_VOID);
extern VS_VOID CheckSS (VS_VOID);
extern VS_VOID ClearCount (VS_VOID);
extern VS_VOID ClearE2 (VS_VOID);
extern VS_VOID ClearLastClose (VS_VOID);
extern VS_VOID ClearLastOpen (VS_VOID);
extern VS_VOID CloseDisable (VS_VOID);
extern VS_VOID CloseEnable (VS_VOID);
extern VS_VOID ClosedNotify (VS_VOID);
extern VS_VOID CounterUp (VS_VOID);
extern VS_VOID DecPar (VS_UINT Par);
extern VS_VOID DecSel (VS_VOID);
extern VS_VOID DefaultValues (VS_VOID);
extern VS_VOID DeltaBrake (VS_VOID);
extern VS_VOID DeltaFail (VS_VOID);
extern VS_VOID DeltaInit (VS_VOID);
extern VS_VOID DeltaRun (VS_VOID);
extern VS_VOID DeltaStart (VS_VOID);
extern VS_VOID Display (VS_UINT16 MsgId);
extern VS_VOID DisplayPar (VS_UINT Par);
extern VS_VOID ErrorNotify (VS_VOID);
extern VS_VOID ErrorOff (VS_VOID);
extern VS_VOID ExitProg (VS_VOID);
extern VS_VOID FailMotNotify (VS_VOID);
extern VS_VOID FailMotOff (VS_VOID);
extern VS_VOID FmOff (VS_VOID);
extern VS_VOID FmOn (VS_VOID);
extern VS_VOID HookupMot (VS_VOID);
extern VS_VOID IncPar (VS_UINT Par);
extern VS_VOID IncSel (VS_VOID);
extern VS_VOID InitProg (VS_VOID);
extern VS_VOID InitPwm (VS_VOID);
extern VS_VOID ManualOff (VS_VOID);
extern VS_VOID ManualOn (VS_VOID);
extern VS_VOID MaskRead (VS_VOID);
extern VS_VOID MaskWrite (VS_VOID);
extern VS_VOID MotCloseBrake (VS_VOID);
extern VS_VOID MotCloseFind (VS_VOID);
extern VS_VOID MotCloseFull00 (VS_VOID);
extern VS_VOID MotCloseFull10 (VS_VOID);
extern VS_VOID MotCloseFull100 (VS_VOID);
extern VS_VOID MotCloseFull20 (VS_VOID);
extern VS_VOID MotCloseFull30 (VS_VOID);
extern VS_VOID MotCloseFull40 (VS_VOID);
extern VS_VOID MotCloseFull50 (VS_VOID);
extern VS_VOID MotCloseFull60 (VS_VOID);
extern VS_VOID MotCloseFull70 (VS_VOID);
extern VS_VOID MotCloseFull80 (VS_VOID);
extern VS_VOID MotCloseFull90 (VS_VOID);
extern VS_VOID MotCloseGoOn (VS_VOID);
extern VS_VOID MotCloseInit (VS_VOID);
extern VS_VOID MotCloseNear (VS_VOID);
extern VS_VOID MotClosePos (VS_VOID);
extern VS_VOID MotCloseStart (VS_VOID);
extern VS_VOID MotOpenBrake (VS_VOID);
extern VS_VOID MotOpenFind (VS_VOID);
extern VS_VOID MotOpenGoOn (VS_VOID);
extern VS_VOID MotOpenInit (VS_VOID);
extern VS_VOID MotOpenNear (VS_VOID);
extern VS_VOID MotOpenPos (VS_VOID);
extern VS_VOID MotOpenPosShk (VS_VOID);
extern VS_VOID MotOpenStart (VS_VOID);
extern VS_VOID MotOpenStartShk (VS_VOID);
extern VS_VOID MotStop (VS_VOID);
extern VS_VOID MovNotify (VS_VOID);
extern VS_VOID Notify (VS_UCHAR Ch);
extern VS_VOID OldMaskLoad (VS_VOID);
extern VS_VOID OldParLoad (VS_VOID);
extern VS_VOID OpenDisable (VS_VOID);
extern VS_VOID OpenEnable (VS_VOID);
extern VS_VOID OpenedNotify (VS_VOID);
extern VS_VOID OverCurDisable (VS_VOID);
extern VS_VOID OverCurEnable (VS_VOID);
extern VS_VOID ParRead (VS_VOID);
extern VS_VOID ParWrite (VS_VOID);
extern VS_VOID PosCloseSet (VS_VOID);
extern VS_VOID PosInit (VS_VOID);
extern VS_VOID PosNotifyDisable (VS_VOID);
extern VS_VOID PosNotifyEnable (VS_VOID);
extern VS_VOID PosOpenSet (VS_VOID);
extern VS_VOID PosReset (VS_VOID);
extern VS_VOID PrgOff (VS_VOID);
extern VS_VOID PrgOn (VS_VOID);
extern VS_VOID ResLock (VS_VOID);
extern VS_VOID ResPOffsetAC (VS_VOID);
extern VS_VOID ResPOffsetAO (VS_VOID);
extern VS_VOID Reset (VS_VOID);
extern VS_VOID STLock (VS_VOID);
extern VS_VOID STTick (VS_VOID);
extern VS_VOID SetLock (VS_VOID);
extern VS_VOID SetPOffsetAC (VS_VOID);
extern VS_VOID SetPOffsetAO (VS_VOID);
extern VS_VOID StopAcqFull (VS_VOID);
extern VS_VOID UnViolateNotify (VS_VOID);
extern VS_VOID UnderCurDisable (VS_VOID);
extern VS_VOID UnderCurEnable (VS_VOID);
extern VS_VOID UnhookMot (VS_VOID);
extern VS_VOID UpMask (VS_VOID);
extern VS_VOID UpdateMaskClose (VS_VOID);
extern VS_VOID UpdateMaskOpen (VS_VOID);
extern VS_VOID UpdatePosClose (VS_VOID);
extern VS_VOID UpdatePosOpen (VS_VOID);
extern VS_VOID ViolateNotify (VS_VOID);
extern VS_VOID TLock (VS_UINT event, VS_UINT ticks);
extern VS_VOID TTick (VS_UINT event, VS_UINT ticks);
extern VS_VOID TViolate (VS_UINT event, VS_UINT ticks);
extern VS_VOID Timer (VS_UINT event, VS_UINT ticks);


/*
 * Include SEM Library Header File.
 */
#include "SEMLibE.h"


/*
 * Action Expression Function Prototypes.
 */
extern VS_VOID VSDoorsVSAction_1 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_2 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_3 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_4 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_5 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_6 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_7 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_8 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_9 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_10 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_11 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_12 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_13 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_14 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_15 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_16 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_17 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_18 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_19 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_20 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_21 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_22 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_23 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_24 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_25 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_26 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_27 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_28 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_29 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_30 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_31 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_32 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_33 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_34 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_35 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_36 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_37 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_38 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_39 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_40 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_41 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_42 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_43 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_44 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_45 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_46 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_47 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_48 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_49 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_50 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_51 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_52 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_53 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_54 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_55 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_56 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_57 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_58 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_59 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_60 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_61 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_62 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_63 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_64 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_65 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_66 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_67 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_68 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_69 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_70 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_71 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_72 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_73 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_74 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_75 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_76 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_77 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_78 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_79 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_80 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_81 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_82 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_83 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_84 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_85 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_86 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_87 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_88 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_89 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_90 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_91 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_92 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_93 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_94 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_95 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_96 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_97 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_98 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_99 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_100 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_101 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_102 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_103 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_104 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_105 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_106 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_107 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_108 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_109 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_110 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_111 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_112 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_113 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_114 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_115 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_116 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_117 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_118 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_119 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_120 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_121 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_122 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_123 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_124 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_125 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_126 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_127 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_128 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_129 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_130 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_131 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_132 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_133 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_134 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_135 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_136 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_137 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_138 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_139 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_140 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_141 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_142 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_143 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_144 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_145 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_146 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_147 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_148 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_149 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_150 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_151 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_152 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_153 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_154 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_155 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_156 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_157 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_158 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_159 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_160 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_161 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_162 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_163 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_164 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_165 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_166 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_167 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_168 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_169 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_170 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_171 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_172 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_173 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_174 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_175 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_176 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_177 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_178 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_179 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_180 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_181 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_182 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_183 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_184 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_185 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_186 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_187 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_188 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_189 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_190 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_191 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_192 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_193 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_194 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_195 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_196 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_197 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_198 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_199 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_200 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_201 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_202 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_203 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_204 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_205 (SEM_CONTEXT VS_TQ_CONTEXT * Context);

extern VS_VOID VSDoorsVSAction_206 (SEM_CONTEXT VS_TQ_CONTEXT * Context);


/*
 * Action Expression Pointer Table.
 */
extern VS_ACTIONEXPR_TYPE const VSDoorsVSAction[207];


#endif
