/*
 * Type:        Source Code File
 * 
 * Id:          SMPInst.c
 *
 * Function:    Contains the SMP_InitInstances and SMP_SetInstance 
 *              SEM Library Expert version 4 functions.
 *
 * Portability: ANSI-C Compiler.
 *
 * Copyright (c) 1999-2006 IAR Systems. All rights reserved.
 */

/*
 * Version   ! Date   ! Status ! Changes
 *----------------------------------------------------------------------------
 * 4.0.0.1   ! 021298 ! Closed ! -
 * 4.0.4     ! 010799 ! Closed ! Minor update, there might be no changes to this particular file.
 */

#include "SEMLibE.h"
#include "limits.h"

#if (VS_REALLINKMODE == 1)
#include "RealLink.h"
#endif

unsigned char SMP_InitInstances (SEM_CONTEXT VS_TQ_CONTEXT *Context)
{
  unsigned long i;

  if (!Context)
    return (SES_NOT_LOADED);

#if VS_USE_HEAP
  if (Context->pIns)
    free (Context->pIns);
  if ((unsigned long) ULONG_MAX / Context->nNofInstances < (unsigned long) Context->nNofStateMachines)
    return (SES_TYPE_OVERFLOW);
#endif
  if (Context->nNofInstances > 1)
  {
#if VS_USE_HEAP
    Context->pIns = (SEM_STATE_TYPE *)malloc ((Context->nNofInstances * Context->nNofStateMachines) * sizeof (SEM_STATE_TYPE));
    if (!Context->pIns)
      return (SES_MEM_ERR);  

#endif
    Context->pCSV = Context->pIns;
    Context->ActIns = 0;

    for (i = 0; i < (unsigned long)Context->nNofStateMachines * Context->nNofInstances; i++)
      Context->pIns[i] = STATE_UNDEFINED;
  }

  return (SES_OKAY);
}


unsigned char SMP_SetInstance (SEM_CONTEXT VS_TQ_CONTEXT *Context, SEM_INSTANCE_TYPE Instance)
{
  if (!Context)
    return (SES_NOT_LOADED);
  if (Instance >= Context->nNofInstances)
    return (SES_RANGE_ERR);
  if ((Context->State != STATE_SEM_INITIALIZE) || (Context->InqAct))
    return (SES_ACTIVE);
  if (Context->nNofInstances > 1)
  {
    Context->pCSV = (Context->pIns + Instance * Context->nNofStateMachines);
    Context->ActIns = Instance;
  }

#if (VS_REALLINKMODE == 1)
  VS_WAIT_ELM(Context, VS_ENABLE_INSTANCE, Ind_INSTANCE_SET, &Instance);
#endif
  return (SES_OKAY);
}
