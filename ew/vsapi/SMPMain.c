/*
 * Type:        Source Code File
 * 
 * Id:          SMPMain.c
 *
 * Function:    Contains the general SEM Library Expert version 4 functions.
 *
 * Portability: ANSI-C Compiler.
 *
 * Copyright (c) 1999-2006 IAR Systems. All rights reserved.
 */

/*
 * Version   ! Date   ! Status ! Changes
 *----------------------------------------------------------------------------
 * 4.0.0.17  ! 290199 ! Closed ! -
 * 4.0.4     ! 010799 ! Closed ! Minor update, there might be no changes to this particular file.
 */

#include "SEMLibE.h"

#if (VS_REALLINKMODE == 1)
#include "RealLink.h"
#endif



#if (VS_REALLINKMODE == 1)
extern VS_VOID volatile const * const vsrl_aVspInfo[];
extern SEM_CONTEXT * vsrl_aVspContextPointer[];
#endif



#ifdef VS_RUNTIME_INFO
VSRunTimeInfo volatile const VS_RUNTIME_INFO_EXTKW vsRunTimeInfo = 
{
  VS_SIGNATURE_VERSION,
  VS_SIGNATURE_CONTENT
};
#endif



#ifdef SE_EXPERTDLL
static unsigned char _SMP_ReadTP (FILE *pRBF, unsigned long **Pos, unsigned long NbR, unsigned long FPos)
{
  unsigned long *p;

  if (FPos)
  {
    if ((p = (unsigned long *)malloc (NbR * sizeof (unsigned long))) == NULL)
      return (SES_MEM_ERR);
    *Pos = p;
    if (fseek (pRBF, (long)FPos, SEEK_SET))
      return (SES_FILE_ERR);
    if (fread (p, sizeof (unsigned long) * NbR, 1, pRBF) != 1)
      return (SES_FILE_ERR);
  }
  return (SES_OKAY);
}
#endif



static unsigned char _SMP_Connect (SEM_CONTEXT VS_TQ_CONTEXT *Context, void VS_TQ_RULEBASE *VSData)
{
  RBH VS_TQ_RULEBASE *pRBH;
  unsigned char CC;

  pRBH = (RBH VS_TQ_RULEBASE *)VSData;
  if ((pRBH->IdAndEventGroupType & 0x0FF) != SEM_VERS_ID)
    goto FileError;

#ifdef SE_EXPERTDLL
  if (!((pRBH->IdAndEventGroupType >> 12) & 1))
    goto FileError;
#endif

  Context->nNofEvents = (SEM_EVENT_TYPE) pRBH->nNofEvents;
#if (SEM_SIGNAL)
  Context->nNofSignals = (SEM_EVENT_TYPE) pRBH->nNofSignals;
  Context->SSize = (SEM_SIGNAL_QUEUE_TYPE) pRBH->SSize;
#endif
  Context->nNofStates = (SEM_STATE_TYPE) pRBH->nNofStates;
  Context->nNofStateMachines = (SEM_STATE_MACHINE_TYPE) pRBH->nNofStateMachines;
  Context->nNofActionFunctions = (SEM_ACTION_FUNCTION_TYPE) pRBH->nNofActionFunctions;
  Context->nNofInstances = (SEM_INSTANCE_TYPE) pRBH->nNofInstances;
  Context->EventGroupType = (unsigned char)((pRBH->IdAndEventGroupType >> 8) & 0x0F);

#if VS_USE_HEAP
  Context->pCSV = NULL;
  Context->pWSV = NULL;
  Context->pIns = NULL;
#if (SEM_SIGNAL)
  Context->pSQueue = NULL;
#endif
#endif
  Context->pENIPos = NULL;
  Context->pSNIPos = NULL;
  Context->pANIPos = NULL;
  Context->pEEIPos = NULL;
  Context->pSEIPos = NULL;
  Context->pAEIPos = NULL;

#if (SEM_GUARD_EXPRESSION)
  Context->pGuard = NULL;
#endif
#if (SEM_SIGNAL_DB)
  Context->pSEM_SignalDB = NULL;
#endif
  
#if VS_USE_HEAP
  if (Context->nNofStates)
  {
    if (Context->nNofInstances == 1)
    {
      if ((Context->pCSV = (SEM_STATE_TYPE *)malloc (sizeof (SEM_STATE_TYPE) * Context->nNofStateMachines)) == NULL)
        goto MemoryError;
    }
    if ((Context->pWSV = (SEM_STATE_TYPE *)malloc (sizeof (SEM_STATE_TYPE) * Context->nNofStateMachines)) == NULL)
      goto MemoryError;
  }
#if (SEM_SIGNAL)
  if (Context->SSize)
  {
    if ((Context->pSQueue = (SEM_EVENT_TYPE *)malloc (sizeof (SEM_EVENT_TYPE) * Context->SSize)) == NULL)
      goto MemoryError;
  }
#endif
#endif
  
  if (Context->nNofStates)
    Context->pSMI = (SEM_STATE_MACHINE_TYPE VS_TQ_RULEBASE *)((char VS_TQ_RULEBASE *)pRBH + pRBH->SMIPos);
  if (Context->EventGroupType != 0)
    Context->pEGT = (SEM_EVENT_GROUP_TYPE VS_TQ_RULEBASE *)((char VS_TQ_RULEBASE *)pRBH + pRBH->EGTPos);
  if (Context->EventGroupType == 2)
    Context->pEGTI = (SEM_EGTI_TYPE VS_TQ_RULEBASE *)((char VS_TQ_RULEBASE *)pRBH + pRBH->EGTIPos);
  Context->pRD = (SEM_RULE_DATA_TYPE VS_TQ_RULEBASE *)((char VS_TQ_RULEBASE *)pRBH + pRBH->RDPos);
  Context->pRI = (SEM_RULE_INDEX_TYPE VS_TQ_RULEBASE *)((char VS_TQ_RULEBASE *)pRBH + pRBH->RIPos);
  Context->pRTI = (SEM_RULE_TABLE_INDEX_TYPE VS_TQ_RULEBASE *)((char VS_TQ_RULEBASE *)pRBH + pRBH->RTIPos);

  if (!Context->pVSFile)
  {
    if (pRBH->ENIPos && pRBH->nNofEvents)
      Context->pENIPos = (unsigned long VS_TQ_RULEBASE *)((char VS_TQ_RULEBASE *)pRBH + pRBH->ENIPos);
    if (pRBH->SNIPos && pRBH->nNofStates)
      Context->pSNIPos = (unsigned long VS_TQ_RULEBASE *)((char VS_TQ_RULEBASE *)pRBH + pRBH->SNIPos);
    if (pRBH->ANIPos && pRBH->nNofActionFunctions)
      Context->pANIPos = (unsigned long VS_TQ_RULEBASE *)((char VS_TQ_RULEBASE *)pRBH + pRBH->ANIPos);
    if (pRBH->EEIPos && pRBH->nNofEvents)
      Context->pEEIPos = (unsigned long VS_TQ_RULEBASE *)((char VS_TQ_RULEBASE *)pRBH + pRBH->EEIPos);
    if (pRBH->SEIPos && pRBH->nNofStates)
      Context->pSEIPos = (unsigned long VS_TQ_RULEBASE *)((char VS_TQ_RULEBASE *)pRBH + pRBH->SEIPos);
    if (pRBH->AEIPos && pRBH->nNofActionFunctions)
      Context->pAEIPos = (unsigned long VS_TQ_RULEBASE *)((char VS_TQ_RULEBASE *)pRBH + pRBH->AEIPos);
  }
  else
  {
#ifndef SE_EXPERTDLL
    CC = SES_NOT_LOADED;
    goto CCError;
#else
    if ((CC = _SMP_ReadTP (Context->pVSFile, &Context->pENIPos, pRBH->nNofEvents, pRBH->ENIPos)) != SES_OKAY)
      goto CCError;
    if ((CC = _SMP_ReadTP (Context->pVSFile, &Context->pSNIPos, pRBH->nNofStates, pRBH->SNIPos)) != SES_OKAY)
      goto CCError;
    if ((CC = _SMP_ReadTP (Context->pVSFile, &Context->pANIPos, pRBH->nNofActionFunctions, pRBH->ANIPos)) != SES_OKAY)
      goto CCError;
    if ((CC = _SMP_ReadTP (Context->pVSFile, &Context->pEEIPos, pRBH->nNofEvents, pRBH->EEIPos)) != SES_OKAY)
      goto CCError;
    if ((CC = _SMP_ReadTP (Context->pVSFile, &Context->pSEIPos, pRBH->nNofStates, pRBH->SEIPos)) != SES_OKAY)
      goto CCError;
    if ((CC = _SMP_ReadTP (Context->pVSFile, &Context->pAEIPos, pRBH->nNofActionFunctions, pRBH->AEIPos)) != SES_OKAY)
      goto CCError;
#endif
  }
  SMP_Init (Context);
#if (SEM_SIGNAL)
  SMP_InitSignalQueue (Context);
#endif

#if (VS_REALLINKMODE == 1)
  Context->systemNo = (VS_UINT8)((pRBH->IdAndEventGroupType >> 24) & 0x0FF);
  vsrl_aVspContextPointer[Context->systemNo] = Context;
#endif

  return (SES_OKAY);

CCError :
  SMP_Free (Context);
  return (CC);

#if VS_USE_HEAP
MemoryError :
  SMP_Free (Context);
  return (SES_MEM_ERR);
#endif

FileError :
  SMP_Free (Context);
  return (SES_FILE_ERR);
}



unsigned char SMP_Connect (SEM_CONTEXT VS_TQ_CONTEXT **Context, void VS_TQ_RULEBASE *VSData)
{
  SEM_CONTEXT VS_TQ_CONTEXT *pCont;

#if VS_USE_HEAP
  if ((pCont = (SEM_CONTEXT VS_TQ_CONTEXT *)malloc (sizeof (SEM_CONTEXT))) == NULL)
    return (SES_MEM_ERR);
#else
  pCont = *(SEM_CONTEXT VS_TQ_CONTEXT * VS_TQ_RULEBASE *)((char VS_TQ_RULEBASE *)VSData + ((RBH VS_TQ_RULEBASE *)VSData)->contextPtrOffset);
#endif
  pCont->pVSData = VSData;
  pCont->pVSFile = NULL;
  *Context = pCont;
  return (_SMP_Connect (pCont, VSData));
}


#ifdef SE_EXPERTDLL
unsigned char SMP_Load (SEM_CONTEXT VS_TQ_CONTEXT **Context, const char *FileName)
{
  FILE *pVSF;
  RBH RBHe;
  char *pc;
  long Bytes;
  size_t Chunk;
  SEM_CONTEXT VS_TQ_CONTEXT *pCont;

  if ((pCont = (SEM_CONTEXT VS_TQ_CONTEXT *)malloc (sizeof (SEM_CONTEXT))) == NULL)
    return (SES_MEM_ERR);

  pCont->pCSV = NULL;
  pCont->pWSV = NULL;
  pCont->pIns = NULL;
#if (SEM_SIGNAL)
  pCont->pSQueue = NULL;
#endif
  pCont->pENIPos = NULL;
  pCont->pSNIPos = NULL;
  pCont->pANIPos = NULL;
  pCont->pEEIPos = NULL;
  pCont->pSEIPos = NULL;
  pCont->pAEIPos = NULL;

  pVSF = fopen (FileName, "rb");
  pCont->pVSFile = pVSF;
  if (!pVSF)
    goto FileError;
  if (fread (&RBHe, sizeof (RBHe), 1, pVSF) != 1)
    goto FileError;
  if ((RBHe.IdAndEventGroupType & 0x0FF) != SEM_VERS_ID)
    goto FileError;
  if (((RBHe.IdAndEventGroupType >> 16) & 0x0FF) != SEM_RDFM_NUMBER)
    goto FileError;
  if (fseek (pVSF, 0L, SEEK_SET))
    goto FileError;

  Bytes = RBHe.TSPos;

  if ((sizeof(size_t) == sizeof (short)) * (Bytes >> 16))
    goto MemoryError;
  if ((pCont->pVSData = (void *)malloc ((size_t)Bytes)) == NULL)
    goto MemoryError;

  pc = (char *)pCont->pVSData;
  while (Bytes)
  {
    if (Bytes > 0x04000)
      Chunk = 0x04000;
    else
      Chunk = (size_t)Bytes;
    Bytes -= Chunk;
    if (fread (pc, Chunk, 1, pVSF) != 1)
      goto FileError;
    pc += Chunk;
  }

  *Context = pCont;
  return (_SMP_Connect (pCont, pCont->pVSData));

MemoryError:
  SMP_Free (pCont);
  return (SES_MEM_ERR);

FileError:
  SMP_Free (pCont);
  return (SES_FILE_ERR);
}
#endif


void SMP_Free (SEM_CONTEXT VS_TQ_CONTEXT *Context)
{
  if (!Context)
    return;

#if (VS_REALLINKMODE == 1)
  VS_WAIT_ELM(Context, VS_ENABLE_SYSTEM, Ind_SYSTEM_FREE, &Context->systemNo);
  vsrl_aVspContextPointer[Context->systemNo] = NULL;
#endif

#if VS_USE_HEAP
  if (Context->pCSV && Context->nNofInstances == 1)
    free (Context->pCSV);
  if (Context->pWSV)
    free (Context->pWSV);
  if (Context->pIns)
    free (Context->pIns);
#if (SEM_SIGNAL)
  if (Context->pSQueue)
    free (Context->pSQueue);
#endif
#endif
#ifdef SE_EXPERTDLL
  if (Context->pVSFile)
  {
    fclose (Context->pVSFile);
    if (Context->pVSData)
      free (Context->pVSData);
    if (Context->pENIPos)
      free (Context->pENIPos);
    if (Context->pSNIPos)
      free (Context->pSNIPos);
    if (Context->pANIPos)
      free (Context->pANIPos);
    if (Context->pEEIPos)
      free (Context->pEEIPos);
    if (Context->pSEIPos)
      free (Context->pSEIPos);
    if (Context->pAEIPos)
      free (Context->pAEIPos);
  }
#endif
#if VS_USE_HEAP
  free (Context);
#endif
}


unsigned char SMP_Init (SEM_CONTEXT VS_TQ_CONTEXT *Context)
{
  SEM_STATE_MACHINE_TYPE i;

#ifdef VS_RUNTIME_INFO
  *vsRunTimeInfo.pSignatureVersion;
#endif
  if (!Context)
    return (SES_NOT_LOADED);
  for (i = 0; i < Context->nNofStateMachines; i++)
  {
    Context->pWSV[i] = STATE_UNDEFINED;
    if (Context->nNofInstances == 1)
      Context->pCSV[i] = STATE_UNDEFINED;
  }
  Context->State = STATE_SEM_INITIALIZE;
  Context->InqAct = VS_FALSE;
#if (SEM_SIGNAL)
  Context->SignalStateChg = 0;
#endif
  Context->StateChange = VS_FALSE;
  return (SES_OKAY);
} 


#if (SEM_GUARD_EXPRESSION)
#if (SEM_FUNCEXPHANDLING == 0)
void SMP_InitGuardCallBack (SEM_CONTEXT VS_TQ_CONTEXT *Context, VS_GUARDEXPR_TYPE VS_TQ_GUARDEXPRCOL * Guard)
#else
void SMP_InitGuardCallBack (SEM_CONTEXT VS_TQ_CONTEXT *Context, unsigned char (* Guard)(SEM_CONTEXT VS_TQ_CONTEXT *Context, SEM_GUARD_EXPRESSION_TYPE i))
#endif
{
  Context->pGuard = Guard;
}
#endif


#if (SEM_SIGNAL_DB)
void SMP_InitSignalDBCallBack (SEM_CONTEXT VS_TQ_CONTEXT *Context,
  void (*SEM_SignalDB)(SEM_CONTEXT VS_TQ_CONTEXT *Context, SEM_EVENT_TYPE SignalNo))
{
  Context->pSEM_SignalDB = SEM_SignalDB;
}
#endif


#if (SEM_SIGNAL)
void SMP_InitSignalQueue (SEM_CONTEXT VS_TQ_CONTEXT *Context)
{
  Context->SPut = 0;
  Context->SGet = 0;
  Context->SUsed = 0;
}


static unsigned char SMP_SignalQueuePut (SEM_CONTEXT VS_TQ_CONTEXT *Context,
  SEM_EVENT_TYPE SignalNo)
{
  if (Context->SUsed == Context->SSize)
    return (SES_SIGNAL_QUEUE_FULL);
  Context->SUsed++;
  Context->pSQueue[Context->SPut] = SignalNo;
  if (++Context->SPut == Context->SSize)
    Context->SPut = 0;
  return (SES_OKAY);
}


static SEM_EVENT_TYPE SMP_SignalQueueGet (SEM_CONTEXT VS_TQ_CONTEXT *Context)
{
  SEM_EVENT_TYPE SignalNo;

  if (!Context->SUsed)
    return (EVENT_UNDEFINED);
  Context->SUsed--;
  SignalNo = Context->pSQueue[Context->SGet];

  if (++Context->SGet == Context->SSize)
    Context->SGet = 0;

  return (SignalNo);
}
#endif


unsigned char SMP_Deduct (SEM_CONTEXT VS_TQ_CONTEXT *Context, SEM_EVENT_TYPE EventNo)
{
  if (!Context)
    return (SES_NOT_LOADED);
  if (Context->nNofEvents <= EventNo)
    return (SES_RANGE_ERR);
  Context->EventNo = EventNo;
  Context->State = STATE_SEM_PREPARE;
  Context->DIt = 2;
  if (Context->InqAct)
    return (SES_ACTIVE);

#if (VS_REALLINKMODE == 1)
  VS_WAIT_ELM(Context, VS_ENABLE_EVENT, Ind_EVENT, &EventNo);
#endif

  return (SES_OKAY);
} 


unsigned char SMP_GetOutput (SEM_CONTEXT VS_TQ_CONTEXT *Context, 
  SEM_ACTION_EXPRESSION_TYPE *ActionNo)
{
  SEM_RULE_DATA_TYPE rd;
  unsigned char i, nPos, nNxt;
#if (SEM_RDHW_TYPE_2 || SEM_RDHW_TYPE_3)
  unsigned char nGuard;
#endif
#if (SEM_SIGNAL)
  unsigned char nSignal;
#endif
  SEM_STATE_MACHINE_TYPE m; 
#if (SEM_RMN_NEGATIVE_STATE_SYNCS || defined VISUALSTATE_EXPERT_DLL)
  unsigned char nNeg;
  SEM_STATE_TYPE s;
#endif

  if (!Context)
    return (SES_NOT_LOADED);
  for(;;)
  {
    switch (Context->State)
    {
#if (SEM_SIGNAL)
      case STATE_SEM_SIGNAL :
Signal :
        Context->EventNo = SMP_SignalQueueGet (Context);
        if (Context->EventNo == EVENT_UNDEFINED)
        {
          Context->State = STATE_SEM_OKAY;
          return (SES_OKAY);
        }
#if (SEM_SIGNAL_DB)
        if (Context->pSEM_SignalDB)
          (*Context->pSEM_SignalDB) (Context, Context->EventNo);
#endif
        for (m = 0; m < Context->nNofStateMachines; m++)
        {
          if (Context->pWSV[m] != STATE_UNDEFINED)
          {
#ifdef SEM_NEXT_STATE_CHG
            if (Context->pCSV[m] != Context->pWSV[m])
              Context->SignalStateChg = 2;
            else
#endif
              Context->SignalStateChg = 1;

            Context->pCSV[m] = Context->pWSV[m];
            Context->pWSV[m] = STATE_UNDEFINED;
          }
        }
#if (VS_REALLINKMODE == 1)
        VS_WAIT_ELM(Context, VS_ENABLE_SIGNAL, Ind_SIGNAL, &Context->EventNo);
#endif 
        goto FirstRule;
#endif
      case STATE_SEM_PREPARE :
#if (SEM_EVENT_GROUP_INDEX) || defined VISUALSTATE_EXPERT_DLL
        if (Context->EventGroupType == 1)
        {
          if (Context->DIt == 1)
          {
            Context->EventNo = (SEM_EVENT_TYPE) Context->pEGT[Context->EventNo];
            if (Context->EventNo == EVENT_UNDEFINED)
            {
#if (SEM_SIGNAL)
              if (Context->nNofSignals != 0)
              {
                Context->DIt = 0;
                goto Signal;
              }
              else
#endif
              {
                Context->State = STATE_SEM_OKAY;
                return (SES_OKAY);
              }
            }
            Context->EventNo = (SEM_EVENT_TYPE) (Context->EventNo + Context->nNofEvents);
          }
          else if (Context->DIt == 0)
          {
#if (SEM_SIGNAL)
            if (Context->nNofSignals != 0)
              goto Signal;
            else
#endif
            {
              Context->State = STATE_SEM_OKAY;
              return (SES_OKAY);
            }
          }
          Context->DIt--;
        }
#endif
#if (SEM_EVENT_GROUP_TABLE_INDEX) || defined VISUALSTATE_EXPERT_DLL
        if (Context->EventGroupType == 2)
        {
          if (Context->DIt == 0)
          {
            if (++Context->iFirstEgi >= Context->iLastEgi)
            {
#if (SEM_SIGNAL)
              if (Context->nNofSignals != 0)
                goto Signal;
              else
#endif
              {
                Context->State = STATE_SEM_OKAY;
                return (SES_OKAY);
              }
            }
            Context->EventNo = Context->pEGT[Context->iFirstEgi];
            Context->EventNo = (SEM_EVENT_TYPE) (Context->EventNo + Context->nNofEvents);
          }
          else if (Context->DIt == 1)
          {
            Context->iFirstEgi = Context->pEGTI[Context->EventNo];
            Context->iLastEgi = Context->pEGTI[Context->EventNo + 1];
            if (Context->iFirstEgi == Context->iLastEgi)
            {
#if (SEM_SIGNAL)
              if (Context->nNofSignals != 0)
              {
                Context->DIt = 0;
                goto Signal;
              }
              else
#endif
              {
                Context->State = STATE_SEM_OKAY;
                return (SES_OKAY);
              }
            }
            Context->EventNo = Context->pEGT[Context->iFirstEgi];
            Context->EventNo = (SEM_EVENT_TYPE) (Context->EventNo + Context->nNofEvents);
            Context->DIt--;
          }
          else if (Context->DIt == 2)
          {
            Context->DIt--;
          }
        }
#endif
#if (SEM_SIGNAL)
FirstRule :
#endif
        Context->iFirstR = Context->pRTI[Context->EventNo];
        Context->iLastR = Context->pRTI[Context->EventNo + 1];
        Context->State = STATE_SEM_CONSULT;
      case STATE_SEM_CONSULT :
        while (Context->iFirstR < Context->iLastR)
        {
          Context->iRI = Context->pRI[Context->iFirstR++];
#if (SEM_RD_WIDTH_8_BIT && SEM_RDHW_TYPE_1 && SEM_RDHW_WIDTH_16_BIT)
          rd = Context->pRD[Context->iRI++];
          nNxt = (unsigned char)(rd & 0x0F);
          Context->nNofActions = (unsigned char)(rd >> 4);
          rd = Context->pRD[Context->iRI++];
          nPos = (unsigned char)(rd & 0x0F);
#if (SEM_RMN_NEGATIVE_STATE_SYNCS || defined VISUALSTATE_EXPERT_DLL)
          nNeg = (unsigned char)(rd >> 4);
#endif
#endif
#if (SEM_RD_WIDTH_8_BIT && SEM_RDHW_TYPE_2 && SEM_RDHW_WIDTH_24_BIT)
          rd = Context->pRD[Context->iRI++];
          nPos = (unsigned char)(rd & 0x0F);
#if (SEM_RMN_NEGATIVE_STATE_SYNCS || defined VISUALSTATE_EXPERT_DLL)
          nNeg = (unsigned char)(rd >> 4);
#endif
          rd = Context->pRD[Context->iRI++];
          nGuard = (unsigned char)(rd & 0x0F);
          nNxt = (unsigned char)(rd >> 4);
          rd = Context->pRD[Context->iRI++];
          Context->nNofActions = (unsigned char)(rd & 0x0F);
#if (SEM_SIGNAL)
          nSignal = (unsigned char)(rd >> 4);
#endif
#endif
#if (SEM_RD_WIDTH_8_BIT && SEM_RDHW_TYPE_1 && SEM_RDHW_WIDTH_32_BIT)
          Context->nNofActions = (unsigned char)Context->pRD[Context->iRI++];
          nNxt = (unsigned char)Context->pRD[Context->iRI++];
#if (SEM_RMN_NEGATIVE_STATE_SYNCS || defined VISUALSTATE_EXPERT_DLL)
          nNeg = (unsigned char)Context->pRD[Context->iRI++];
#else
          Context->iRI++;
#endif
          nPos = (unsigned char)Context->pRD[Context->iRI++];
#endif
#if (SEM_RD_WIDTH_8_BIT && SEM_RDHW_TYPE_2 && SEM_RDHW_WIDTH_48_BIT)
#if (SEM_RMN_NEGATIVE_STATE_SYNCS || defined VISUALSTATE_EXPERT_DLL)
          nNeg = (unsigned char)Context->pRD[Context->iRI++];
#else
          Context->iRI++;
#endif
          nPos = (unsigned char)Context->pRD[Context->iRI++];
          nNxt = (unsigned char)Context->pRD[Context->iRI++];
          nGuard = (unsigned char)Context->pRD[Context->iRI++];
#if (SEM_SIGNAL)
          nSignal = (unsigned char)Context->pRD[Context->iRI++];
#else
          Context->iRI++;
#endif
          Context->nNofActions = (unsigned char)Context->pRD[Context->iRI++];
#endif
#if (SEM_RD_WIDTH_16_BIT && SEM_RDHW_TYPE_1 && SEM_RDHW_WIDTH_16_BIT)
          rd = Context->pRD[Context->iRI++];
          nPos = (unsigned char)(rd & 0x0F);
#if (SEM_RMN_NEGATIVE_STATE_SYNCS || defined VISUALSTATE_EXPERT_DLL)
          nNeg = (unsigned char)((rd >> 4) & 0x0F);
#endif
          nNxt = (unsigned char)((rd >> 8) & 0x0F);
          Context->nNofActions = (unsigned char)((rd >> 12) & 0x0F);
#endif
#if (SEM_RD_WIDTH_16_BIT && SEM_RDHW_TYPE_3 && SEM_RDHW_WIDTH_32_BIT)
          rd = Context->pRD[Context->iRI++];
          nPos = (unsigned char)(rd & 0x0F);
#if (SEM_RMN_NEGATIVE_STATE_SYNCS || defined VISUALSTATE_EXPERT_DLL)
          nNeg = (unsigned char)((rd >> 4) & 0x0F);
#endif
          nGuard = (unsigned char)((rd >> 8) & 0x0F);
          nNxt = (unsigned char)(rd >> 12);
          rd = Context->pRD[Context->iRI++];
          Context->nNofActions = (unsigned char)(rd & 0x0F);
#if (SEM_SIGNAL)
          nSignal = (unsigned char)((rd >> 4) & 0x0F);
#endif
#endif
#if (SEM_RD_WIDTH_16_BIT && SEM_RDHW_TYPE_1 && SEM_RDHW_WIDTH_32_BIT)
          rd = Context->pRD[Context->iRI++];
          nNxt = (unsigned char)(rd & 0x0FF);
          Context->nNofActions = (unsigned char)(rd >> 8);
          rd = Context->pRD[Context->iRI++];
          nPos = (unsigned char)(rd & 0x0FF);
#if (SEM_RMN_NEGATIVE_STATE_SYNCS || defined VISUALSTATE_EXPERT_DLL)
          nNeg = (unsigned char)(rd >> 8);
#endif
#endif
#if (SEM_RD_WIDTH_16_BIT && SEM_RDHW_TYPE_2 && SEM_RDHW_WIDTH_48_BIT)
          rd = Context->pRD[Context->iRI++];
          nPos = (unsigned char)(rd & 0x0FF);
#if (SEM_RMN_NEGATIVE_STATE_SYNCS || defined VISUALSTATE_EXPERT_DLL)
          nNeg = (unsigned char)(rd >> 8);
#endif
          rd = Context->pRD[Context->iRI++];
          nGuard = (unsigned char)(rd & 0x0FF);
          nNxt = (unsigned char)(rd >> 8);
          rd = Context->pRD[Context->iRI++];
          Context->nNofActions = (unsigned char)(rd & 0x0FF);
#if (SEM_SIGNAL)
          nSignal = (unsigned char)(rd >> 8);
#endif
#endif
#if (SEM_RD_WIDTH_32_BIT && SEM_RDHW_TYPE_1 && SEM_RDHW_WIDTH_32_BIT)
          rd = Context->pRD[Context->iRI++];
          nPos = (unsigned char)(rd & 0X0FF);
#if (SEM_RMN_NEGATIVE_STATE_SYNCS || defined VISUALSTATE_EXPERT_DLL)
          nNeg = (unsigned char)((rd >> 8) & 0X0FF);
#endif
          nNxt = (unsigned char)((rd >> 16) & 0x0FF);
          Context->nNofActions = (unsigned char)(rd >> 24);
#endif
#if (SEM_RD_WIDTH_32_BIT && SEM_RDHW_TYPE_3 && SEM_RDHW_WIDTH_64_BIT)
          rd = Context->pRD[Context->iRI++];
          nPos = (unsigned char)(rd & 0x0FF);
#if (SEM_RMN_NEGATIVE_STATE_SYNCS || defined VISUALSTATE_EXPERT_DLL)
          nNeg = (unsigned char)((rd >> 8) & 0x0FF);
#endif
          nGuard = (unsigned char)((rd >> 16) & 0x0FF);
          nNxt = (unsigned char)(rd >> 24);
          rd = Context->pRD[Context->iRI++];
          Context->nNofActions = (unsigned char)(rd & 0x0FF);
#if (SEM_SIGNAL)
          nSignal = (unsigned char)((rd >> 8) & 0x0FF);
#endif
#endif
          for (i = 0; i < nPos; i++)
          {
            rd = Context->pRD[Context->iRI++];
            if (rd != Context->pCSV[Context->pSMI[rd]])
              goto NextRule;
          }
#if (SEM_RMN_NEGATIVE_STATE_SYNCS || defined VISUALSTATE_EXPERT_DLL)
          for (i = 0; i < nNeg; i++)
          {
            rd = Context->pRD[Context->iRI++];
            s = Context->pCSV[Context->pSMI[rd]];
            if ((s == STATE_UNDEFINED) || (s == (SEM_STATE_TYPE) rd))
              goto NextRule;
          }
#endif
#if (SEM_GUARD_EXPRESSION)
          if (nGuard)
          {
            if (Context->pGuard)
              for (i = 0; i < nGuard; i++)
#if (SEM_FUNCEXPHANDLING == 0)
                if ((*Context->pGuard[Context->pRD[Context->iRI++]])(Context) == VS_FALSE)
#else
                if ((*Context->pGuard)(Context, (SEM_GUARD_EXPRESSION_TYPE) Context->pRD[Context->iRI++]) == VS_FALSE)
#endif
                  goto NextRule;
          }
#endif

#if (VS_REALLINKMODE == 1)
          {
          SEM_RULE_TABLE_INDEX_TYPE ri = Context->iFirstR - 1;
          VS_WAIT_ELM(Context, VS_ENABLE_TRANS, Ind_TRANS, &ri);
          }
#endif

          for (i = 0; i < nNxt; i++)
          {
            rd = Context->pRD[Context->iRI++];
            m = Context->pSMI[rd];
            if (Context->pWSV[m] == STATE_UNDEFINED)
              Context->pWSV[m] = (SEM_STATE_TYPE) rd;
            else if (Context->pWSV[m] != rd)
              return (SES_CONTRADICTION);
          }
#if (SEM_SIGNAL)
          if (nSignal)
          {
            for (i = 0; i < nSignal; i++)
            {
              rd = Context->pRD[Context->iRI++];
#if (SEM_SIGNAL_QUEUE_ERROR_IF_FULL)
              if (SMP_SignalQueuePut (Context, (SEM_EVENT_TYPE)rd) == SES_SIGNAL_QUEUE_FULL)
                return (SES_SIGNAL_QUEUE_FULL);
#endif
#if (SEM_SIGNAL_QUEUE_NO_ERROR_IF_FULL)
              SMP_SignalQueuePut (Context, (SEM_EVENT_TYPE)rd);
#endif              
            }
          }
#endif
          if (Context->nNofActions)
          {
            *ActionNo = (SEM_ACTION_EXPRESSION_TYPE)Context->pRD[Context->iRI];
            if (Context->nNofActions > 1)
            {
              Context->iRI++;
              Context->nNofActions--;
              Context->State = STATE_SEM_OUTPUT;
            }

#if (VS_REALLINKMODE == 1)
            VS_WAIT_ELM(Context, VS_ENABLE_ACTION, Ind_ACTION, ActionNo);
#endif

            return (SES_FOUND);
          }
NextRule :
          ;
        }
#if (SEM_EVENT_GROUP_INDEX || SEM_EVENT_GROUP_TABLE_INDEX) || defined VISUALSTATE_EXPERT_DLL
        if (Context->EventGroupType != 0)
        {
          Context->State = STATE_SEM_PREPARE;
          break;
        }
        else
        {
#if (SEM_SIGNAL)
          if (Context->nNofSignals != 0)
            goto Signal;
          else
#endif
          {
            Context->State = STATE_SEM_OKAY;
            return (SES_OKAY);
          }
        }
#else
#if (SEM_SIGNAL)
        if (Context->nNofSignals != 0)
          goto Signal;
        else
#endif
        {
          Context->State = STATE_SEM_OKAY;
          return (SES_OKAY);
        }
#endif
      case STATE_SEM_OUTPUT :
        if (Context->nNofActions)
        {
          *ActionNo = (SEM_ACTION_EXPRESSION_TYPE)Context->pRD[Context->iRI++];
          Context->nNofActions--;

#if (VS_REALLINKMODE == 1)
          VS_WAIT_ELM(Context, VS_ENABLE_ACTION, Ind_ACTION, ActionNo);
#endif

          return (SES_FOUND);
        }
        Context->State = STATE_SEM_CONSULT;
        break;
      case STATE_SEM_OKAY :
        return (SES_OKAY);
      default :
        return (SES_EMPTY);
    }
  }
} 


#if defined SEM_GET_OUTPUT_ALL || defined VISUALSTATE_EXPERT_DLL
#if (SEM_SIGNAL)
#ifndef VISUALSTATE_EXPERT_DLL
#error SMP_GetOutputAll cannot be used when the Project contains signals.
#endif
#endif
unsigned char SMP_GetOutputAll (SEM_CONTEXT VS_TQ_CONTEXT *Context, 
  SEM_ACTION_EXPRESSION_TYPE *ActionExprVector, 
  SEM_ACTION_EXPRESSION_TYPE MaxSize)
{
  SEM_ACTION_EXPRESSION_TYPE i;
  unsigned char CC;

  if (!Context)
    return (SES_NOT_LOADED);

  for (i = 0; ((CC = SMP_GetOutput (Context, &ActionExprVector[i])) == SES_FOUND) && i < MaxSize - 1; i++);

  if (CC == SES_OKAY)
  {
    ActionExprVector[i] = ACTION_EXPRESSION_TERMINATION_ID;
    return (SES_OKAY);
  }
  if (CC == SES_FOUND)
    return (SES_BUFFER_OVERFLOW);
  return (CC);
} 
#endif



#if defined SEM_NEXT_STATE_CHG || defined VISUALSTATE_EXPERT_DLL
unsigned char _SMP_NextStateChg (SEM_CONTEXT VS_TQ_CONTEXT *Context, unsigned char nextStateChg)
#else
unsigned char _SMP_NextStateChg (SEM_CONTEXT VS_TQ_CONTEXT *Context)
#endif
{
  unsigned char CC;
  SEM_STATE_MACHINE_TYPE i;
  SEM_ACTION_EXPRESSION_TYPE ActionNo;

  if (!Context)
    return (SES_NOT_LOADED);
  if (Context->State != STATE_SEM_OKAY)
  {
    while ((CC = SMP_GetOutput (Context, &ActionNo)) == SES_FOUND);
    if (CC != SES_OKAY)
      return (CC);
  }
  CC = SES_OKAY;

#if (SEM_SIGNAL)
  if (Context->SignalStateChg != 0)
  {
#if defined SEM_NEXT_STATE_CHG || defined VISUALSTATE_EXPERT_DLL
    if (nextStateChg && Context->SignalStateChg == 2)
      CC = SES_FOUND;
#endif
    Context->StateChange = VS_TRUE;
    Context->SignalStateChg = 0;
  }
  else
#endif
    Context->StateChange = VS_FALSE;

  for (i = 0; i < Context->nNofStateMachines; i++)
  {
    if (Context->pWSV[i] != STATE_UNDEFINED)
    {
#if defined SEM_NEXT_STATE_CHG || defined VISUALSTATE_EXPERT_DLL
      if (nextStateChg && Context->pCSV[i] != Context->pWSV[i])
        CC = SES_FOUND;
#endif
      Context->pCSV[i] = Context->pWSV[i];
      Context->pWSV[i] = STATE_UNDEFINED;
      Context->StateChange = VS_TRUE;
    }
  }
  Context->State = STATE_SEM_INITIALIZE;

  return CC;
}
