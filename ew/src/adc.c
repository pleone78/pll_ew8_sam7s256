// -----------------------------------------------------------------------------
//
// Headers
//
// -----------------------------------------------------------------------------
#include "AT91SAM7S256.h"
#define __inline inline
#include "lib_AT91SAM7S256.h"

#include "adc.h"
#include "deviceSetup.h"

#define MAX_ADC_VALUES 128
#define MAX_ADC_CHANNELS 8

static unsigned Values[MAX_ADC_CHANNELS][MAX_ADC_VALUES];    // valori campionati per 8 canali
static unsigned ValIndex[MAX_ADC_CHANNELS] = {0,0,0,0,0,0,0,0}; // puntatore in valori campionati per 8 canali
static unsigned ValLoaded[MAX_ADC_CHANNELS] = {0,0,0,0,0,0,0,0};// contatore di campioni letti dall'inizio per correggere media iniziale


// -----------------------------------------------------------------------------
//
// Inizializzazione porta ADC
//
// -----------------------------------------------------------------------------

void ADC_init ( void )
{

  // Enable ADC's Clock at PMC level

  AT91F_PMC_EnablePeriphClock ( AT91C_BASE_PMC, 1 << AT91C_ID_ADC );

  // Clear all previous setting and result

  AT91F_ADC_SoftReset ( AT91C_BASE_ADC );

  // Set up by using ADC Mode register

  AT91F_ADC_CfgModeReg ( AT91C_BASE_ADC, ADC_MODE );

}

// -----------------------------------------------------------------------------
//
// Conversione ADC
//
// -----------------------------------------------------------------------------

unsigned int ADC_read ( unsigned int channel, unsigned char n_read )
{
  unsigned int media = 0;
  unsigned char nn, OrdChn;
  // Abilita il canale
  switch (channel)
  {
    case CHANNEL0: OrdChn = 0; break;
    case CHANNEL1: OrdChn = 1; break;
    case CHANNEL2: OrdChn = 2; break;
    case CHANNEL3: OrdChn = 3; break;
    case CHANNEL4: OrdChn = 4; break;
    case CHANNEL5: OrdChn = 5; break;
    case CHANNEL6: OrdChn = 6; break;
    case CHANNEL7: OrdChn = 7; break;
    default: OrdChn = 0;
  }

  AT91F_ADC_EnableChannel (AT91C_BASE_ADC, channel);
  AT91F_ADC_StartConversion (AT91C_BASE_ADC);
  while (!((AT91F_ADC_GetStatus (AT91C_BASE_ADC)) & (channel)));

  if (n_read > (MAX_ADC_VALUES - 1))
  {
    n_read = MAX_ADC_VALUES - 1;
  }

  if (OrdChn < MAX_ADC_CHANNELS)
  {
    if (ValIndex[OrdChn] < MAX_ADC_VALUES)
    {
      Values[OrdChn][ValIndex[OrdChn]] = AT91F_ADC_GetLastConvertedData(AT91C_BASE_ADC);
      ValIndex[OrdChn]++;
      if (ValIndex[OrdChn] > n_read)
      {
        ValIndex[OrdChn] = 0;
      }
      if (ValLoaded[OrdChn] < n_read)
      {
        // limita media ai valori letti effettivamente se inferiori a n_read
        ValLoaded[OrdChn]++;
      }
      else
      {
        if (ValLoaded[OrdChn] > n_read)
        {
          // controllo di sicurezza
          ValLoaded[OrdChn] = 0;
        }
      }
      n_read = ValLoaded[OrdChn];
      if (n_read <= 0)
      {
        n_read = 1;
      }
      for (nn = 0; nn < n_read; nn++)
      {
        media += Values[OrdChn][nn];
      }
    }
  }
  AT91F_ADC_DisableChannel (AT91C_BASE_ADC, channel);
/*
  for (nn = 0; nn < n_read; nn++)
  {
    // Avvia la conversione

    AT91F_ADC_StartConversion (AT91C_BASE_ADC);

    // Attendi fine conversione

    while (!((AT91F_ADC_GetStatus (AT91C_BASE_ADC)) & (channel)));

    // Somma le misure

    media += AT91F_ADC_GetLastConvertedData(AT91C_BASE_ADC);
  }
*/
  media = media / n_read;
  return media;
}


