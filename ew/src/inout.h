// -----------------------------------------------------------------------------
//
// Headers
//
// -----------------------------------------------------------------------------

#ifndef inout_h
#define inout_h

#include <stdio.h>


#include "AT91SAM7S256.h"
#define __inline inline
#include "lib_AT91SAM7S256.h"

#define DEBSTD 120   // ~40ms = 40 * 1ms /3  1ms = tempo medio ciclo di progr.
                     // /3 perch� SerialInOut � chiamata 3 volte nel ciclo
#define IN_MAX  51+1   // Dimensione massima array degli ingressi
//#define OUT_MAX 48+1   // Dimensione massima array delle uscite
#define OUT_MAX 50+1   // Dimensione massima array delle uscite

#define INSER_MAX   48  // Dimensione massima array degli ingressi seriali
#define OUTSER_MAX  40  // Dimensione massima array delle uscite seriali

extern unsigned char INGRESSI[];
extern unsigned char USCITE[], UscitePuls[];
extern unsigned char OUT_ACT;
extern unsigned char T200,T300,T400;

// -----------------------------------------------------------------------------
//
//  Definizione prototipi delle funzioni
//
// -----------------------------------------------------------------------------
//
//  Funzioni per gli I/O seriali
//
// -----------------------------------------------------------------------------

#define DEBOUNCEVALUE 1000

void IO_init(void);
void SerialInOut(void);
void RunOut(unsigned char num,unsigned char blink,unsigned char trun);


#endif
