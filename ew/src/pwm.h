// -----------------------------------------------------------------------------

// Headers

// -----------------------------------------------------------------------------

#ifndef pwm_h
#define pwm_h

#include "AT91SAM7S256.h"
#define __inline inline
#include "lib_AT91SAM7S256.h"

// -----------------------------------------------------------------------------
// 
//  Definizione dei valori di prescaler del clock del PWM
//
//  MCKA e MCKB si possono usare solo a livello di canale PWM mentre gli altri 
//  si usano anche per definire MCKA e MCKB nel registro PWM_MR
// 
// -----------------------------------------------------------------------------

#define PWMC_PRE_MCK        0        
#define PWMC_PRE_MCK_2      1
#define PWMC_PRE_MCK_4      2
#define PWMC_PRE_MCK_8      3 
#define PWMC_PRE_MCK_16     4
#define PWMC_PRE_MCK_32     5
#define PWMC_PRE_MCK_64     6  
#define PWMC_PRE_MCK_128    7
#define PWMC_PRE_MCK_256    8
#define PWMC_PRE_MCK_512    9
#define PWMC_PRE_MCK_1024  10
#define PWMC_PRE_MCKA      11
#define PWMC_PRE_MCKB      12

#define PWMC_PRE_DIVA   ((unsigned int) 0x09 <<  0)       // Valori da 2 a 255
#define PWMC_PRE_DIVB   ((unsigned int) 0x07 <<  0)       // Valori da 2 a 255

#define PWM_PERIOD  ((unsigned int) 0x0710 <<  0)     // Valori da 2 a 8192
                                                          // 0x0710 + (23 canale default* PWM_CHANNEL_BASE) = 0x1E1E
                                                                 // periodo su flash 2.56 ms
                                                          // circa come canale 23 di M3 8.0 con 78CP14
#define PWM_DUTY50  ((unsigned int) 0x0388 <<  0)     // Encoder veloce
#define PWM_CHANNEL_BASE 256
#define PWM_CHANNEL_DEFAULT 23
#define PWM_CHANNEL_MAX 100


#define PWMC_CH0 0
#define PWMC_CH1 1
#define PWMC_CH2 2
#define PWMC_CH3 3

#define DirClose 0
#define DirOpen 1

// -----------------------------------------------------------------------------
// 
//  Prototipi
// 
// -----------------------------------------------------------------------------

void AT91F_PWMC_CfgClkAB(
        AT91PS_PWMC pPWM,
        unsigned int divA,
        unsigned int preA,
        unsigned int divB,
        unsigned int preB );

void  AT91F_PWMC_InitCh( 
        AT91PS_PWMC pPWM,
        unsigned int channelId,
        unsigned int mode,
        unsigned int duty,
        unsigned int period);

void AT91F_PWMC_StartCh( AT91PS_PWMC pPWM, unsigned int chNum );

void AT91F_PWMC_StopCh( AT91PS_PWMC pPWM, unsigned int chNum );
        
unsigned int AT91F_PWMC_GetIntStatus ( AT91PS_PWMC pPWM, unsigned int flag );

void PWM_init ( unsigned int System, unsigned int Period );

void AT91F_PWMC_CpolCh( AT91PS_PWMC pPWM, unsigned int chNum, unsigned char Cpol);

void SetStop( unsigned int chNum );

void SetPwm( unsigned int chNum, unsigned int chId, unsigned int chPh, unsigned int DirVal, unsigned int PwmVal, unsigned int Period );

#endif
