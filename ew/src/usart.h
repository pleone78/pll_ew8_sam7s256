// -----------------------------------------------------------------------------
//
//  Definizioni
//
// -----------------------------------------------------------------------------

#ifndef usart_h
#define usart_h

#include "AT91SAM7S256.h"
#define __inline inline
#include "lib_AT91SAM7S256.h"

#define MCK             47923200   // MCK (PLLRC div by 2)

#define DISPLAY_MODE_DEBUG 0
#define DISPLAY_MODE_M3 1

#define B_rate_9600      9600
#define B_rate_19200    19200
#define B_rate_38400    38400
#define B_rate_57600    57600
#define B_rate_115200  115200
#define B_rate_230400  230400

#define AT91C_US_ASYNC_MODE ( AT91C_US_USMODE_NORMAL + \
                              AT91C_US_NBSTOP_1_BIT + \
                              AT91C_US_PAR_NONE + \
                              AT91C_US_CHRL_8_BITS + \
                              AT91C_US_CLKS_CLOCK )

#define DISPLAY_SIZE 32
#define DISPLAY_SIZE_DBGU 60 //40

#define US1_INTERRUPT_LEVEL   7
#define DBGU_INTERRUPT_LEVEL  7





// -----------------------------------------------------------------------------
//
// Variabili
//
// -----------------------------------------------------------------------------
extern AT91PS_DBGU DBGU_pt;
extern unsigned char char_rx_DBGU;
extern unsigned DisplayMode;
extern char DisplayBuff[];
extern unsigned char OpenedTransmission;


// -----------------------------------------------------------------------------
//
//  Prototipi
//
// -----------------------------------------------------------------------------

void Usart_init ( unsigned int baudRate );

//void US_SendMessage(AT91PS_USART USpt, char *buffer);
void US_PutStr(AT91PS_USART USpt, char* buffer, char size);
void Row1(AT91PS_USART USpt, char *buffer);
void Row2(AT91PS_USART USpt, char *buffer);
void Fls2(AT91PS_USART USpt, char *buffer,unsigned char DispF);
void Dbg(AT91PS_USART USpt, char *buffer);
void US_SendCharDbg(AT91PS_USART USpt, char ch);
void US_SendCharDisplay(AT91PS_USART USpt, char ch);
void US_UpdateDisplay(AT91PS_USART USpt);
void US_UpdateDisplay2(AT91PS_USART USpt);
void US_SetModeDisplay(AT91PS_USART USpt);
void US_ResetModeDisplay(AT91PS_USART USpt);
void US_ClearDisplay(AT91PS_USART USpt);
void DisplayValue(unsigned int Value, char ch);
void DisplayValue2(unsigned int Value, char ch);
unsigned char Mirror(unsigned char ch);
void BinBcdAscii(unsigned int num);
void BinBcdAscii2(unsigned char num);
void US_SwitchDisplay(void);
unsigned char BinBcd2(unsigned char num);

void millisecondi(unsigned int millisec);
void microsecondi(unsigned int millisec);

#endif
