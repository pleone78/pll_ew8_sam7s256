// -----------------------------------------------------------------------------
//
// 	Inclusione headers
//
// -----------------------------------------------------------------------------

#include	"deviceSetup.h"
#include	"inout.h"
#include	"timer.h"

#ifdef  ORSINI
#define INSER_ACT   16  // Ingressi seriali reali
#define OUTSER_ACT  8   // Uscite seriali reali
#else
#define INSER_ACT   48  // Dimensione massima array degli ingressi seriali
#define OUTSER_ACT  40  // Dimensione massima array delle uscite seriali
#endif

#define TIMER_STEP_MS 880

#define TIMER_STEP_MS_CB 785


// -----------------------------------------------------------------------------
//
// 	Definizione variabili	
//
// -----------------------------------------------------------------------------

unsigned char INGRESSI[IN_MAX];  //  Variabile di lettura ingressi
unsigned char USCITE[OUT_MAX], UscitePuls[OUT_MAX];  //inizia da 1 per comaptibilita` con schema, da 41 a 49 uscite scheda motore
unsigned short InDeb0[IN_MAX],InDeb1[IN_MAX],DebVal[IN_MAX];
unsigned char IN_ACT, OUT_ACT;
unsigned int TIM_STEP = TIMER_STEP_MS_CB;

// -----------------------------------------------------------------------------
//
//  Inizializzazione I/O
//
// -----------------------------------------------------------------------------

void IO_init(void)
{
	unsigned char	nn;
  extern unsigned int TIM_STEP;

  AT91F_PMC_EnablePeriphClock ( AT91C_BASE_PMC, 1 << AT91C_ID_PIOA ) ;

  AT91F_PIO_CfgOutput( AT91C_BASE_PIOA,OE_OUT) ;
  AT91F_PIO_SetOutput( AT91C_BASE_PIOA,OE_OUT) ;
  AT91F_PIO_CfgOutput( AT91C_BASE_PIOA,MASK_OUT) ;
  AT91F_PIO_ClearOutput( AT91C_BASE_PIOA,MASK_OUT) ;

  AT91F_PIO_CfgInput( AT91C_BASE_PIOA,MASK_IN) ;

  IN_ACT  = INSER_MAX;
  OUT_ACT = OUTSER_MAX;

	for (nn = 1; nn < OUT_MAX; nn++)												
	{
    USCITE[nn] = 0;
	}
			
  AT91F_PIO_ClearOutput( AT91C_BASE_PIOA,OE_OUT);

	for (nn = 1; nn < IN_MAX; nn++)	 // Inizializzazione array ingressi	e debounce									
	{
	  INGRESSI[nn] = 0;
	  InDeb0[nn] = 0;
	  InDeb1[nn] = 0;
      DebVal[nn] = 0;
	}

  INGRESSI[15] = 1;
  INGRESSI[16] = 1;
  	
  SerialInOut();

  if ((INGRESSI[15] == 0) && (INGRESSI[16] == 0))
  {
    IN_ACT = 32;
    OUT_ACT = 24;
    TIM_STEP = TIMER_STEP_MS;
  }

  if (INSER_ACT <32)
  {
    IN_ACT = INSER_ACT;
    OUT_ACT = OUTSER_ACT;
  }

	for (nn = 1; nn < IN_MAX; nn++)	 // Inizializzazione array ingressi	e debounce									
	{  	
	  InDeb0[nn] = DEBSTD;
	  InDeb1[nn] = DEBSTD;
      DebVal[nn] = DEBSTD;
	}

// -----------------------------------------------------------------------------
//  Inserire qui eventuali valori diversi di debounce
//  L'esempio che segue � per il sensore atrio pari a 500ms (*3 perch� la
//  SerialInOut() � chiamata 3 volte in un ciclo di programma da 1ms
// -----------------------------------------------------------------------------

  InDeb0[ATR] = 1500;
  InDeb1[ATR] = 1500;
  DebVal[ATR] = 1500;

  InDeb0[SS1] = 900;
  InDeb1[SS1] = 900;
  DebVal[SS1] = 900;

  InDeb0[SP1] = 900;
  InDeb1[SP1] = 900;
  DebVal[SP1] = 900;

  InDeb0[SS2] = 900;
  InDeb1[SS2] = 900;
  DebVal[SS2] = 900;

  InDeb0[SP2] = 900;
  InDeb1[SP2] = 900;
  DebVal[SP2] = 900;

  InDeb0[SS1_CB] = 900;
  InDeb1[SS1_CB] = 900;
  DebVal[SS1_CB] = 900;

  InDeb0[SP1_CB] = 900;
  InDeb1[SP1_CB] = 900;
  DebVal[SP1_CB] = 900;

  InDeb0[SS2_CB] = 900;
  InDeb1[SS2_CB] = 900;
  DebVal[SS2_CB] = 900;

  InDeb0[SP2_CB] = 900;
  InDeb1[SP2_CB] = 900;
  DebVal[SP2_CB] = 900;
  
  InDeb0[SW_AFT_LB_ST] = 1500;
  InDeb1[SW_AFT_LB_ST] = 1500;
  DebVal[SW_AFT_LB_ST] = 1500;
}

// -----------------------------------------------------------------------------
//
// 	Lettura e scrittura I/O serializzati
//
// -----------------------------------------------------------------------------

void	SerialInOut(void)
{
    unsigned char	nn;

    AT91F_PIO_SetOutput( AT91C_BASE_PIOA,ST_IN );			    //  Latch INGRESSI
    __no_operation();
    AT91F_PIO_ClearOutput( AT91C_BASE_PIOA,ST_IN );		        //
    AT91F_PIO_ClearOutput( AT91C_BASE_PIOA,PL_IN );		        //  Parallel load
    __no_operation();
    __no_operation();
    __no_operation();
    AT91F_PIO_SetOutput( AT91C_BASE_PIOA,PL_IN );			    //
    __no_operation();

    for (nn = INSER_MAX; nn >= (INSER_MAX-IN_ACT+1); nn--) {     //  Leggi 6� byte ingressi		
        if ((AT91F_PIO_GetInput(AT91C_BASE_PIOA) & DS_IN ) == DS_IN ){
            InDeb1[nn] = DebVal[nn];
            if (InDeb0[nn] !=0) {
                InDeb0[nn]--;
            }
            else {
                INGRESSI[nn] = 0;                           //  Se INPUT = 1
            }
        }
        else {
            InDeb0[nn] = DebVal[nn];
            if (InDeb1[nn] !=0) {
                InDeb1[nn]--;
            }
            else {
                INGRESSI[nn] = 1;                           //  Se INPUT = 0
            }
        }
        if (nn >(INSER_MAX-OUT_ACT+1)) {
            switch (USCITE[nn-(INSER_MAX-OUT_ACT)]) {
                case 0:
                    AT91F_PIO_ClearOutput( AT91C_BASE_PIOA,DS_OUT );
                    break;
                case 1:
                    AT91F_PIO_SetOutput( AT91C_BASE_PIOA,DS_OUT );
                    break;
                case 2:
                    if (T200 != 0) {
                        AT91F_PIO_SetOutput( AT91C_BASE_PIOA,DS_OUT );
                    }
                    else {
                        AT91F_PIO_ClearOutput( AT91C_BASE_PIOA,DS_OUT );
                    }
                    break;
                case 3:                                               //zuck
                    if (T300 != 0) {
                        AT91F_PIO_SetOutput( AT91C_BASE_PIOA,DS_OUT );
                    }
                    else {
                        AT91F_PIO_ClearOutput( AT91C_BASE_PIOA,DS_OUT );
                    }
                    break;
                case 4:
                    if (T400 != 0) {
                        AT91F_PIO_SetOutput( AT91C_BASE_PIOA,DS_OUT );
                    }
                    else {
                        AT91F_PIO_ClearOutput( AT91C_BASE_PIOA,DS_OUT );
                    }
                    break;
                default:
                    AT91F_PIO_ClearOutput( AT91C_BASE_PIOA,DS_OUT );
                    break;
            }

            if ((USCITE[nn-(INSER_MAX-OUT_ACT)]) >127) {
                AT91F_PIO_SetOutput( AT91C_BASE_PIOA,DS_OUT );
                if (UscitePuls[nn-(INSER_MAX-OUT_ACT)] == 0) {
                    UscitePuls[nn-(INSER_MAX-OUT_ACT)] = USCITE[nn-(INSER_MAX-OUT_ACT)] - 127;
                }
            }
  		
            AT91F_PIO_SetOutput( AT91C_BASE_PIOA,SH_IO );		//  Impulso di shift
            __no_operation();
            AT91F_PIO_ClearOutput( AT91C_BASE_PIOA,SH_IO );	//    		
        }

        else if (nn == (INSER_MAX-OUT_ACT)+1) {	
            if ((USCITE[nn-(INSER_MAX-OUT_ACT)] == 1)) {
                AT91F_PIO_SetOutput( AT91C_BASE_PIOA,DS_OUT );			//  Scrivi 1			
            }
            else {
                AT91F_PIO_ClearOutput( AT91C_BASE_PIOA,DS_OUT );		//  Scrivi 0			
            }

            AT91F_PIO_SetOutput( AT91C_BASE_PIOA,SH_IO );		//  Impulso di shift
            __no_operation();
            AT91F_PIO_ClearOutput( AT91C_BASE_PIOA,SH_IO );	//

            AT91F_PIO_SetOutput( AT91C_BASE_PIOA,ST_OUT );					//  Impulso di latch
            __no_operation();
            AT91F_PIO_ClearOutput( AT91C_BASE_PIOA,ST_OUT );	
        }
        else {		
            AT91F_PIO_SetOutput( AT91C_BASE_PIOA,SH_IO );		//  Impulso di shift
            __no_operation();
            AT91F_PIO_ClearOutput( AT91C_BASE_PIOA,SH_IO );	//
        }      	
    }
    if ((AT91F_PIO_GetInput(AT91C_BASE_PIOA) & PROX ) == PROX )	{
        InDeb1[AYPROX] = DebVal[AYPROX];
        if (InDeb0[AYPROX] !=0) {
            InDeb0[AYPROX]--;
        }
        else {
            INGRESSI[AYPROX] = 0;                           //  Se INPUT = 1
        }
    }
    else {
        InDeb0[AYPROX] = DebVal[AYPROX];
        if (InDeb1[AYPROX] !=0) {
            InDeb1[AYPROX]--;
        }
        else {
            INGRESSI[AYPROX] = 1;                           //  Se INPUT = 0
        }
    }
    if ((AT91F_PIO_GetInput(AT91C_BASE_PIOA) & INT_SPI ) == INT_SPI ) {
        InDeb1[AYINT_SPI] = DebVal[AYINT_SPI];
        if (InDeb0[AYINT_SPI] !=0) {
            InDeb0[AYINT_SPI]--;
        }
        else {
            INGRESSI[AYINT_SPI] = 1;                           //  Se INPUT = 1
        }
    }
    else {
        InDeb0[AYINT_SPI] = DebVal[AYINT_SPI];
        if (InDeb1[AYINT_SPI] !=0) {
            InDeb1[AYINT_SPI]--;
        }
        else {
            INGRESSI[AYINT_SPI] = 0;                           //  Se INPUT = 0
        }
    }
}

// -----------------------------------------------------------------------------
//
// 	Blink uscite serializzate
//
//  La routine agisce sull'uscita num:
//
//  trun = 0  blink = 0  --> uscita = 0;
//  trun = 0  blink = 1  --> uscita = 1;
//  trun = 0  blink = 2  --> uscita = blink 100-100 ms;
//  trun = 0  blink = 3  --> uscita = blink 200-200 ms;
//  trun = 0  blink = 4  --> uscita = blink 400-400 ms;
//  trun = nn blink = 0  --> uscita = 0 per nn*100 ms;
//  trun = nn blink = 1  --> uscita = 1 per nn*100 ms;
//
// -----------------------------------------------------------------------------

void RunOut(unsigned char num,unsigned char blink,unsigned char trun)

{
  // se per una uscita e` usata RunOut non e` possibile settare o resettare direttamente
  // ma occorre chiamare RunOut( numero uscita, 0 oppure 1, 0)
  AT91F_AIC_DisableIt (AT91C_BASE_AIC, AT91C_ID_TC2);

  if ((trun !=0) && (blink < 2))
  {
    UscitePuls[num] = trun;
  }
  else
  {
    UscitePuls[num] = 0;
  }
  USCITE[num] = blink;

  AT91F_AIC_EnableIt (AT91C_BASE_AIC, AT91C_ID_TC2);
}

