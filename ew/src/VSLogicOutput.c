/*****************************************************************************
* IAR visualSTATE action functions and error handler.
*****************************************************************************/
/* *** include directives *** */
#include "AT91SAM7S256.h"
#define __inline inline
#include "lib_AT91SAM7S256.h"
#include <ctype.h>
#include "VSLogicData.h"
#include "VSLogicAction.h"

#include "usart.h"
#include "usart1.h"

#include "VSLogicMain.h"
#include "deviceSetup.h"
#include "VSLogicOutput.h"
#include "VSDoorsOutput.h"//da rivedere per pilotare porta
#include "inout.h"
#include "biometrico.h"
#include "spi.h"
#include "twi.h"

#define OUT_OFFSET 30
#define OFFSET_CFG_COPY 128

unsigned int DisTempCnt = 0;
unsigned char DayM = 0;

const unsigned char aFmwV1 = 0, aFmwV2 = 1;
extern volatile unsigned char SysTimeCount;


/* *** function declarations *** */
//Leone
extern void UnhookDoor(void);
extern void HookupDoor(void);
extern void BrakeMot(unsigned char);
extern void WildMot(unsigned char);
/* *** function definitions *** */

VS_VOID Dis (VS_INT MsgId)
{
  LogicMsgId = MsgId;
  FlashOn = 0;
  if ((bDisPrg==0) && (DisTempCnt==0))
    {
    LogicDisplay(MsgId);
    LastMsgIdSys[Sys] = MsgId;
    }
}

VS_VOID DisTemp (VS_INT MsgId, VS_INT Ticks)
{
	DisTempCnt = Ticks;
  FlashOn = 0;
  if (bDisPrg==0)
    {
      LogicDisplay(MsgId);
      LastMsgIdSys[Sys] = MsgId;
    }
}

VS_VOID DisTempClr (VS_VOID)
{
	DisTempCnt = 1;  
}

VS_VOID DisPrg (VS_INT MsgId)
{
  FlashOn = 0;
  if (bDisPrg==1)
  {
    LogicDisplay(MsgId);
    LastMsgIdSys[Sys] = MsgId;
  }
}

VS_VOID DisPrgF (VS_INT MsgId)
{
  if (bDisPrg==1)
  {
    LogicDisplay(MsgId);
    LastMsgIdSys[Sys] = MsgId;
    FlashOn = 1;
  }
}

VS_VOID Open1(VS_VOID)
{
  MotSys[0].bOpen = 1;//usare SetCp di VSMain o VSDoorsOutputs e spostare Cp e detected su struttura
  SEQ_AddEvent(eCmdOpen1,Sys);
}
	
VS_VOID Close1(VS_VOID)
{
  MotSys[0].bOpen = 0;
}

VS_VOID Open2(VS_VOID)
{
  MotSys[1].bOpen = 1;//usare SetCp di VSMain o VSDoorsOutputs e spostare Cp e detected su struttura
  SEQ_AddEvent(eCmdOpen2,Sys);
}
	
VS_VOID Close2(VS_VOID)
{
  MotSys[1].bOpen = 0;
}

/* //Utilizzata in prima versione gestione semafori interni dove era necessario conoscere a quale
   //delle due porte � associata l'anta manuale.
VS_VOID LoadMotors(VS_VOID)
{
   Motor1 = MotSys[0].Motor;
   Motor2 = MotSys[1].Motor;
}
*/


VS_VOID LoadMotors(VS_VOID)
{
  if ( MotSys[0]._Motor!=0 && MotSys[1]._Motor==0)
    OnlyM2Off=1;
  else   OnlyM2Off=0;

}

VS_VOID Unhook (VS_VOID)
{
    UnhookDoor();
}

VS_VOID Hookup (VS_VOID)
{
    HookupDoor();
}

VS_VOID T0 (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys, 0);
  TIMER_SW_start(event, ticks, Sys, 0);
}

VS_VOID T1 (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys,1);
  TIMER_SW_start(event, ticks, Sys, 1);
}

VS_VOID T2 (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys,2);
  TIMER_SW_start(event, ticks, Sys, 2);
}

VS_VOID TMBtn1 (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys, 3);
  TIMER_SW_start(event, ticks, Sys, 3);
}

VS_VOID TMBtn2 (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys, 4);
  TIMER_SW_start(event, ticks, Sys, 4);
}

VS_VOID TBtn1 (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys, 5);
  TIMER_SW_start(event, ticks, Sys, 5);
}

VS_VOID TBtn2 (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys, 6);
  TIMER_SW_start(event, ticks, Sys, 6);
}

VS_VOID TMPlayBtn (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys, 7);
  TIMER_SW_start(event, ticks, Sys, 7);
}

VS_VOID TBio (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys, 8);
  TIMER_SW_start(event, ticks, Sys, 8);
}

VS_VOID TBioRec (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys, 9);
  TIMER_SW_start(event, ticks, Sys, 9);
}

VS_VOID TProg (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys, 10);
  TIMER_SW_start(event, ticks, Sys, 10);
}

VS_VOID TCount (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys, 11);
  TIMER_SW_start(event, ticks, Sys, 11);
}

VS_VOID TCns1 (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys, 12);
  TIMER_SW_start(event, ticks, Sys, 12);
}

VS_VOID TClose (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys, 13);
  TIMER_SW_start(event, ticks, Sys, 13);
}

VS_VOID T3 (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys,14);
  TIMER_SW_start(event, ticks, Sys, 14);
}

VS_VOID TVip (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys,15);
  TIMER_SW_start(event, ticks, Sys, 15);
}

VS_VOID TCns2 (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys, 16);
  TIMER_SW_start(event, ticks, Sys, 16);
}

VS_VOID TCns3 (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys, 17);
  TIMER_SW_start(event, ticks, Sys, 17);
}

VS_VOID TSession (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys, 18);
  TIMER_SW_start(event, ticks, Sys, 18);
}

VS_VOID TPolTr (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys, 19);
  TIMER_SW_start(event, ticks, Sys, 19);
}

VS_VOID TService (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys, 20);
  TIMER_SW_start(event, ticks, Sys, 20);
}

VS_VOID TOnePass (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys, 21);
  TIMER_SW_start(event, ticks, Sys, 21);
}

VS_VOID TReady (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys, 22);
  TIMER_SW_start(event, ticks, Sys, 22);
}

VS_VOID TReserv (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys, 23);
  TIMER_SW_start(event, ticks, Sys, 23);
}

VS_VOID TVisEx (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys,24);
  TIMER_SW_start(event, ticks, Sys, 24);
}

VS_VOID TSwALB (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys,25);
  TIMER_SW_start(event, ticks, Sys, 25);
}

VS_VOID ST0 (VS_VOID)
{
  TIMER_SW_stop(Sys, 0);
}

VS_VOID ST1 (VS_VOID)
{
  TIMER_SW_stop(Sys, 1);
}

VS_VOID ST2 (VS_VOID)
{
  TIMER_SW_stop(Sys, 2);
}

VS_VOID STMBtn1 (VS_VOID)
{
  TIMER_SW_stop(Sys, 3);
}

VS_VOID STMBtn2 (VS_VOID)
{
  TIMER_SW_stop(Sys, 4);
}

VS_VOID STBtn1 (VS_VOID)
{
  TIMER_SW_stop(Sys, 5);
}

VS_VOID STBtn2 (VS_VOID)
{
  TIMER_SW_stop(Sys, 6);
}

VS_VOID STMPlayBtn (VS_VOID)
{
  TIMER_SW_stop(Sys, 7);
}

VS_VOID STBio (VS_VOID)
{
  TIMER_SW_stop(Sys, 8);
}

VS_VOID STBioRec (VS_VOID)
{
  TIMER_SW_stop(Sys, 9);
}

VS_VOID STProg (VS_VOID)
{
  TIMER_SW_stop(Sys, 10);
}

VS_VOID STCount (VS_VOID)
{
  TIMER_SW_stop(Sys, 11);
}

VS_VOID STCns1 (VS_VOID)
{
  TIMER_SW_stop(Sys, 12);
}

VS_VOID STTClose (VS_VOID)
{
  TIMER_SW_stop(Sys, 13);
}

VS_VOID ST3 (VS_VOID)
{
  TIMER_SW_stop(Sys, 14);
}

VS_VOID STVip (VS_VOID)
{
  TIMER_SW_stop(Sys, 15);
}

VS_VOID STCns2 (VS_VOID)
{
  TIMER_SW_stop(Sys, 16);
}

VS_VOID STCns3 (VS_VOID)
{
  TIMER_SW_stop(Sys, 17);
}

VS_VOID STSession (VS_VOID)
{
  TIMER_SW_stop(Sys, 18);
}

VS_VOID STPolTr (VS_VOID)
{
  TIMER_SW_stop(Sys, 19);
}

VS_VOID STService (VS_VOID)
{
  TIMER_SW_stop(Sys, 20);
}

VS_VOID STOnePass (VS_VOID)
{
  TIMER_SW_stop(Sys, 21);
}

VS_VOID STReady (VS_VOID)
{
  TIMER_SW_stop(Sys, 22);
}

VS_VOID STReserv (VS_VOID)
{
  TIMER_SW_stop(Sys, 23);
}

VS_VOID STVisEx (VS_VOID)
{
  TIMER_SW_stop(Sys, 24);
}

VS_VOID ChkPresent(VS_VOID)
{
    if(SimulPc==0){
        if (INGRESSI[ATR] == 0)

            SEQ_AddEvent(eNotPresent,Sys);
        else

            SEQ_AddEvent(ePresent,Sys);
    }
}

VS_VOID ChkClosed1(VS_VOID)
{
    if (USCITE[PC1] == 0)
        SEQ_AddEvent(eNotClosed1,Sys);
    else
        SEQ_AddEvent(eClosed1,Sys);
}

VS_VOID ChkClosed2(VS_VOID)
{
    if (USCITE[PC2] == 0)
        SEQ_AddEvent(eNotClosed2,Sys);
    else
        SEQ_AddEvent(eClosed2,Sys);
}


VS_VOID ChkOpened1(VS_VOID)
{
    if (USCITE[PA1] == 1)
        SEQ_AddEvent(eOpened1,Sys);
}

VS_VOID ChkOpened2(VS_VOID)
{
    if (USCITE[PA2] == 1)
        SEQ_AddEvent(eOpened2,Sys);
}

VS_VOID ChkService(VS_VOID)
{
    if (INGRESSI[SERVICE] == 1)
        SEQ_AddEvent(eServiceOn,Sys);
    else
        SEQ_AddEvent(eServiceOff,Sys);
}

VS_VOID ChkFingPres(VS_VOID)
{
    if (RxBiom[5] == 0x31)
        SEQ_AddEvent(eFingPres,Sys);
    else
        SEQ_AddEvent(eFingNotPres,Sys);
}

VS_VOID ChkBioAmp(VS_VOID)
{
    if (RxBiom[4] == 0x31)
        SEQ_AddEvent(ePCBioAmpOn,Sys);
    else
        SEQ_AddEvent(ePCBioAmpOff,Sys);
}

VS_VOID ChkABT1Sens(VS_VOID)
{
    if ((INGRESSI[ANTI_BT1] == 0) && (ABT1 == 1))
        SEQ_AddEvent(eABT1Off,Sys);
    else if ((INGRESSI[ANTI_BT1] == 1) && (ABT1 == 1))
        SEQ_AddEvent(eABT1On,Sys);
}

VS_VOID ChkABT2Sens(VS_VOID)
{
    if ((INGRESSI[ANTI_BT2] == 0) && (ABT2 == 1))
        SEQ_AddEvent(eABT2Off,Sys);
    else if ((INGRESSI[ANTI_BT2] == 1) && (ABT2 == 1))
        SEQ_AddEvent(eABT2On,Sys);
}

VS_VOID ChkSwAftLowBStat(VS_VOID)
{
    
	if (INGRESSI[SW_AFT_LB_ST] == 0)		
		SEQ_AddEvent(eResAftLowBStat,Sys);
	else
		SEQ_AddEvent(eSetAftLowBStat,Sys);
	
}


VS_VOID BrakeMot1(VS_VOID)
{
    BrakeMot(1);
}

VS_VOID WildMot1(VS_VOID)
{
    WildMot(1);
}

VS_VOID BrakeMot2(VS_VOID)
{
    BrakeMot(2);
}

VS_VOID WildMot2(VS_VOID)
{
    WildMot(2);
}

/*
 if (RxBiom[4] == 0x31){
      if (catchBIOAMP == 0){
          catchBIOAMP = 1;
          SEQ_AddEvent(ePCBioAmpOn,Sys);
      }
  }
  else{
      if (catchBIOAMP == 1){
          catchBIOAMP = 0;
          SEQ_AddEvent(ePCBioAmpOff,Sys);
      }
  }
*/


VS_VOID Normal(VS_VOID)
{
  D_EMERGp = 0;
  if (AIn == 1)
  {
    USCITE[LAMP_IN] = 1;
  }
  else
  {
    USCITE[LAMP_IN] = 0;
  }
  if (AOut == 1)
  {
    USCITE[LAMP_OUT] = 1;
  }
  else
  {
    USCITE[LAMP_OUT] = 0;
  }
}

VS_VOID Auto(VS_VOID)
{
  D_MANUp = 0;
  USCITE[IMPOFF] = 0;
  USCITE[IMPOFF_CB] = 0;
  USCITE[MAN_AUTO] = 0;
  bManu = 0;
}

VS_VOID Manual(VS_VOID)
{
  D_MANUp = 1;
  USCITE[IMPOFF] = 0;
  USCITE[IMPOFF_CB] = 0;
  USCITE[MAN_AUTO] = 1;
  bManu = 1;
}

VS_VOID GetStrokeEntity(VS_VOID)
{
	FullStroke = 0;
	if(MotSys[0].StrokeShk == 100 && MotSys[1].StrokeShk == 100){
		FullStroke = 1;
	}
		
}

VS_VOID Emergency(VS_VOID)
{
  D_EMERGp = 1;
  MotSys[0]._bEmergency = 1;
  MotSys[1]._bEmergency = 1;
}

VS_VOID EmergencyR(VS_VOID)
{
  D_EMERGp = 0;
  MotSys[0]._bEmergency = 0;
  MotSys[1]._bEmergency = 0;
}

VS_VOID OkSupplyNotify(VS_VOID)
{	
	MotSys[0]._bLowBatt = 0;
	MotSys[1]._bLowBatt = 0;	
}

VS_VOID LowSupplyNotify(VS_VOID)
{	
	MotSys[0]._bLowBatt = 1;
	MotSys[1]._bLowBatt = 1;	
}


VS_VOID On(VS_VOID)
{
  LedsConsoleVisible = 1;
  SimulPc = 0;
  SimulPcMoreFlag = 0;
  SimulPcObjectFlag = 0;
}

VS_VOID Off(VS_VOID)
{
  D_MANUp = 0;
  D_EMERGp = 0;
  USCITE[IMPOFF] = 1;
  USCITE[IMPOFF_CB] = 1;
  USCITE[MAN_AUTO] = 0;
  LedsConsoleVisible = 0;
}

VS_VOID ForceOnOff(VS_VOID)
{
    SEQ_AddEvent(eOnOff,Sys);
}
 

VS_VOID ForceAuto(VS_VOID)
{	
	SEQ_AddEvent(eAuto,Sys);
}

VS_VOID ForceManual(VS_VOID)
{	
		SEQ_AddEvent(eAuto,Sys);    
}

VS_VOID OffError(VS_VOID)
{

}

VS_VOID SetDinDon(VS_VOID)
{
  D_SUOp = 1;
}

VS_VOID ResDinDon(VS_VOID)
{
  D_SUOp = 0;
}


VS_VOID BusyVerify(VS_VOID)
{
  RunOut(AV1, 0, 0);
  RunOut(AT1, 1, 0);
  RunOut(AV2, 0, 0);
  RunOut(AT2, 1, 0);
  if ((USCITE[LP1]>0)&&(USCITE[LP1]<OUT_OFFSET))
    USCITE[LP1] = USCITE[LP1] + OUT_OFFSET;
  if ((USCITE[LP2]>0)&&(USCITE[LP2]<OUT_OFFSET))
    USCITE[LP2] = USCITE[LP2] + OUT_OFFSET;
}

VS_VOID Busy(VS_VOID)
{
  if (AIn == 1)
  {
    RunOut(AV1, 0, 0);
    RunOut(AT1, 4, 0);
  }
  if (AOut == 1)
  {
    RunOut(AV2, 0, 0);
    RunOut(AT2, 4, 0);
  }
  if ((USCITE[LP1]>0)&&(USCITE[LP1]<OUT_OFFSET))
    USCITE[LP1] = USCITE[LP1] + OUT_OFFSET;
  if ((USCITE[LP2]>0)&&(USCITE[LP2]<OUT_OFFSET))
    USCITE[LP2] = USCITE[LP2] + OUT_OFFSET;
}

VS_VOID BusySod(VS_VOID)
{
  if (AIn == 1)
  {
    RunOut(AV1, 0, 0);
    RunOut(AT1, 2, 0);
  }
  if (AOut == 1)
  {
    RunOut(AV2, 0, 0);
    RunOut(AT2, 2, 0);
  }
  if ((USCITE[LP1]>0)&&(USCITE[LP1]<OUT_OFFSET))
    USCITE[LP1] = USCITE[LP1] + OUT_OFFSET;
  if ((USCITE[LP2]>0)&&(USCITE[LP2]<OUT_OFFSET))
    USCITE[LP2] = USCITE[LP2] + OUT_OFFSET;
}

VS_VOID GoIn(VS_VOID)
{
  RunOut(AV1, 4, 0);
  RunOut(AT1, 0, 0);
  RunOut(AV2, 0, 0);
  RunOut(AT2, 4, 0);
  if ((USCITE[LP1]>0)&&(USCITE[LP1]<OUT_OFFSET))
    USCITE[LP1] = USCITE[LP1] + OUT_OFFSET;
  if ((USCITE[LP2]>0)&&(USCITE[LP2]<OUT_OFFSET))
    USCITE[LP2] = USCITE[LP2] + OUT_OFFSET;
}

VS_VOID GoOut(VS_VOID)
{
  RunOut(AV1, 0, 0);
  RunOut(AT1, 4, 0);
  RunOut(AV2, 4, 0);
  RunOut(AT2, 0, 0);
  if ((USCITE[LP1]>0)&&(USCITE[LP1]<OUT_OFFSET))
    USCITE[LP1] = USCITE[LP1] + OUT_OFFSET;
  if ((USCITE[LP2]>0)&&(USCITE[LP2]<OUT_OFFSET))
    USCITE[LP2] = USCITE[LP2] + OUT_OFFSET;
}

VS_VOID ResWG12(VS_VOID)
{
  RunOut(AV1, 0, 0);
  RunOut(AT1, 0, 0);
  RunOut(AV2, 0, 0);
  RunOut(AT2, 0, 0);
  if (USCITE[LP1]>OUT_OFFSET)
    RunOut(LP1, USCITE[LP1]-OUT_OFFSET, 0);
  if (USCITE[LP2]>OUT_OFFSET)
    RunOut(LP2, USCITE[LP2]-OUT_OFFSET, 0);
}

VS_VOID SetW1(VS_VOID)
{
  RunOut(AT1, 4, 0);
  RunOut(AV1, 0, 0);
  if ((USCITE[LP1]>0)&&(USCITE[LP1]<OUT_OFFSET))
    USCITE[LP1] = USCITE[LP1] + OUT_OFFSET;
}

VS_VOID ResW1(VS_VOID)
{
  RunOut(AT1, 0, 0);
  if (USCITE[LP1]>OUT_OFFSET)
    RunOut(LP1, USCITE[LP1]-OUT_OFFSET, 0);
}

VS_VOID SetW2(VS_VOID)
{
  RunOut(AT2, 4, 0);
  RunOut(AV2, 0, 0);
  if ((USCITE[LP2]>0)&&(USCITE[LP2]<OUT_OFFSET))
    USCITE[LP2] = USCITE[LP2] + OUT_OFFSET;
}

VS_VOID ResW2(VS_VOID)
{
  RunOut(AT2, 0, 0);
  if (USCITE[LP2]>OUT_OFFSET)
    RunOut(LP2, USCITE[LP2]-OUT_OFFSET, 0);
}

VS_VOID SetG1(VS_VOID)
{
  RunOut(AV1, 4, 0);
  RunOut(AT1, 0, 0);
  if ((USCITE[LP1]>OUT_OFFSET)&&(USCITE[AV1]==0))
    RunOut(LP1, USCITE[LP1]-OUT_OFFSET, 0);
}

VS_VOID ResG1(VS_VOID)
{
  RunOut(AV1, 0, 0);
  if (USCITE[LP1]>OUT_OFFSET)
    RunOut(LP1, USCITE[LP1]-OUT_OFFSET, 0);
}

VS_VOID SetG2(VS_VOID)
{
  RunOut(AV2, 4, 0);
  RunOut(AT2, 0, 0);
  if ((USCITE[LP2]>OUT_OFFSET)&&(USCITE[AV2]==0))
    RunOut(LP2, USCITE[LP2]-OUT_OFFSET, 0);
}

VS_VOID ResG2(VS_VOID)
{
  RunOut(AV2, 0, 0);
  if (USCITE[LP2]>OUT_OFFSET)
    RunOut(LP2, USCITE[LP2]-OUT_OFFSET, 0);
}

VS_VOID SetWI(VS_VOID)
{
#ifndef DEBUG_LEONE
  RunOut(AT, 4, 0);
  RunOut(AV, 0, 0);
#endif
}

VS_VOID SetGI(VS_VOID)
{
#ifndef DEBUG_LEONE	
  RunOut(AT, 0, 0);
  RunOut(AV, 4, 0);
#endif  
}

VS_VOID ResWI(VS_VOID)
{
#ifndef DEBUG_LEONE
    RunOut(AT, 1, 0);
#endif
}

VS_VOID ResWGI(VS_VOID)
{
#ifndef DEBUG_LEONE
    RunOut(AT, 0, 0);
    RunOut(AV, 0, 0);
#endif
}

VS_VOID ChkObject(VS_VOID)
{
    if(SimulPc==0){       //leone
        if ( INGRESSI[OBJECT] == 1 )
            SEQ_AddEvent(eObject,Sys);
    }
    else {
        if(SimulPcObjectFlag == 1)
            SEQ_AddEvent(eObject,Sys);
    }
}

VS_VOID ChkEmergency(VS_VOID)
{
    if ( EMERGp != 0 )
        SEQ_AddEvent(eEmergency,Sys);
    if ( INGRESSI[EMERGrem] != 0 )
        SEQ_AddEvent(eEmergency,Sys);
    if ( INGRESSI[FIRE] != 0 )
        SEQ_AddEvent(eFireAlm,Sys);

}

VS_VOID SetResetSod(VS_VOID)
{
  USCITE[RESSOD] = 1;
}

VS_VOID ResResetSod(VS_VOID)
{
  USCITE[RESSOD] = 0;
}

VS_VOID ResStartBio(VS_VOID)
{
  StopBiom();
}

VS_VOID SetStartBio(VS_VOID)
{
  StartBiom();
}

VS_VOID ResStartBioCnb(VS_VOID)
{
  USCITE[START_BIO] = 0;
}

VS_VOID SetStartBioCnb(VS_VOID)
{
  USCITE[START_BIO] = 1;
}

VS_VOID ChkMore(VS_VOID)
{
  if(SimulPc==0){       //leone
    if ( INGRESSI[MORE] == 0 )
      SEQ_AddEvent(eNotMore,Sys);
    else
      SEQ_AddEvent(eMore,Sys);
  }
  else {
    if(SimulPcMoreFlag == 0)
      SEQ_AddEvent(eNotMore,Sys);
    else
      SEQ_AddEvent(eMore,Sys);
  }
}

VS_VOID ChkMetalOn(VS_VOID)
{
  if ( INIB_METp == 0 )
    catchINIB_METp = 0;
  else
    catchINIB_METp = 1;
}

VS_VOID ChkPCBio(VS_VOID)
{
  if ( INGRESSI[PCON] == 0 )
  {
    //catchPCON = 0;
    SEQ_AddEvent(ePCBioOff,Sys);
  }
  else
  {
    //catchPCON = 1;
    SEQ_AddEvent(ePCBioOn,Sys);
  }
}

VS_VOID MoreCBOn(VS_VOID)
{
  USCITE[MORE_CB] = 1;
}

VS_VOID MoreCBOff(VS_VOID)
{
  USCITE[MORE_CB] = 0;
}

VS_VOID Beep(VS_VOID)
{
  BeepRun = 1;
}

VS_VOID Beep2(VS_VOID)
{
  BeepRun = 1;
  SlowBeep = 1;
}

VS_VOID BeepR(VS_VOID)
{
  BeepRun = 0;
  SlowBeep = 0;
}

VS_VOID LedMetPres(VS_VOID)
{
  LedMetRun = 1;
}

VS_VOID LedMetPresR(VS_VOID)
{
  LedMetRun = 0;
}


VS_VOID SetBtn1(VS_VOID)
{
  if ((USCITE[AT1]==0)&&(USCITE[AV1]==0))
    RunOut(LP1, 1, 0);
  else
    RunOut(LP1, 31, 0);
}

VS_VOID SetBtn2(VS_VOID)
{
  if ((USCITE[AT2]==0)&&(USCITE[AV2]==0))
    RunOut(LP2, 1, 0);
  else
    RunOut(LP2, 31, 0);
}

VS_VOID FlashBtn1(VS_VOID)
{
  if ((USCITE[AT1]==0)&&(USCITE[AV1]==0))
    RunOut(LP1, 4, 0);
  else
    RunOut(LP1, 34, 0);
}

VS_VOID FlashBtn2(VS_VOID)
{
  if ((USCITE[AT2]==0)&&(USCITE[AV2]==0))
    RunOut(LP2, 4, 0);
  else
    RunOut(LP2, 34, 0);
}

VS_VOID ResBtn1(VS_VOID)
{
  RunOut(LP1, 0, 0);
  if ((USCITE[LP1]>OUT_OFFSET)&&(USCITE[AV1]==0))
    RunOut(LP1, USCITE[LP1]-OUT_OFFSET, 0);
}

VS_VOID ResBtn2(VS_VOID)
{
  RunOut(LP2, 0, 0);
  if ((USCITE[LP2]>OUT_OFFSET)&&(USCITE[AV2]==0))
    RunOut(LP2, USCITE[LP2]-OUT_OFFSET, 0);
}

VS_VOID StartPlay(VS_UCHAR NumMsg)
{
  PlayMsg(NumMsg);
}

VS_VOID StartRec(VS_UCHAR NumMsg)
{
  RecMsg(NumMsg);
}

VS_UCHAR ReadClock(VS_UCHAR Pos)
{
  return Clk_ReadSingle(Pos);
}

VS_VOID WriteClock(VS_UCHAR Pos)
{
  Clk_WriteSingle(Pos);
}

VS_VOID PCBioPulse(VS_VOID)
{
  RunOut(RESPC, 1, 20);
}

VS_VOID PCBioOn(VS_VOID)
{
  RunOut(RESPC, 1, 0);
}

VS_VOID PCBioOff(VS_VOID)
{
  RunOut(RESPC, 0, 0);
}

VS_VOID InibMetOn(VS_VOID)
{
  RunOut(INIB_MET, 1, 0);
}

VS_VOID InibMetOff(VS_VOID)
{
  RunOut(INIB_MET, 0, 0);
}

VS_VOID InPassOk(VS_VOID)
{
	RunOut(ING_OK,1,10);
	if(CN93_4AsIntTrLight == 0){
#ifndef DEBUG_LEONE  
		RunOut(AT,1,10);
#endif
	}
}

VS_VOID OutPassOk(VS_VOID)
{
	RunOut(USC_OK,1,10);
	if(CN93_4AsIntTrLight == 0){
#ifndef DEBUG_LEONE 
		RunOut(AV,1,10);
#endif
	}
}

VS_VOID ReadyOn(VS_VOID)
{
  USCITE[READY] = 1;
}

VS_VOID ReadyOff(VS_VOID)
{
  USCITE[READY] = 0;
}

VS_VOID ResetSimulPc(VS_VOID)    //leone
{
 SimulPc = 0;
 SimulPcMoreFlag = 0;
 SimulPcObjectFlag = 0;
}



VS_VOID DbgAVOn(VS_VOID)
{
#ifdef DEBUG_LEONE
  USCITE[AV]=1;     // Debug
#endif
}

VS_VOID DbgAVOff(VS_VOID)
{
#ifdef DEBUG_LEONE
  USCITE[AV]=0;     // Debug
#endif
}

VS_VOID DbgATOn(VS_VOID)
{
#ifdef DEBUG_LEONE
  USCITE[AT]=1;     // Debug
#endif
}

VS_VOID DbgATOff(VS_VOID)
{
#ifdef DEBUG_LEONE
  USCITE[AT]=0;     // Debug
#endif
}

// -----------------------------------------------------------------------------
//
//  Parametri di configurazione
//
//  I parametri di configurazione PLL vengono memorizzati nella EEPROM a partire
//  dalla locazione CfgAdd = 0x7F00 per la 24AA256
//
// -----------------------------------------------------------------------------

VS_UCHAR CfgR(VS_UCHAR Add)
{
  unsigned char Value;
  TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256 , CfgAdd+Add,(unsigned char *)&Value,1);
  return Value;
}

VS_VOID CfgW (VS_UCHAR Add, VS_UCHAR Val)
{
  TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256 , CfgAdd+Add,(unsigned char *)&Val, 1);
}


VS_VOID CfgWeek (VS_VOID)
{
  unsigned char CfgRead, CfgLanguage;

  CfgLanguage=CfgR(aLanguage);

  if (CfgLanguage==0)		//Italiano
  {
  	THolyD = 'F';
  	TWorkD = 'L';
  }
  else					//Inglese
  {
  	THolyD = 'H';
  	TWorkD = 'W';
  }
  CfgRead=CfgR(aMon);

  if (CfgRead == 0)
  {
    CfgMon = THolyD;
  }
  else if (CfgRead == 1)
  {
    CfgMon = TWorkD;
  }
  else
  {
    CfgMon = TWorkD;
    CfgW(aMon,1);
  }

  CfgRead=CfgR(aTue);

  if (CfgRead == 0)
  {
    CfgTue = THolyD;
  }
  else if (CfgRead == 1)
  {
    CfgTue = TWorkD;
  }
  else
  {
    CfgTue = TWorkD;
    CfgW(aTue,1);
  }

  CfgRead=CfgR(aWed);

  if (CfgRead == 0)
  {
    CfgWed = THolyD;
  }
  else if (CfgRead == 1)
  {
    CfgWed = TWorkD;
  }
  else
  {
    CfgWed = TWorkD;
    CfgW(aWed,1);
  }

  CfgRead=CfgR(aThu);

  if (CfgRead == 0)
  {
    CfgThu = THolyD;
  }
  else if (CfgRead == 1)
  {
    CfgThu = TWorkD;
  }
  else
  {
    CfgThu = TWorkD;
    CfgW(aThu,1);
  }

  CfgRead=CfgR(aFri);

  if (CfgRead == 0)
  {
    CfgFri = THolyD;
  }
  else if (CfgRead == 1)
  {
    CfgFri = TWorkD;
  }
  else
  {
    CfgFri = TWorkD;
    CfgW(aFri,1);
  }

  CfgRead=CfgR(aSat);

  if (CfgRead == 0)
  {
    CfgSat = THolyD;
  }
  else if (CfgRead == 1)
  {
    CfgSat = TWorkD;
  }
  else
  {
    CfgSat = THolyD;
    CfgW(aSat,0);
  }

  CfgRead=CfgR(aSun);

  if (CfgRead == 0)
  {
    CfgSun = THolyD;
  }
  else if (CfgRead == 1)
  {
    CfgSun = TWorkD;
  }
  else
  {
    CfgSun = THolyD;
    CfgW(aSun,0);
  }
}

// -----------------------------------------------------------------------------
//
//  Tipo tunnel : bidirezionale, solo ingresso, solo uscita
//
// -----------------------------------------------------------------------------

VS_VOID CfgBio(VS_VOID)
{
    switch (BioMode)
    {
      case  0:
              TxBiom[6] = 0x31;     //  6 FINGER  Assente           //zuck
              TxBiom[7] = 0x31;     //  7 FACE    Assente           //zuck
              break;
      case  1:
              TxBiom[6] = 0x30;     //  6 FINGER                    //zuck
              TxBiom[7] = 0x31;     //  7 FACE    Assente           //zuck
              break;
      case  2:
              TxBiom[6] = 0x31;     //  6 FINGER  Assente           //zuck
              TxBiom[7] = 0x30;     //  7 FACE                      //zuck
              break;
      case  3:
              TxBiom[6] = 0x31;     //  6 FINGER                    //zuck
              TxBiom[7] = 0x31;     //  7 FACE                      //zuck
              break;
      case  4:                      //  BioConnectionBox            //zuck
              break;
      default:
              TxBiom[6] = 0x31;     //  6 FINGER                    //zuck
              TxBiom[7] = 0x31;     //  7 FACE                      //zuck
              BioMode = 0;
              break;
    }
}
// -----------------------------------------------------------------------------
//
//  Tipo tunnel : bidirezionale, solo ingresso, solo uscita
//
// -----------------------------------------------------------------------------

VS_VOID TunnelMode(VS_VOID)
{
  switch (TunMode)
  {
    case  0:
            AIn  = 1;
            AOut = 1;
            break;
    case  1:
            AIn  = 1;
            AOut = 0;
            break;
    case  2:
            AIn  = 0;
            AOut = 1;
            break;
    default:
            AIn  = 1;
            AOut = 1;
            TunMode = 0;
            break;
  }
}

// -----------------------------------------------------------------------------
//
//  Porte chiuse in funzionamento diurno
//
// -----------------------------------------------------------------------------

void CfgDoors(void)
{
  switch (DoorsMode)
  {
    case  0:
            AInNc  = 1;
            AOutNc = 1;
            break;
    case  1:
            AInNc  = 0;
            AOutNc = 1;
            break;
    case  2:
            AInNc  = 1;
            AOutNc = 0;
            break;
    default:
            AInNc  = 1;
            AOutNc = 1;
            DoorsMode = 0;
            break;
  }
}

// -----------------------------------------------------------------------------
//
//  Scrivi configurazione
//
// -----------------------------------------------------------------------------

VS_VOID CfgWrite (VS_VOID)
{
  unsigned char Value;
  Value = FMWV1;
  TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256 , CfgAdd+aFmwV1, (unsigned char *)&Value, 1);
  Value = FMWV2;
  TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256 , CfgAdd+aFmwV2, (unsigned char *)&Value, 1);
  CfgW(aMoreIn,AInMore);
  CfgW(aXChgIn,AInChange);
  CfgW(aXChgOut,AOutChange);
  CfgW(aF1Sel,F1Sel);
  CfgW(aF2Sel,F2Sel);
  CfgW(aF3Sel,F3Sel);
  CfgW(aHHDay,HHDay);
  CfgW(aMMDay,MMDay);
  CfgW(aHHNight,HHNight);
  CfgW(aMMNight,MMNight);
  CfgW(aBioIn,AInBio);
  CfgW(aMail,Mail);
  CfgW(aMOutAuto,MOutAuto);
  CfgW(aNightNc,NightNc);
  CfgW(aLampOn,LampOn);
  CfgW(aVfy,AVerify);
  CfgW(aVfyIn,AVerifyIn);
  CfgW(aVfyOut,AVerifyOut);
  CfgW(aBioMode,BioMode);
  CfgW(aTunMode,TunMode);
  CfgW(aDoorsOff,DoorsMode);
  CfgW(aCAM,CfgCAM);
  CfgW(aEmeMode,EmeMode);
  CfgW(aFInMetal,FInMetal);
  CfgW(aFInMore,FInMore);
  CfgW(aLastOutEm,LastOutEm);
  CfgW(aBlkAlarmRed,BlkAlarmRed);
  CfgW(aMoreOut,AOutMore);
  CfgW(aLanguage,Language);
  CfgW(aTResSOD,TResSOD);
  CfgW(aIntraSOD,IntraSOD);
  CfgW(aMemoReq,MemoReq);
  CfgW(aProxIn,ProxIn);
  CfgW(aProxOut,ProxOut);
  CfgW(aBioOut,AOutBio);
  CfgW(aBioCnbTOut,BioCnbTOut);
  CfgW(aEnSynHall,EnSynHall);
  CfgW(aEnPanicBtn,EnPanicBtn);
  CfgW(aEnNightRSOD,EnNightRSOD);
  CfgW(aNhtFireAlm,NightFireAlarm);
  CfgW(aServiceMode,ServMode);
  CfgW(aAccessCtrl,AccessCtrl);
  CfgW(aRestoreMode,RestoreMode);
  CfgW(aMemBioRes,MemBioRes);
  CfgW(aEasyTrans,EasyTrans);
  CfgW(aAdvanceConf,AdvanceConf);
  CfgW(aPresentMode,PresentMode);
  CfgW(aViewMode,ViewMode);
  CfgW(aABT1,ABT1);
  CfgW(aABT2,ABT2);
  CfgW(aDelayVisEx,DelayVisEx);
  CfgW(aEnReady,EnReady);
  CfgW(aViolateNtfy,ViolateNtfy);
  CfgW(aTOutOpDrCfg,TOutOpDrCfg);
  CfgW(aCN93_4AsIntTrLight,CN93_4AsIntTrLight);  
  CfgW(aTOutObjVfyCfg,TOutObjVfyCfg);
  CfgW(aVfyRepeat,VfyRepeat);
  CfgW(aAfterLowBatStat,AfterLowBatStat);
  CfgW(aMoreMode,MoreMode);

}

// -----------------------------------------------------------------------------
//
//  Scrivi configurazione  di BackUp
//
// -----------------------------------------------------------------------------

VS_VOID CfgSave (VS_VOID)
{
  unsigned char Value;
  Value = FMWV1;
  TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256 , CfgAdd+OFFSET_CFG_COPY+aFmwV1, (unsigned char *)&Value, 1);
  Value = FMWV2;
  TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256 , CfgAdd+OFFSET_CFG_COPY+aFmwV2, (unsigned char *)&Value, 1);
  CfgW(aMoreIn+OFFSET_CFG_COPY,AInMore);
  CfgW(aXChgIn+OFFSET_CFG_COPY,AInChange);
  CfgW(aXChgOut+OFFSET_CFG_COPY,AOutChange);
  CfgW(aF1Sel+OFFSET_CFG_COPY,F1Sel);
  CfgW(aF2Sel+OFFSET_CFG_COPY,F2Sel);
  CfgW(aF3Sel+OFFSET_CFG_COPY,F3Sel);
  CfgW(aHHDay+OFFSET_CFG_COPY,HHDay);
  CfgW(aMMDay+OFFSET_CFG_COPY,MMDay);
  CfgW(aHHNight+OFFSET_CFG_COPY,HHNight);
  CfgW(aMMNight+OFFSET_CFG_COPY,MMNight);
  CfgW(aBioIn+OFFSET_CFG_COPY,AInBio);
  CfgW(aMail+OFFSET_CFG_COPY,Mail);
  CfgW(aMOutAuto+OFFSET_CFG_COPY,MOutAuto);
  CfgW(aNightNc+OFFSET_CFG_COPY,NightNc);
  CfgW(aLampOn+OFFSET_CFG_COPY,LampOn);
  CfgW(aVfy+OFFSET_CFG_COPY,AVerify);
  CfgW(aVfyIn+OFFSET_CFG_COPY,AVerifyIn);
  CfgW(aVfyOut+OFFSET_CFG_COPY,AVerifyOut);
  CfgW(aBioMode+OFFSET_CFG_COPY,BioMode);
  CfgW(aTunMode+OFFSET_CFG_COPY,TunMode);
  CfgW(aDoorsOff+OFFSET_CFG_COPY,DoorsMode);
  CfgW(aCAM+OFFSET_CFG_COPY,CfgCAM);
  CfgW(aEmeMode+OFFSET_CFG_COPY,EmeMode);
  CfgW(aFInMetal+OFFSET_CFG_COPY,FInMetal);
  CfgW(aFInMore+OFFSET_CFG_COPY,FInMore);
  CfgW(aLastOutEm+OFFSET_CFG_COPY,LastOutEm);
  CfgW(aBlkAlarmRed+OFFSET_CFG_COPY,BlkAlarmRed);
  CfgW(aMoreOut+OFFSET_CFG_COPY,AOutMore);
  CfgW(aLanguage+OFFSET_CFG_COPY,Language);
  CfgW(aTResSOD+OFFSET_CFG_COPY,TResSOD);
  CfgW(aIntraSOD+OFFSET_CFG_COPY,IntraSOD);
  CfgW(aMemoReq+OFFSET_CFG_COPY,MemoReq);
  CfgW(aProxIn+OFFSET_CFG_COPY,ProxIn);
  CfgW(aProxOut+OFFSET_CFG_COPY,ProxOut);
  CfgW(aBioOut+OFFSET_CFG_COPY,AOutBio);
  CfgW(aBioCnbTOut+OFFSET_CFG_COPY,BioCnbTOut);
  CfgW(aEnSynHall+OFFSET_CFG_COPY,EnSynHall);
  CfgW(aEnPanicBtn+OFFSET_CFG_COPY,EnPanicBtn);
  CfgW(aServiceMode+OFFSET_CFG_COPY,ServMode);
  CfgW(aAccessCtrl+OFFSET_CFG_COPY,AccessCtrl);
  CfgW(aRestoreMode+OFFSET_CFG_COPY,RestoreMode);
  CfgW(aMemBioRes+OFFSET_CFG_COPY,MemBioRes);
  CfgW(aEasyTrans+OFFSET_CFG_COPY,EasyTrans);
  CfgW(aAdvanceConf+OFFSET_CFG_COPY,AdvanceConf);
  CfgW(aPresentMode+OFFSET_CFG_COPY,PresentMode);
  CfgW(aViewMode+OFFSET_CFG_COPY,ViewMode);
  CfgW(aABT1+OFFSET_CFG_COPY,ABT1);
  CfgW(aABT2+OFFSET_CFG_COPY,ABT2);
  CfgW(aDelayVisEx+OFFSET_CFG_COPY,DelayVisEx);
  CfgW(aEnReady+OFFSET_CFG_COPY,EnReady);
  CfgW(aViolateNtfy+OFFSET_CFG_COPY,ViolateNtfy);
  CfgW(aTOutOpDrCfg+OFFSET_CFG_COPY,TOutOpDrCfg);
  CfgW(aCN93_4AsIntTrLight+OFFSET_CFG_COPY,CN93_4AsIntTrLight);
  CfgW(aTOutObjVfyCfg+OFFSET_CFG_COPY,TOutObjVfyCfg);
  CfgW(aVfyRepeat+OFFSET_CFG_COPY,VfyRepeat); 
  CfgW(aAfterLowBatStat+OFFSET_CFG_COPY,AfterLowBatStat);
  CfgW(aMoreMode+OFFSET_CFG_COPY,MoreMode);
  
}

// -----------------------------------------------------------------------------
//
//  Leggi configurazione
//
// -----------------------------------------------------------------------------

VS_VOID CfgLoad (VS_VOID)
{
    unsigned int ToWrite = 0;
    FmwV1       = CfgR(aFmwV1);
    FmwV2       = CfgR(aFmwV2);

    //se si resta comaptibili con questa versione mantenere FMWV1
    //se si resta comaptibili e si aggiunge un blocco di parametri in coda incrementare FMWV1
    //le versioni successive dalla 1.1 in avanti) controllano corrispondenza con FMWV2
    // prima di caricare i singoli blocchi aggiunti nelle varie sottoversioni FMWV2
    if (FmwV1==FMWV1)
    {
        CfgV = CfgR(aMoreIn);
        if (CfgV <= 1)
            AInMore = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aXChgIn);
        if (CfgV <= 1)
            AInChange = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aXChgOut);
        if (CfgV <= 1)
            AOutChange = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aF1Sel);
        if (CfgV <= 6)
            F1Sel = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aF2Sel);
        if (CfgV <= 6)
            F2Sel = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aF3Sel);
        if (CfgV <= 6)
            F3Sel = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aHHDay);
        if (CfgV <= 23)
            HHDay = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aMMDay);
        if (CfgV <= 59)
            MMDay = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aHHNight);
        if (CfgV <= 23)
            HHNight = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aMMNight);
        if (CfgV <= 59)
            MMNight = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aBioIn);
        if (CfgV <= 2)
            AInBio = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aMail);
        if (CfgV <= 1)
            Mail = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aMOutAuto);
        if (CfgV <= 1)
            MOutAuto = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aNightNc);
        if (CfgV <= 1)
            NightNc = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aLampOn);
        if (CfgV <= 1)
            LampOn = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aVfy);
        if (CfgV <= 1)
            AVerify = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aVfyIn);
        if (CfgV <= 1)
            AVerifyIn = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aVfyOut);
        if (CfgV <= 1)
            AVerifyOut = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aBioMode);
        if (CfgV <= 4)                    //zuck
            BioMode = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aTunMode);
        if (CfgV <= 2)
            TunMode = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aDoorsOff);
        if (CfgV <= 2)
            DoorsMode = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aCAM);
        if (CfgV <= 99)
            CfgCAM = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aEmeMode);
        if (CfgV <= 2)
            EmeMode = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aFInMetal);
        if (CfgV <= 1)
            FInMetal = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aFInMore);
        if (CfgV <= 1)
            FInMore = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aLastOutEm);
        if (CfgV <= 1)
            LastOutEm = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aBlkAlarmRed);
        if (CfgV <= 1)
            BlkAlarmRed = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aMoreOut);
        if (CfgV <= 1)
            AOutMore = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aLanguage);
        if (CfgV <= 1)
            Language = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aTResSOD);
        if (CfgV <= 1)
            TResSOD = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aIntraSOD);
        if (CfgV <= 2)
            IntraSOD = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aMemoReq);
        if (CfgV <= 2)
            MemoReq = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aProxIn);
        if (CfgV <= 1)
            ProxIn = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aProxOut);
        if (CfgV <= 1)
            ProxOut = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aBioOut);
        if (CfgV <= 2)
            AOutBio = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aBioCnbTOut);
        if (CfgV <= 6)
            BioCnbTOut = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aEnSynHall);
        if (CfgV <= 1)
            EnSynHall = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aEnPanicBtn);
        if (CfgV <= 1)
            EnPanicBtn = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aEnNightRSOD);
        if (CfgV <= 1)
            EnNightRSOD = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aNhtFireAlm);
        if (CfgV <= 1)
            NightFireAlarm = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aServiceMode);
        if (CfgV <= 99)
            ServMode = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aAccessCtrl);
        if (CfgV <= 1)
            AccessCtrl = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aRestoreMode);
        if (CfgV <= 1)
            RestoreMode = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aPresentMode);
        if (CfgV <= 1)
            PresentMode = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aMemBioRes);
        if (CfgV <= 1)
            MemBioRes = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aEasyTrans);
        if (CfgV <= 1)
            EasyTrans = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aAdvanceConf);
        if (CfgV <= 1)
            AdvanceConf = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aViewMode);
        if (CfgV <= 1)
            ViewMode = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aABT1);
        if (CfgV <= 1)
            ABT1 = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aABT2);
        if (CfgV <= 1)
            ABT2 = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aDelayVisEx);
        if ((2<=CfgV)&&(CfgV <= 99))
            DelayVisEx = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aEnReady);
        if (CfgV <=1)
            EnReady = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aViolateNtfy);
        if (CfgV <= 1)
             ViolateNtfy = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aTOutOpDrCfg);
        if ((1<=CfgV)&&(CfgV <= 10))
            TOutOpDrCfg = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aCN93_4AsIntTrLight);
        if (CfgV <= 1)
            CN93_4AsIntTrLight = CfgV;
        else
            ToWrite = 1;
		
		CfgV = CfgR(aTOutObjVfyCfg);
        if ((5 <= CfgV)&&(CfgV <= 40))
            TOutObjVfyCfg = CfgV;
        else
            ToWrite = 1;		
		
		CfgV = CfgR(aVfyRepeat);
        if ((1 <= CfgV)&&(CfgV <= 4))
            VfyRepeat = CfgV;
        else
            ToWrite = 1;	
		
		CfgV = CfgR(aAfterLowBatStat);
        if (CfgV <= 2)
            AfterLowBatStat = CfgV;
        else
            ToWrite = 1;	
		
		CfgV = CfgR(aMoreMode);
        if (CfgV <=1)
            MoreMode = CfgV;
        else
            ToWrite = 1;
	}

	
    else
    {
        ToWrite=1;
    }




    CfgBio();
    TunnelMode();
    CfgDoors();
    /*
    if (DayM != Orario[3])
    {
    CfgDay = CfgR(aSun+Orario[3]-1);
    DayM = Orario[3];
}
    */
    CfgDay = CfgR(aSun+Orario[3]-1);

    if (ToWrite==1)
        CfgWrite();
}

// -----------------------------------------------------------------------------
//
//  Leggi configurazione di BackUp
//
// -----------------------------------------------------------------------------

VS_VOID OldCfgLoad (VS_VOID)
{
    unsigned int ToWrite = 0;
    FmwV1       = CfgR(aFmwV1+OFFSET_CFG_COPY);
    FmwV2       = CfgR(aFmwV2+OFFSET_CFG_COPY);

    //se si resta comaptibili con questa versione mantenere FMWV1
    //se si resta comaptibili e si aggiunge un blocco di parametri in coda incrementare FMWV1
    //le versioni successive dalla 1.1 in avanti) controllano corrispondenza con FMWV2
    // prima di caricare i singoli blocchi aggiunti nelle varie sottoversioni FMWV2
    if (FmwV1==FMWV1)
    {
        CfgV = CfgR(aMoreIn+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            AInMore = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aXChgIn+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            AInChange = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aXChgOut+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            AOutChange = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aF1Sel+OFFSET_CFG_COPY);
        if (CfgV <= 6)
            F1Sel = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aF2Sel+OFFSET_CFG_COPY);
        if (CfgV <= 6)
            F2Sel = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aF3Sel+OFFSET_CFG_COPY);
        if (CfgV <= 6)
            F3Sel = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aHHDay+OFFSET_CFG_COPY);
        if (CfgV <= 23)
            HHDay = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aMMDay+OFFSET_CFG_COPY);
        if (CfgV <= 59)
            MMDay = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aHHNight+OFFSET_CFG_COPY);
        if (CfgV <= 23)
            HHNight = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aMMNight+OFFSET_CFG_COPY);
        if (CfgV <= 59)
            MMNight = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aBioIn+OFFSET_CFG_COPY);
        if (CfgV <= 2)
            AInBio = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aMail+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            Mail = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aMOutAuto+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            MOutAuto = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aNightNc+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            NightNc = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aLampOn+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            LampOn = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aVfy+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            AVerify = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aVfyIn+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            AVerifyIn = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aVfyOut+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            AVerifyOut = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aBioMode+OFFSET_CFG_COPY);
        if (CfgV <= 4)                    //zuck
            BioMode = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aTunMode+OFFSET_CFG_COPY);
        if (CfgV <= 2)
            TunMode = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aDoorsOff+OFFSET_CFG_COPY);
        if (CfgV <= 2)
            DoorsMode = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aCAM+OFFSET_CFG_COPY);
        if (CfgV <= 99)
            CfgCAM = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aEmeMode+OFFSET_CFG_COPY);
        if (CfgV <= 2)
            EmeMode = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aFInMetal+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            FInMetal = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aFInMore+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            FInMore = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aLastOutEm+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            LastOutEm = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aBlkAlarmRed+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            BlkAlarmRed = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aMoreOut+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            AOutMore = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aLanguage+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            Language = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aTResSOD+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            TResSOD = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aIntraSOD+OFFSET_CFG_COPY);
        if (CfgV <= 2)
            IntraSOD = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aMemoReq+OFFSET_CFG_COPY);
        if (CfgV <= 2)
            MemoReq = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aProxIn+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            ProxIn = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aProxOut+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            ProxOut = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aBioOut+OFFSET_CFG_COPY);
        if (CfgV <= 2)
            AOutBio = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aBioCnbTOut+OFFSET_CFG_COPY);
        if (CfgV <= 6)
            BioCnbTOut = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aEnSynHall+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            EnSynHall = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aEnPanicBtn+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            EnPanicBtn = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aServiceMode+OFFSET_CFG_COPY);
        if (CfgV <= 99)
            ServMode = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aAccessCtrl+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            AccessCtrl = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aRestoreMode+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            RestoreMode = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aPresentMode+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            PresentMode = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aMemBioRes+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            MemBioRes = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aEasyTrans+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            EasyTrans = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aAdvanceConf+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            AdvanceConf = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aViewMode+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            ViewMode = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aABT1+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            ABT1 = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aABT2+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            ABT2 = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aDelayVisEx+OFFSET_CFG_COPY);
        if ((2<=CfgV)&&(CfgV <= 99))
            DelayVisEx = CfgV;
        else
            ToWrite = 1;

        CfgV = CfgR(aEnReady+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            EnReady = CfgV;
        else
            ToWrite = 1;
        CfgV = CfgR(aViolateNtfy+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            ViolateNtfy = CfgV;
        else
            ToWrite = 1;
		
        CfgV = CfgR(aTOutOpDrCfg+OFFSET_CFG_COPY);
        if ((1<=CfgV)&&(CfgV <= 10))
            TOutOpDrCfg = CfgV;
        else
            ToWrite = 1;
		
		  CfgV = CfgR(aCN93_4AsIntTrLight+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            CN93_4AsIntTrLight = CfgV;
        else
            ToWrite = 1;		
		
		CfgV = CfgR(aTOutObjVfyCfg+OFFSET_CFG_COPY);
        if ((5 <= CfgV)&&(CfgV <= 40))
            TOutObjVfyCfg = CfgV;
        else
            ToWrite = 1;	
		
   		CfgV = CfgR(aVfyRepeat+OFFSET_CFG_COPY);
        if ((1 <= CfgV)&&(CfgV <= 4))
            VfyRepeat = CfgV;
        else
            ToWrite = 1;		
		CfgV = CfgR(aAfterLowBatStat+OFFSET_CFG_COPY);
        if (CfgV <= 2)
            AfterLowBatStat = CfgV;
        else
            ToWrite = 1;
		
		CfgV = CfgR(aMoreMode+OFFSET_CFG_COPY);
        if (CfgV <= 1)
            MoreMode = CfgV;
        else
            ToWrite = 1;
		
	}
    else
    {
        ToWrite=1;
    }

    CfgBio();
    TunnelMode();
    CfgDoors();
    /*
    if (DayM != Orario[3])
    {
    CfgDay = CfgR(aSun+Orario[3]-1);
    DayM = Orario[3];
}
    */
    // CfgDay = CfgR(aSun+OFFSET_CFG_COPY+Orario[3]-1);   //P.Leone

    if (ToWrite==1)
        CfgSave();
}


VS_VOID SetInLamp(VS_VOID)
{
  USCITE[LAMP_IN] = 1;
}

VS_VOID ResInLamp(VS_VOID)
{
  USCITE[LAMP_IN] = 0;
}

VS_VOID SetOutLamp(VS_VOID)
{
  USCITE[LAMP_OUT] = 1;
}

VS_VOID ResOutLamp(VS_VOID)
{
  USCITE[LAMP_OUT] = 0;
}

VS_VOID SetLamp(VS_VOID)
{
  RunOut(LAMP, 1, 0);
}

VS_VOID ResLamp(VS_VOID)
{
  RunOut(LAMP, 0, 0);
}

VS_VOID ResDelLamp(VS_UINT MSec)
{
  if (USCITE[LAMP] == 1)
  {
    if (MSec > 100)
      RunOut(LAMP, 1, (unsigned short) MSec/100);
    else
      RunOut(LAMP, 1, 1);
  }
}

VS_VOID ResSyn(VS_VOID)
{
  StopMsg();
}

VS_VOID SetAmp(VS_VOID)
{
  AT91F_PIO_SetOutput( AT91C_BASE_PIOA,V_SAMP);
}

VS_VOID ResAmp(VS_VOID)
{
  AT91F_PIO_ClearOutput( AT91C_BASE_PIOA,V_SAMP);
}

VS_VOID SetPCBioRec(VS_VOID)
{
  FotoOnBiom();
}

VS_VOID ResPCBioRec(VS_VOID)
{
  FotoOffBiom();
}

VS_VOID SetOutAux(VS_VOID)
{
  RunOut(OUT_AUX, 1, 0);
}

VS_VOID ResOutAux(VS_VOID)
{
  RunOut(OUT_AUX, 0, 0);
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
//  Funzioni per gestione chiave di protezzione
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

// *****************************************************************************
// Legge in EEPROM il valore di LeftTimeSession e in base al suo valore genera
// l'evento eOutSession(=0) o eInSession(>0)
// *****************************************************************************
VS_VOID RetrieveSessionState(VS_VOID)
{

#ifdef  NO_KEY_VER    //in caso di Versione senza chiave
  SEQ_AddEvent(eFreeSession,Sys);
#else
  TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, ADDTSESSION,(unsigned char *)&LeftTimeSession, 4);

  if(    LeftTimeSession<=50        //Test tempo scaduto
      || LeftTimeSession>172800){  // o memoria E2PROM vergine  LeftTimeSession = 0xFFFF
    LeftTimeSession=0;
    SEQ_AddEvent(eOutSession,Sys);
  }
  else{
    LeftTimeSession -= 50; //decremento tempo dopo off->on...evita sessioni prolungate indefinitamente
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, ADDTSESSION,(unsigned char *)&LeftTimeSession, 4);
    SEQ_AddEvent(eInSession,Sys);
  }
#endif
}

// *****************************************************************************
// Aggiorna (legge il contatore associato a TSession (18) dal vettore dei timer
// software: SW_timer_ticks) e scrive in EEPROM il valore di LeftTimeSession
// *****************************************************************************
VS_VOID HoldTSession(VS_VOID)
{
  extern unsigned int CountTSession;
  extern unsigned int TIM_STEP;
  unsigned int LftTmSession = 0;
  if  ( CountTSession > 4294967) //0xFFFFFFFF / 1000
          LftTmSession = (CountTSession / TIM_STEP) * 1000;
        else
          LftTmSession = (CountTSession * 1000) / TIM_STEP;

  TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, ADDTSESSION,(unsigned char *)&LftTmSession, 4);

}

// *****************************************************************************
// debug TestE2
// *****************************************************************************
VS_VOID TestE2(VS_VOID)
{
  unsigned int Dummy = 0;
  TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, ADDTSESSION,(unsigned char *)&Dummy, 4);
  Dummy += 5;
  TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, ADDTSESSION,(unsigned char *)&Dummy, 4);
   Dummy -=5;
  TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, ADDTSESSION,(unsigned char *)&Dummy, 4);
  Dummy -=5;
  TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, ADDTSESSION,(unsigned char *)&Dummy, 4);
}





// *****************************************************************************
// Avvia la trasmissione su DBGU nella modalit� continua (come ver. precedenti)
// *****************************************************************************
VS_VOID OpenTransmission(VS_VOID)
{
  OpenedTransmission = 1;
}

// *****************************************************************************
// Arresta la trasmissione su DBGU nella modalit� continua
// *****************************************************************************
VS_VOID CloseTransmission(VS_VOID)
{
   OpenedTransmission = 0;
}

// *****************************************************************************
// Controlla che il campo ID nella EEPROM della PLL non contenga un valore
// valido
// *****************************************************************************
VS_VOID TestBlankKey(VS_VOID)
{
  unsigned char IDKey[7]={0,0,0,0,0,0,0};
  unsigned char  i=0, BlankKey=1;
  millisecondi(2);

  TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, ADDIDKEY,(unsigned char *)&IDKey[0], 7);

  if(IDKey[0] >= 'D'&& isupper(IDKey[0]) )
    BlankKey = 0;

  for(i=1; i<7; i++){
    if(isdigit(IDKey[i]))
      BlankKey = 0;
  }
  if (BlankKey){
    SEQ_AddEvent(eBlankKey,Sys);
  }
}


// *****************************************************************************
// Memorizza in EEPROM l'ID della chiave instaurando la relazione biunivoca
// fra PLL e chiave
// *****************************************************************************
VS_VOID StoreIDKey(VS_VOID)
{
  unsigned char IDKey[7]={0,0,0,0,0,0,0};
  unsigned char TimeFreeze=0, i=0, Delay=0, GoodIDKey=1;

  for(i=0; i<7; i++){
    TimeFreeze=SysTimeCount;
    Delay=(unsigned char)(SysTimeCount-TimeFreeze);
    while(!(AT91F_US_RxReady( (AT91PS_USART)DBGU_pt ))&&(Delay < 30)){
      Delay=(unsigned char)(SysTimeCount-TimeFreeze);
    }
    if(Delay < 30)
      IDKey[i] = Mirror(AT91F_US_GetChar( (AT91PS_USART)DBGU_pt ));
    else {
      IDKey[i] = i;
      GoodIDKey = 0;
    }
  }
  if (GoodIDKey){
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, ADDIDKEY,(unsigned char *)&IDKey[0], 7);
  }
}
// *****************************************************************************
// ==========                    Versione -PT                         ==========
// Aggiorna il valore di LeftTimeSession e lo invia (4 cifre decimali in ASCII)
// sulla DBGU dopo il carattere '~'  (p.e.'~0120' per 1 ora e 20 min rimasti)
// *****************************************************************************
// ==========                    Versione -AM                         ==========
// Aggiorna il valore di LeftTimeSession; se diverso da zero invia l'ID della
// chiave memorizzato in EEPROM (1 cifra esadecimale + 6 cifre decimali );
// altrimenti 7 zeri consecutivi comunque dopo il carattere '!'
// *****************************************************************************

VS_VOID SendAnswer(VS_VOID)
{
  extern unsigned int  CountTSession, TIM_STEP ;
  extern unsigned char bCheckIDKey;
  unsigned int LftTmSession = 0;
  if  ( CountTSession > 4294967)  //0xFFFFFFFF/1000
    LftTmSession = (CountTSession / (60*TIM_STEP)) * 1000;
  else
    LftTmSession = (CountTSession * 1000) / (60*TIM_STEP);
  millisecondi(100);
#ifdef AM_KEY_VER
  
  unsigned char TBuff[8] = {'!','D','0','0','0','0','0','0'};

  if  (LftTmSession != 0 || bCheckIDKey != 0){
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, ADDIDKEY,(unsigned char *)&TBuff[1], 7);
  }
  US_PutStr((AT91PS_USART)DBGU_pt, (char*) TBuff,8);

#else

  unsigned char TBuff[5] = {'~','0','0','0','0'};

  if (LftTmSession<6000){
    TBuff[1] = 0;
  	while (LftTmSession > 599)
  	{
  		TBuff[1] ++;
  		LftTmSession = LftTmSession - 600;
  	}

    TBuff[1] += 0x30;   //decine di ore

    TBuff[2] = 0;
  	while (LftTmSession > 59)
  	{
  		TBuff[2] ++;
  		LftTmSession = LftTmSession - 60;
  	}
    TBuff[2] += 0x30;   //ore

    TBuff[3] = 0;
  	while (LftTmSession > 9)
  	{
  		TBuff[3] ++;
  		LftTmSession = LftTmSession - 10;
  	}
    TBuff[3] += 0x30;   //decine di minuti
    TBuff[4] = LftTmSession + 0x30;   //minuti
  }
  else{
  TBuff[0] = '~'; TBuff[1] = 'F'; TBuff[2] = 'F'; TBuff[3] = 'F'; TBuff[4] = 'F';
  }

  US_PutStr((AT91PS_USART)DBGU_pt, (char*) TBuff,5);
#endif

}
