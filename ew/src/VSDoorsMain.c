/*****************************************************************************
* IAR visualSTATE Main Loop Source File
*
* The file contains an implementation for a main loop using the visualSTATE
* basic API.
*
* The code uses a simple queue for storing events. The functions for inter-
* facing to the queue are described in the sample code file
* simpleEventHandler.h.
*****************************************************************************/

/* *** include directives *** */

#include "VSDoors.h"
#include "VSDoorsData.h"
#include "VSDoorsAction.h"
#include "VSDoorsMain.h"
#include "VSDoorsOutput.h"
#include "adc.h"
#include "pwm.h"
#include "usart.h"
#include "deviceSetup.h"
#include "inout.h"

#define RAC_COUNT_MAX (2 * FLASH_FACTOR)
#define TOGGLE_FAST (2314 * FLASH_FACTOR)
#define TOGGLE_SLOW (9257 * FLASH_FACTOR)
#define TOGGLE_FIX 1

#define MASK_FILTER_UP 3    //Leone 4
#define MASK_FILTER_DOWN 3  //Leone 4
#define PWM_MIN_THR_FACTOR 40
#define PWM_MAX_THR_FACTOR 50


#define CUR_THR_MIN 10



/* *** function declarations *** */

/* *** variable definitions *** */

extern unsigned char IN_ACT;

/* *** function definitions *** */
extern void DecodeCommand(void);
/* ***  Input functions *** */

unsigned int UpdateMaskValue(unsigned int OldVal, unsigned int NewVal, unsigned int NowCurThr)
{
    unsigned int MaskFilter;
    if (OldVal < NewVal){
        MaskFilter = NewVal - OldVal;
        if (MaskFilter < 2){
            MaskFilter = 2;
        }
        else{
            if (MaskFilter > (NowCurThr * CUR_THR_FACTOR)){
                // solo se differenza minore della soglia lascia filtro che consente di raggiungere allineamento
                // altrimenti allienamento piu` veloce con meida MASK_FILTER_UP
                MaskFilter = MASK_FILTER_UP;
            }
        }
        if (OldVal == 0 ){
            OldVal = 1;
        }
        return ((((MaskFilter-1) * OldVal) + NewVal) / MaskFilter);
    }
    else{
        return ((((MASK_FILTER_DOWN-1) * OldVal) + NewVal) / MASK_FILTER_DOWN);
    }
}

VS_VOID UpdateMaskClose (VS_VOID)
{
#ifdef CUR_CONTROL
    unsigned int i = 0, Diff=0, OutOfRange=0, CurThr=0;

    CurThr = pm->CurThrClose > CUR_THR_MIN ? pm->CurThrClose : CUR_THR_MIN;
    for (; i<MASK_SIZE; i++)
    {
        if (pm->CurLastClose[i] > 0)
        {
            if ((pm->_PosClose <= i) && (i <= pm->_PosOpen)){ //monitoraggio allineamento maschere
                Diff = pm->MaxCurADC[pm->PwmMaskClose[i]/10] > pm->CurMaskClose[i] ? pm->MaxCurADC[pm->PwmMaskClose[i]/10] - pm->CurMaskClose[i] : 0;
                if (Diff< (CurThr + (50/CurThr) + 5)){
                    OutOfRange++;
                }
            }
            //aggiorna maschera
            pm->CurMaskClose[i] = UpdateMaskValue( pm->CurMaskClose[i], pm->CurLastClose[i], pm->CurThrClose );
        }
        pm->CurLastClose[i] = 0;
    }
    if (OutOfRange > ((pm->_PosOpen - pm->_PosClose) >> 2)){   //25% dei valori utili fuori range
        pm->CurMaskCloseOk = 0;
    }
    else{
        pm->CurMaskCloseOk = 1;
    }
#endif

}

VS_VOID UpdateMaskOpen (VS_VOID)
{
#ifdef CUR_CONTROL
    unsigned int i = 0, Diff=0, OutOfRange=0, CurThr=0;

    CurThr = pm->CurThrOpen > CUR_THR_MIN ? pm->CurThrOpen : CUR_THR_MIN;
    for (; i<MASK_SIZE; i++){
        if (pm->CurLastOpen[i] > 0){
            if ((pm->_PosClose <= i) && (i <= pm->_PosOpen)){ //monitoraggio allineamento maschere
                Diff = pm->MaxCurADC[pm->PwmMaskOpen[i]/10] > pm->CurMaskOpen[i] ? pm->MaxCurADC[pm->PwmMaskOpen[i]/10] - pm->CurMaskOpen[i] : 0;
                if (Diff < (CurThr + (50/CurThr) + 5) ){
                    OutOfRange++;
                }
            }
            //aggiorna maschera
            pm->CurMaskOpen[i] = UpdateMaskValue( pm->CurMaskOpen[i], pm->CurLastOpen[i], pm->CurThrOpen );
        }
        pm->CurLastOpen[i] = 0;
    }
    if (OutOfRange > ((pm->_PosOpen - pm->_PosClose) >> 2)){   //25% dei valori utili fuori range
        pm->CurMaskOpenOk = 0;
    }
    else{
        pm->CurMaskOpenOk = 1;
    }
#endif
}


void ClearLastClose(void)
{
  #ifdef CUR_CONTROL
  unsigned int i = 0;
  for (; i<MASK_SIZE; i++)
  {
    pm->CurLastClose[i] = 0;
  }
  #endif
}

void ClearLastOpen(void)
{
  #ifdef CUR_CONTROL
  unsigned int i = 0;
  for (; i<MASK_SIZE; i++)
  {
    pm->CurLastOpen[i] = 0;
  }
  #endif
}



void CheckCur(unsigned int CurNew, unsigned int CurMask)
{
    unsigned int CurThrOver;
    if (pm->Dir == DirClose){
        CurThrOver = (CurThrOverClose + pm->CurThrClose) * CUR_THR_FACTOR;
    }
    else{
        CurThrOver = (CurThrOverOpen + CurThrOverClose + pm->CurThrOpen) * CUR_THR_FACTOR;
    }
    if (pm->PwmActive >= PWM_MAX_THR_FACTOR){
        CurThrOver = CurThrOver;
    }
    else{
        switch (pm->CurThrSlope){
          case 5:
            CurThrOver = CurThrOver + CurThrOver;
            break;
          case 4:
            CurThrOver = CurThrOver + (CurThrOver * 50)/100;
            break;
          case 3:
            CurThrOver = CurThrOver;
            break;
          case 2:
            CurThrOver = CurThrOver - (CurThrOver * 20)/100;
            break;
          case 1:
            CurThrOver = CurThrOver - (CurThrOver * 30)/100;
            break;
          default:
            CurThrOver = CurThrOver;

        }

    }
    /*if ((CurThrOver + CurMask) >= 1023)  //Saturazione ADC
        CurThrOver = (1024 - CurMask);*/
    if (CurThrOver < CUR_THR_MIN)
        CurThrOver = CUR_THR_MIN;

    if ((CurMask + CurThrOver) < CurNew ){
        if (pm->bOverCurEnabled == 1){
            if (pm->bOverCurDetected == 0){
                if (++pm->OverCount > pm->OverCountMax){
                    SEQ_AddEvent(eOverCur,pm->Sys);
                    pm->bOverCurDetected = 1;
                    pm->OverCount = 0;
                }
            }
            else{
                pm->OverCount = 0;
            }
        }
        else{
            pm->OverCount = 0;
        }
    }
    else{
        if ((CurMask + (CurThrOver/2)) > CurNew ){
            if (pm->bUnderCurEnabled == 1){
                if (pm->bUnderCurDetected == 0){
                    SEQ_AddEvent(eUnderCur,pm->Sys);
                    if (DisplayMode == DISPLAY_MODE_DEBUG){
                        US_SendCharDbg((AT91PS_USART)DBGU_pt, 85);
                        US_SendCharDbg((AT91PS_USART)DBGU_pt, 85);
                        US_SendCharDbg((AT91PS_USART)DBGU_pt, 85);
                    }
                    pm->bUnderCurDetected = 1;
                }
            }
        }
    }
}

void UpCurMask(void)
{
  #ifdef CUR_CONTROL
  unsigned int i = 0;
  for (; i<MASK_SIZE; i++)
  {
    pm->CurMaskOpen[i] = MASK_MAX_VAL;
    pm->CurMaskClose[i] = MASK_MAX_VAL;
    pm->CurMaskOpenOk = 0;
    pm->CurMaskCloseOk = 0;
  }
  #endif
}


unsigned short GetMaxCloseMask(unsigned short PosNew)
{
  #ifdef CUR_CONTROL
  unsigned short i;
  unsigned short PosTemp;
  unsigned short MaxValue = 0;
  for ( i=0; i<EEPROM_MASK_RATIO; i++ )
  {
    PosTemp = PosNew+i;
    if  (   PosTemp > PosMaxClose
         && PosTemp < PosMinOpen
         && MaxValue < pm->CurMaskClose[PosTemp])
    {
      MaxValue = pm->CurMaskClose[PosTemp];
    }
  }
  return(MaxValue<MASK_MAX_VAL ? MaxValue : MASK_MAX_VAL);
  #else
  return(0);
  #endif
}


unsigned short GetMaxOpenMask(unsigned short PosNew)
{
  #ifdef CUR_CONTROL
  unsigned short i;
  unsigned short PosTemp;
  unsigned short MaxValue = 0;
  for ( i=0; i<EEPROM_MASK_RATIO; i++ )
  {
    PosTemp = PosNew+i;
    if (   PosTemp > PosMaxClose
        && PosTemp < PosMinOpen
        && MaxValue < pm->CurMaskOpen[PosTemp])
    {
      MaxValue = pm->CurMaskOpen[PosTemp];
    }
  }
  return(MaxValue<MASK_MAX_VAL ? MaxValue : MASK_MAX_VAL);
  #else
  return(0);
  #endif
}

void SetMaxCloseMask(unsigned short PosNew, unsigned short Value)
{
  #ifdef CUR_CONTROL
  unsigned short i;
  unsigned short UpValue=0;
  UpValue = Value + MASK_10;
  UpValue = (UpValue<MASK_MAX_VAL ? UpValue : MASK_MAX_VAL);

  for ( i=0; i<EEPROM_MASK_RATIO; i++ )
  {
    pm->CurMaskClose[PosNew+i] = UpValue;
  }
  #endif
}


void SetMaxOpenMask(unsigned short PosNew, unsigned short Value)
{
  #ifdef CUR_CONTROL
  unsigned short i;
  unsigned short UpValue=0;
  UpValue = Value + MASK_10;
  UpValue = (UpValue<MASK_MAX_VAL ? UpValue : MASK_MAX_VAL);

  for ( i=0; i<EEPROM_MASK_RATIO; i++ )
  {
    pm->CurMaskOpen[PosNew+i] = UpValue;

  }
  #endif
}

void CheckPosCur(void)
{
    unsigned int MaxToggle;

    // legge posizione
    if (pm->EncoderCCWCfg == 0)
        Pos = ADC_read( pm->PosCh, (NLETT));
    else
        Pos = 1023 - ADC_read( pm->PosCh, (NLETT));

    if (Pos >= POS_MAX_VAL){
        Pos = POS_MAX_VAL-1;
    }

    // genera eventi di movimento (con gestione primo ciclo di lettura)
    if (pm->FirstUpdate == 0)  {
        if (Pos >= (pm->OldPos + pm->MovDelta))    {
            if (Pos != pm->LastPosMovOpen)      {
                SEQ_AddEvent(eMovOpen,pm->Sys);
                pm->LastPosMovOpen = Pos;
            }
            pm->OldPos = Pos;
        }
        else {
            if ((Pos + pm->MovDelta) <= pm->OldPos) {
                if (Pos != pm->LastPosMovClose) {
                    SEQ_AddEvent(eMovClose,pm->Sys);
                    pm->LastPosMovClose = Pos;
                }
                pm->OldPos = Pos;
            }
        }
    }
    else{
        pm->FirstUpdate = 0;
        pm->OldPos = Pos;
        pm->Toggle = 0;
        ClearLastOpen();
        ClearLastClose();
    }

    // se abilitato segnala su PRG posizione rispetto a centro
    if (pm->bPosNotifyEnabled == VS_TRUE){
        if ((Pos > (PosMid+POS_NOTIFY_DELTA))){
            //lampeggio lento: lato chiuso
            MaxToggle = TOGGLE_SLOW;
        }
        else  if (Pos <= (PosMid-POS_NOTIFY_DELTA)){
            MaxToggle = TOGGLE_FIX;
            //acceso fisso: lato aperto
        }
        else{
            //lampeggio veloce: posizione centrale
            MaxToggle = TOGGLE_FAST;
        }
        if (pm->Toggle-- == 0){
            pm->Toggle = MaxToggle;
        }
        if (pm->Toggle >= (MaxToggle/2)){
            USCITE[ pm->Prg ] = 1;
        }
        else{
            USCITE[ pm->Prg ] = 0;
        }
    }

    // legge corrente
    pm->Cur = 1023 - ADC_read( pm->CurCh, NLETT );

    // aggiorna posizione per controllo e aggiornamento maschera
    if (!(Pos == pm->PosCur)){
        pm->PosCur = Pos;
        pm->CurPos = pm->Cur;
    }
    else{
        if (pm->Cur > pm->CurPos){
            pm->CurPos = pm->Cur;
        }
    }

#ifdef CUR_CONTROL
    unsigned int CurCheck;
    // controllo e aggiornamento maschera
    if (pm->Dir == DirClose){
        if (pm->bUpdateMaskEnabled == 0){
            CurCheck = (pm->CurPos + pm->CurLastClose[pm->PosCur]) / 2;
            if (pm->CurLastClose[pm->PosCur] < pm->CurPos){
                pm->CurLastClose[pm->PosCur] = pm->CurPos;
            }
            pm->PwmMaskClose[pm->PosCur] = pm->PwmActive;
            CheckCur(CurCheck, pm->CurMaskClose[pm->PosCur]);			
            //aggiorna massimo valore corrente da ADC
            if(pm->bAcqMaxCurADC==1){
                if(pm->MaxCurADC[(pm->PwmActive/10)] < pm->CurPos){
                    pm->MaxCurADC[(pm->PwmActive/10)] = pm->CurPos;
                }
            }
        }
    }
    else{
        if ((pm->bUpdateMaskEnabled == 0) && (bEmergency == 0)){		//daPLL_2.30 gestione corsa ridotta in emergenza
            CurCheck = (pm->CurPos + pm->CurLastOpen[pm->PosCur]) / 2;
            if (pm->CurLastOpen[pm->PosCur] < pm->CurPos){
                pm->CurLastOpen[pm->PosCur] = pm->CurPos;
            }
            pm->PwmMaskOpen[pm->PosCur] = pm->PwmActive;
			if(bEmergency == 0){
				CheckCur(CurCheck, pm->CurMaskOpen[pm->PosCur]);
			}
			else{
				unsigned int TempCurMask = 0;
				int diff = (pm->MaxCurADC[(pm->PwmActive/10)] - (((CurThrOverOpen + CurThrOverClose + pm->CurThrOpen) * 110)/100));
				if (diff > 0){
					TempCurMask = diff;
				}
				else{
					TempCurMask = 1;
				}
				CheckCur(pm->CurPos, TempCurMask);
			}
        }
    }
#endif
}

void UpdatePwm(void)
{
  if (pm->RacCount > 0)
  {
    pm->RacCount--;
  }
  if(pm->RacCount == 0)
  {
    pm->RacCount = RAC_COUNT_MAX;
    if (pm->PwmTarget > pm->PwmActive)
    {
      pm->PwmActive++;
    }
    else
    {
      if (pm->PwmTarget < pm->PwmActive)
      {
        pm->PwmActive--;
      }
    }
  }
  if (pm->PwmTarget > 0)
  {
    if (pm->PwmActive == 0 )
    {
      pm->PwmActive = 1;
    }
    SetPwm(pm->PwmCh, pm->PwmChId, pm->PwmPh, pm->Dir, pm->PwmActive, (PWM_PERIOD + pm->PwmChannel));
  }
}
VS_VOID UnhookDoor(VS_VOID)
{
    SEQ_AddEvent(eUnhook,1);
}
VS_VOID HookupDoor(VS_VOID)
{
    SEQ_AddEvent(eHookup,1);
}

VS_VOID BrakeMot(VS_UCHAR sys)
{
    switch (sys){
      case 1:
        SEQ_AddEvent(eBreakThrOn,0);
        break;
      case 2:
        SEQ_AddEvent(eBreakThrOn,1);
        break;
    }
}

VS_VOID WildMot(VS_UCHAR sys)
{
    switch (sys){
      case 1:
        SEQ_AddEvent(eBreakThrOff,0);
        break;
      case 2:
        SEQ_AddEvent(eBreakThrOff,1);
        break;
    }
}


VS_VOID CheckExtProt( VS_VOID  )
{
    if (INGRESSI[pm->Gom] != 0 && pm->Dip6 == 0){
        SEQ_AddEvent(eExtProtLev,pm->Sys);
    }
}

void DoorsManageInputs()
{
    if  (   INGRESSI[pm->CpOk] != 0
         || (   INGRESSI[pm->CpNok] == 0
             && pm->bOpen != 0))
    {
        if ((pm->bOpenDetected == 0) && (pm->bOpenEnabled == 1))    {
            if (pm->bManual == 0){
                SEQ_AddEvent(eOpen,pm->Sys);
                pm->bOpenDetected = 1;
                pm->bCloseDetected = 0;
            }
        }
    }
    else{
        if (    pm->bCloseDetected == 0
             && pm->bCloseEnabled == 1
             && (   !(   (INGRESSI[pm->Gom] != 0
                       && pm->Dip6 == 0))
                 || Pos <= PosMaxClose))
        {
            if (pm->bManual == 0){
                SEQ_AddEvent(eClose,pm->Sys);
                pm->bCloseDetected = 1;
                pm->bOpenDetected = 0;
            }
        }
    }

    if(pm->Mot==0) {
        if ((INGRESSI[SS1] == 1)||(INGRESSI[SS1_CB] == 1)){
            if (pm->CatchSS == 1){
                SEQ_AddEvent(eLocked,pm->Sys);
                pm->CatchSS = 0;
            }
        }
        else{
            if (pm->CatchSS == 0){
                SEQ_AddEvent(eUnLocked,pm->Sys);
                pm->CatchSS = 1;
            }
        }
    }

    else if(pm->Mot==1){
        if ((INGRESSI[SS2] == 1)||(INGRESSI[SS2_CB] == 1)){
            if (pm->CatchSS == 1){
                SEQ_AddEvent(eLocked,pm->Sys);
                pm->CatchSS = 0;
            }
        }
        else{
            if (pm->CatchSS == 0){
                SEQ_AddEvent(eUnLocked,pm->Sys);
                pm->CatchSS = 1;
            }
        }
    }

    if (Motor == 0){
        if(pm->Mot==0){
            if ((INGRESSI[SP1] == 1)||(INGRESSI[SP1_CB] == 1)){
                if (pm->CatchSP == 1){
                    SEQ_AddEvent(eClosed,pm->Sys);
                    pm->CatchSP = 0;
                }
            }
            else{
                if (pm->CatchSP == 0){
                    SEQ_AddEvent(eOpened,pm->Sys);
                    pm->CatchSP = 1;
                }
            }
        }

        else if(pm->Mot==1){
            if ((INGRESSI[SP2] == 1)||(INGRESSI[SP2_CB] == 1)){
                if (pm->CatchSP == 1){
                    SEQ_AddEvent(eClosed,pm->Sys);
                    pm->CatchSP = 0;
                }
            }
            else{
                if (pm->CatchSP == 0){
                    SEQ_AddEvent(eOpened,pm->Sys);
                    pm->CatchSP = 1;
                }
            }
        }
    }
	
    if ( INGRESSI[ pm->Gom ] != 0){
        if (pm->CatchGom == 1){
            if ( (pm->Dip2 != 0) || (pm->Dir == DirClose)){
                if (pm->ExtProtEnabled != 0){
                    SEQ_AddEvent(eExtProt,pm->Sys);
                }
                else {
                    if (pm->Run != 0)
                        pm->ExtProtEnabled = 1;
                }
            }
            pm->CatchGom = 0;
        }
    }
    else{
        if(pm->CatchGom == 0){
            pm->CatchGom = 1;
        }
    }

    if ( pm->Dip5 == 0 ){
        if (pm->CatchDip5 == 0){
            // aggiorna per antinversione
            UpdateSwitch();
            pm->CatchDip5 = 1;
        }
    }
    else{
        if (pm->CatchDip5 == 1){
            // aggiorna per antinversione
            UpdateSwitch();
            pm->CatchDip5 = 0;
        }
    }

    if ( pm->Run != 0 ){
        if (pm->CatchRun == 0){
            SEQ_AddEvent(euRun,pm->Sys);
            pm->CatchRun = 1;
        }
    }
    else{
        if (pm->CatchRun == 1){
            if (pm->bManual == 0){
                SEQ_AddEvent(euEsc,pm->Sys);
            }
            pm->CatchRun = 0;
        }
    }
}

void DoorsManageCommands(void)
{
   if(OpenedTransmission!=0){
      char ch;
      if (Sys==SysDisplay){
         if(AT91F_US_Error ( (AT91PS_USART)DBGU_pt ) & AT91C_US_OVRE){
            millisecondi(5);
            AT91F_US_GetChar( (AT91PS_USART)DBGU_pt );
            DBGU_pt -> DBGU_CR = AT91C_US_RSTSTA;
            millisecondi(1);
         }
         if (AT91F_US_RxReady( (AT91PS_USART)DBGU_pt ))
         {
            ch = Mirror(AT91F_US_GetChar( (AT91PS_USART)DBGU_pt ));
            if (!((ch == 81) || (ch == 27)))
            {
               EscCount = 0;
            }
            switch (ch)
            {
            //consente ai sistemi "motore" di essere "sensibili" ai comandi provenienti dalla key
            //ed utilizzati per la gestione della comunicazione
#ifdef AM_KEY_VER
			 case '!':
#else
             case '~':
#endif
                  DecodeCommand();
                  break;

             case '3':
                  bDisDbg = 0;
                  SysDisplay = 2;
                  //SEQ_AddEvent(eMetalOn,SysDisplay);
                  US_SwitchDisplay();
                  break;
             case '1':
             case '2':
                  bDisDbg = 0;
                  SysDisplay = ch-49;
                  US_SwitchDisplay();
                  break;
             case 'd':
             case 'D':
                  SEQ_AddEvent(euDefault,pm->Sys);
                  break;
             case 'u':
             case 'U':
                  SEQ_AddEvent(euUpMask,pm->Sys);
                  break;
             case 'q':
             case 'Q':
             case 27: //esc = 'Q'
                  EscCount++;
                  if ((EscCount > 1) || (pm->bProg != 0))
                  {
                    EscCount = 0;
                    pm->ExtProtEnabled = 1;
                    SEQ_AddEvent(euEsc,pm->Sys);
                  }
                  break;
             case 'k':
             case 'K':
                  if (pm->Run == 0)
                  {
                    pm->Run = 1;
                    ParWrite();
                  }
                  pm->bManual = 0;
                  SEQ_AddEvent(euRun,pm->Sys);
                  EscCount = 0;
                  break;
             case 13: //enter
                  SEQ_AddEvent(euEnter,pm->Sys);
                  break;
             case '/':
                  SEQ_AddEvent(euPrior,pm->Sys);
                  break;
             case '*':
                  SEQ_AddEvent(euNext,pm->Sys);
                  break;
             case '+':
                  SEQ_AddEvent(euUp,pm->Sys);
                  break;
             case '-':
                  SEQ_AddEvent(euDown,pm->Sys);
                  break;
             case 'x':
             case 'X':
                  SEQ_AddEvent(euInit,pm->Sys);
                  break;
             case 'r':
             case 'R':
                  SEQ_AddEvent(euRead,pm->Sys);
                  break;
             case 'p':
             case 'P':
                  SEQ_AddEvent(euClearCount,pm->Sys);
                  break;
             case 'l':
             case 'L':
                  SEQ_AddEvent(euLoad,pm->Sys);
                  break;
   /*
             case 'c':
             case 'C':
                  SEQ_AddEvent(euClearE2,pm->Sys); //ripristina il vaolore 0xFFFF nella EEPROM
                  break;
     */
             case 's':
             case 'S':
                  if (EscCount > 1)
                    for (;;)
                    {
                      //test reset da wd
                    }
                  break;
            }
            ((AT91PS_USART)DBGU_pt)->US_CR = AT91C_US_RSTSTA;
         }
      }
   }
}

unsigned char DoorsInstance(SEM_CONTEXT *Context, SEM_EVENT_TYPE EventNo,
      SEM_INSTANCE_TYPE InstanceNo)
{
  /* Define completion code variable. */
  unsigned char cc;
  SEM_ACTION_EXPRESSION_TYPE ActionExpress;

        /* Set active instance */
      if ((cc = SMP_SetInstance(Context, InstanceNo)) != SES_OKAY)
      {
        return(cc);
      }
        /* Deduct the event */
      if ((cc = SMP_Deduct(Context, EventNo)) != SES_OKAY)
      {
        return(cc);
      }

        /* Get resulting action expressions and execute them. */
      while ((cc = SMP_GetOutput(Context, &ActionExpress)) == SES_FOUND)
        SMP_TableAction(Context, VSDoorsVSAction, ActionExpress);

        /* Check for error. */
      if (cc != SES_OKAY)
      {
        return(cc);
      }

        /* Change the next state vector. */
      if ((cc = SMP_NextState(Context)) != SES_OKAY)
      {
        return(cc);
      }

      return(cc);
}

unsigned char DoorsInit(SEM_CONTEXT VS_TQ_CONTEXT * * Context)
{
  unsigned char cc;

  for (cc=0;cc<MOT_SYSTEMS;cc++)
    {
    Cp[cc] = 0;
    MotSys[cc].Mot = cc;
    MotSys[cc].Sys = Sys++;
    MotSys[cc].CountDown = UPDATE_POS_SAVE_COUNT;
    MotSys[cc].EntryPwmDecClose = 0; //cattura sempre ultima uscita in entrata in decelerazione
    MotSys[cc].EntryPwmDecOpen = 0;
    MotSys[cc].EntryPosDecClose = 0;
    MotSys[cc].EntryPosDecOpen = 0;
    MotSys[cc].OverCount = 0;
    MotSys[cc].OldPos = 0;
    MotSys[cc].LastPosMovClose = 0;
    MotSys[cc].LastPosMovOpen = 0;
    MotSys[cc].FirstUpdate = 1;
    MotSys[cc].OverCountMax = 5;
    MotSys[cc].Toggle = 0;
    MotSys[cc].ToWriteOpened = 0;
    MotSys[cc].RacCount = 0;
    MotSys[cc].CatchGom = 1;
    MotSys[cc].CatchRun = 0;
    MotSys[cc].Run = 1;
    MotSys[cc].CatchSS = 1;
    MotSys[cc].CatchSP = 1;
    switch (cc)
    {
      case 0:
        MotSys[cc].PwmPh = PH_M0;
        MotSys[cc].PwmCh = PWM_CH1_M0;
        MotSys[cc].PwmChId = PWM_CHID1_M0;
        MotSys[cc].PosCh = POS_CH_M0;
        MotSys[cc].CurCh = CUR_CH_M0;
        MotSys[cc].CpOk = CP1_OK;
        MotSys[cc].CpNok = CP1_NOK;
        MotSys[cc].Gom = GOM1;
        MotSys[cc].Pc = PC1;
        MotSys[cc].Pa = PA1;
        MotSys[cc].Prg = PRG1;
        MotSys[cc].Err = ERR1;
        MotSys[cc].Frn = FRN1;
        MotSys[cc].ViolP = VIOLP1;

        break;
      case 1:
        MotSys[cc].PwmPh = PH_M1;
        MotSys[cc].PwmCh = PWM_CH1_M1;
        MotSys[cc].PwmChId = PWM_CHID1_M1;
        MotSys[cc].PosCh = POS_CH_M1;
        MotSys[cc].CurCh = CUR_CH_M1;
        MotSys[cc].CpOk = CP2_OK;
        MotSys[cc].CpNok = CP2_NOK;
        MotSys[cc].Gom = GOM2;
        MotSys[cc].Pc = PC2;
        MotSys[cc].Pa = PA2;
        MotSys[cc].Prg = PRG2;
        MotSys[cc].Err = ERR2;
        MotSys[cc].Frn = FRN2;
        MotSys[cc].ViolP = VIOLP2;

        break;
    }
  }
  pm = &MotSys[1];



  /* Initialize the CPUM3 VS System. */
  if ((cc = VSDoorsSMP_InitAll(Context)) != SES_OKAY)
    return(cc);
  return(cc);
}

unsigned char DoorsMain(SEM_CONTEXT *pSEMCont)
{
  unsigned char cc=SES_OKAY;
  SEM_EVENT_TYPE eventNo;

  pm = &MotSys[Sys];
  RestoreMotSys(pm);
  if (DisplayMode != DISPLAY_MODE_DEBUG)
  {
    DisplayValues();
    if (bDisDbg == 0)
      US_UpdateDisplay((AT91PS_USART)DBGU_pt);
    else
      US_UpdateDisplay2((AT91PS_USART)DBGU_pt);
  }
  UpdatePwm();
  SerialInOut();
  DoorsManageInputs();
  DoorsManageCommands();
  CheckPosCur();
  SaveMotSys(pm);
  if(SEQ_RetrieveEvent(&eventNo,Sys) != UCC_QUEUE_EMPTY)
        {
          switch (Sys)
          {
            case 0:
            case 1:
              if ((cc = DoorsInstance(pSEMCont, eventNo, pm->Sys)) != SES_OKAY)
                return(cc);
              SaveMotSys(pm);
              break;
            case 2:
              break;
          }
        }

  return(cc);
}


