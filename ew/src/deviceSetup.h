// -----------------------------------------------------------------------------
//
// Headers
//
// -----------------------------------------------------------------------------

#ifndef deviceSetup_h
#define deviceSetup_h

#include <intrinsics.h>

//definizioni da mentenere mutuamente esclusive
//#define AM_KEY_VER       //AM_KEY_VER = versione Americana  con chiave 1:1
//#define PT_KEY_VER          //PT_KEY_VER = versione Europea  con chiave a punteggio
#define __inline inline

extern unsigned int MaskInt;
extern __istate_t istate;

// -----------------------------------------------------------------------------
//
// Disabilita tutti gli interrupt
//
// -----------------------------------------------------------------------------
__inline void DisAllInt(void)
{
  //MaskInt= AT91C_BASE_AIC->AIC_IMR;
  //AT91C_BASE_AIC->AIC_IDCR = 0xFFFFFFFF;
	istate = __get_interrupt_state();
	__disable_interrupt();
}
// -----------------------------------------------------------------------------
//
// Abilita tutti gli interrupt
//
// ---------------------------------------------------------------------------
__inline void EnAllInt(void)
{
 // AT91C_BASE_AIC->AIC_IECR = MaskInt;
	__set_interrupt_state(istate);	
}




//#define ORSINI

// -----------------------------------------------------------------------------
#ifdef ORSINI
// -----------------------------------------------------------------------------
#define PC1           28 //USCITA APLL
#define PA1           42 //USCITA APLL
#define PC2           29 //USCITA APLL
#define PA2           44 //USCITA APLL
#define PRG1          45 //USCITA APLL
#define PRG2          46 //USCITA APLL
#define ERR1          47 //USCITA APLL
#define ERR2          48 //USCITA APLL

#define AYPROX        49 //INGRESSO APLL
#define AYINT_SPI     50 //INGRESSO APLL
#define AYVBUS        51 //INGRESSO APLL

//#define DIP4           1 //INGRESSO APLL

//  NOMENCLATURA PORTE DELL'ARM PER LA GESTIONE DEGLI I/O SERIALIZZATI
#define DS_IN   AT91C_PIO_PA26    // Ingresso dati seriali
#define SH_IO   AT91C_PIO_PA18    // Uscita clock shift dati I/O
#define ST_IN   AT91C_PIO_PA17    // Uscita per il latch dati Input
#define PL_IN   AT91C_PIO_PA23    // Uscita di parallel load dati Input

#define OE_OUT  AT91C_PIO_PA15    // Uscita Output enable 74HC595
#define DS_OUT  AT91C_PIO_PA30    // Uscita dati seriali Output
#define ST_OUT  AT91C_PIO_PA8     // Uscita di latch dati di Output

#define INT_SPI AT91C_PIO_PA25    // Ingresso Segnale /INT da ISD
#define MASK_IN (DS_IN | INT_SPI) // Maschera ingressi PIO per config.

#define MASK_OUT_HC597 (SH_IO | ST_IN | PL_IN)
#define MASK_OUT_HC595 (DS_OUT | ST_OUT)

//  NOMENCLATURA PORTE DELL'ARM PER LA GESTIONE DEI MOTORI

#define CP1_OK    17                        // Indisponibile
#define CP1_NOK   18                        // Indisponibile
#define GOM1      48                        //
#define FRN1      19                        // Indisponibile

#define CP2_OK     3                        // Indisponibile
#define CP2_NOK    4                        // Indisponibile
#define GOM2      36                        //
#define FRN2      20                        // Indisponibile

#define V_SAMP  AT91C_PIO_PA16              // Alimentazione sintesi
#define LEDRUN  AT91C_PIO_PA31              // LED RUN

// Maschera delle uscite PIO per configurazione
#define MASK_OUT (MASK_OUT_HC597 | MASK_OUT_HC595 | V_SAMP | LEDRUN)

#define RESPC     21                        // Indisponibile

#define LP1       22                        // Indisponibile
#define AV1        7                        //
#define AT1        6                        //

#define LP2       23                        // Indisponibile
#define AV2        5                        //
#define AT2        4                        //
#define LAMP       8                        //
#define RESSOD    24                        // Indisponibile
#define PCHIUSE   25                        // Indisponibile
#define IMPOFF    26                        // Indisponibile
#define LAMP_IN   27                        // Indisponibile
#define LAMP_OUT   3                        // Uscita aux2
#define OUT_AUX    2                        // Uscita aux1


#define PULS1     44                        //
#define PULS2     34                        //
#define UUSC      38                        //
#define PRING     37                        //
#define PCON      24                        // Indisponibile
#define ATR       45                        //
#define METAL     35                        //
#define OBJECT    47                        //
#define MORE      46                        //
#define RESrem    18                        // Indisponibile
#define AMANrem   21                        // Indisponibile
#define EMERGrem  20                        // Indisponibile
#define RETE      39                        //
#define BLOW      40                        //

//  NOMENCLATURA PORTE SPI DELL'ARM PER LA GESTIONE DELLA SINTESI VOCALE
#define REC_MSG   41                        //
#define PLAY_MSG  33                        //

#define PH_M0           0
#define PWM_CH1_M0      PWMC_CH3
#define PWM_CHID1_M0    AT91C_PWMC_CHID3
#define PWM_CH2_M0      PWMC_CH2
#define PWM_CHID2_M0    AT91C_PWMC_CHID2
#define POS_CH_M0       CHANNEL5
#define CUR_CH_M0       CHANNEL6

#define PH_M1           0
#define PWM_CH1_M1      PWMC_CH1
#define PWM_CHID1_M1    AT91C_PWMC_CHID1
#define PWM_CH2_M1      PWMC_CH0
#define PWM_CHID2_M1    AT91C_PWMC_CHID0
#define POS_CH_M1       CHANNEL7
#define CUR_CH_M1       CHANNEL4

// -----------------------------------------------------------------------------
#else                         // ALLUSER
// -----------------------------------------------------------------------------

#define PA1           42 //APLL
#define PA2           44 //APLL
#define PRG1          45 //APLL
#define PRG2          46 //APLL
#define ERR1          47 //APLL
#define ERR2          48 //APLL
#define VIOLP1		  49 //APLL
#define VIOLP2		  50 //APLL		


#define AYPROX        49 //APLL
#define AYINT_SPI     50 //APLL
#define AYVBUS        51 //APLL

//#define DIP4          1 //APLL

//  NOMENCLATURA PORTE DELL'ARM PER LA GESTIONE DEGLI I/O SERIALIZZATI
#define DS_IN   AT91C_PIO_PA26  // Porta PA26 Ingresso dati seriali
#define SH_IO   AT91C_PIO_PA18  // Porta PA18 Uscita clock shift dati I/O
#define ST_IN   AT91C_PIO_PA17  // Porta PA17 Uscita per il latch dati Input
#define PL_IN   AT91C_PIO_PA23  // Porta PA23 Uscita di parallel load dati Input

#define OE_OUT  AT91C_PIO_PA15  // Porta PA15 Uscita Output enable 74HC595
#define DS_OUT  AT91C_PIO_PA30  // Porta PA30 Uscita dati seriali Output
#define ST_OUT  AT91C_PIO_PA8  // Porta PA8 Uscita di latch dati di Output

#define PROX    AT91C_PIO_PA0   // Porta PA0 Ingresso da lettore prox
#define INT_SPI AT91C_PIO_PA1   // PA1 - pin31 Segnale /INT da ISD

#define MASK_IN (DS_IN | PROX | INT_SPI)  // Maschera ingressi PIO per config.
#define MASK_OUT_HC597 (SH_IO | ST_IN | PL_IN)
#define MASK_OUT_HC595 (DS_OUT | ST_OUT)

//  NOMENCLATURA PORTE DELL'ARM PER LA GESTIONE DEI MOTORI
// I COMANDI ENABLE1 ENABLE2 SONO DEFINITI CONTESTUALMENTE AL PWM
#define PHASE1  AT91C_PIO_PA28              // PA28- pin38 Uscita direzione M1


#define PHASE2  AT91C_PIO_PA27              // PA27 - pin 37 Uscita direzione M2


#define V_SAMP AT91C_PIO_PA29               // PA29 - pin 41 ALIMENTAZIONE SINTESI
#define LEDRUN  AT91C_PIO_PA2               // PA2 - pin 44 Uscita direzione M2
#define RTS1    AT91C_PIO_PA24              // PA24 - pin 37 Uscita direzione M2

// Maschera delle uscite PIO per configurazione
#define MASK_OUT (MASK_OUT_HC597 | MASK_OUT_HC595 | PHASE1 | PHASE2 | V_SAMP | LEDRUN | RTS1)

#define INIB_MET     1                      // Uscita latch HC595_1 IC7 pin 15 Uscita inibizione metal
#define IMPOFF       2                      // Uscita latch HC595_1 IC7 pin 1
#define MAN_AUTO     3                      // Uscita latch HC595_1 IC7 pin 2
#define LAMP         4                      // Uscita latch HC595_1 IC7 pin 3
#define LAMP_IN      5                      // Uscita latch HC595_1 IC7 pin 4 Lampada di ingresso
#define LAMP_OUT     6                      // Uscita latch HC595_1 IC7 pin 5 Lampada di uscita
#define RESSOD       7                      // Uscita latch HC595_1 IC7 pin 6
#define PCHIUSE      8                      // Uscita latch HC595_1 IC7 pin 7

#define OUT_AUX      9                      // Uscita latch HC595_1 IC8 pin 15 Uscita ausiliaria
#define PC1         10
#define REL_P1      11
#define PC2         12
#define REL_P2      13
#define RESPC       14                      // Uscita latch HC595_2 IC8 pin 5
#define FRN2        15                      // Uscita latch HC595_2 IC8 pin 6
#define FRN1        16                      // Uscita latch HC595_2 IC8 pin 7

#define LP1         17                      // Uscita latch HC595_3 IC9 pin 15
#define AV1         18                      // Uscita latch HC595_3 IC9 pin 1
#define AT1         19                      // Uscita latch HC595_3 IC9 pin 2
#define LP2         20                      // Uscita latch HC595_3 IC9 pin 3
#define AV2         21                      // Uscita latch HC595_3 IC9 pin 4
#define AT2         22                      // Uscita latch HC595_3 IC9 pin 5
#define AV          23                      // Uscita latch HC595_3 IC9 pin 6
#define AT          24                      // Uscita latch HC595_3 IC9 pin 7

#define REL_P2CB    25                      // Uscita latch HC595_3 IC9 pin 15
#define REL_P1CB    26                      // Uscita latch HC595_3 IC9 pin 15
#define P2CH        27                      // Uscita latch HC595_3 IC9 pin 15
#define P1CH        28                      // Uscita latch HC595_3 IC9 pin 15
#define ING_OK      29                      // Uscita latch HC595_3 IC9 pin 15
#define USC_OK      30                      // Uscita latch HC595_3 IC9 pin 15
#define IMPOFF_CB   31                      // Uscita latch HC595_3 IC9 pin 15
#define ATR_CB      32                      // Uscita latch HC595_3 IC9 pin 15

#define RETE_CB     33                      // Uscita latch HC595_3 IC9 pin 15
#define START_BIO   34                      // Uscita latch HC595_3 IC9 pin 15
#define MORE_CB     35                      // Uscita latch HC595_3 IC9 pin 15
#define BIOM_NOK    36                      // Uscita latch HC595_3 IC9 pin 15
#define READY       37                      // Uscita latch HC595_3 IC9 pin 15
#define P1P2AP      38                      // Uscita latch HC595_3 IC9 pin 15

#ifdef CLEAR_ID_DEBUG
#define SW_PA3       1                      // In.latch HC597  IC2_CB pin 5
#else
#define SW_AFT_LB_ST 1                      // In.latch HC597  IC2_CB pin 5
#endif
#define FIRE         2                      // In.latch HC597  IC1_CB pin 15
#define SW_PC3       3                      // In.latch HC597  IC2_CB pin 4
#define BIOM_OK      4                      // In.latch HC597  IC1_CB pin 1
#define ROT_EMERG    5                      // In.latch HC597  IC2_CB pin 3
#define BIOM_NOTOK   6                      // In.latch HC597  IC1_CB pin 2
#define PROX_DIS2    7                      // In.latch HC597  IC2_CB pin 2
#define PROX_DIS1    8                      // In.latch HC597  IC1_CB pin 3
#define PROX_NORM2   9                      // In.latch HC597  IC2_CB pin 1
#define PROX_NORM1  10                      // In.latch HC597  IC1_CB pin 4
#define SS2_CB      11                      // In.latch HC597  IC2_CB pin 15
#define SS1_CB      12                      // In.latch HC597  IC1_CB pin 5
#define SP2_CB      13                      // In.latch HC597  IC1_CB pin 7
#define SP1_CB      14                      // In.latch HC597  IC1_CB pin 6
#define CB_ON1      15                      // In.latch HC597  IC2_CB pin 6
#define CB_ON2      16                      // In.latch HC597  IC2_CB pin 7

#define METAL       17                      // Ingresso latch HC597_7 IC6 pin 15
#define RESrem      18                      // Ingresso latch HC597_7 IC6 pin 1
#define ON_OFFrem   19                      // Ingresso latch HC597_7 IC6 pin 2
#define EMERGrem    20                      // Ingresso latch HC597_7 IC6 pin 3
#define AMANrem     21                      // Ingresso latch HC597_7 IC6 pin 4
#define P2rem       22                      // Ingresso latch HC597_7 IC6 pin 5
#define ANTI_BT2    22                      // Ingresso latch HC597_4 IC3 pin 5
#define P1rem       23                      // Ingresso latch HC597_7 IC6 pin 6
#define ANTI_BT1    23                      // Ingresso latch HC597_7 IC6 pin 6
#define PCON        24                      // Ingresso latch HC597_7 IC6 pin 7

#define PRING       25                      // Ingresso latch HC597_6 IC5 pin 15
#define VIP         26                      // Ingresso latch HC597_6 IC5 pin 1
#define UUSC        27                      // Ingresso latch HC597_6 IC5 pin 2
#define RETE        28                      // Ingresso latch HC597_6 IC5 pin 3
#define BLOW        29                      // Ingresso latch HC597_6 IC5 pin 4
#define ATR         30                      // Ingresso latch HC597_6 IC5 pin 5
#define MORE        31                      // Ingresso latch HC597_6 IC5 pin 6
#define OBJECT      32                      // Ingresso latch HC597_6 IC5 pin 7

#define CP1_OK      33                      // Ingresso latch HC597_5 IC4 pin 15
#define CP1_NOK     34                      // Ingresso latch HC597_5 IC4 pin 1
#define GOM1        35                      // Ingresso latch HC597_5 IC4 pin 2
#define SP1         36                      // Ingresso latch HC597_5 IC4 pin 3
#define SS1         37                      // Ingresso latch HC597_5 IC4 pin 4
#define CP2_OK      38                      // Ingresso latch HC597_5 IC4 pin 5
#define CP2_NOK     39                      // Ingresso latch HC597_5 IC4 pin 6
#define GOM2        40                      // Ingresso latch HC597_5 IC4 pin 7

#define SP2         41                      // Ingresso latch HC597_4 IC3 pin 15
#define SS2         42                      // Ingresso latch HC597_4 IC3 pin 1
#define PULS        43                      // Ingresso latch HC597_4 IC3 pin 2
#define PULS2       44                      // Ingresso latch HC597_4 IC3 pin 3
#define PULS1       45                      // Ingresso latch HC597_4 IC3 pin 4
#define REC_MSG     46                      // Ingresso latch HC597_4 IC3 pin 5
#define PLAY_MSG    47                      // Ingresso latch HC597_4 IC3 pin 6
#define SERVICE     48                      // Ingresso latch HC597_4 IC3 pin 7


#define PH_M0           PHASE1
#define PWM_CH1_M0      PWMC_CH3
#define PWM_CHID1_M0    AT91C_PWMC_CHID3
#define PWM_CH2_M0      0
#define PWM_CHID2_M0    0
#define POS_CH_M0       CHANNEL4
#define CUR_CH_M0       CHANNEL5

#define PH_M1           PHASE2
#define PWM_CH1_M1      PWMC_CH2
#define PWM_CHID1_M1    AT91C_PWMC_CHID2
#define PWM_CH2_M1      0
#define PWM_CHID2_M1    0
#define POS_CH_M1       CHANNEL6
#define CUR_CH_M1       CHANNEL7

// -----------------------------------------------------------------------------
#endif
// -----------------------------------------------------------------------------
void ResetWatchdog(void);

void InitDevice(void);

void LoadParameter(void);

#endif
