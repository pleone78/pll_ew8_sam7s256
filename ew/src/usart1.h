// -----------------------------------------------------------------------------
//
//  Definizioni
//
// -----------------------------------------------------------------------------

#ifndef usart1_h
#define usart1_h

#include "AT91SAM7S256.h"
#define __inline inline
#include "lib_AT91SAM7S256.h"

#define US1_CLKDIV  (unsigned int) 16          // B_187200


// -----------------------------------------------------------------------------
//
//  Definizione I/O consolle
//
// -----------------------------------------------------------------------------

typedef struct
{
  unsigned int b0 : 1;
  unsigned int b1 : 1;
  unsigned int b2 : 1;
  unsigned int b3 : 1;
  unsigned int b4 : 1;
  unsigned int b5 : 1;
  unsigned int b6 : 1;
  unsigned int b7 : 1;
  unsigned int b8 : 1;
  unsigned int b9 : 1;
  unsigned int b10 : 1;
  unsigned int b11 : 1;
  unsigned int b12 : 1;
  unsigned int b13 : 1;
  unsigned int b14 : 1;
  unsigned int b15 : 1;
  unsigned int     : 16;
} bit32;


union byte4bit32
{
  unsigned char BB[3];
  bit32 bb32;
} ;

typedef struct
{
  unsigned char cnsout0;
  unsigned char cnsout1;
  unsigned short nmes;
  unsigned char pmes;
  unsigned char outv1;
  unsigned char outv2;
  unsigned char outv3;
  unsigned char outv4;
  unsigned char outp;
  unsigned char outd;
} cnsdsp;

union txdsp
{
  unsigned char txdspcns[14];
  cnsdsp cns13;
} ;

// -----------------------------------------------------------------------------
//
//  Definizione I/O consolle standard principale
//
// -----------------------------------------------------------------------------

//  Definizione I/O consolle principale con display

#define  CONF_F2      INGRESSICNS[1].bb32.b0    // Tasto conferma comando / f2
#define  AUTO_GST_F1  INGRESSICNS[1].bb32.b1    // Selezione autogest o no / f1
#define  SEL_UUEM_F3  INGRESSICNS[1].bb32.b8    // Selezione ult.usc. in emerg.
#define  SEL_OPC      INGRESSICNS[1].bb32.b12   // Selezione funz.porte chiuse
#define  SEL_VAUTO    INGRESSICNS[1].bb32.b13   // Selezione verifica automatica
#define  SEL_MTLAB    INGRESSICNS[1].bb32.b14   // Selez. metal abilitato in P.I
#define  SEL_BLK      INGRESSICNS[1].bb32.b15   // Blocco in allarme rosso
#define  SEL_PERS_ENT INGRESSICNS[1].bb32.b7    // Selezione controllo + persone / enter

#define  ON_OFFp      INGRESSICNS[1].bb32.b5    // Chiave accensione
#define  EMERGp       INGRESSICNS[1].bb32.b10   // Chiave emergenza
#define  INIB_METp    INGRESSICNS[1].bb32.b9    // Chiave metal
#define  MAN_AUTp     INGRESSICNS[1].bb32.b2    // Tasto manuale
#define  SEL_DIRp     INGRESSICNS[1].bb32.b11   // Selezione DIREZIONE
#define  COM_MANOUTp  INGRESSICNS[1].bb32.b6    // Comando manuale USCITA
#define  RST_MDp      INGRESSICNS[1].bb32.b3    // Reset metal
#define  COM_MANINp   INGRESSICNS[1].bb32.b4    // Comando manuale INGRESSO

#define  D_RETEp      USCITECNS[1].bb32.b15
#define  D_BATTp      USCITECNS[1].bb32.b14
#define  D_MDISBp     USCITECNS[1].bb32.b1
#define  D_EMERGp     USCITECNS[1].bb32.b0
#define  D_SINGRp     USCITECNS[1].bb32.b12
#define  D_SUSCIp     USCITECNS[1].bb32.b13
//#define  D_MANUp      USCITECNS[1].bb32.b11
#define  D_AUTOp      USCITECNS[1].bb32.b11
#define  D_PUINTp     USCITECNS[1].bb32.b2

#define  D_SWINTp     USCITECNS[1].bb32.b3
#define  D_SWEXTp     USCITECNS[1].bb32.b5
#define  D_PUEXTp     USCITECNS[1].bb32.b4
#define  D_CICp       USCITECNS[1].bb32.b9
#define  D_ATRp       USCITECNS[1].bb32.b6
//#define  D_AUTOp      USCITECNS[1].bb32.b8
#define  D_MANUp      USCITECNS[1].bb32.b8
#define  D_ALLp       USCITECNS[1].bb32.b7
#define  D_SUOp       USCITECNS[1].bb32.b10

//  Definizione I/O consolle principale standard senza display

#define  CONFp_nd        INGRESSICNS_NODISP[1].bb32.b0    // Comando manuale INGRESSO

#define  ON_OFFp_nd      INGRESSICNS_NODISP[1].bb32.b8    // Chiave accensione
#define  EMERGp_nd       INGRESSICNS_NODISP[1].bb32.b9    // Chiave emergenza
#define  INIB_METp_nd    INGRESSICNS_NODISP[1].bb32.b10   // Chiave metal
#define  MAN_AUTp_nd     INGRESSICNS_NODISP[1].bb32.b11   // Tasto manuale
#define  SEL_DIRp_nd     INGRESSICNS_NODISP[1].bb32.b12   // Selezione DIREZIONE
#define  COM_MANOUTp_nd  INGRESSICNS_NODISP[1].bb32.b13   // Comando manuale USCITA
#define  RST_MDp_nd      INGRESSICNS_NODISP[1].bb32.b14   // Reset metal
#define  COM_MANINp_nd   INGRESSICNS_NODISP[1].bb32.b15   // Comando manuale INGRESSO

#define  D_RETEp_nd      USCITECNS_NODISP[1].bb32.b0
#define  D_BATTp_nd      USCITECNS_NODISP[1].bb32.b1
#define  D_MDISBp_nd     USCITECNS_NODISP[1].bb32.b2
#define  D_EMERGp_nd     USCITECNS_NODISP[1].bb32.b3
#define  D_SINGRp_nd     USCITECNS_NODISP[1].bb32.b4
#define  D_SUSCIp_nd     USCITECNS_NODISP[1].bb32.b5
#define  D_MANUp_nd      USCITECNS_NODISP[1].bb32.b6
#define  D_PUINTp_nd     USCITECNS_NODISP[1].bb32.b7

#define  D_SWINTp_nd     USCITECNS_NODISP[1].bb32.b8
#define  D_SWEXTp_nd     USCITECNS_NODISP[1].bb32.b9
#define  D_PUEXTp_nd     USCITECNS_NODISP[1].bb32.b10
#define  D_CICp_nd       USCITECNS_NODISP[1].bb32.b11
#define  D_ATRp_nd       USCITECNS_NODISP[1].bb32.b12
#define  D_AUTOp_nd      USCITECNS_NODISP[1].bb32.b13
#define  D_ALLp_nd       USCITECNS_NODISP[1].bb32.b14
#define  D_SUOp_nd       USCITECNS_NODISP[1].bb32.b15

// -----------------------------------------------------------------------------
//
//  Definizione I/O consolle standard remota
//
// -----------------------------------------------------------------------------

#define  D_RETEr  USCITECNS[2].bb32.b0
#define  D_BATTr  USCITECNS[2].bb32.b1
#define  D_MDISBr USCITECNS[2].bb32.b2
#define  D_EMERGr USCITECNS[2].bb32.b3
#define  D_SINGRr USCITECNS[2].bb32.b4
#define  D_SUSCIr USCITECNS[2].bb32.b5
#define  D_MANUr  USCITECNS[2].bb32.b6
#define  D_PUINTr USCITECNS[2].bb32.b7

#define  D_SWINTr USCITECNS[2].bb32.b8
#define  D_SWEXTr USCITECNS[2].bb32.b9
#define  D_PUEXTr USCITECNS[2].bb32.b10
#define  D_CICr   USCITECNS[2].bb32.b11
#define  D_ATRr   USCITECNS[2].bb32.b12
#define  D_AUTOr  USCITECNS[2].bb32.b13
#define  D_ALLr   USCITECNS[2].bb32.b14
#define  D_SUOr   USCITECNS[2].bb32.b15


// -----------------------------------------------------------------------------
//
//  Definizione I/O consolle S.Paolo principale
//
// -----------------------------------------------------------------------------
/*
#define  L_RETEp  USCITECNS[1].bb32.b15
#define  L_BATTp  USCITECNS[1].bb32.b14
#define  L_MDISBp USCITECNS[1].bb32.b1
#define  L_EMERGp USCITECNS[1].bb32.b0
#define  L_SINGRp USCITECNS[1].bb32.b12
#define  L_SUSCIp USCITECNS[1].bb32.b13
#define  L_MANUp  USCITECNS[1].bb32.b11
#define  L_PUINTp USCITECNS[].bb32.b2

#define  L_SWINTp USCITECNS[1].bb32.b3
#define  L_SWEXTp USCITECNS[1].bb32.b5
#define  L_PUEXTp USCITECNS[1].bb32.b4
#define  L_CICp   USCITECNS[1].bb32.b9
#define  L_ATRp   USCITECNS[1].bb32.b6
#define  L_AUTOp  USCITECNS[1].bb32.b8
#define  L_ALLp   USCITECNS[1].bb32.b7
#define  L_SUOp   USCITECNS[1].bb32.b10
*/
// -----------------------------------------------------------------------------
//
//  Definizione I/O consolle S.Paolo remota
//
// -----------------------------------------------------------------------------

#define  L_RETEr  USCITECNS[2].bb32.b0
#define  L_BATTr  USCITECNS[2].bb32.b1
#define  L_MDISBr USCITECNS[2].bb32.b2
#define  L_EMERGr USCITECNS[2].bb32.b3
#define  L_SINGRr USCITECNS[2].bb32.b4
#define  L_SUSCIr USCITECNS[2].bb32.b5
#define  L_MANUr  USCITECNS[2].bb32.b6
#define  L_PUINTr USCITECNS[2].bb32.b7

#define  L_SWINTr USCITECNS[2].bb32.b8
#define  L_SWEXTr USCITECNS[2].bb32.b9
#define  L_PUEXTr USCITECNS[2].bb32.b10
#define  L_CICr   USCITECNS[2].bb32.b11
#define  L_ATRr   USCITECNS[2].bb32.b12
#define  L_AUTOr  USCITECNS[2].bb32.b13
#define  L_ALLr   USCITECNS[2].bb32.b14
#define  L_SUOr   USCITECNS[2].bb32.b15

extern union byte4bit32 USCITECNS[3];
extern union byte4bit32 INGRESSICNS[3];
extern AT91PS_USART US1_pt;
extern unsigned char NByteTx;
extern unsigned char Nbb, Ncns, CnsBusy/*, Chks*/;

extern union txdsp TxCns[];

extern unsigned char BeepRun,SlowBeep,LedMetRun;

void US1_init ( void );
void ConsolleCom(void);
unsigned char ChangeCns (unsigned char);

#endif
