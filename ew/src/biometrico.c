// -----------------------------------------------------------------------------
//
// Note tecniche sul protocollo di dialogo con il biometrico
//
// -----------------------------------------------------------------------------

// Il biometrico invia ogni 500-700 ms un messaggio che pu� essere una
// richiesta di stato o la lista dei comandi di uscita a cui la logica deve
// rispondere nei rispettivi modi.

// Richiesta di stato da biometrico a logica

// \001DE\005 dove E � il checksum xor di \001 e D

// Risposta della logica

// \001R12345678C\005 dove 1...8 sono 0 o 1 a seconda se l'ingresso associato
// sia attivo oppure no

// \001 - Inizio pacchetto

// 1 - Start controllo biometrico
// 2 - Non usato
// 3 - Cancellazione file su evento
// 4 - Fotogrammi di allarme
// 5 - Rileva solo FING
// 6 - Rileva solo FACE
// 7 - Non usato
// 8 - Non usato

// C xor dei byte precedenti compresi \001 ed R (0x01^0x0x52^0x31^...0x31)

// \005 - Fine pacchetto

// Lista comandi di uscita

// \001O1234C\005

// \001 - Inizio pacchetto

// 1 - Biometrico OK
// 2 - Biometrico NOK
// 3 - Attiva audio
// 4 - Blocca porta per lettura dito

// C xor dei byte precedenti compresi \001 ed O (0x01^0x0x4F^0x30^...0x30)

// Dall'attivazione del biometrico il segnale FING o FACE deve essere attivo

// Start biometrico deve essere attivo fino all'uscita dei comandi OK o NOK

// L'accensione del PC biometrico deve essere comandata manualmente da consolle
// e la consolle deve evidenziare lo stato del PC biometrico: PC acceso, PC in
// linea; � in linea quando la seriale della logica riceve richieste e comandi


// Attiva audio deve accendere l'amplificatore quando viene ricevuto

// Fotogrammi di allarme corrisponde allo STBM2 "Start registrazione video"

// Risposta della logica alla lista dei comandi di uscita

// \001OK\006\005

// Significa: lista dei comandi ricevuta.

// -----------------------------------------------------------------------------
//
// Headers
//
// -----------------------------------------------------------------------------

#include	"VSLogicData.h"
#include	"deviceSetup.h"

#include "biometrico.h"

// -----------------------------------------------------------------------------
//
// Variabili
//
// -----------------------------------------------------------------------------

unsigned char char_rx_US0;

unsigned char NByBiom;

unsigned char RxBiom[12];

unsigned char TxBiom[13];

unsigned char PCBioPolling = 0;

char Ackn[] =
{
  "\001OK\006\005\0"                // Comando del biometrico ricevuto
};

// -----------------------------------------------------------------------------
//
// Costanti
//
// -----------------------------------------------------------------------------

AT91PS_USART US0_pt = AT91C_BASE_US0;

// -----------------------------------------------------------------------------

// Gestione interrupt da porta seriale US0 per Biometrico

// La routine registra i caratteri ricevuti nel buffer RxBiom: ogni dato viene
// sottoposto a xor con il risultato precedente e depositato nella locazione
// RxBiom[8]: poich� il checksum dei dati in arrivo precede l'ultimo carattere
// ricevuto lo xor calcolato per la verifica comprende anche l'ultimo carattere
// ed � uguale al carattere di chiusura pacchetto 0x05.

// Quando riceve l'interrupt di TIMEOUT significa fine del pacchetto dati o dei
// caratteri spurii dovuti a disturbi e quindi va a verificare i dati:
// se l'indice dei caratteri ricevuti � 4 � una richiesta stato mentre se � 8
// � una lista di comandi: nel primo caso viene inviato il contenuto del buffer
// TxBiom che contiene i dati di stato (Start biometrico, ecc) mentre nel
// secondo caso viene inviata la risposta standard OK

// Il buffer di trasmissione TxBiom � soggetto a variazioni quindi il checksum �
// calcolato dinamicamente e depositato nella locazione TxBiom[10].

// Il canale di trasmissione � abilitato solo durante l'invio per evitare gli
// interrupt del flag TXEMPTY.

// -----------------------------------------------------------------------------

__ramfunc void US0_irq_handler( void )
{
  unsigned int Status;
  static  unsigned char tt = 0,BiomBusy = 0;

  Status = US0_pt->US_CSR;

  if (( Status & AT91C_US_RXRDY ) && (BiomBusy == 0)) // Ricezione caratteri
  {
    char_rx_US0 = US0_pt->US_RHR & 0xFF;
    RxBiom[NByBiom++] = char_rx_US0;
    RxBiom[8] ^= char_rx_US0;
  }

  if ( Status & AT91C_US_TIMEOUT )                   //  Verifiche a timeout
  {
    if (RxBiom[8]== 0x05)
    {
      PCBioPolling = 1;
      switch ( NByBiom )
      {
        case 4:
                tt = 0;
                BiomBusy = 1;
                US0_pt->US_CR = AT91C_US_TXEN;
                US0_pt->US_IER = AT91C_US_TXEMPTY;
                break;
        case 8:
                tt = 0;
                BiomBusy = 2;
                US0_pt->US_CR = AT91C_US_TXEN;
                US0_pt->US_IER = AT91C_US_TXEMPTY;
                break;
        default:
                RxBiom[8] = 0x00;
                NByBiom=0;
      }
    }
    else
    {
      RxBiom[8] = 0x00;                               // In caso di errori di
      NByBiom=0;                                      // ricezione ricomincia
    }

    US0_pt->US_CR = AT91C_US_STTTO;                   // Restart TIMEOUT
  }

  if (Status & AT91C_US_TXEMPTY)                      // Tx ad intrrupt su
  {                                                   // flag TXEMPTY
    switch (BiomBusy)
    {
      case 1:
              US0_pt->US_THR = TxBiom[tt];
              TxBiom[10] ^= TxBiom[tt];
              tt++;
              if (TxBiom[tt] == '\0')
              {
                US0_pt->US_IDR = AT91C_US_TXEMPTY;
                US0_pt->US_CR = AT91C_US_TXDIS;
                tt = 0;
                BiomBusy = 0;
                TxBiom[10] = 0x00;
                RxBiom[8] = 0x00;
                NByBiom=0;
              }
              break;
      case 2:
              US0_pt->US_THR = Ackn[tt];
              tt++;
              if (Ackn[tt] == '\0')
              {
                US0_pt->US_IDR = AT91C_US_TXEMPTY;
                US0_pt->US_CR = AT91C_US_TXDIS;
                tt = 0;
                BiomBusy = 0;
                TxBiom[10] = 0x00;
                RxBiom[8] = 0x00;
                NByBiom=0;
              }
              break;              
      default:
              BiomBusy = 0;                     
    }
  }

  US0_pt->US_CR = AT91C_US_RSTSTA;
}

// -----------------------------------------------------------------------------
//
//  Inizializzazione porta seriale US0
//
// -----------------------------------------------------------------------------

void US0_init ( void )
{
  //  Configure PIO controllers to periph mode

  AT91F_PIO_CfgPeriph( AT91C_BASE_PIOA,
                     ( AT91C_PA5_RXD0 ) |
                     ( AT91C_PA6_TXD0 ),
                                     0);     // Peripheral B

  // Enable the clock of the PIOA

  AT91F_PMC_EnablePeriphClock ( AT91C_BASE_PMC, 1 << AT91C_ID_US0 );

  // Disable interrupts

  US0_pt->US_IDR = 0xFFFFFFFF;//(unsigned int) -1;

  // Reset receiver and transmitter

  US0_pt->US_CR =AT91C_US_RSTRX | AT91C_US_RSTTX | AT91C_US_RXDIS | AT91C_US_TXDIS;

  // Define the USART mode

  US0_pt->US_MR =AT91C_US_USMODE_NORMAL |
                 AT91C_US_CLKS_CLOCK |
                 AT91C_US_CHRL_8_BITS |
                 AT91C_US_PAR_NONE |
                 AT91C_US_NBSTOP_1_BIT;

  // Define the baud rate divisor register

  US0_pt->US_BRGR = US0_CLKDIV;


//  AT91F_US_EnableIt(US1_pt, AT91C_US_TXRDY );

  US0_pt->US_RTOR = 4;

 // Usart Configure

  // Enable usart

  US0_pt->US_CR = AT91C_US_RXEN | AT91C_US_TXEN;

  // Open Usart  interrupt

  AT91F_AIC_ConfigureIt

  (AT91C_BASE_AIC,

    AT91C_ID_US0,

    US0_INTERRUPT_LEVEL,

    AT91C_AIC_SRCTYPE_INT_HIGH_LEVEL,

    US0_irq_handler);

  // Enable USART IT error and RXRDY

  //AT91F_US_EnableIt( US0_pt, AT91C_US_TIMEOUT | AT91C_US_RXRDY | AT91C_US_TXEMPTY );
  AT91F_US_EnableIt

  ( US0_pt, AT91C_US_TIMEOUT | AT91C_US_RXRDY );

  US0_pt->US_CR = AT91C_US_RSTSTA;

  AT91F_AIC_EnableIt (AT91C_BASE_AIC, AT91C_ID_US0);
                                     // Default buffer dati biometrico
  TxBiom[0] = 0x01;     //  0 \001 Inizio pacchetto
  TxBiom[1] = 0x52;     //  1 R
  StopBiom();           //  2 Start/Stop biometrico
  TxBiom[3] = 0x31;     //  3 Non usato
  TxBiom[4] = 0x31;     //  4 Cancellazione file su evento
//  FotoOffBiom();        //  5 Fotogrammi di allarme
//  FingerBiom();         //  6-7 FINGER / FACE
  TxBiom[5] = 0x31;     //  5 Fotogrammi di allarme     //zuck
  TxBiom[6] = 0x31;     //  6 FINGER                    //zuck
  TxBiom[7] = 0x31;     //  7 FACE                      //zuck
  TxBiom[8] = 0x31;     //  8 Non usato
  TxBiom[9] = 0x31;     //  9 Non usato
  TxBiom[10] = 0x00;    // 10 Checksum 0..9
  TxBiom[11] = 0x05;    // 11 \005 Fine pacchetto
  TxBiom[12] = 0x00;    // 12 \0 Fine buffer
}

// -----------------------------------------------------------------------------
//
// Start Biometrico
//
// -----------------------------------------------------------------------------

void StartBiom(void)
{           
  if (PCBioStatus!=0)
    TxBiom[2] = 0x30;
  else
    TxBiom[2] = 0x31;
}

// -----------------------------------------------------------------------------
//
// Stop Biometrico
//
// -----------------------------------------------------------------------------

void StopBiom(void)
{
  TxBiom[2] = 0x31;
}

// -----------------------------------------------------------------------------
//
// Biometrico Fotogrammi allarme
//
// -----------------------------------------------------------------------------

void FotoOffBiom(void)
{
  TxBiom[5] = 0x31;
}

// -----------------------------------------------------------------------------
//
// Biometrico Fotogrammi allarme
//
// -----------------------------------------------------------------------------

void FotoOnBiom(void)
{
  TxBiom[5] = 0x30;
}

// -----------------------------------------------------------------------------
//
// Biometrico Face
//
// -----------------------------------------------------------------------------

void FaceBiom(void)
{
  TxBiom[6] = 0x31;
  TxBiom[7] = 0x30;
}

// -----------------------------------------------------------------------------
//
// Biometrico Finger
//
// -----------------------------------------------------------------------------

void FingerBiom(void)
{
  TxBiom[6] = 0x30;
  TxBiom[7] = 0x31;
}





