// -----------------------------------------------------------------------------

// Headers

// -----------------------------------------------------------------------------

#ifndef biometrico_h
#define biometrico_h

#include "AT91SAM7S256.h"
#define __inline inline
#include "lib_AT91SAM7S256.h"

#define US0_INTERRUPT_LEVEL   6

#define AT91C_US_ASYNC_MODE ( AT91C_US_USMODE_NORMAL + \
                              AT91C_US_NBSTOP_1_BIT + \
                              AT91C_US_PAR_NONE + \
                              AT91C_US_CHRL_8_BITS + \
                              AT91C_US_CLKS_CLOCK )
                             

//#define US0_CLKDIV  (unsigned int) 16          // B_187200

//#define US0_CLKDIV  (unsigned int) 312         // B_9600

#define US0_CLKDIV  (unsigned int) 156           // B_19200

// #define US0_CLKDIV  (unsigned int) 78         // B_38400

// #define US0_CLKDIV  (unsigned int) 52         // B_57600

// #define US0_CLKDIV  (unsigned int) 26         // B_115200

extern unsigned char RxBiom[];
extern unsigned char TxBiom[];
extern unsigned char PCBioPolling;

// -----------------------------------------------------------------------------
//
// Prototipi
//
// -----------------------------------------------------------------------------

__ramfunc void US0_irq_handler( void ); 

void US0_init ( void );

void StartBiom(void);

void StopBiom(void);

void FaceBiom(void);

void FingerBiom(void);

void FotoOffBiom(void); 

void FotoOnBiom(void);

unsigned char AnswerBiom(void);

#endif
