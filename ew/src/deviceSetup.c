// *** include directives ***

#include <stdio.h>

#include "AT91SAM7S256.h"
#define __inline inline
#include "lib_AT91SAM7S256.h"

#include "deviceSetup.h"
#include "adc.h"
#include "pwm.h"
#include "usart.h"
#include "twi.h"
#include "inout.h"
#include "timer.h"
#include "biometrico.h"
#include "spi.h"

// *** variable definitions ***

unsigned int MaskInt = 0;
 __istate_t istate;

const char MSG_RESET[]= {
  "\n\rReset\n\r"
  };


// *** function definitions ***

void ResetWatchdog(void)
{
  static unsigned int Old=0;
  if (Old == 0)
  {
    Old = 1;
  }
  else
  {
    Old = 0;
  }
  //wd interno
  AT91F_WDTRestart( AT91C_BASE_WDTC );
}

void InitPeripheralClock (unsigned int peripheralID)
{
  AT91C_BASE_PMC->PMC_PCER = peripheralID;
}


void InitDevice(void)
{	
	AT91F_AT24C_MemoryReset (AT91C_BASE_PIOA,
							 AT91C_ID_PIOA,
							 AT91C_PA4_TWCK,
							 AT91C_PA3_TWD);
	
	// enable the PIO clock
	InitPeripheralClock((unsigned)(1 << AT91C_ID_PIOA));
	
	IO_init();
	ADC_init();
	TWI_init();
	SPI_init();
	TC2_init();
	US0_init();
	
	US_SetModeDisplay((AT91PS_USART)DBGU_pt);
	Usart_init(B_rate_9600);
	US_ClearDisplay((AT91PS_USART)DBGU_pt);
	
	Dbg((AT91PS_USART)DBGU_pt,(char *)MSG_RESET);
	
}


