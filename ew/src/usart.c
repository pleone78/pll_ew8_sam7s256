// -----------------------------------------------------------------------------
//
// Headers
//
// -----------------------------------------------------------------------------

#include <stdio.h>

#include "AT91SAM7S256.h"
#define __inline inline
#include "lib_AT91SAM7S256.h"

#include "timer.h"
#include "usart.h"
#include "deviceSetup.h"
#include "VSMain.h"

#define DBGU_CLKDIV  (unsigned int) 312        // B_9600





// -----------------------------------------------------------------------------
//
// Variabili
//
// -----------------------------------------------------------------------------


unsigned char char_rx_DBGU;
unsigned char OpenedTransmission = 0;

unsigned DisplayMode = DISPLAY_MODE_DEBUG;
char DisplayBuff[DISPLAY_SIZE+1];
char DbguBuff[DISPLAY_SIZE_DBGU+1];

unsigned PosCursorReadDbgu = 0;
unsigned PosCursorWriteDisplay = 0;
unsigned PosCursorWriteDbgu = 0;
unsigned int Step = 1;

unsigned char BcdBuff[10];

AT91PS_DBGU DBGU_pt = AT91C_BASE_DBGU;
extern unsigned char T400;

// -----------------------------------------------------------------------------
//
//   Inizializzazione porte seriali
//
// -----------------------------------------------------------------------------

void Usart_init ( unsigned int baudRate )
{
  //  Configure PIO controllers to periph mode
  AT91F_PIO_CfgPeriph( AT91C_BASE_PIOA,
                     ( AT91C_PA9_DRXD ) |
                     ( AT91C_PA10_DTXD ),
                     0);     // Peripheral B

  // Define the USART mode

  DBGU_pt->DBGU_MR =AT91C_US_USMODE_NORMAL | AT91C_US_PAR_NONE;

  // Define the baud rate divisor register

  DBGU_pt->DBGU_BRGR = DBGU_CLKDIV;

  // Enable usart

  DBGU_pt->DBGU_CR = AT91C_US_RXEN | AT91C_US_TXEN;


  // open Usart  interrupt

/*  AT91F_AIC_ConfigureIt ( AT91C_BASE_AIC,

                          AT91C_ID_SYS,

                          DBGU_INTERRUPT_LEVEL,

                          AT91C_AIC_SRCTYPE_INT_LEVEL_SENSITIVE,

                          DBGU_irq_handler);
*/
  // Enable the clock of the PIOA
//  AT91F_PMC_EnablePeriphClock ( AT91C_BASE_PMC, 1 << AT91C_ID_DBGU );
  // Usart Configure
//  AT91F_US_Configure ((AT91PS_USART)DBGU_pt, MCK, AT91C_US_ASYNC_MODE, baudRate, 0);
  // Enable usart
//  (AT91PS_USART)DBGU_pt->US_CR = AT91C_US_RXEN | AT91C_US_TXEN;
  // Enable USART IT error and RXRDY
  AT91F_US_EnableIt( (AT91PS_USART)DBGU_pt ,AT91C_US_TIMEOUT | AT91C_US_FRAME | AT91C_US_OVRE | AT91C_US_RXRDY );
  // open Usart  interrupt
}



// -----------------------------------------------------------------------------
//
// Invia array sulla seriale
//
// -----------------------------------------------------------------------------

void US_PutStr(AT91PS_USART USpt, char* buffer, char size)
{
  unsigned char TimeFreeze=0, i=0, Delay=0;
  extern volatile unsigned char SysTimeCount;

  for(i=0; i<size; i++){
    TimeFreeze=SysTimeCount;
    Delay=(unsigned char)(SysTimeCount-TimeFreeze);
    while(!(AT91F_US_TxReady( (AT91PS_USART)DBGU_pt ))&&(Delay<3)){
      //DisAllInt();
      Delay=(unsigned char)(SysTimeCount-TimeFreeze);
      //EnAllInt();
    }
    if(Delay<3)
      AT91F_US_PutChar(USpt, Mirror(buffer[i]));
  }
}





// -----------------------------------------------------------------------------
//
// Invia messaggio sulla riga 1
//
// -----------------------------------------------------------------------------

void Row1(AT91PS_USART USpt, char *buffer)
{
  PosCursorWriteDisplay = 0;
  while(*buffer != '\0')
  {
    if (PosCursorWriteDisplay < DISPLAY_SIZE)
    {
      DisplayBuff[PosCursorWriteDisplay++] = *buffer++;
    }
    else
    {
      buffer++;
    }
  }

  while(PosCursorWriteDisplay < 16)
  {
      US_SendCharDisplay(USpt, 32);
  }
}

// -----------------------------------------------------------------------------
//
// Invia messaggio sulla riga 2
//
// -----------------------------------------------------------------------------

void Row2(AT91PS_USART USpt, char *buffer)
{
  PosCursorWriteDisplay = 16;
  while(*buffer != '\0')
  {
    if (PosCursorWriteDisplay < DISPLAY_SIZE)
    {
      DisplayBuff[PosCursorWriteDisplay++] = *buffer++;
    }
    else
    {
      buffer++;
    }
  }
  while(PosCursorWriteDisplay < DISPLAY_SIZE)
  {
      US_SendCharDisplay(USpt, 32);
  }
}

// -----------------------------------------------------------------------------
//
// Invia messaggio sulla riga 2 con flash
//
// -----------------------------------------------------------------------------

void Fls2(AT91PS_USART USpt, char *buffer,unsigned char DispF)
{
  PosCursorWriteDisplay = 16;
  if (DispF == 0)
  {
    while(*buffer != '\0')
    {
      if (PosCursorWriteDisplay < DISPLAY_SIZE)
      {
        DisplayBuff[PosCursorWriteDisplay++] = *buffer++;
      }
      else
      {
        buffer++;
      }
    }
  }
  else if (DispF == 1)
  {
    if(T400 == 0)
    {
      while(*buffer != '\0')
      {
        if (PosCursorWriteDisplay < DISPLAY_SIZE)
        {
          DisplayBuff[PosCursorWriteDisplay++] = *buffer++;
        }
        else
        {
          buffer++;
        }
      }
    }
  }

  while(PosCursorWriteDisplay < DISPLAY_SIZE)
  {
      US_SendCharDisplay(USpt, 32);
  }
}

// -----------------------------------------------------------------------------
//
// Invia pacchetto dati sulla seriale
//
// -----------------------------------------------------------------------------

void Dbg(AT91PS_USART USpt, char *buffer)
{
  if (Sys==SysDisplay)
  {
    while(*buffer != '\0')
    {
      if (*buffer == 13)
      {
        PosCursorWriteDbgu = 0;
        buffer++;
      }
      if (PosCursorWriteDbgu < DISPLAY_SIZE_DBGU)
      {
        DbguBuff[PosCursorWriteDbgu++] = *buffer++;
      }
      else
      {
        buffer++;
      }
    }
  }
}

// -----------------------------------------------------------------------------
//
// Invia carattere sulla seriale
//
// -----------------------------------------------------------------------------

void US_SendCharDbg(AT91PS_USART USpt, char ch)
{
  if (Sys==SysDisplay)
  {
    if (DisplayMode == DISPLAY_MODE_DEBUG)
    {
      while (!AT91F_US_TxReady(USpt));
      AT91F_US_PutChar(USpt, Mirror(ch));
    }
    else
    {
      if (ch == 13)
      {
        PosCursorWriteDbgu = 0;
      }
      if (PosCursorWriteDbgu < DISPLAY_SIZE_DBGU)
      {
        DbguBuff[PosCursorWriteDbgu++] = ch;
      }
    }
  }
}

// -----------------------------------------------------------------------------
//
// Invia carattere sulla seriale
//
// -----------------------------------------------------------------------------

void US_SendCharDisplay(AT91PS_USART USpt, char ch)
{
  if (ch == 13)
  {
    PosCursorWriteDisplay = 0;
  }
  if (PosCursorWriteDisplay < DISPLAY_SIZE)
  {
    DisplayBuff[PosCursorWriteDisplay++] = ch;
  }
}


void US_ClearDisplay(AT91PS_USART USpt)
{
  unsigned int i;
    for (i=0;i<DISPLAY_SIZE_DBGU;i++)
    {
      DbguBuff[i] = 0;
    }
    PosCursorWriteDbgu = 0;
}

void US_UpdateDisplay(AT91PS_USART USpt)
{
  #define DISPLAY_DELAY_TIME 10
  static unsigned int OldMaxPosCursorReadDbgu = DISPLAY_SIZE_DBGU;
  static unsigned int MaxPosCursorReadDbgu = 0;
  static unsigned int DelayTime = 0;

  if(OpenedTransmission!=0){
    if (DisplayMode != DISPLAY_MODE_DEBUG)
    {
      if (DelayTime > 0)
      {
        DelayTime--;
      }
      else
      {
        DelayTime = DISPLAY_DELAY_TIME;
        if (AT91F_US_TxReady(USpt))
        {
          if (Step == 0)
          {
            switch (DbguBuff[PosCursorReadDbgu])
            {
              case 0:
                if (PosCursorReadDbgu <= OldMaxPosCursorReadDbgu)
                {
                  AT91F_US_PutChar(USpt, Mirror(32)); //95='_'
                }
                break;
              case 13:
                //AT91F_US_PutChar(USpt, Mirror(126)); //126'~'
                break;
              case 10:
                //AT91F_US_PutChar(USpt, Mirror(124)); //124='|'
                break;
            default:
                AT91F_US_PutChar(USpt, Mirror(DbguBuff[PosCursorReadDbgu]));
                MaxPosCursorReadDbgu++;
                break;
            }
            if (++PosCursorReadDbgu >= DISPLAY_SIZE_DBGU)
            {
              AT91F_US_PutChar(USpt, Mirror(32));
              Step = 1;
              PosCursorReadDbgu = 0;
              OldMaxPosCursorReadDbgu = MaxPosCursorReadDbgu+1;
              MaxPosCursorReadDbgu = 0;
            }
          }
          else
          {
            switch (Step)
            {
              case 1:
                AT91F_US_PutChar(USpt, Mirror(32));
                Step = 2;
                break;
              case 2:
                AT91F_US_PutChar(USpt, Mirror(13));
                Step = 3;
                break;
              case 3:
                AT91F_US_PutChar(USpt, Mirror(13));
                Step = 4;
                break;
              case 4:
                AT91F_US_PutChar(USpt, Mirror(40));//40='('
                Step = 5;
                break;
              case 5:
                AT91F_US_PutChar(USpt, Mirror(SysDisplay + 49));
                Step = 6;
                break;
              case 6:
                AT91F_US_PutChar(USpt, Mirror(41));//41=')'
                Step = 0;
                break;
              default:
                AT91F_US_PutChar(USpt, Mirror(13));
                Step = 0;
                break;
            }
          }
        }
      }
    }
  }
}

void US_SwitchDisplay(void)
{
  US_ClearDisplay((AT91PS_USART)DBGU_pt);
  PosCursorReadDbgu = 0;
  Step = 1;
}

void US_UpdateDisplay2(AT91PS_USART USpt)
{
  #define DISPLAY_DELAY_TIME 10
  static unsigned int OldMaxPosCursorReadDbgu = DISPLAY_SIZE_DBGU;
  static unsigned int MaxPosCursorReadDbgu = 0;
  static unsigned int DelayTime = 0;

  if(OpenedTransmission!=0){
    if (DisplayMode != DISPLAY_MODE_DEBUG)
    {
      if (DelayTime > 0)
      {
        DelayTime--;
      }
      else
      {
        DelayTime = DISPLAY_DELAY_TIME;
        if (AT91F_US_TxReady(USpt))
        {
          if (Step == 0)
          {
            switch (DisplayBuff[PosCursorReadDbgu])
            {
              case 0:
                if (PosCursorReadDbgu <= OldMaxPosCursorReadDbgu)
                {
                  AT91F_US_PutChar(USpt, Mirror(32)); //95='_'
                }
                break;
              case 13:
                //AT91F_US_PutChar(USpt, Mirror(126)); //126'~'
                break;
              case 10:
                //AT91F_US_PutChar(USpt, Mirror(124)); //124='|'
                break;
            default:
                AT91F_US_PutChar(USpt, Mirror(DisplayBuff[PosCursorReadDbgu]));
                MaxPosCursorReadDbgu++;
                break;
            }
            if (++PosCursorReadDbgu >= DISPLAY_SIZE_DBGU)
            {
              AT91F_US_PutChar(USpt, Mirror(32));
              Step = 1;
              PosCursorReadDbgu = 0;
              OldMaxPosCursorReadDbgu = MaxPosCursorReadDbgu+1;
              MaxPosCursorReadDbgu = 0;
            }
          }
          else
          {
            switch (Step)
            {
              case 1:
                AT91F_US_PutChar(USpt, Mirror(32));
                Step = 2;
                break;
              case 2:
                AT91F_US_PutChar(USpt, Mirror(13));
                Step = 3;
                break;
              case 3:
                AT91F_US_PutChar(USpt, Mirror(13));
                Step = 4;
                break;
              case 4:
                AT91F_US_PutChar(USpt, Mirror(40));//40='('
                Step = 5;
                break;
              case 5:
                AT91F_US_PutChar(USpt, Mirror(SysDisplay + 49));
                Step = 6;
                break;
              case 6:
                AT91F_US_PutChar(USpt, Mirror(41));//41=')'
                Step = 0;
                break;
              default:
                AT91F_US_PutChar(USpt, Mirror(13));
                Step = 0;
                break;
            }
          }
        }
      }
    }
  }
}


void US_SetModeDisplay(AT91PS_USART USpt)
{
  DisplayMode = DISPLAY_MODE_M3;
}

void US_ResetModeDisplay(AT91PS_USART USpt)
{
  DisplayMode = DISPLAY_MODE_DEBUG;
}

void DisplayValue(unsigned int Value, char ch)
{
  BinBcdAscii(Value);
  US_SendCharDbg((AT91PS_USART)DBGU_pt, ch);
  if (DisplayMode == DISPLAY_MODE_DEBUG)
  {
    Dbg((AT91PS_USART)DBGU_pt, (char *)BcdBuff+5);
  }
  else
  {
    Dbg((AT91PS_USART)DBGU_pt, (char *)BcdBuff);
  }
}

void DisplayValue2(unsigned int Value, char ch)
{
  BinBcdAscii(Value);
  US_SendCharDbg((AT91PS_USART)DBGU_pt, ch);
  if (DisplayMode == DISPLAY_MODE_DEBUG)
  {
    Dbg((AT91PS_USART)DBGU_pt, (char *)BcdBuff+5);
  }
  else
  {
    Dbg((AT91PS_USART)DBGU_pt, (char *)BcdBuff+1);
  }
}


// -----------------------------------------------------------------------------
//
// Cripta i byte da inviare e ricevere sulla seriale
//
// -----------------------------------------------------------------------------
unsigned char Mirror(unsigned char ch)
{
  unsigned char MirroredCh=0, i=0;
#ifdef NO_KEY_VER
  MirroredCh = ch;
  i=i;    //DeWarning
#else
  MirroredCh = ch & 0x01;

  for(i=1; i<8; i++){
    MirroredCh <<= 1;
    MirroredCh |= (ch >> i) & 0x01;
  }
#endif
  return (MirroredCh);
}



// -----------------------------------------------------------------------------
//
// BIN --> Nibble BCD ASCII
//
// Carica su BIN_BCD in formato ASCII le singole cifre di un numero decimale di 5 cifre
//
// -----------------------------------------------------------------------------

void BinBcdAscii(unsigned int num)
{
  unsigned int ZeroValid = 0;
  BcdBuff[0] = 0;
	while (num > 99999999)
	{
		BcdBuff[0] ++;
		num = num - 100000000;
	}
  if ((BcdBuff[0] == 0) && (ZeroValid == 0))
  {
    BcdBuff[0] = 32;
  }
  else
  {
    BcdBuff[0] += 0x30;
    ZeroValid = 1;
  }

  BcdBuff[1] = 0;
	while (num > 9999999)
	{
		BcdBuff[1] ++;
		num = num - 10000000;
	}
  if ((BcdBuff[1] == 0) && (ZeroValid == 0))
  {
    BcdBuff[1] = 32;
  }
  else
  {
    BcdBuff[1] += 0x30;
    ZeroValid = 1;
  }

  BcdBuff[2] = 0;
	while (num > 999999)
	{
		BcdBuff[2] ++;
		num = num - 1000000;
	}
  if ((BcdBuff[2] == 0) && (ZeroValid == 0))
  {
    BcdBuff[2] = 32;
  }
  else
  {
    BcdBuff[2] += 0x30;
    ZeroValid = 1;
  }

  BcdBuff[3] = 0;
	while (num > 99999)
	{
		BcdBuff[3] ++;
		num = num - 100000;
	}
  if ((BcdBuff[3] == 0) && (ZeroValid == 0))
  {
    BcdBuff[3] = 32;
  }
  else
  {
    BcdBuff[3] += 0x30;
    ZeroValid = 1;
  }

  BcdBuff[4] = 0;
	while (num > 9999)
	{
		BcdBuff[4] ++;
		num = num - 10000;
	}
  if ((BcdBuff[4] == 0) && (ZeroValid == 0))
  {
    BcdBuff[4] = 32;
  }
  else
  {
    BcdBuff[4] += 0x30;
    ZeroValid = 1;
  }

  BcdBuff[5] = 0;
	while (num > 999)
	{
		BcdBuff[5] ++;
		num = num - 1000;
	}
  if ((BcdBuff[5] == 0) && (ZeroValid == 0))
  {
    BcdBuff[5] = 32;
  }
  else
  {
    BcdBuff[5] += 0x30;
    ZeroValid = 1;
  }

  BcdBuff[6] = 0;
	while (num > 99)
	{
		BcdBuff[6] ++;
		num = num - 100;
	}
  if ((BcdBuff[6] == 0) && (ZeroValid == 0))
  {
    BcdBuff[6] = 32;
  }
  else
  {
    BcdBuff[6] += 0x30;
    ZeroValid = 1;
  }

  BcdBuff[7] = 0;
	while (num > 9)
	{
		BcdBuff[7] ++;
		num = num - 10;
	}
  if ((BcdBuff[7] == 0) && (ZeroValid == 0))
  {
    BcdBuff[7] = 32;
  }
  else
  {
    BcdBuff[7] += 0x30;
    ZeroValid = 1;
  }

	BcdBuff[8] = num + 0x30;
	BcdBuff[9] = 0;	
}
// -----------------------------------------------------------------------------
//
// BIN --> Nibble BCD ASCII
//
// Carica su BIN_BCD in formato ASCII le singole cifre di un numero decimale di 2 cifre
//
// -----------------------------------------------------------------------------

unsigned char BinBcd2(unsigned char num)
{
  BcdBuff[0] = 0;

  if (num>99)
  {
    num = 0;
  }

	while (num > 9)
	{
		BcdBuff[0] ++;
		num = num - 10;
	}
	
	return (BcdBuff[0]<<4) + num;
}

// -----------------------------------------------------------------------------
//
// BIN --> Nibble BCD ASCII
//
// Carica su BIN_BCD in formato ASCII le singole cifre di un numero decimale di 2 cifre
//
// -----------------------------------------------------------------------------

void BinBcdAscii2(unsigned char num)
{
  BcdBuff[0] = 0;

  if (num>99)
  {
    num = 0;
  }

	while (num > 9)
	{
		BcdBuff[0] ++;
		num = num - 10;
	}
	
  BcdBuff[0] += 0x30;
	BcdBuff[1] = num + 0x30;
}

// -----------------------------------------------------------------------------
//
//  Ritardo a loop di programma in millisecondi    MAX 537 940
//
// -----------------------------------------------------------------------------

void millisecondi(unsigned int millisec)
{
  unsigned int kk,kkmax;
  kkmax = millisec * 7984;
  for(kk = 0; kk <= kkmax; kk++){}
}

  // -----------------------------------------------------------------------------
//
//  Ritardo a loop di programma in microsecondi   MAX 536 870 910
//
// -----------------------------------------------------------------------------

void microsecondi(unsigned int millisec)
{
  unsigned int kk,kkmax;
  kkmax = millisec * 8;
  for(kk = 0; kk <= kkmax; kk++){}
}
