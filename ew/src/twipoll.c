// -----------------------------------------------------------------------------
//
//  Headers
//
// -----------------------------------------------------------------------------

#include "twi.h"
#include "deviceSetup.h"
#include "usart.h"

#define MAX_LOOP_COUNT 20000

unsigned int TWI_ReadError = 0;
unsigned int TWI_WriteError = 0;

unsigned char SecPLL[2], MinPLL[2], HourPLL[2], DayPLL[2], DatePLL[2];
unsigned char MonthPLL[2], YearPLL[2], DayName[3]; 
unsigned char Orario[7],DataOra[7], Trickle = 0xA5, Tricklev;

// -----------------------------------------------------------------------------
//
//  Impostazione clock TWI
//
// -----------------------------------------------------------------------------

void  Cfg_Clk_TWI ( AT91PS_TWI pTWI,
                  unsigned int CKDIV,
                  unsigned int CHDIV,
                  unsigned int CLDIV )
{
  CHDIV = CHDIV << 8;
  CKDIV = CKDIV << 16;
  pTWI->TWI_CWGR = CKDIV | CHDIV | CLDIV;
}


void TWI_init ( void )
{
//  AT91F_PMC_EnablePeriphClock ( AT91C_BASE_PMC, (1 << AT91C_ID_PIOA));

// Definisci puntatore ai registri PIOA

//	AT91PS_PIO pPio = AT91C_BASE_PIOA;
	
// Definisci puntatore ai registri TWI

	AT91PS_TWI pTWI = AT91C_BASE_TWI;

// Abilita clock sulla porta TWI

  AT91F_PMC_EnablePeriphClock ( AT91C_BASE_PMC, (1 << AT91C_ID_TWI) );

// Assegna la funzione TWI alle porte: PA3 = TWD; PA4 = TWCK (peripheral A)

  AT91F_PIO_CfgPeriph(AT91C_BASE_PIOA, AT91C_PA3_TWD | AT91C_PA4_TWCK, 0);

// Elimina pull-up dalle porte TWD e TWCK
// non disabilitare se non strettamente necessario
//(al resetARM li ha attivi e se si disabilitano si alterano tutti)

//  AT91F_PIO_CfgPullup(AT91C_BASE_PIOA, AT91C_PA3_TWD | AT91C_PA4_TWCK);

// Configura porta TWD in open drain per la bidirezionalitÓ

	AT91F_PIO_CfgOpendrain(AT91C_BASE_PIOA, AT91C_PA3_TWD);

// Configura porta clock


	pTWI->TWI_IDR = (unsigned) -1;
	pTWI->TWI_CR = AT91C_TWI_SWRST;
	pTWI->TWI_CR = AT91C_TWI_MSEN | AT91C_TWI_SVDIS;

  Cfg_Clk_TWI ( AT91C_BASE_TWI, 1, 100, 100 );
  
  millisecondi(20);
  TWI_Write ( AT91C_BASE_TWI, DS1339, 0x10,(unsigned char *)&Trickle , 1);  
}

// -----------------------------------------------------------------------------
//
//  Scrivi TWI/I2C EEPROM_24AA08 DS1339 EEPROM_24AA256
//
// -----------------------------------------------------------------------------

void TWI_Write ( AT91PS_TWI pTwi,         // Registri TWI

               unsigned int TYPE_TWI,     // Tipo slave

               unsigned int address,      // Indirizzo 1░ dato su slave

               unsigned char *dataw,      // 1░ dato da scrivere

               unsigned int size )        // Numero byte da scrivere
{
  unsigned int LoopCount = MAX_LOOP_COUNT;
// Copia registro di stato porta TWI

  unsigned int status;

  // Configura registro TWI mode : tipo slave e master mode

  pTwi->TWI_MMR = TYPE_TWI & ~AT91C_TWI_MREAD;

  // Imposta indirizzo di memoria slave dove inizia la scrittura

  pTwi->TWI_IADR = address;

  // Leggi registro di stato

  status = pTwi->TWI_SR;

  // Carica registro di scrittura con il byte di dati

  pTwi->TWI_THR = *( dataw++ );

  // Avvia la scrittura

  pTwi->TWI_CR = AT91C_TWI_START;

  LoopCount = MAX_LOOP_COUNT;

  while ((!(pTwi->TWI_SR & AT91C_TWI_TXRDY & ~AT91C_TWI_NACK)) && (LoopCount > 0))
  {
    ResetWatchdog();
    if (LoopCount > 0)
    {
      LoopCount--;
    }
    pTwi->TWI_CR = AT91C_TWI_START;
  } 
  if (LoopCount == 0)
  {
    TWI_WriteError = 3;
  }

  // Ciclo di scrittura dati

  while (size-- >1)
  {

    // Attendi che il registro di scrittura sia vuoto
    LoopCount = MAX_LOOP_COUNT;
    
    while ((!(pTwi->TWI_SR & AT91C_TWI_TXRDY)) && (LoopCount > 0))
    {
      ResetWatchdog();
      if (LoopCount > 0)
      {
        LoopCount--;
      }
    }
    if (LoopCount == 0)
    {
      TWI_WriteError = 1;
    }

    // Carica il byte successivo

    pTwi->TWI_THR = *( dataw++ );

  }

  // Ferma la scrittura

  pTwi->TWI_CR = AT91C_TWI_STOP;

  // Leggi registro di stato

  status = pTwi->TWI_SR;

  // Attendi il completamento delle operazioni
  
  LoopCount = MAX_LOOP_COUNT;
  while ((!(pTwi->TWI_SR & AT91C_TWI_TXCOMP)) && (LoopCount > 0))
  {
    ResetWatchdog();
    if (LoopCount > 0)
    {
      LoopCount--;
    }
  }
  if (LoopCount == 0)
  {
    TWI_WriteError = 2;
  }

  // Evita warning su status

  status  = status;
}


// -----------------------------------------------------------------------------
//
//  Leggi TWI/I2C EEPROM_24AA08 DS1339 EEPROM_24AA256
//
// -----------------------------------------------------------------------------


void TWI_Read ( AT91PS_TWI pTwi,          // Registri TWI

               unsigned int TYPE_TWI,     // Tipo slave

               unsigned int address,      // Indirizzo 1░ dato su slave

               unsigned char *datar,      // 1░ dato da leggere

               unsigned int size )        // Numero byte da leggere
{

  unsigned int LoopCount = MAX_LOOP_COUNT;

// Copia registro di stato porta TWI

  unsigned int status;

  // Configura registro TWI mode : tipo slave e master mode

  pTwi->TWI_MMR = TYPE_TWI | AT91C_TWI_MREAD;

  // Imposta indirizzo di memoria slave dove inizia la scrittura

  pTwi->TWI_IADR = address;

  // Avvia la lettura

  pTwi->TWI_CR = AT91C_TWI_START;

  LoopCount = MAX_LOOP_COUNT;

  while ((!(pTwi->TWI_SR & AT91C_TWI_RXRDY & ~AT91C_TWI_NACK)) && (LoopCount > 0))
  {
    ResetWatchdog();

    if (LoopCount > 0)
    {
      LoopCount--;
    }        
    pTwi->TWI_CR = AT91C_TWI_START;    
  }
  if (LoopCount == 0)
  {
    TWI_ReadError = 3;
  }

  // Ciclo di lettura dati

  while (size-- >1)
  {

    // Attendi che il registro di lettura sia pieno

    LoopCount = MAX_LOOP_COUNT;
    
    while ((!(pTwi->TWI_SR & AT91C_TWI_RXRDY)) && (LoopCount > 0))
    {
      ResetWatchdog();
      if (LoopCount > 0)
      {
        LoopCount--;
      }
    }
    if (LoopCount == 0)
    {
      TWI_ReadError = 1;
    }
    // Memorizza il byte letto

    *(datar++) = pTwi->TWI_RHR;
  }

  // Ferma la lettura

  pTwi->TWI_CR = AT91C_TWI_STOP;

  // Leggi registro di stato

  status = pTwi->TWI_SR;

  // Attendi il completamento delle operazioni

  LoopCount = MAX_LOOP_COUNT;
  
  while ((!(pTwi->TWI_SR & AT91C_TWI_TXCOMP)) && (LoopCount > 0))
  {
    ResetWatchdog();
    if (LoopCount > 0)
    {
      LoopCount--;
    }
  }
  if (LoopCount == 0)
  {
    TWI_ReadError = 2;
  }
  // Evita warning su status

  status  = status;

  // Memorizza anche l'ultimo byte letto

  *datar = pTwi->TWI_RHR;
}


// -------------------------------------------------------------------2----------
// 
//  Leggi orologio DS1339
// 
// -----------------------------------------------------------------------------

void Clk_Read( void )
{
  TWI_Read ( AT91C_BASE_TWI, DS1339 , 0x00,(unsigned char *)&Orario, 7);
  
  DatePLL[0]  = ((Orario[4] & 0xF0) >> 4) | 0x30;
  DatePLL[1]  =  (Orario[4] & 0x0F)       | 0x30;

  MonthPLL[0] = ((Orario[5] & 0xF0) >> 4) | 0x30;
  MonthPLL[1] =  (Orario[5] & 0x0F)       | 0x30;

  YearPLL[0]  = ((Orario[6] & 0xF0) >> 4) | 0x30;
  YearPLL[1]  =  (Orario[6] & 0x0F)       | 0x30;

  HourPLL[0]  = ((Orario[2] & 0xF0) >> 4) | 0x30;
  HourPLL[1]  =  (Orario[2] & 0x0F)       | 0x30;
  
  MinPLL[0]   = ((Orario[1] & 0xF0) >> 4) | 0x30;
  MinPLL[1]   =  (Orario[1] & 0x0F)       | 0x30;

  SecPLL[0]   = ((Orario[0] & 0xF0) >> 4) | 0x30;
  SecPLL[1]   =  (Orario[0] & 0x0F)       | 0x30;

  DayPLL[0]   = ((Orario[3] & 0xF0) >> 4) | 0x30;
  DayPLL[1]   =  (Orario[3] & 0x0F)       | 0x30;
  
  switch (DayPLL[1])
  {
    case  0x31:
                DayName[0] = 'D';
                DayName[1] = 'o';
                DayName[2] = 'm';
                break;
    case  0x32:
                DayName[0] = 'L';
                DayName[1] = 'u';
                DayName[2] = 'n';
                break;
    case  0x33:
                DayName[0] = 'M';
                DayName[1] = 'a';
                DayName[2] = 'r';
                break;
    case  0x34:
                DayName[0] = 'M';
                DayName[1] = 'e';
                DayName[2] = 'r';
                break;
    case  0x35:
                DayName[0] = 'G';
                DayName[1] = 'i';
                DayName[2] = 'o';
                break;
    case  0x36:
                DayName[0] = 'V';
                DayName[1] = 'e';
                DayName[2] = 'n';
                break;
    case  0x37:
                DayName[0] = 'S';
                DayName[1] = 'a';
                DayName[2] = 'b';
                break;
  } 
}

// -------------------------------------------------------------------2----------
// 
//  Leggi singolo parametro dell'orologio DS1339 
// 
// -----------------------------------------------------------------------------

unsigned char Clk_ReadSingle(unsigned char Pos)
{
  TWI_Read ( AT91C_BASE_TWI, DS1339 , Pos,(unsigned char *)&Orario, 1);
  
  DatePLL[0]  = ((Orario[0] & 0xF0) >> 4) | 0x30;
  DatePLL[1]  =  (Orario[0] & 0x0F)       | 0x30;
  return (Orario[0]&0x0F)+((Orario[0]>>4)*0x0A);
}

// -------------------------------------------------------------------2----------
// 
//  Aggiorna singolo parametro dell'orologio DS1339 
// 
// -----------------------------------------------------------------------------

void Clk_WriteSingle(unsigned char Pos)
{
  Orario[0]=BinBcd2(CfgV);
  TWI_Write ( AT91C_BASE_TWI, DS1339 , Pos,(unsigned char *)&Orario, 1);
}

// -----------------------------------------------------------------------------
// 
//  Ora legale
// 
// -----------------------------------------------------------------------------

void LegalHour( void )
{
  static unsigned char Ottobre=0;
  
  if (Orario[5] != 0x10)
  {
    Ottobre = 0;
  }
   
  if ((Orario[5] == 0x03) &&       // Se mese di marzo (03)
      (Orario[4] >  0x24) &&       // se ultima
      (Orario[3] == 0x01) &&       // domenica del mese
      (Orario[2] == 0x01) &&       // se ora 01
      (Orario[1] == 0x59))         // se minuto 59     
  {    
    DataOra[2] = 0x02;             // l'ora viene messa avanti di 1
  
    TWI_Write ( AT91C_BASE_TWI, DS1339, 0x02,(unsigned char *)&DataOra[2], 1);   
  }

  if ((Orario[5] == 0x10) &&       // Se mese di ottobre (10)
      (Orario[4] >  0x24) &&       // se ultima
      (Orario[3] == 0x01) &&       // domenica del mese
      (Orario[2] == 0x02) &&       // se ora 02
      (Orario[1] == 0x59) &&       // se minuto 59
      (Ottobre == 0))                
  {    
    DataOra[2] = 0x01;             // l'ora viene messa indietro di 1
  
    TWI_Write ( AT91C_BASE_TWI, DS1339, 0x02,(unsigned char *)&DataOra[2], 1);
    
    Ottobre = 1;                   // flag che impedisce la ripetizione   
  }

}


