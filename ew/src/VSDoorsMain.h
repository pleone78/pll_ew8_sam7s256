/*****************************************************************************
* IAR visualSTATE Main Loop Header File
*
* The file contains an implementation for a main loop using the visualSTATE
* basic API.
*
* The code uses a simple queue for storing events. The functions for inter-
* facing to the queue are described in the sample code file
* simpleEventHandler.h.
*****************************************************************************/

#ifndef _VSDOORSMAIN_H
#define _VSDOORSMAIN_H

/* *** include directives *** */

#include "VSMain.h"

#define no_ENCODER
#define CUR_CONTROL


#define MASK_SIZE 1024
#define MASK_MAX_VAL 1024
#define MASK_10 102   //10% di 1024
#define MASK_20 204   //20% di 1024
#define POS_MAX_VAL 1024
#define CUR_THR_FACTOR 1
#define POS_NOTIFY_DELTA 10

/* *** variable declarations *** */

/* *** function declarations *** */

void UpCurMask(void);

unsigned short GetMaxCloseMask(unsigned short PosNew);

unsigned short GetMaxOpenMask(unsigned short PosNew);

void SetMaxCloseMask(unsigned short PosNew, unsigned short Value);

void SetMaxOpenMask(unsigned short PosNew, unsigned short Value);

unsigned char DoorsInit(SEM_CONTEXT VS_TQ_CONTEXT * * Context);

unsigned char DoorsMain(SEM_CONTEXT *pSEMCont);
//Leone
void UnhookDoor(void);
void HookUpDoor(void);
#endif
