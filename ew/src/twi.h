// -----------------------------------------------------------------------------
// 
// Note sulla gestione della versione firmware e dei parametri
// 
// -----------------------------------------------------------------------------

// La versione firmware � depositata nelle prime due locazioni della pagina di
// eeprom dedicata alla memorizzazione dei parametri di configurazione.
// La versione � codificata xx.yy e viene scritta dal fmw quando viene modifica-
// to un parametro di configurazione e viene rilevata una versione diversa
// Dove possibile le funzioni di lettura parametri ne controllano il range e nel
// caso di out of range scrivono il valore di default
// -----------------------------------------------------------------------------
// 
// Definizioni
// 
// -----------------------------------------------------------------------------

#ifndef twi_h
#define twi_h

#include "AT91SAM7S256.h"
#define __inline inline
#include "lib_AT91SAM7S256.h"

#define AT91C_TWI_SVDIS ((unsigned int) 0x1 <<  5)

#define DS1339 (( 0xD0<<15 ) | AT91C_TWI_IADRSZ_1_BYTE )

//#define EEPROM_24AA08 (( 0xA0<<15 ) | AT91C_TWI_IADRSZ_1_BYTE )
#define EEPROM_24AA256 (( 0xA0<<15 ) | AT91C_TWI_IADRSZ_2_BYTE )

#ifdef EEPROM_24AA08
  #define EEPROM_PAGE 16
#endif

#ifdef EEPROM_24AA256
  #define EEPROM_PAGE 64
#endif

extern unsigned int TWI_ReadError;
extern unsigned int TWI_WriteError;
extern unsigned char CfgV;

// -----------------------------------------------------------------------------
// 
// Prototipi
// 
// -----------------------------------------------------------------------------
void TWI_Write ( AT91PS_TWI pTwi,         // Registri TWI

               unsigned int TYPE_TWI,     // Tipo slave 
               
               unsigned int address,      // Indirizzo 1� dato su slave
              
               unsigned char *dataw,      // 1� dato da scrivere
               
               unsigned int size ) ;      // Numero byte da scrivere

void TWI_Read ( AT91PS_TWI pTwi,          // Registri TWI

               unsigned int TYPE_TWI,     // Tipo slave 
               
               unsigned int address,      // Indirizzo 1� dato su slave
              
               unsigned char *datar,      // 1� dato da leggere
               
               unsigned int size ) ;      // Numero byte da leggere

void TWI_init ( void );

void Clk_Read(void);    //zuck

void AT91F_AT24C_MemoryReset (const AT91PS_PIO pPio,
                            unsigned int PioID,
                            unsigned int Twck,
                            unsigned int Twd);

unsigned char Clk_ReadSingle(unsigned char Pos); //zuck

void Clk_WriteSingle(unsigned char Pos); //zuck

void LegalHour( void ); //zuck

#endif
