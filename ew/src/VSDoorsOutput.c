/*****************************************************************************
* IAR visualSTATE action functions and error handler.
*****************************************************************************/
/* *** include directives *** */
#include "AT91SAM7S256.h"
#define __inline inline
#include "lib_AT91SAM7S256.h"

#include "VSDoorsData.h"
#include "VSDoorsAction.h"
#include "VSDoorsMain.h"
#include "deviceSetup.h"
#include "VSDoorsOutput.h"
#include "adc.h"
#include "twi.h"
#include "inout.h"
#include "pwm.h"
#include "usart.h"

#define MULTIPLE_LINE_LOG


/*
#define CUR_THR_DEFAULT 21
#define PWM_ACC_CLOSE_DEFAULT 52
#define PWM_ACC_OPEN_DEFAULT 53
#define PWM_DEC_CLOSE_DEFAULT 34
#define PWM_DEC_OPEN_DEFAULT 35
#define PWM_FIND_CLOSE_DEFAULT 36
#define PWM_FIND_OPEN_DEFAULT 35
#define PWM_NEAR_CLOSE_DEFAULT 34
#define PWM_NEAR_OPEN_DEFAULT 33
#define PWM_INIT_CLOSE_DEFAULT 61
#define PWM_INIT_OPEN_DEFAULT 60
#define PWM_START_CLOSE_DEFAULT 34
#define PWM_START_OPEN_DEFAULT 35
#define ACC_OPEN_DEFAULT 11
#define ACC_CLOSE_DEFAULT 11
#define DEC_OPEN_DEFAULT 72
#define DEC_CLOSE_DEFAULT 72
*/

#define CUR_THR_DEFAULT 20
#define CUR_THR_SLOPE_DEFAULT 5
#define PWM_ACC_CLOSE_DEFAULT 50
#define PWM_ACC_OPEN_DEFAULT 50
#define PWM_DEC_CLOSE_DEFAULT 33
#define PWM_DEC_OPEN_DEFAULT 33
#define PWM_FIND_CLOSE_DEFAULT 33
#define PWM_FIND_OPEN_DEFAULT 33
#define PWM_NEAR_CLOSE_DEFAULT 33
#define PWM_NEAR_OPEN_DEFAULT 33
#define PWM_INIT_CLOSE_DEFAULT 60
#define PWM_INIT_OPEN_DEFAULT 60
#define PWM_START_CLOSE_DEFAULT 33
#define PWM_START_OPEN_DEFAULT 33
#define ACC_OPEN_DEFAULT 10
#define ACC_CLOSE_DEFAULT 10
#define DEC_OPEN_DEFAULT 70
#define DEC_CLOSE_DEFAULT 70


tMotSys MotSys[MOT_SYSTEMS];
tMotSys * pm;

const unsigned int MovDeltaFail = 20;
const unsigned int MovDeltaInit =  3;
const unsigned int MovDeltaRun  =  1;
const unsigned int MovDeltaStart =  8;
const unsigned int MovDeltaBrake = 5;
const unsigned int PosUpdateOffset = 1;

unsigned int ParValuesMin[PAR_NUM_MAX]
  = {   5,   5,   1,   1,    5,   1,   1,   33,    1,    1,    1,                0,    0, 0, 0, 0, 0,       0, 0, 0, 0, 0,  0, 0, 0, 50 };
unsigned int ParValuesMax[PAR_NUM_MAX]
  = { 100, 100,  99,  99,  100,  99,  99,  100,  999,  999,    5, PWM_CHANNEL_MAX-1,   1, 1, 1, 1, 1, 9999999, 1, 1, 1, 1, 59, 1, 1,100 };
const char ParDesc[PAR_NUM_MAX][20] = {
    "OP/FIN    %     "  //0
   ,"CL/FIN    %     "  //1
   ,"OP/DEC    %     "  //2
   ,"CL/DEC    %     "  //3
   ,"ACC/INI   %     "  //4
   ,"OP/ACC    %     "  //5
   ,"CL/ACC    %     "  //6
   ,"PRX/FIN   %     "  //7
   ,"OP/THR/LEV      "  //8
   ,"CL/THR/LEV      "  //9
   ,"THR/SLOPE       "  //10
   ,"PWM/CHN         "  //11
   ,"LOCK/NC         "  //12    //dip1=on elettromagnete (chiude uscita se PortaChiusa) dip1=off pistone (bibussola)
   ,"PHOTO/OP        "  //13    //(invertito ON OFF rispetto DIP) dip2=on gomma in apertura inibita
   ,"PHOTO/LEV       "  //14    //(invertito ON OFF rispetto DIP) dip6=on gomma su fronte
   ,"THR             "  //15    //dip5=on gomma elettronica attiva
   ,"RUN             "  //16    //dip7=on controllo motore attivo
   ,"COUNTER         "  //17
   ,"BRAKE/ON        "  //18
   ,"MOTOR           "  //19
   ,"SS              "  //20
   ,"SP              "  //21
   ,"TS              "  //22
   ,"CLOSE/ON        "  //23
   ,"ENCODER REV TYPE"  //24
   ,"EMER. STROKE %  "	//25   
   };

const char MSG_0[]=  {  "\n\rERROR           "  };
const char MSG_1[]=  {  "\n\rStartOpening    "  };
const char MSG_2[]=  {  "\n\rWaitOpening     "  };
const char MSG_3[]=  {  "\n\rOpening         "  };
const char MSG_4[]=  {  "\n\rNearOpened      "  };
const char MSG_5[]=  {  "\n\rFindOpened      "  };
const char MSG_6[]=  {  "\n\rOpened          "  };
const char MSG_7[]=  {  "\n\rStartClosing    "  };
const char MSG_8[]=  {  "\n\rWaitClosing     "  };
const char MSG_9[]=  {  "\n\rClosing         "  };
const char MSG_10[]= {  "\n\rNearClosed      "  };
const char MSG_11[]= {  "\n\rFindClosed      "  };
const char MSG_12[]= {  "\n\rClosed          "  };
const char MSG_13[]= {  "\n\rFAIL            "  };
const char MSG_14[]= {  "\n\rInitOpened      "  };
const char MSG_15[]= {  "\n\rInitClosed      "  };
const char MSG_16[]= {  "\n\rWAIT            "  };
const char MSG_17[]= {  "\n\rREAD            "  };
const char MSG_18[]= {  "\n\rWRITE           "  };
const char MSG_19[]= {  "\n\rUP/MASK         "  };
const char MSG_20[]= {  "\n\rDEFAULT         "  };
const char MSG_21[]= {  "\n\rCHECK           "  };
const char MSG_22[]= {  "\n\rCHECKDOOR       "  };
const char MSG_23[]= {  "\n\rOLD_PAR_LOAD    "  };
const char MSG_24[]= {  "\n\rOpened Freewheel"  };
const char MSG_25[]= {  "\n\rBrakeOpening    "  };
const char MSG_26[]= {  "\n\rBrakeClosing    "  };
const char MSG_27[]= {  "\n\rBreakThrough    "  };
const char MSG_28[]= {  "\n\rCloseToOpen     "  };
const char MSG_29[]= {  "\n\rCheckLocked     "  };

/* *** function declarations *** */

void SetPosOpenShk( void );
/* *** function definitions *** */

VS_VOID CheckManual (VS_VOID)
{
  if (pm->bManual != 0)
  {
    SEQ_AddEvent(euEsc,pm->Sys);
  }
}


VS_VOID CheckEsc( VS_VOID  )
{
  if ( pm->Run == 0 )
  {
    SEQ_AddEvent(euEsc,pm->Sys);
    EscCount = 0;
  }
}

void ValuesCheck(void)
{
    if (PosClose > 1023)
        PosClose = 0;
    if (PosOpen > 1023)
        PosOpen = 0;
	 if (PosOpenShk > 1023)
        PosOpenShk = 0;
    if ((pm->PwmDecOpen > 100) || (pm->PwmDecOpen == 0))
        pm->PwmDecOpen = PWM_DEC_OPEN_DEFAULT;
    if ((pm->PwmDecClose > 100) || (pm->PwmDecClose == 0))
        pm->PwmDecClose = PWM_DEC_CLOSE_DEFAULT;
    if ((pm->DecOpen > 100) || (pm->DecOpen == 0))
        pm->DecOpen = DEC_OPEN_DEFAULT;
    if ((pm->DecClose > 100) || (pm->DecClose == 0))
        pm->DecClose = DEC_CLOSE_DEFAULT;
    if ((pm->PwmAccOpen > 100) || (pm->PwmAccOpen == 0))
        pm->PwmAccOpen = PWM_ACC_OPEN_DEFAULT;
    if ((pm->PwmAccClose > 100) || (pm->PwmAccClose == 0))
        pm->PwmAccClose = PWM_ACC_CLOSE_DEFAULT;
    if ((pm->AccOpen > 100) || (pm->AccOpen == 0))
        pm->AccOpen = ACC_OPEN_DEFAULT;
    if ((pm->AccClose > 100) || (pm->AccClose == 0))
        pm->AccClose = ACC_CLOSE_DEFAULT;
    if ((pm->PwmFindClose > 100) || (pm->PwmFindClose == 0))
        pm->PwmFindClose = PWM_FIND_CLOSE_DEFAULT;
    if ((pm->PwmFindOpen > 100) || (pm->PwmFindOpen == 0))
        pm->PwmFindOpen = PWM_FIND_OPEN_DEFAULT;
    if ((pm->PwmNearClose > 100) || (pm->PwmNearClose == 0))
        pm->PwmNearClose = PWM_NEAR_CLOSE_DEFAULT;
    if ((pm->PwmNearOpen > 100) || (pm->PwmNearOpen == 0))
        pm->PwmNearOpen = PWM_NEAR_OPEN_DEFAULT;
    if ((pm->PwmInitClose > 100) || (pm->PwmInitClose == 0))
        pm->PwmInitClose = PWM_INIT_CLOSE_DEFAULT;
    if ((pm->PwmInitOpen > 100) || (pm->PwmInitOpen == 0))
        pm->PwmInitOpen = PWM_INIT_OPEN_DEFAULT;
    if ((pm->PwmStartClose > 100) || (pm->PwmStartClose == 0))
        pm->PwmStartClose = PWM_START_CLOSE_DEFAULT;
    if ((pm->PwmStartOpen > 100) || (pm->PwmStartOpen == 0))
        pm->PwmStartOpen = PWM_START_OPEN_DEFAULT;
    if ((pm->CurThrOpen > 999) || (pm->CurThrOpen == 0))
        pm->CurThrOpen = CUR_THR_DEFAULT;
    if ((pm->CurThrClose > 999) || (pm->CurThrClose == 0))
        pm->CurThrClose = CUR_THR_DEFAULT;
    if ((pm->CurThrSlope > 5) || (pm->CurThrSlope == 0))
        pm->CurThrSlope = CUR_THR_SLOPE_DEFAULT;
    if ((pm->PwmChannel > (PWM_CHANNEL_MAX * PWM_CHANNEL_BASE)) || (pm->PwmChannel == 0))
        pm->PwmChannel = PWM_CHANNEL_DEFAULT * PWM_CHANNEL_BASE;
    if ((pm->Counter >= 9999999))
        pm->Counter = 0;
    if (pm->Dip1 > 1)
        pm->Dip1 = 1;
    if (pm->Dip2 > 1)
        pm->Dip2 = 1;
    if (pm->Dip5 > 1)
        pm->Dip5 = 1;
    if (pm->Dip6 > 1)
        pm->Dip6 = 1;
    if (pm->Run > 1)
        pm->Run = 1;
    if (BrakeOnOff > 1)
        BrakeOnOff = 1;
    if (Motor > 1)
        Motor = 1;
    if (SSCfg > 1)
        SSCfg = 1;
    if (SPCfg > 1)
        SPCfg = 1;
    if (STCfg > 59)
        STCfg = 0;
    if (CloseOnOff > 1)
        CloseOnOff = 0;
    if (pm->EncoderCCWCfg > 1)
        pm->EncoderCCWCfg = 0;
	if ( (pm->StrokeShk < 50) || ( pm->StrokeShk > 100))
		pm->StrokeShk = 100;
}

VS_VOID ReadCfg (VS_VOID)
{
    unsigned int add = EEPROM_CFG_ADDRESS;
    add = add + 256*(pm->Mot);
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->Version, 4);
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&PosClose, 4);				// +4
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&PosOpen, 4);				// +8
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->PwmDecOpen, 4);		// +12
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->PwmDecClose, 4);		// +16
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->DecOpen, 4);			// +20
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->DecClose, 4);			// +24
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->PwmAccOpen, 4);		// +28
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->AccOpen, 4);			// +32
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->AccClose, 4);			// +36
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->PwmFindOpen, 4);		// +40
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->CurThrOpen, 4);		// +44
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->CurThrClose, 4);		// +48
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->CurThrSlope, 4);		// +52
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->PwmChannel, 4);		// +56
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->Counter, 4);			// +60
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->Dip1, 4);				// +64
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->Dip2, 4);				// +68
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->Dip6, 4);				// +72
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->Dip5, 4);				// +76
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->Run, 4);				// +80
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&BrakeOnOff, 4);			// +88
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&Motor, 4);				// +92
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&SSCfg, 4);				// +96
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&SPCfg, 4);				//+100
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&STCfg, 4);				//+104
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&CloseOnOff, 4);			//+108
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->EncoderCCWCfg, 4);	//+112
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&PosOpenShk, 4);			//+116 //Aggiunto V. 2.29
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->StrokeShk, 4);		//+116 //Aggiunto V. 2.29
    ValuesCheck();
}


VS_VOID MaskRead (VS_VOID)
{
  unsigned int add = EEPROM_MASK_ADDRESS;
  unsigned short val;
  unsigned short i;
  add = add + 256*(pm->Mot);
  for (i=0; i<MASK_SIZE; i+=EEPROM_MASK_RATIO)
  {
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&val, 2);
    SetMaxCloseMask(i,val + (pm->CurThrClose * CUR_THR_FACTOR));
    add += 2;
  }
  for (i=0; i<MASK_SIZE; i+=EEPROM_MASK_RATIO)
  {
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&val, 2);
    SetMaxOpenMask(i,val + (pm->CurThrOpen * CUR_THR_FACTOR));
    add += 2;
  }
  for (i=0; i<11; i++)
  {
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&val, 2);
    pm->MaxCurADC[i] = val;
    add += 2;
  }
}

VS_VOID MaskWrite (VS_VOID)
{
  unsigned int add = EEPROM_MASK_ADDRESS;
  unsigned short val;
  unsigned short i;
  add = add + 256*(pm->Mot);
  for (i=0; i<MASK_SIZE; i+=EEPROM_MASK_RATIO)
  {
    //per test val = 1000+i;
    val = GetMaxCloseMask(i);
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&val, 2);
    add += 2;
  }
  for (i=0; i<MASK_SIZE; i+=EEPROM_MASK_RATIO)
  {
    //per test val = 2000+i;
   val = GetMaxOpenMask(i);
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&val, 2);
    add += 2;
  }
  for (i=0; i<11; i++)
  {
   val = pm->MaxCurADC[i];
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&val, 2);
    add += 2;
  }

  if (TWI_WriteError != 0)
  {
    TWI_WriteError = 0;
    SEQ_AddEvent(eWriteError,pm->Sys);
  }
}

VS_VOID OldMaskLoad (VS_VOID)
{
#ifndef AM_KEY_VER
  unsigned int add = EEPROM_MASK_ADDRESS;
  unsigned short val;
  unsigned short i;
    add = add + 256*(pm->Mot);
    for (i=0; i<MASK_SIZE; i+=EEPROM_MASK_RATIO)
    {
      TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&val, 2);
      TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&val, 2);
      SetMaxCloseMask(i,val + (pm->CurThrClose * CUR_THR_FACTOR));
      add += 2;
    }
    for (i=0; i<MASK_SIZE; i+=EEPROM_MASK_RATIO)
    {
      TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&val, 2);
      TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&val, 2);
      SetMaxOpenMask(i,val + (pm->CurThrOpen * CUR_THR_FACTOR));
      add += 2;
    }
    if (TWI_WriteError != 0)
    {
      TWI_WriteError = 0;
      SEQ_AddEvent(eWriteError,pm->Sys);
    }

#endif
}


VS_VOID ExitProg (VS_VOID)
{
    pm->PwmDecOpen = pm->ParValues[0];
    pm->PwmDecClose = pm->ParValues[1];
    pm->DecOpen = pm->ParValues[2];
    pm->DecClose = pm->ParValues[3];
    pm->PwmAccOpen = pm->ParValues[4];//uguale a PwmAccClose
    pm->AccOpen = pm->ParValues[5];
    pm->AccClose = pm->ParValues[6];
    pm->PwmFindOpen = pm->ParValues[7];//uguale a PwmFindClose PwmNearOpen PwmNearClose
    pm->CurThrOpen = pm->ParValues[8];
    pm->CurThrClose = pm->ParValues[9];
    pm->CurThrSlope = pm->ParValues[10];
    pm->PwmChannel = pm->ParValues[11] * PWM_CHANNEL_BASE;
    pm->Dip1 = pm->ParValues[12];
    pm->Dip2 = pm->ParValues[13];
    pm->Dip6 = pm->ParValues[14];
    pm->Dip5 = pm->ParValues[15];
    pm->Run = pm->ParValues[16];
    pm->Counter = pm->ParValues[17];
    BrakeOnOff = pm->ParValues[18];
    Motor = pm->ParValues[19];
    SSCfg = pm->ParValues[20];
    SPCfg = pm->ParValues[21];
    STCfg = pm->ParValues[22];
    CloseOnOff = pm->ParValues[23];
    pm->EncoderCCWCfg = pm->ParValues[24];
	pm->StrokeShk = pm->ParValues[25];

    UpdateSwitch();
    pm->bProg = 0;
}

VS_VOID InitProg (VS_VOID)
{
    pm->ParValues[0] = pm->PwmDecOpen;
    pm->ParValues[1] = pm->PwmDecClose;
    pm->ParValues[2] = pm->DecOpen;
    pm->ParValues[3] = pm->DecClose;
    pm->ParValues[4] = pm->PwmAccOpen;//uguale a PwmAccClose
    pm->ParValues[5] = pm->AccOpen;
    pm->ParValues[6] = pm->AccClose;
    pm->ParValues[7] = pm->PwmFindOpen;//uguale a PwmFindClose PwmNearOpen PwmNearClose
    pm->ParValues[8] = pm->CurThrOpen;
    pm->ParValues[9] = pm->CurThrClose;
    pm->ParValues[10] = pm->CurThrSlope;
    pm->ParValues[11] = pm->PwmChannel / PWM_CHANNEL_BASE;
    pm->ParValues[12] = pm->Dip1;
    pm->ParValues[13] = pm->Dip2;
    pm->ParValues[14] = pm->Dip6;
    pm->ParValues[15] = pm->Dip5;
    pm->ParValues[16] = pm->Run;
    pm->ParValues[17] = pm->Counter;  ParNumCounter = 17;
    pm->ParValues[18] = BrakeOnOff;
    pm->ParValues[19] = Motor;
    pm->ParValues[20] = SSCfg;
    pm->ParValues[21] = SPCfg;
    pm->ParValues[22] = STCfg;
    pm->ParValues[23] = CloseOnOff;
    pm->ParValues[24] = pm->EncoderCCWCfg;
	pm->ParValues[25] = pm->StrokeShk;
    pm->bProg = 1;
}

VS_VOID UpdateSwitch (VS_VOID)
{
  //calcolati
  pm->PwmAccClose = pm->PwmAccOpen;
  pm->PwmFindClose = pm->PwmFindOpen;
  pm->PwmNearOpen = pm->PwmFindOpen;
  pm->PwmNearClose = pm->PwmFindOpen;

  pm->PosAccClose = PosOpen-(((PosOpen-PosClose)*pm->AccClose)/100);
  pm->PosAccOpen = PosClose+(((PosOpen-PosClose)*pm->AccOpen)/100);
  pm->PosAccOpenShk = PosClose+(((PosOpenShk-PosClose)*pm->AccOpen)/100);
  
  pm->PosDecClose = PosClose+(((PosOpen-PosClose)*pm->DecClose)/100);
  pm->PosDecOpen = PosOpen-(((PosOpen-PosClose)*pm->DecOpen)/100);
  pm->PosDecOpenShk = PosOpenShk-(((PosOpenShk-PosClose)*pm->DecOpen)/100);

  PosInvClose = PosOpen-(((PosOpen-PosClose)*95)/100);
  PosInvOpen = PosClose+(((PosOpen-PosClose)*95)/100);
  PosInvOpenShk = PosClose+(((PosOpenShk-PosClose)*95)/100);
  
  /*if ( pm->Dip5 != 0 ) estensione antinversione anche se DIP5==OFF
  {
    PosInvClose = PosOpen-(((PosOpen-PosClose)*75)/100);
    PosInvOpen = PosClose+(((PosOpen-PosClose)*75)/100);
  }
  else
  {
    PosInvClose = POS_MAX_VAL;
    PosInvOpen = 0;
  }*/

  pm->PosDelta = ((PosOpen - PosClose) * 30) / 1000;  // 3.0 %
  PosMaxClose = PosClose + pm->PosDelta;
  if (PosClose > (4 * pm->PosDelta))
  {
    pm->PosMinClose = PosClose - (4 * pm->PosDelta);
  }
  else
  {
    pm->PosMinClose = 0;
  }
  PosMinOpen = PosOpen - pm->PosDelta;
  PosMinOpenShk = PosOpenShk - pm->PosDelta;
  PosNearClose = PosClose + pm->PosDelta + pm->PosDelta;
  PosNearOpen = PosOpen - pm->PosDelta - pm->PosDelta;
  PosNearOpenShk = PosOpenShk - pm->PosDelta - pm->PosDelta;
  SetPosOpenShk();
}

VS_VOID SetPOffsetAO(VS_VOID){
    PosOffsetAO = pm->PosDelta / 3;
    PosOffsetAO = (PosOffsetAO > 5) ? PosOffsetAO : 5;
}

VS_VOID SetPOffsetAC(VS_VOID){
    PosOffsetAC = pm->PosDelta / 3;
    PosOffsetAC = (PosOffsetAC > 5) ? PosOffsetAC : 5;
}

VS_VOID ResPOffsetAO(VS_VOID){
      PosOffsetAO = 0;
}

VS_VOID ResPOffsetAC(VS_VOID){
      PosOffsetAC =0;
}

VS_VOID DefaultValues (VS_VOID)
{
//  pm->Version = 0x8100;
//  pm->Version = 0x8101;					//Allargato numero parametri da fmw 1.11.8.9
  pm->Version = 0x8102;					//Allargato numero parametri da fmw 2.10.1
  pm->CurThrClose = CUR_THR_DEFAULT;
  pm->CurThrOpen = CUR_THR_DEFAULT;
  pm->CurThrSlope = CUR_THR_SLOPE_DEFAULT;
  CurStep = 10;
  CurThrOverClose = 0;
  CurThrOverOpen = 0;
  pm->PwmChannel = PWM_CHANNEL_DEFAULT * PWM_CHANNEL_BASE;
  pm->PwmAccClose = PWM_ACC_CLOSE_DEFAULT;
  pm->PwmAccOpen = PWM_ACC_OPEN_DEFAULT;
  pm->PwmDecClose = PWM_DEC_CLOSE_DEFAULT;
  pm->PwmDecOpen = PWM_DEC_OPEN_DEFAULT;
  pm->PwmFindClose = PWM_FIND_CLOSE_DEFAULT;
  pm->PwmFindOpen = PWM_FIND_OPEN_DEFAULT;
  pm->PwmNearClose = PWM_NEAR_CLOSE_DEFAULT;
  pm->PwmNearOpen = PWM_NEAR_OPEN_DEFAULT;
  pm->PwmInitClose = PWM_INIT_CLOSE_DEFAULT;
  pm->PwmInitOpen = PWM_INIT_OPEN_DEFAULT;
  pm->PwmStartClose = PWM_START_CLOSE_DEFAULT;
  pm->PwmStartOpen = PWM_START_OPEN_DEFAULT;
  pm->AccOpen = ACC_OPEN_DEFAULT;
  pm->AccClose = ACC_CLOSE_DEFAULT;
  pm->DecOpen = DEC_OPEN_DEFAULT;
  pm->DecClose = DEC_CLOSE_DEFAULT;

  pm->Dip1 = 1;
  pm->Dip2 = 1;
  #ifdef CUR_CONTROL
  pm->Dip5 = 1;
  #else
  pm->Dip5 = 0;
  #endif
  pm->Dip6 = 1;
  pm->CatchDip5 = 0;
  BrakeOnOff = 0;
  Motor = 1;
  SSCfg = 0;
  SPCfg = 0;
  STCfg = 0;
  ParSel = 0;
  CloseOnOff = 0;
  pm->EncoderCCWCfg = 0;
  pm->StrokeShk = 100;	//Per default la corsa ridotta � posta uguale alla corsa completa 
  UpdateSwitch();
}

VS_VOID UpMask (VS_VOID)
{
  UpCurMask();
}

VS_VOID ClearCount (VS_VOID)
{
  pm->Counter = 0;
  ParWrite();
}

VS_VOID PosInit (VS_VOID)
{
  // valori di default da aggiornare con valori programmati
  UpdateSwitch();
  USCITE[ pm->Pa ] = 0;
  USCITE[ pm->Pc ] = 0;
}


VS_VOID PosReset (VS_VOID)
{

}

void SetPosOpenShk( void )
{
 	PosOpenShk = PosClose + (((PosOpen - PosClose) * (pm->StrokeShk * 10)) / 1000);
}
	

VS_VOID PosCloseSet (VS_VOID)
{
  PosClose = Pos;
  SetPosOpenShk();
}

VS_VOID PosOpenSet (VS_VOID)
{
  PosOpen = Pos;  

}

VS_VOID UpdatePosClose( VS_VOID  )
{
  unsigned int NewPosClose;

  NewPosClose = ((PosClose * (UPDATE_POS_FILTER - 1)) + (Pos - PosUpdateOffset)) / UPDATE_POS_FILTER;
  // saturazione (PosDelta) aggiornamento per evitare impossibilita` di raggiungere fine corsa in caso di disturbo
  if (NewPosClose > PosClose)
  {
    if ((NewPosClose - PosClose) >= pm->PosDelta)
    {
      NewPosClose = PosClose + pm->PosDelta - 1;
    }
  }
  else
  {
    if ((PosClose - NewPosClose) >= pm->PosDelta)
    {
      NewPosClose = PosClose - pm->PosDelta + 1;
    }
  }

  PosClose = NewPosClose;
  SetPosOpenShk();


  if (--pm->CountDown == 0)
  {
    pm->CountDown = UPDATE_POS_SAVE_COUNT;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, EEPROM_CFG_ADDRESS+4+256*(pm->Mot), (unsigned char *)&PosClose, 4);
   // TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, EEPROM_CFG_ADDRESS+52+256*(pm->Mot), (unsigned char *)&pm->Counter, 4);
	TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, EEPROM_CFG_ADDRESS+60+256*(pm->Mot), (unsigned char *)&pm->Counter, 4);		//Modificato da V2.29
	TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, EEPROM_CFG_ADDRESS+116+256*(pm->Mot), (unsigned char *)&PosOpenShk, 4);	//Aggiunto da V2.29
    MaskWrite();
    pm->ToWriteOpened = 1;
  }
}

VS_VOID UpdatePosOpen( VS_VOID  )
{
  unsigned int NewPosOpen;
  NewPosOpen = ((PosOpen * (UPDATE_POS_FILTER - 1)) + (Pos + PosUpdateOffset)) / UPDATE_POS_FILTER;
  // saturazione (PosDelta) aggiornamento per evitare impossibilita` di raggiungere fine corsa in caso di disturbo
  if (NewPosOpen > PosOpen)
  {
    if ((NewPosOpen - PosOpen) >= pm->PosDelta)
    {
      NewPosOpen = PosOpen + pm->PosDelta - 1;
    }
  }
  else
  {
    if ((PosOpen - NewPosOpen) >= pm->PosDelta)
    {
      NewPosOpen = PosOpen - pm->PosDelta + 1;
    }
  }
  PosOpen = NewPosOpen;
  SetPosOpenShk();
  
  if (pm->ToWriteOpened != 0)
  {
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, EEPROM_CFG_ADDRESS+8+256*(pm->Mot), (unsigned char *)&PosOpen, 4);
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, EEPROM_CFG_ADDRESS+116+256*(pm->Mot), (unsigned char *)&PosOpenShk, 4);
	MaskWrite();
    pm->ToWriteOpened = 0;
  }
}



void UpdatePwmPos( void )
{
    //variazione lineare close
    if (Pos < pm->PosDecClose){
        if (pm->EntryPwmDecClose == 0){
            pm->EntryPwmDecClose = pm->PwmTarget;
            pm->EntryPosDecClose = Pos + 1;
        }
        if (Pos > PosClose){
            if ((pm->EntryPwmDecClose > pm->PwmDecClose) && (pm->EntryPosDecClose > Pos)) {
                pm->PwmPosClose = pm->EntryPwmDecClose - (((pm->EntryPwmDecClose - pm->PwmDecClose)*(pm->EntryPosDecClose-Pos))/(pm->PosDecClose-PosClose));
            }
            else {
                pm->PwmPosClose = pm->EntryPwmDecClose + (((pm->PwmDecClose - pm->EntryPwmDecClose)*(pm->EntryPosDecClose-Pos))/(pm->PosDecClose-PosClose));
            }
        }
        else{
            pm->PwmPosClose = pm->PwmDecClose;
        }
    }
    else{
        pm->EntryPwmDecClose = 0;
        if (Pos > pm->PosAccClose) {
            if ((Pos < PosOpen) && (pm->PwmAccClose<100)) {
                pm->PwmPosClose = pm->PwmAccClose + (((100 - pm->PwmAccClose)*(PosOpen-Pos))/(PosOpen-pm->PosAccClose));
            }
            else{
                pm->PwmPosClose = pm->PwmAccClose;
            }
        }
        else{
            pm->PwmPosClose = 100; //100
        }
    }

    //variazione lineare open

    if (Pos > pm->PosDecOpen){
        if (pm->EntryPwmDecOpen == 0){
            pm->EntryPwmDecOpen = pm->PwmTarget;
            pm->EntryPosDecOpen = Pos - 1;
        }
        if (Pos < PosOpen) {
            if ((pm->EntryPwmDecOpen > pm->PwmDecOpen) && (pm->EntryPosDecOpen < Pos)){
                pm->PwmPosOpen = pm->EntryPwmDecOpen - (((pm->EntryPwmDecOpen - pm->PwmDecOpen)*(Pos-pm->EntryPosDecOpen))/(PosOpen-pm->PosDecOpen));
            }
            else{
                pm->PwmPosOpen = pm->EntryPwmDecOpen + (((pm->PwmDecOpen - pm->EntryPwmDecOpen)*(Pos-pm->EntryPosDecOpen))/(PosOpen-pm->PosDecOpen));
            }
        }
        else{
            pm->PwmPosOpen = pm->PwmDecOpen;
        }
    }
    else{
        pm->EntryPwmDecOpen = 0;
        if ((Pos < pm->PosAccOpen) && (pm->PwmAccOpen<100)){
            if (Pos > PosClose) {
                pm->PwmPosOpen = pm->PwmAccOpen + (((100 - pm->PwmAccOpen)*(Pos-PosClose))/(pm->PosAccOpen-PosClose));
            }
            else{
                pm->PwmPosOpen = pm->PwmAccOpen;
            }
        }
        else{
            pm->PwmPosOpen = 100; //100
        }
    }
}

void UpdatePwmPosOpenShk( void )
{

    //variazione lineare open

    if (Pos > pm->PosDecOpenShk){
        if (pm->EntryPwmDecOpen == 0){
            pm->EntryPwmDecOpen = pm->PwmTarget;
            pm->EntryPosDecOpen = Pos - 1;
        }
        if (Pos < PosOpenShk) {
            if ((pm->EntryPwmDecOpen > pm->PwmDecOpen) && (pm->EntryPosDecOpen < Pos)){
                pm->PwmPosOpen = pm->EntryPwmDecOpen - (((pm->EntryPwmDecOpen - pm->PwmDecOpen)*(Pos-pm->EntryPosDecOpen))/(PosOpenShk-pm->PosDecOpenShk));
            }
            else{
                pm->PwmPosOpen = pm->EntryPwmDecOpen + (((pm->PwmDecOpen - pm->EntryPwmDecOpen)*(Pos-pm->EntryPosDecOpen))/(PosOpenShk-pm->PosDecOpenShk));
            }
        }
        else{
            pm->PwmPosOpen = pm->PwmDecOpen;
        }
    }
    else{
        pm->EntryPwmDecOpen = 0;
        if ((Pos < pm->PosAccOpenShk) && (pm->PwmAccOpen<100)){
            if (Pos > PosClose) {
                pm->PwmPosOpen = pm->PwmAccOpen + (((100 - pm->PwmAccOpen)*(Pos-PosClose))/(pm->PosAccOpenShk-PosClose));
            }
            else{
                pm->PwmPosOpen = pm->PwmAccOpen;
            }
        }
        else{
            pm->PwmPosOpen = 100; //100
        }
    }
}


void DisplayValues(VS_VOID)
{
    if (DisplayMode == DISPLAY_MODE_DEBUG){
#ifdef MULTIPLE_LINE_LOG
        US_SendCharDbg((AT91PS_USART)DBGU_pt, 10);
#endif
        US_SendCharDbg((AT91PS_USART)DBGU_pt, 13);
        if (pm->Dir == DirClose){
            DisplayValue(Pos, CHR_CLOSE);
        }
        else{
            DisplayValue(Pos, CHR_OPEN);
        }
        DisplayValue(pm->Cur, CHR_CUR);
        if (pm->Dir == DirClose){
#ifdef CUR_CONTROL
            DisplayValue(pm->CurMaskClose[pm->PosCur], CHR_MASK);
#endif
        }
        else{
#ifdef CUR_CONTROL
            DisplayValue(pm->CurMaskOpen[pm->PosCur], CHR_MASK);
#endif
        }
        DisplayValue(pm->PwmTarget, CHR_PWM);
    }
    else{
        if (Sys==SysDisplay){
            if (pm->bProg == 0){
                LastMsgId = LastMsgIdSys[Sys];
                Display(LastMsgId);
                //DisplayValue(SysDisplay, 32);
                DisplayValue2(pm->Cur, 32);
                DisplayValue2(Pos/4, 32);
               // DisplayValue2(Pos, 32);
               // DisplayValue2(pm->PwmActive, 32);
                if (pm->CurMaskOpenOk!=0){
                    Dbg((AT91PS_USART)DBGU_pt, " THR/OP OK");
                }
                else{
                    Dbg((AT91PS_USART)DBGU_pt, " THR/OP --");
                }

                //DisplayValue2(pm->CurMaskCloseOk, 32);
                if (pm->CurMaskCloseOk!=0){
                    Dbg((AT91PS_USART)DBGU_pt, " THR/CL OK");
                }
                else{
                    Dbg((AT91PS_USART)DBGU_pt, " THR/CL --");
                }
            }
        }
    }
}


VS_VOID Display (VS_UINT16 MsgId)
{
  if (DisplayMode == DISPLAY_MODE_DEBUG)
  {
    US_ClearDisplay((AT91PS_USART)DBGU_pt);
  }
  switch (MsgId)
  {
    case 1:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_1);
      break;
    case 2:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_2);
      break;
    case 3:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_3);
      break;
    case 4:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_4);
      break;
    case 5:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_5);
      break;
    case 6:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_6);
      break;
    case 7:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_7);
      break;
    case 8:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_8);
      break;
    case 9:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_9);
      break;
    case 10:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_10);
      break;
    case 11:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_11);
      break;
    case 12:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_12);
      break;
    case 13:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_13);
      break;
    case 14:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_14);
      break;
    case 15:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_15);
      break;
    case 16:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_16);
      break;
    case 17:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_17);
      break;
    case 18:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_18);
      break;
    case 19:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_19);
      break;
    case 20:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_20);
      break;
    case 21:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_21);
      break;
    case 22:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_22);
      break;
    case 23:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_23);
      break;
    case 24:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_24);
      break;
    case 25:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_25);
      break;
    case 26:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_26);
      break;
    case 27:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_27);
      break;
    case 28:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_28);
      break;
    case 29:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_29);
      break;
    default:
      Dbg((AT91PS_USART)DBGU_pt, (char *)MSG_0);
      break;
  }
  if (DisplayMode == DISPLAY_MODE_DEBUG)
  {
    DisplayValues();
    US_SendCharDbg((AT91PS_USART)DBGU_pt, 13);
    #ifndef MULTIPLE_LINE_LOG
      US_SendCharDbg((AT91PS_USART)DBGU_pt, 10);
    #endif
  }
  LastMsgIdSys[Sys] = MsgId;
}

VS_VOID DisplayDesc (VS_UINT Par)
{
  Dbg((AT91PS_USART)DBGU_pt, (char *)ParDesc[Par]);
  Dbg((AT91PS_USART)DBGU_pt, "     ");
}

VS_VOID DisplayPar (VS_UINT Par)
{
//  #ifndef MULTIPLE_LINE_LOG
    US_SendCharDbg((AT91PS_USART)DBGU_pt, 13);
//  #endif
    US_ClearDisplay((AT91PS_USART)DBGU_pt);
    DisplayDesc( Par );
    if ((ParValuesMax[Par]==1)&&(ParValuesMin[Par]==0)){
        if (pm->ParValues[Par]!=0){
            switch(Par){
                case 24:
                Dbg((AT91PS_USART)DBGU_pt, "       CCW");
                break;
            default:
                Dbg((AT91PS_USART)DBGU_pt, "        ON");
                break;
            }
        }
        else{
            switch(Par){
              case 24:
                Dbg((AT91PS_USART)DBGU_pt, "        CW");
                break;
              default:
                Dbg((AT91PS_USART)DBGU_pt, "       OFF");
                break;
            }
        }
    }
    else{
        DisplayValue( pm->ParValues[Par], 32 );
    }
}

VS_VOID IncPar (VS_UINT Par)
{
  if (pm->ParValues[Par] < ParValuesMax[Par])
  {
    pm->ParValues[Par]++;
  };
}

VS_VOID IncSel (VS_VOID)
{
  if (ParSel < (PAR_NUM_MAX-1))
  {
    ParSel++;
  }
  else
  {
    ParSel = 0;
  }
}

VS_VOID DecPar (VS_UINT Par)
{
  if (pm->ParValues[Par] > ParValuesMin[Par])
  {
    pm->ParValues[Par]--;
  };
}

VS_VOID DecSel (VS_VOID)
{
  if (ParSel > 0)
  {
    ParSel--;
  }
  else
  {
    ParSel = PAR_NUM_MAX-1;
  }
}

VS_VOID Reset (VS_VOID)
{
  pm->AccClose = 0;
  pm->AccOpen = 0;
  pm->Counter = 0;
  pm->Cur = 0;
  pm->CurPos = 0;
  CurStep = 0;
  pm->CurThrClose = 0;
  pm->CurThrOpen = 0;
  pm->CurThrSlope = 0;
  CurThrOverClose = 0;
  CurThrOverOpen = 0;
  pm->DecClose = 0;
  pm->DecOpen = 0;
  pm->Dir = 0;
  pm->Run = 0;
  pm->MovDelta = 0;
  ParNumCounter = 17;
  ParSel = 0;
  Pos = 0;//PosMid;
  pm->PosAccClose = 0;
  pm->PosAccOpen = 0;
  pm->PosAccOpenShk = 0;
  PosClose = 0;
  pm->PosCur = 0;
  pm->PosDecClose = 0;
  pm->PosDecOpen = 0;
  pm->PosDecOpenShk = 0;
  pm->PosDelta = 0;
  PosInvClose = 0;
  PosInvOpen = 0;
  PosInvOpenShk = 0;
  PosMaxClose = 0;
  pm->PosMinClose = 0;
  PosMinOpen = 0;
  PosMinOpenShk = 0;
  PosNearClose = 0;
  PosNearOpen = 0;
  PosNearOpenShk = 0;
  PosOpen = 0;
  PosOpenShk = 0;
  pm->PwmAccClose = 0;
  pm->PwmAccOpen = 0;
  pm->PwmActive = 0;
  pm->PwmChannel = 0;
  pm->PwmDecClose = 0;
  pm->PwmDecOpen = 0;
  pm->PwmFindClose = 0;
  pm->PwmFindOpen = 0;
  pm->PwmInitClose = 0;
  pm->PwmInitOpen = 0;
  pm->PwmNearClose = 0;
  pm->PwmNearOpen = 0;
  pm->PwmPosClose = 0;
  pm->PwmPosOpen = 0;
  pm->PwmStartClose = 0;
  pm->PwmStartOpen = 0;
  pm->PwmTarget = 0;
  pm->Version = 0;
  pm->bCloseDetected = 0;
  pm->bCloseEnabled = 0;
  pm->bManual = 0;
  pm->bOpen = 0;
  pm->bOpenDetected = 0;
  pm->bOpenEnabled = 0;
  pm->bOverCurDetected = 0;
  pm->bOverCurEnabled = 0;
  pm->bPosNotifyEnabled = 0;
  pm->bProg = 0;
  pm->bUnderCurDetected = 0;
  pm->bUnderCurEnabled = 0;
  pm->bUpdateMaskEnabled = 0;
  pm->bAcqMaxCurADC = 0;
unsigned int i;
  for(i=0; i<11; i++){
      pm->MaxCurADC[i] = 0;
  };
  for(i=0; i<MASK_SIZE; i++){
      pm->PwmMaskOpen[i] = 0;
      pm->PwmMaskClose[i] = 0;
  }
  //Leone
  BrakeOnOff = 0;
  Motor = 1;  //ante motorizzate
  SSCfg = 0;
  SPCfg = 0;
  STCfg = 0;
  CloseOnOff = 0;
  pm->EncoderCCWCfg = 0;
  pm->StrokeShk = 100;
}

VS_VOID ParRead (VS_VOID)
{
  DefaultValues();
  //lettura parametri da eeprom
  ReadCfg();
  //MaskRead();
  if (TWI_ReadError != 0)
  {
    TWI_ReadError = 0;
    DefaultValues();
    UpCurMask();
  }
  UpdateSwitch();//non eliminare anche se chiamato in DefaultValues per aggiornare dopo lettura eeprom
}

VS_VOID ParWrite (VS_VOID)
{
    unsigned int add = EEPROM_CFG_ADDRESS;
    ValuesCheck();
    add = add + 256*(pm->Mot);
    // versione
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->Version, 4);
    add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&PosClose, 4);
    add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&PosOpen, 4);
    add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->PwmDecOpen, 4);
    add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->PwmDecClose, 4);
    add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->DecOpen, 4);
    add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->DecClose, 4);
    add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->PwmAccOpen, 4);
    add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->AccOpen, 4);
    add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->AccClose, 4);
    add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->PwmFindOpen, 4);
    add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->CurThrOpen, 4);
    add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->CurThrClose, 4);
    add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->CurThrSlope, 4);
    add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->PwmChannel, 4);
    add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->Counter, 4);
    add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->Dip1, 4);
    add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->Dip2, 4);
    add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->Dip6, 4);
    add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->Dip5, 4);
    add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->Run, 4);
    add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&BrakeOnOff, 4);
    add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&Motor, 4);
    add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&SSCfg, 4);
    add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&SPCfg, 4);
    add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&STCfg, 4);
    add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&CloseOnOff, 4);
    add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->EncoderCCWCfg, 4);
	add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&PosOpenShk, 4);			//+116 //Aggiunto V. 2.29
	add += 4;
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->StrokeShk, 4);			//+120 //Aggiunto V. 2.29


    if (TWI_WriteError != 0)
    {
        TWI_WriteError = 0;
        SEQ_AddEvent(eWriteError,pm->Sys);
    }
}



VS_VOID OldParLoad (VS_VOID)
{
#ifndef AM_KEY_VER
    unsigned int add = EEPROM_CFG_ADDRESS, TmpValue = 0;

    ValuesCheck();
    add = add + 256*(pm->Mot);
    // versione
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&pm->Version, 4);
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->Version, 4);
    add += 4;

    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue > 0
        &&  TmpValue < 1024){
            PosClose = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&PosClose, 4);
        }
    add += 4;

    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue > 0
        &&  TmpValue < 1024){
            PosOpen = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&PosOpen, 4);
        }
    add += 4;

    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue > ParValuesMin[0]
        &&  TmpValue < ParValuesMax[0]){
            pm->PwmDecOpen = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->PwmDecOpen, 4);
        }
    add += 4;

    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue > ParValuesMin[1]
        &&  TmpValue < ParValuesMax[1]){
            pm->PwmDecClose = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->PwmDecClose, 4);
        }
    add += 4;

    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue > ParValuesMin[2]
        &&  TmpValue < ParValuesMax[2]){
            pm->DecOpen = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->DecOpen, 4);
        }
    add += 4;

    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue > ParValuesMin[3]
        &&  TmpValue < ParValuesMax[3]){
            pm->DecClose = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->DecClose, 4);
        }
    add += 4;

    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue > ParValuesMin[4]
        &&  TmpValue < ParValuesMax[4]){
            pm->PwmAccOpen = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->PwmAccOpen, 4);
        }
    add += 4;

    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue > ParValuesMin[5]
        &&  TmpValue < ParValuesMax[5]){
            pm->AccOpen = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->AccOpen, 4);
        }
    add += 4;

    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue > ParValuesMin[6]
        &&  TmpValue < ParValuesMax[6]){
            pm->AccClose = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->AccClose, 4);
        }
    add += 4;

    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue > ParValuesMin[7]
        &&  TmpValue < ParValuesMax[7]){
            pm->PwmFindOpen = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->PwmFindOpen, 4);
        }
    add += 4;

    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue > ParValuesMin[8]
        &&  TmpValue < ParValuesMax[8]){
            pm->CurThrOpen = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->CurThrOpen, 4);
        }
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue > ParValuesMin[9]
        &&  TmpValue < ParValuesMax[9]){
            pm->CurThrClose = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->CurThrClose, 4);
        }
    add += 4;
    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue > ParValuesMin[10]
        &&  TmpValue < ParValuesMax[10]){
            pm->CurThrSlope = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->CurThrSlope, 4);
        }
    add += 4;

    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue > ParValuesMin[11]
        &&  TmpValue < ParValuesMax[11]){
            pm->PwmChannel = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->PwmChannel, 4);
        }
    add += 4;

    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue > ParValuesMin[12]
        &&  TmpValue < ParValuesMax[12]){
            pm->Dip1 = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->Dip1, 4);
        }
    add += 4;

    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue > ParValuesMin[13]
        &&  TmpValue < ParValuesMax[13]){
            pm->Dip2 = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->Dip2, 4);
        }
    add += 4;

    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue > ParValuesMin[14]
        &&  TmpValue < ParValuesMax[14]){
            pm->Dip6 = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->Dip6, 4);
        }
    add += 4;

    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue > ParValuesMin[15]
        &&  TmpValue < ParValuesMax[15]){
            pm->Dip5 = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->Dip5, 4);
        }
    add += 4;

    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue > ParValuesMin[16]
        &&  TmpValue < ParValuesMax[16]){
            pm->Run = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->Run, 4);
        }
    add += 4;

    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue > ParValuesMin[17]
        &&  TmpValue < ParValuesMax[17]){
            pm->Counter = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->Counter, 4);
        }
    add += 4;

    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue > ParValuesMin[18]
        &&  TmpValue < ParValuesMax[18]){
            BrakeOnOff = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&BrakeOnOff, 4);
        }
    add += 4;

    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue > ParValuesMin[19]
        &&  TmpValue < ParValuesMax[19]){
            Motor = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&Motor, 4);
        }
    add += 4;

    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue > ParValuesMin[20]
        &&  TmpValue < ParValuesMax[20]){
            SSCfg = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&SSCfg, 4);
        }
    add += 4;

    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue > ParValuesMin[21]
        &&  TmpValue < ParValuesMax[21]){
            SPCfg = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&SPCfg, 4);
        }
    add += 4;

    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue > ParValuesMin[22]
        &&  TmpValue < ParValuesMax[22]){
            STCfg = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&STCfg, 4);
        }
	add += 4;

    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue > ParValuesMin[23]
        &&  TmpValue < ParValuesMax[23]){
            CloseOnOff = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&CloseOnOff, 4);
        }
	add += 4;

    TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue > ParValuesMin[24]
        &&  TmpValue < ParValuesMax[24]){
            pm->EncoderCCWCfg = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->EncoderCCWCfg, 4);
        }
	add += 4;
	
	TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue > 0
        &&  TmpValue < 1024){
            PosOpenShk = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&PosOpenShk, 4);;
        }	
 	add += 4;
	
	TWI_Read ( AT91C_BASE_TWI, EEPROM_24AA256, add-EEPROM_OLD_OFFSET, (unsigned char *)&TmpValue, 4);
    if (    TmpValue >= 50
        &&  TmpValue <= 100){
            pm->StrokeShk = TmpValue;
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&pm->StrokeShk, 4);;
        }	
	
    if (TWI_WriteError != 0)
    {
        TWI_WriteError = 0;
        SEQ_AddEvent(eWriteError,pm->Sys);
    }


    UpdateSwitch();

#endif
}

// *****************************************************************************
// debug Clear EEPROM     Per rendere disponibile riabilitare comando "c"
// *****************************************************************************
VS_VOID ClearE2(VS_VOID)
{
  unsigned int add = EEPROM_CFG_ADDRESS;
  unsigned int DummyVal = 0xFFFFFFFF, i = 0 ;
  add = add + 256*(pm->Mot);
  for(i=0; i<64; i++){
    TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, add, (unsigned char *)&DummyVal, 4);
    add += 4;
  }

}


VS_VOID InitPwm (VS_VOID)
{
  if (pm->PwmChannel > (PWM_CHANNEL_MAX * PWM_CHANNEL_BASE))
  {
    pm->PwmChannel = (PWM_CHANNEL_DEFAULT * PWM_CHANNEL_BASE);
  }
  PWM_init( Sys, PWM_PERIOD + pm->PwmChannel );
}


VS_VOID SetPwmTarget (VS_UINT Pwm)
{
  pm->PwmTarget = Pwm;
}

VS_VOID PrgOn (VS_VOID)
{
  USCITE[ pm->Prg ] = 1;
}

VS_VOID PrgOff (VS_VOID)
{
  USCITE[ pm->Prg ] = 0;
}

VS_VOID FmOn (VS_VOID)
{
  // sblocca solo se in prossimita` di PosClose
//  if ((Pos < PosNearClose) || (PosOpen == 0))
//  {
    if ( pm->Dip1 != 0 )
    {
      USCITE[ pm->Frn ] = 1;
    }
    else
    {
      USCITE[ pm->Frn ] = 0;
    }
//  }
}

VS_VOID FmOff (VS_VOID)
{
    if ( pm->Dip1 != 0 ){
        USCITE[ pm->Frn ] = 0;
    }
    else{
        USCITE[ pm->Frn ] = 1;
    }
}

VS_VOID MotClose (VS_UINT Pwm)
{
  pm->ExtProtEnabled = 1;
  pm->Dir = DirClose;
  SetPwmTarget( Pwm );
  pm->bUpdateMaskEnabled = 0;
  DisplayValues();
}

VS_VOID MotOpen (VS_UINT Pwm)
{
  pm->ExtProtEnabled = 1;
  pm->Dir = DirOpen;
  SetPwmTarget( Pwm );
  pm->bUpdateMaskEnabled = 0;
  DisplayValues();
}

VS_VOID MotCloseStart (VS_VOID)
{
    pm->Dir = DirClose;
    UpdatePwmPos();
    if (Pos > (PosClose + ((PosOpen - PosClose)/5))){
        if (pm->PwmPosClose > pm->PwmAccClose){
            MotClose( pm->PwmPosClose );
        }
        else{
            MotClose( pm->PwmAccClose );
        }
    }
    else{
        if (pm->PwmPosClose > pm->PwmFindClose){
            MotClose( pm->PwmPosClose );
        }
        else{
            MotClose( pm->PwmFindClose );
        }
    }
}

VS_VOID MotCloseFind (VS_VOID)
{
  MotClose( pm->PwmFindClose );
}

VS_VOID MotCloseInit (VS_VOID)
{
  MotClose( pm->PwmInitClose );
}

VS_VOID MotCloseNear (VS_VOID)
{
  MotClose( pm->PwmNearClose );
}

VS_VOID MotCloseBrake (VS_VOID)
{
  MotClose( 5 );
}

VS_VOID MotCloseFull00 (VS_VOID)
{
    unsigned int i = 0;
    MotStop();
    for(i=0; i<11; i++){
        pm->MaxCurADC[i] = 0;
    }
    pm->bAcqMaxCurADC = 1;
}

VS_VOID MotCloseFull10 (VS_VOID)
{
  MotClose( 10 );
}

VS_VOID MotCloseFull20 (VS_VOID)
{
  MotClose( 20 );
}

VS_VOID MotCloseFull30 (VS_VOID)
{
  MotClose( 30 );
}

VS_VOID MotCloseFull40 (VS_VOID)
{
  MotClose( 40 );
}

VS_VOID MotCloseFull50 (VS_VOID)
{
  MotClose( 50 );
}

VS_VOID MotCloseFull60 (VS_VOID)
{
  MotClose( 60 );
}

VS_VOID MotCloseFull70 (VS_VOID)
{
  MotClose( 70 );
}

VS_VOID MotCloseFull80 (VS_VOID)
{
  MotClose( 80 );
}

VS_VOID MotCloseFull90 (VS_VOID)
{
  MotClose( 90 );
}

VS_VOID MotCloseFull100 (VS_VOID)
{
  MotClose( 100 );
}

/*------------------------------------------------------------------------------*/
/* Function     :                                                           */
/*------------------------------------------------------------------------------*/
VS_VOID MotCloseGoOn (VS_VOID)
{
    unsigned char StepPwm=3;
    pm->Dir = DirClose;
    if (pm->PwmTarget >= (100- StepPwm))
        MotClose(100);
    else
        MotClose(pm->PwmTarget +  StepPwm );
}

VS_VOID MotOpenFind (VS_VOID)
{
  MotOpen( pm->PwmFindOpen );
}

VS_VOID MotOpenInit (VS_VOID)
{
  MotOpen( pm->PwmInitOpen );
}

VS_VOID MotOpenNear (VS_VOID)
{
  MotOpen( pm->PwmNearOpen );
}

VS_VOID MotOpenBrake (VS_VOID)
{
  MotOpen( 5 );
}

VS_VOID MotOpenStart (VS_VOID)
{
    pm->Dir = DirOpen;
    UpdatePwmPos();
    if (Pos < (PosOpen - ((PosOpen - PosClose)/5))){
        if (pm->PwmPosOpen > pm->PwmAccOpen){
            MotOpen( pm->PwmPosOpen );
        }
        else{
            MotOpen( pm->PwmAccOpen );
        }
    }
    else{
        if (pm->PwmPosOpen > pm->PwmFindOpen){
            MotOpen( pm->PwmPosOpen );
        }
        else{
            MotOpen( pm->PwmFindOpen );
        }
    }
}

VS_VOID MotOpenStartShk (VS_VOID)
{
    pm->Dir = DirOpen;
    UpdatePwmPosOpenShk();
    if (Pos < (PosOpenShk - ((PosOpenShk - PosClose)/5))){
        if (pm->PwmPosOpen > pm->PwmAccOpen){
            MotOpen( pm->PwmPosOpen );
        }
        else{
            MotOpen( pm->PwmAccOpen );
        }
    }
    else{
        if (pm->PwmPosOpen > pm->PwmFindOpen){
            MotOpen( pm->PwmPosOpen );
        }
        else{
            MotOpen( pm->PwmFindOpen );
        }
    }
}


/*------------------------------------------------------------------------------*/
/* Function     :                                                           */
/*------------------------------------------------------------------------------*/
VS_VOID MotOpenGoOn (VS_VOID)
{
    unsigned char StepPwm=3;
    pm->Dir = DirOpen;
    if (pm->PwmTarget >= (100-StepPwm))
        MotOpen(100);
    else
        MotOpen(pm->PwmTarget + StepPwm);
}

VS_VOID MotClosePos (VS_VOID)
{
    pm->Dir = DirClose;
    UpdatePwmPos();
    MotClose( pm->PwmPosClose );
}

VS_VOID MotOpenPos (VS_VOID)
{
    pm->Dir = DirOpen;
    UpdatePwmPos();
    MotOpen( pm->PwmPosOpen );
}

VS_VOID MotOpenPosShk (VS_VOID)
{
    pm->Dir = DirOpen;
    UpdatePwmPosOpenShk();
    MotOpen( pm->PwmPosOpen );
}

VS_VOID MotStop (VS_VOID)
{
    SetStop(pm->PwmCh);
    pm->PwmTarget = 0;
    pm->PwmActive = 0;
    pm->bUpdateMaskEnabled = 1;
    pm->ExtProtEnabled = 0;
    DisplayValues();
}

VS_VOID StopAcqFull (VS_VOID){
     pm->bAcqMaxCurADC = 0;
}

VS_VOID OverCurDisable (VS_VOID)
{    
    pm->bOverCurEnabled = 0;
    pm->bOverCurDetected = 0;
}

VS_VOID OverCurEnable (VS_VOID)
{
	if ( pm->Dip5 != 0 ){
		if (pm->bOverCurEnabled == 0){			
			pm->bOverCurEnabled = 1;
			pm->bOverCurDetected = 0;
		}
	}
	else{
		OverCurDisable();
	}
}

VS_VOID UnderCurDisable (VS_VOID)
{
  pm->bUnderCurEnabled = 0;
  pm->bUnderCurDetected = 0;
}

VS_VOID UnderCurEnable (VS_VOID)
{
  if (pm->bUnderCurEnabled == 0)
  {
    pm->bUnderCurEnabled = 1;
    pm->bUnderCurDetected = 0;
    if (DisplayMode == DISPLAY_MODE_DEBUG)
    {
      US_SendCharDbg((AT91PS_USART)DBGU_pt, 69);
      US_SendCharDbg((AT91PS_USART)DBGU_pt, 69);
      US_SendCharDbg((AT91PS_USART)DBGU_pt, 69);
    }
  }
}

VS_VOID OpenDisable (VS_VOID)
{
  pm->bOpenEnabled = 0;
}

VS_VOID OpenEnable (VS_VOID)
{
  pm->bOpenEnabled = 1;
  pm->bOpenDetected = 0;
}

VS_VOID CloseDisable (VS_VOID)
{
  pm->bCloseEnabled = 0;
}

VS_VOID CloseEnable (VS_VOID)
{
  pm->bCloseEnabled = 1;
  pm->bCloseDetected = 0;
}

VS_VOID Timer (VS_UINT event, VS_UINT ticks)
{
  //intercettare contesto attivo
  TIMER_SW_stop(Sys, 0);
  TIMER_SW_start(event, ticks, Sys, 0);
}

VS_VOID TLock (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys, 1);
  TIMER_SW_start(event, ticks, Sys, 1);
}

VS_VOID TTick (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys, 2);
  TIMER_SW_start(event, ticks, Sys, 2);
}

VS_VOID TViolate (VS_UINT event, VS_UINT ticks)
{
  TIMER_SW_stop(Sys, 3);
  TIMER_SW_start(event, ticks, Sys, 3);
}

VS_VOID STLock (VS_VOID)
{
  TIMER_SW_stop(Sys, 1);
}

VS_VOID STTick (VS_VOID)
{
  TIMER_SW_stop(Sys, 2);
}

VS_VOID Notify (VS_UCHAR Ch)
{
  if (DisplayMode == DISPLAY_MODE_DEBUG)
  {
    US_SendCharDbg((AT91PS_USART)DBGU_pt, Ch);
    US_SendCharDbg((AT91PS_USART)DBGU_pt, Ch);
    US_SendCharDbg((AT91PS_USART)DBGU_pt, Ch);
  }
}


VS_VOID MovNotify (VS_VOID)
{
  USCITE[ pm->Pa ] = 0;
  USCITE[ pm->Pc ] = 0;
}


VS_VOID ClosedNotify (VS_VOID)
{
    if (Motor == 0){
        USCITE[ pm->Pc ] = 1;
        USCITE[ pm->Pa ] = 0;
    }
    else{

        if (CloseOnOff==0){
            if ((Pos < (PosMaxClose - PosOffsetAC))&&(Pos > pm->PosMinClose)){
                USCITE[ pm->Pc ] = 1;
            }
            else{
                if ((Pos > (PosMaxClose - PosOffsetAC))||(Pos <= pm->PosMinClose)){
                    USCITE[ pm->Pc ] = 0;
                }
            }
        }
        else {
            if(SSCfg==1){
                if (   ((Pos < (PosMaxClose-PosOffsetAC))&&(Pos > pm->PosMinClose))
                    && (   (   (pm->Mot==0)
                            && (   (INGRESSI[SS1] == 1)
                                || (INGRESSI[SS1_CB] == 1)))
                        || (   (pm->Mot==1)
                            && (   (INGRESSI[SS2] == 1)
                                || (INGRESSI[SS2_CB] == 1)))))
                {
                    USCITE[ pm->Pc ] = 1;
                }
                else{
                    USCITE[ pm->Pc ] = 0;
                }
            }
            else{
                if ((Pos < (PosMaxClose - PosOffsetAC))&&(Pos > pm->PosMinClose)){
                    USCITE[ pm->Pc ] = 1;
                }
                else{
                    if ((Pos > (PosMaxClose - PosOffsetAC))||(Pos <= pm->PosMinClose)){
                        USCITE[ pm->Pc ] = 0;
                    }
                }
            }
        }

        USCITE[ pm->Pa ] = 0;
    }
}


VS_VOID OpenedNotify (VS_VOID)
{
    if (Motor == 1){
        if (Pos > (PosMinOpen + PosOffsetAO)){
            USCITE[ pm->Pa ] = 1;
        }
        else{
            if (Pos < (PosMinOpen + PosOffsetAO)){
                USCITE[ pm->Pa ] = 0;
            }
        }
        USCITE[ pm->Pc ] = 0;
    }
    else{
        USCITE[ pm->Pc ] = 0;
        USCITE[ pm->Pa ] = 1;
    }
}

VS_VOID ErrorNotify (VS_VOID)
{
  USCITE[ pm->Err ] = 1;
  USCITE[ pm->Pa ] = 0;
  USCITE[ pm->Pc ] = 0;
}

VS_VOID ErrorOff (VS_VOID)
{
  USCITE[ pm->Err ] = 0;
}

VS_VOID ViolateNotify (VS_VOID)
{
    USCITE[ pm->ViolP ] = 0;
}

VS_VOID UnViolateNotify (VS_VOID)
{
    USCITE[ pm->ViolP ] = 1;
}

VS_VOID PosNotifyEnable (VS_VOID)
{
  pm->bPosNotifyEnabled = VS_TRUE;
}

VS_VOID PosNotifyDisable (VS_VOID)
{
  pm->bPosNotifyEnabled = VS_FALSE;
}

VS_VOID ManualOn (VS_VOID)
{
  pm->bManual = 1;
}

VS_VOID ManualOff (VS_VOID)
{
  if (pm->Run == 1)
    pm->bManual = 0;
}

VS_VOID CounterUp (VS_VOID)
{
    if ((pm->Counter < 9999999))
        pm->Counter++;
    else
        pm->Counter = 0;
}

VS_VOID DeltaRun (VS_VOID)
{
  pm->MovDelta = MovDeltaRun;
}

VS_VOID DeltaFail (VS_VOID)
{
  pm->MovDelta = MovDeltaFail;
}

VS_VOID DeltaInit (VS_VOID)
{
  pm->MovDelta = MovDeltaInit;
}

VS_VOID DeltaBrake (VS_VOID)
{
  pm->MovDelta = MovDeltaBrake;
}
/*------------------------------------------------------------------------------*/
/* Function     :                                                           */
/*------------------------------------------------------------------------------*/
VS_VOID DeltaStart (VS_VOID)
{
    pm->MovDelta = ((PosOpen-PosClose)*20) /1000;   //2% della corsa
    pm->MovDelta = pm->MovDelta > MovDeltaStart ? pm->MovDelta : MovDeltaStart;
}

void RestoreMotSys(tMotSys * pmot)
{
  //variabili di elaborazione condivise con VS
  CurStep = pmot->_CurStep;
  CurThrOverClose = pmot->_CurThrOverClose;
  CurThrOverOpen = pmot->_CurThrOverOpen;
  ParNumCounter = pmot->_ParNumCounter;
  ParSel = pmot->_ParSel;
  Pos = pmot->_Pos;
  PosClose = pmot->_PosClose;
  PosInvClose = pmot->_PosInvClose;
  PosInvOpen = pmot->_PosInvOpen;
  PosInvOpenShk = pmot->_PosInvOpenShk;
  PosMaxClose = pmot->_PosMaxClose;
  PosMinOpen = pmot->_PosMinOpen;
  PosMinOpenShk = pmot->_PosMinOpenShk;
  PosNearClose = pmot->_PosNearClose;
  PosNearOpen = pmot->_PosNearOpen;
  PosNearOpenShk = pmot->_PosNearOpenShk;
  PosOpen = pmot->_PosOpen;
  PosOpenShk = pmot->_PosOpenShk;
  PosOffsetAO = pmot->_PosOffsetAO;
  PosOffsetAC = pmot->_PosOffsetAC;
  MovCount = pmot->_MovCount;
  bParReread = pmot->_bParReread;
  bEnExtProt = pmot->_bEnExtProt;
  bEmergency = pmot->_bEmergency;
  bPassiveMov = pmot->_bPassiveMov;
  bLowBatt = pmot->_bLowBatt;
  //variabili di configurazione condivise con VS
  BrakeOnOff = pmot->_BrakeOn;
  Motor = pmot->_Motor;
  SSCfg = pmot->_SSCfg;
  SPCfg = pmot->_SPCfg;
  STCfg = pmot->_STCfg;
  CloseOnOff = pmot->_CloseOn;
}

void SaveMotSys(tMotSys * pmot)
{
  //variabili di elaborazione condivise con VS
  pmot->_CurStep = CurStep;
  pmot->_CurThrOverClose = CurThrOverClose;
  pmot->_CurThrOverOpen = CurThrOverOpen;
  pmot->_ParNumCounter = ParNumCounter;
  pmot->_ParSel = ParSel;
  pmot->_Pos = Pos;
  pmot->_PosClose = PosClose;
  pmot->_PosInvClose = PosInvClose;
  pmot->_PosInvOpen = PosInvOpen;
  pmot->_PosInvOpenShk = PosInvOpenShk;
  pmot->_PosMaxClose = PosMaxClose;
  pmot->_PosMinOpen = PosMinOpen;
  pmot->_PosMinOpenShk = PosMinOpenShk;
  pmot->_PosNearClose = PosNearClose;
  pmot->_PosNearOpen = PosNearOpen;
  pmot->_PosNearOpenShk = PosNearOpenShk;
  pmot->_PosOpen = PosOpen;
  pmot->_PosOpenShk = PosOpenShk;
  pmot->_PosOffsetAO = PosOffsetAO;
  pmot->_PosOffsetAC = PosOffsetAC;
  pmot->_MovCount = MovCount;
  pmot->_bParReread = bParReread;
  pmot->_bEnExtProt = bEnExtProt;
  pmot->_bEmergency = bEmergency;
  pmot->_bPassiveMov = bPassiveMov;
  pmot->_bLowBatt = bLowBatt;
  //variabili di configurazione condivise con VS
  pmot->_BrakeOn = BrakeOnOff;
  pmot->_Motor = Motor;
  pmot->_SSCfg = SSCfg;
  pmot->_SPCfg = SPCfg;
  pmot->_STCfg = STCfg;
  pmot->_CloseOn = CloseOnOff;
}

VS_VOID CheckRun( VS_VOID  )
{
    if ( pm->Run != 0 ){
        SEQ_AddEvent(euRun,pm->Sys);
        EscCount = 0;
    }
}

VS_VOID CheckSP( VS_VOID  )
{
//  if ( pm->Run != 0 )
//  {
    if(pm->Mot==0)
    {
      if ((INGRESSI[SP1] == 1)||(INGRESSI[SP1_CB] == 1))
      {
        SEQ_AddEvent(eClosed,pm->Sys);
      }
      else
      {
        SEQ_AddEvent(eOpened,pm->Sys);
      }
    }

    else if(pm->Mot==1)
    {
      if ((INGRESSI[SP2] == 1)||(INGRESSI[SP2_CB] == 1))
      {
        SEQ_AddEvent(eClosed,pm->Sys);
      }
      else
      {
        SEQ_AddEvent(eOpened,pm->Sys);
      }
    }
//  }
}

VS_VOID CheckSS( VS_VOID  )
{
//  if ( pm->Run != 0 )
//  {
    if(pm->Mot==0)
    {
      if ((INGRESSI[SS1] == 1)||(INGRESSI[SS1_CB] == 1))
      {
        SEQ_AddEvent(eLocked,pm->Sys);
      }
      else
      {
        SEQ_AddEvent(eUnLocked,pm->Sys);
      }
    }

    else if(pm->Mot==1)
    {
      if ((INGRESSI[SS2] == 1)||(INGRESSI[SS2_CB] == 1))
      {
        SEQ_AddEvent(eLocked,pm->Sys);
      }
      else
      {
        SEQ_AddEvent(eUnLocked,pm->Sys);
      }
    }
//  }
}


VS_VOID ResLock( VS_VOID  )
{
   if(pm->Mot == 0){
      USCITE[REL_P1] = 1;
	   USCITE[REL_P1CB] = 1;
   }
   else if(pm->Mot == 1){
      USCITE[REL_P2] = 1;
	   USCITE[REL_P2CB] = 1;
   }
}

VS_VOID SetLock( VS_VOID  )
{
    if(pm->Mot == 0){
        USCITE[REL_P1] = 0;
        USCITE[REL_P1CB] = 0;
    }
    else if(pm->Mot == 1){
        USCITE[REL_P2] = 0;
        USCITE[REL_P2CB] = 0;
   }	
}

VS_VOID UnhookMot( VS_VOID  )
{
    if(pm->Mot == 0){
        USCITE[REL_P1] = 1;	
    }
    else if(pm->Mot == 1){
        USCITE[REL_P2] = 1;	
    }
}

VS_VOID HookupMot( VS_VOID  )
{
    if(pm->Mot == 0){
        USCITE[REL_P1] = 0;
    }
    else if(pm->Mot == 1){
        USCITE[REL_P2] = 0;	
   }	
}

VS_VOID FailMotNotify (VS_VOID)
{
   if(pm->Mot == 0){
	   USCITE[REL_P1CB] = 1;
   }
   else if(pm->Mot == 1){
	   USCITE[REL_P2CB] = 1;
   }
}

VS_VOID FailMotOff (VS_VOID)
{
   if(pm->Mot == 0){
      USCITE[REL_P1] = 0;
	   USCITE[REL_P1CB] = 0;
   }
   else if(pm->Mot == 1){
      USCITE[REL_P2] = 0;
	   USCITE[REL_P2CB] = 0;
   }
}
