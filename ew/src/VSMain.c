/*****************************************************************************
* IAR visualSTATE Main Loop Source File
*
* The file contains an implementation for a main loop using the visualSTATE
* basic API.
*
* The code uses a simple queue for storing events. The functions for inter-
* facing to the queue are described in the sample code file
* simpleEventHandler.h.
*****************************************************************************/

/* *** include directives *** */

#include "PLL_PEvent.h"
#include "VSMain.h"
#include "VSLogicMain.h"
#include "VSDoorsMain.h"
#include "inout.h"
#include "deviceSetup.h"
#include "usart.h"
#include "twi.h"                        //zuck

//#define SW_TIMER_BASE_COUNT (6 * FLASH_FACTOR)
//#define SW_TIMER_BASE_COUNT 1
//#define TIMER_STEP_MS 1024


//attenzione se cambia FLASH_FACTOR occorre correggere il valore

/* *** function declarations *** */

void RealLinkCommInit(void);
/* *** constant definitions *** */

const unsigned int SW_TIMER_BASE_COUNT[EVENT_QUEUE_SYSTEMS][SYSTEM_TIMERS] = {
                 /*  0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17   18   19   20   21   22   23   24	25*/
                    {1,100,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,   1,   1,   1,   1,   1,   1,   1,	 1},  //mot1
                    {1,100,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,   1,   1,   1,   1,   1,   1,   1,	 1},  //mot2
                    {1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,1000,   1,1000,   1,   1,   1,1000,	 1}}; //logica
/* *** variable definitions *** */

unsigned int Sys = 0;
unsigned int SysDisplay = 2;
unsigned int LastMsgId = 0;
unsigned int LastMsgIdSys[EVENT_QUEUE_SYSTEMS];// = 0;
VS_BOOL bDisDbg = 0;
unsigned int EscCount = 0;
unsigned int CycleCount = 0;
static unsigned int SW_timer_basecount[EVENT_QUEUE_SYSTEMS][SYSTEM_TIMERS];// = SW_TIMER_BASE_COUNT;
static unsigned int SW_timer_ticks[EVENT_QUEUE_SYSTEMS][SYSTEM_TIMERS];// = SW_TIMER_BASE_COUNT;
static unsigned char SW_timer_running[EVENT_QUEUE_SYSTEMS][SYSTEM_TIMERS]; // = 0;
static SEM_EVENT_TYPE SW_timer_event[EVENT_QUEUE_SYSTEMS][SYSTEM_TIMERS];

unsigned int CountTSession = 0, CountTVisEx = 0;

unsigned short TReadClk = 0;

unsigned int Cp[MOT_SYSTEMS];

extern unsigned int TIM_STEP;


/* Timer routines */

void TIMER_SW_init(void)
{
  unsigned int i = 0;
  unsigned int j = 0;
  for (; i<EVENT_QUEUE_SYSTEMS; i++)
  {
    for (j=0; j<SYSTEM_TIMERS; j++)
    {
      SW_timer_running[i][j] = 0;
      SW_timer_basecount[i][j] = SW_TIMER_BASE_COUNT[i][j];
    }
  }
}

void TIMER_SW_start(SEM_EVENT_TYPE event, unsigned int ticks, VS_UINT8 system, VS_UINT8 timerid)
{
  if ((system < EVENT_QUEUE_SYSTEMS)&&(timerid < SYSTEM_TIMERS))
    if (!SW_timer_running[system][timerid])
    {
      SW_timer_basecount[system][timerid] = SW_TIMER_BASE_COUNT[system][timerid];
      if  (ticks < 2)
      {
        SW_timer_ticks[system][timerid] = 2;
      }
      else     //Normalizzazione dei millisecondi con durata loop
      {
        if  ( ticks > (0xFFFFFFFF / TIM_STEP) )
          SW_timer_ticks[system][timerid] = (ticks / 1000) * TIM_STEP;
        else
          SW_timer_ticks[system][timerid] = (ticks * TIM_STEP) / 1000;

      }
      SW_timer_running[system][timerid] = 1;
      SW_timer_event[system][timerid] = event;

      if(system ==2 && timerid == 18 )                    //Leone: rende visibile all'esterno il valore del contatore
      CountTSession = SW_timer_ticks[system][timerid];   //Leone: del timer TSession (18)
      if(system ==2 && timerid == 24 )
      CountTVisEx = SW_timer_ticks[system][timerid];
    }
}

void TIMER_SW_stop(VS_UINT8 system, VS_UINT8 timerid)
{
  if ((system < EVENT_QUEUE_SYSTEMS)&&(timerid < SYSTEM_TIMERS))
    SW_timer_running[system][timerid] = 0;
  if(system ==2 && timerid == 18 )      //Leone
    CountTSession = 0;
  if(system ==2 && timerid == 24 )
      CountTVisEx = 0;
}

void TIMER_SW_tick(VS_UINT8 system, VS_UINT8 timerid)
{
  if ((system < EVENT_QUEUE_SYSTEMS)&&(timerid < SYSTEM_TIMERS))
    if (SW_timer_running[system][timerid])
    {
      if (!--SW_timer_basecount[system][timerid])
      {
        SW_timer_basecount[system][timerid] = SW_TIMER_BASE_COUNT[system][timerid];
        if (!--SW_timer_ticks[system][timerid])
        {
          SW_timer_running[system][timerid] = 0;
          SEQ_AddEvent(SW_timer_event[system][timerid],system);
        }
      }
    if(system ==2 && timerid == 18 )                      //Leone
      CountTSession = SW_timer_ticks[system][timerid];
    if(system ==2 && timerid == 24 )
      CountTVisEx = SW_timer_ticks[system][timerid];
    }
}

void HandleError(unsigned char cc)
{
  /* Oh, no - an error has occured during the deduction
     Turn on all the LEDs and stay in a loop !!!! */
  #define LOOP_VS_ERR 100000
  unsigned int Loops = LOOP_VS_ERR;


//fermare motori qui


  cc = cc;
  while (1)
  {
    if (Loops > 0)
    {
      Loops--;
    }
    else
    {
      Loops = LOOP_VS_ERR;


      //segnalare errore qui
      //setoutput( pm->Err );
      //setoutput( pm->Prg );


    }
  }
}

void main(void)
{
    unsigned char cc;

    VS_UINT8 j;
    SEM_CONTEXT *pDoorsContext;
    SEM_CONTEXT *pLogicContext;

    TIMER_SW_init();
    InitDevice();

    if ((cc=DoorsInit(&pDoorsContext))!= SES_OKAY)
        HandleError(cc);
    if ((cc=LogicInit(&pLogicContext))!= SES_OKAY)
        HandleError(cc);

	__enable_interrupt();
    
	ResetWatchdog();

	/* Initialize the VS Event queue. */
    SEQ_Initialize();
    /* Add SE_RESET to initialize system. */
    for (Sys=0; Sys<EVENT_QUEUE_SYSTEMS; Sys++)
        SEQ_AddEvent(SE_RESET,Sys);

    millisecondi(1500);

    Clk_Read();

    /* Do forever: */
    for (;;)
    {
#if (VS_REALLINKMODE == 1)
        VS_WAIT();
#endif

        ResetWatchdog();

        TReadClk++;

        if (TReadClk >500){
            TReadClk= 0;
            Clk_Read();
        }

        CycleCount++;
		

        if  (CycleCount < ((TIM_STEP*90)/100)){
            AT91F_PIO_ClearOutput(AT91C_BASE_PIOA, LEDRUN);
        }
        else{			
            AT91F_PIO_SetOutput(AT91C_BASE_PIOA, LEDRUN);
        }

        if  (CycleCount > TIM_STEP){
            CycleCount = 0;
        }

        for (Sys=0; Sys<EVENT_QUEUE_SYSTEMS; Sys++){
            for (j=0; j<SYSTEM_TIMERS; j++)
                TIMER_SW_tick(Sys, j);
            switch (Sys){
              case 0:
              case 1:
                if ((cc=DoorsMain(pDoorsContext))!= SES_OKAY)
                    HandleError(cc);
                break;
              case 2:
                if ((cc=LogicMain(pLogicContext))!= SES_OKAY)
                    HandleError(cc);
                break;
            }
        }
    }
}

