/*****************************************************************************
* IAR visualSTATE Main Loop Source File
*
* The file contains an implementation for a main loop using the visualSTATE
* basic API.
*
* The code uses a simple queue for storing events. The functions for inter-
* facing to the queue are described in the sample code file
* simpleEventHandler.h.
*****************************************************************************/

/* *** include directives *** */

#include "VSLogic.h"
#include "VSLogicData.h"
#include "VSLogicAction.h"

#include "VSLogicMain.h"
#include "VSLogicOutput.h"
#include "timer.h"
#include "usart.h"
#include "usart1.h"
#include "deviceSetup.h"
#include "inout.h"
#include "biometrico.h"
#include "twi.h"
#include <string.h>
#include <stdio.h>


#define MAX_DEBOUNCE 50
#define CMD_DEBUG
#define MAXDELAY  300
#define DISP_STRG_SIZE 17
#define ROW_STRG_SIZE 195

//#define PASSCOUNTMAX 10

/* *** function declarations *** */

/* *** variable definitions *** */

unsigned char catchON_OFF0 = MAX_DEBOUNCE;
unsigned char catchON_OFF1 = MAX_DEBOUNCE;
unsigned char catchON_OFFrem0 = MAX_DEBOUNCE;
unsigned char catchON_OFFrem1 = MAX_DEBOUNCE;
unsigned char catchMAN_AUT1 = MAX_DEBOUNCE;
unsigned char catchEnter0 = MAX_DEBOUNCE;
unsigned char catchEnter1 = MAX_DEBOUNCE;

unsigned char FlashOn=0;
unsigned char CfgDay;
unsigned char LedsConsoleVisible=0;
unsigned char NT200=0;
unsigned char SimulPc=0;
unsigned char SimulPcMoreFlag = 0;
unsigned char SimulPcObjectFlag = 0;



char catchON_OFF = 1;
char catchON_OFF_TOGGLE = 0;
char catchMAN_AUT = 1;
char catchSEL_DIRp = 1;
char catchCOM_MANOUT = 1;
char catchCOM_MANIN = 1;
char catchCOM_MANBOTH = 1;
char catchINIB_METp = 1;
short catchINIB_METp0 = MAXDELAY;
short catchINIB_METp1 = MAXDELAY;
char catchRST_MDp = 1;
char catchSEL_PERS_ENT = 1;
char catchAUTO_GST_F1 = 1;
char catchCONF_F2 = 1;
char catchSEL_UUEM_F3 = 1;
char catchEMERGp = 1;

char catchATR = 1;
char catchUUSC = 1;
char catchPRING = 1;
char catchPULS1 = 1;
char catchPULS2 = 1;
char catchMETAL = 1;
char catchMORE = 1;
char catchOBJECT = 1;
char catchPCON  = 1;
char catchON_OFFrem = 1;
char catchAMANrem = 1;
char catchRESrem = 1;
char catchEMERGrem = 1;
char catchP1rem = 1;
char catchP2rem = 1;
char catchANTI_BT1 = 1;
char catchANTI_BT2 = 1;
char catchPLAY_MSG  = 1;
char catchREC_MSG  = 1;
char catchINT_SPI = 1;
char catchPROX = 1;
char catchRETE = 1;
char catchBLOW = 1;
char catchVIP = 1;
char catchPC1 = 1;
char catchPC2 = 1;
char catchPA1 = 1;
char catchPA2 = 1;
char catchVIOLP1 = 1;
char catchVIOLP2 = 1;
char catchSERVICE = 1;
char catchSERVICE0 = MAX_DEBOUNCE;
char catchSERVICE1 = MAX_DEBOUNCE;


char catchBIOOK = 1;
char catchBIONOK = 1;
char catchBIOAMP = 1;
char catchFING_IN = 1;


char catchT200 = 1;

char Riga1[18],Riga2[18];

// -----------------------------------------------------------------------------

// Ingressi da Connection Box

// -----------------------------------------------------------------------------

char catchPROX_NORM1 = 1;
char catchPROX_NORM2 = 1;
char catchPROX_DIS1 = 1;
char catchPROX_DIS2 = 1;
char catchBioOkCB = 1;
char catchBioNotOkCB = 1;
char catchFIRE = 1;
char catchROT_EMERG = 1;
#ifdef CLEAR_ID_DEBUG 
char catchSW_PA3 = 1;
#else
char catchSW_AF_LB_ST = 1;
#endif

extern unsigned char BcdBuff[];
extern unsigned short TReadClk;
extern unsigned char SecPLL[], MinPLL[], HourPLL[], DayPLL[], DatePLL[];
extern unsigned char MonthPLL[], YearPLL[], DayName[];
extern volatile unsigned char SysTimeCount;
extern unsigned int  CountTVisEx ;
/* *** constant definitions *** */
const char StrgMask[3][DISP_STRG_SIZE] = {
	"          :  :  ",		//  0
	"                ",		//  1
	"       :        ",		//  2	
	
};
const char *pDispMask =  StrgMask[0];

const char StrgIta[ROW_STRG_SIZE][DISP_STRG_SIZE] = {  	
	"Auto    Ingresso",		//  0
	"P.interna aperta",     //  1
	"P.esterna aperta",     //  2
	"Pers. in  tunnel",     //  3
	"  Controllo +P  ",     //  4
	"   Allarme +P   ",     //  5
	"Pers.in ingresso",     //  6
	"Auto      Uscita",     //  7
	" Pers.in uscita ",     //  8
	" Allarme  metal ",     //  9
	"Tunnel  occupato",     // 10
	" Tunnel  libero ",     // 11
	"Verifica Oggetto",     // 12
	"Allarme  Oggetto",     // 13
	"Controllo  biom.",     // 14
	"  in  ingresso  ",     // 15
	"    negativo    ",     // 16
	" Fine transito  ",     // 17
	"Mancato transito",     // 18
	"   in  uscita   ",     // 19
	"Manuale Ingresso",     // 20
	"Manuale   Uscita",     // 21
	"  Porte chiuse  ",     // 22
	"  Porte aperte  ",     // 23
	"  in emergenza  ",     // 24
	"     Reset      ",     // 25
	"    impianto    ",     // 26
	" Pausa prereset ",     // 27
	"Notte           ",     // 28
	"     Alluser    ",     // 29
	"  Industrie PD  ",     // 30
#ifdef AM_KEY_VER
	"PLL v.2.34.5-AM ",     // 31
#else
    "PLL v.2.34.5-PT ",     // 31
#endif
	"    Opzioni     ",     // 32
	"  Pulsante F1   ",     // 33
	"    Nessuna     ",     // 34
	"   Biometrico   ",     // 35
	"   Emergenza    ",     // 36
	"  Solo uscita   ",     // 37
	"     Metal      ",     // 38
	"    + Persone   ",     // 39
	"  Pulsante F2   ",     // 40
	"  Pulsante F3   ",     // 41	
	"  Tipo Tunnel   ",     // 42
	" Bidirezionale  ",     // 43
	" Solo Ingresso  ",     // 44
	"  Solo Uscita   ",     // 45
	"  Porte Aperte: ",     // 46
	"    Nessuna     ",     // 47
	" Solo Porta Ext.",     // 48
	" Porta Interna  ",     // 49
	"Chiusa di Notte ",     // 50
	"Aperta di Notte ",     // 51
	" +P in Ingresso ",     // 52
	"   Abilitato    ",     // 53
	"  Disabilitato  ",     // 54
	"  +P in Uscita  ",     // 55
	"Metal in pr.ing.",     // 56
	"Ult.uscita in em",     // 57
	"  Disabilitata  ",     // 58
	"Bloc. in pr.ing.",     // 59
	"      Anno      ",     // 60
	"      Mese      ",     // 61
	" Giorno  (1-31) ",     // 62
	"Giorno (Lun-Dom)",     // 63
	"      Ore       ",     // 64
	"     Minuti     ",     // 65
	"    Secondi     ",     // 66
	"  Inizio Notte  ",     // 67
	" Inizio Mattino ",     // 68
	"   Numero CAM   ",     // 69
	" L M M G V S D  ",     // 70
	"Biometrico ingr.",     // 71
	"    Assente     ",     // 72
	"Cambio pers. ing",     // 73
	"Cambio pers. usc",     // 74
	" PC  Biometrico ",     // 75
	"     Finger     ",     // 76
	"      Face      ",     // 77
	" Finger + Face  ",     // 78
	" Lampada accesa ",     // 79
	"   Abilitata    ",     // 80
	"Verifica  Autom.",     // 81
	"Verif. Ogg. Usc.",     // 82
	" Funzione Posta ",     // 83
	" Usc.automatica ",     // 84
	"     acceso     ",     // 85
	"     spento     ",     // 86
	"     on-line    ",     // 87
	"    off-line    ",     // 88
	" in accensione  ",     // 89
	" in spegnimento ",     // 90
	"     P1 P2      ",     // 91
	"      P1        ",     // 92
	"      P2        ",     // 93
	"    Funzione    ",     // 94
	"non  disponibile",     // 95
	"Auto    Solo Usc",		// 96
	"Auto    Solo Ing",		// 97
	"             Off",		// 98
	"Auto      /  /  ",		// 99
	"Batteria bassa  ",		//100
	"Assenza rete 230",		//101
	"PC Bio  off-line",		//102
	"Manuale Solo Usc",		//103
	"Manuale Solo Ing",		//104
	"Manuale   /  /  ",		//105
	"Mattino   /  /  ",		//106
	"Notte     /  /  ",		//107
	"DomLunMarMerGioE",		//108
	"VenSab+PBiomMetr",		//109
	"      Menu      ",		//110
	" Pulsanti Funz. ",		//111
	"     Tunnel     ",		//112
	"  Modo Transito ",		//113
	"    Data/Ora    ",		//114
	"     Lingua:    ",		//115
	"    Italiano    ",		//116
	"     Inglese    ",		//117
	"      Data      ",		//118
	"      Ora       ",		//119
	"Biometrico usc. ",     //120
	" Connection Box ",		//121
	"Tempo Espulsione",		//122
	"     15 sec     ",		//123
	"     45 sec     ",		//124
	" Solo Porta Int.",		//125
	"Durata Reset APD",		//126
	"      3 sec     ",		//127
	"      6 sec     ",		//128
	"Intra Reset APD ",		//129
	"     15 min     ",		//130
	"     45 min     ",		//131
	"    240 min     ",		//132
	" Mem. Rich. Tr. ",		//133
	"Abilitata 1 Liv ",		//134
	"Abilitata 2 Liv ",		//135
	"Prox in ingresso",		//136
	" Prox in uscita ",		//137	
	"+P in primo ing.",     //138
	"Biom. ingr. Dis.",     //139
	"Biom. usc.  Dis.",     //140	
	"     Nessun     ",		//141
	"   messaggio    ",		//142
	"Programmazione: ",		//143
	"   EXT + INT    ",		//144
	" Tappeto 2 Zone ",		//145
	"Puls. Antipanico",		//146
	"     Backup     ",		//147
	"     Restore    ",		//148	
	"    Eseguito    ",		//149
	"     Esegui     ",		//150
	"Reset APD Notte ",		//151
    "  Service Mode  ",     //152
    "      1 min     ",     //153
    "      2 min     ",     //154
    "      3 min     ",     //155
    "      4 min     ",     //156
    "      5 min     ",     //157
    "CambPersContrAcc",     //158
    "     Riavvio    ",     //159
    "Start Bio P.Open",     //160
    " Mem Cambio Prox",     //161
    "TransitOk P.Open",     //162
    "  Stesso Stato  ",     //163
    "      Notte     ",     //164
    "Verif. Ogg. Ing.",     //165
    "FireAlarm Notte ",     //166
    "Anti Blocco Pers",     //167
    "Visualizza Data ",     //168
    "    GG/MM/AA    ",     //169
    "    MM/GG/AA    ",     //170
    " INPUT CN17.10  ",     //171
    " INPUT CN17.11  ",     //172
    "   P-EXT-MAN    ",     //173
    "   P-INT-MAN    ",     //174
    "    AntiSF 1    ",     //175
    "    AntiSF 2    ",     //176
    "Controllo Visivo",     //177
    "Tempo Contr.Vis.",     //178
    "Ready X Rich.Tr.",		//179
    "Alarm Sabot.Ante",     //180
    "ScadPresP.Aperta",     //181
    "OUTPUTS CN9.3-4 ",     //182
    "LED Semaforo Aux",     //183
    " Transito OK MB ",     //184	
	"ChiusuraPortePer",		//185
	"DurataVerif.Ogg.",		//186
	"Prove Verif.Ogg.",		//187
	" Ass.Rete+Batt. ",		//188
	" Ante Bloccate  ",		//189
	"Non in Emergenza",		//190
	"Emergenza SoloON",		//191
	"Emergenza Sempre",		//192
	"Stare in Bussola",		//193
	"Uscire daBussola",		//194
	
};

const char StrgEng[ROW_STRG_SIZE][DISP_STRG_SIZE] = {  	
	"Auto Entry Now  ",     //  0
	"INT Door2 Open  ",     //  1
	"EXT Door1 Open  ",     //  2
	"Person Inside   ",     //  3
	" A.P.D. Control ",     //  4
	"  A.P.D. Alarm  ",     //  5
	"   Person In    ",     //  6
	" Auto Exit Now  ",     //  7
	"   Person Out   ",     //  8
	"   M.D. Alarm   ",     //  9
	" Person Inside  ",     // 10
	" Portal  Empty  ",     // 11
	"Object Detection",     // 12
	"Object Detected ",     // 13
	"   Biom. Check  ",     // 14
	"     Way In     ",     // 15
	"    Negative    ",     // 16
	"Access Completed",     // 17
	"Access Aborted  ",     // 18
	"     Way Out    ",     // 19
	"Manual Entry Now",     // 20
	"Manual Exit Now ",     // 21
	"All Doors Closed",     // 22
	"All Doors Open  ",     // 23
	" Emergency Mode ",     // 24
	"  Reset Cycle   ",     // 25
	"     System     ",     // 26
	"Pre-reset Pause ",     // 27
	"Stand by        ",     // 28
	"     Alluser    ",     // 29
	"  Industrie PD  ",     // 30
#ifdef AM_KEY_VER
	"PLL v.2.34.5-AM ",     // 31
#else
    "PLL v.2.34.5-PT ",     // 31
#endif
	"    Settings    ",     // 32
	" Push Button F1 ",     // 33
	"      None      ",     // 34
	"on/off Biometric",     // 35
	" Emergency Mode ",     // 36
	"  Way Out Only  ",     // 37
	"   On/Off M.D.  ",     // 38
	"  A.P.D. Check  ",     // 39
	" Push Button F2 ",     // 40
	" Push Button F3 ",     // 41	
	"  Access Type   ",     // 42
	"Way In / Way Out",     // 43
	"  Way In Only   ",     // 44
	"  Way Out Only  ",     // 45
	"OpenDoors PwrOn:",     // 46
	"     None       ",     // 47
	" Only EXT Door1 ",     // 48
	"   INT Door2    ",     // 49
	"Close In StandBy",     // 50
	"Open In StandBy ",     // 51
	"A.P.D. On WayIn ",     // 52
	"       ON       ",     // 53
	"       OFF      ",     // 54
	"A.P.D. On WayOut",     // 55
	" M.D. 1st Entry ",     // 56
	" Last Exit E.M. ",     // 57
	"       OFF      ",     // 58
	" 1st Entry Lock ",     // 59
	"      Year      ",     // 60
	"      Month     ",     // 61
	"   Day  (1-31)  ",     // 62
	"  Day (Mon-Sun) ",     // 63
	"     Hours      ",     // 64
	"     Minutes    ",     // 65
	"     Seconds    ",     // 66
	" StandBy Start  ",     // 67
	" PowerOn Start  ",     // 68
	"  CAM Number    ",     // 69
	" M T W T F S S  ",     // 70
	"Biometric Way In",     // 71
	" Not Installed  ",     // 72
	"AntiCrossoverIn ",     // 73
	"AntiCrossoverOut",     // 74
	" Biometric PC   ",     // 75
	"  Finger Only   ",     // 76
	"   Face Only    ",     // 77
	" Finger + Face  ",     // 78
	"Internal LightOn",     // 79
	"       ON       ",     // 80
	"ObjDtc MD Alarm ",     // 81
	"ObjectDtc WayOut",     // 82
	"    Postman     ",     // 83
	"  Auto Way Out  ",     // 84
	"   Turned On    ",     // 85
	"   Turned Off   ",     // 86
	"     On-Line    ",     // 87
	"    Off-Line    ",     // 88
	"   Turning On   ",     // 89
	"   Turning Off  ",     // 90
	"   Both Doors   ",     // 91
	"EXT Door1 Only  ",     // 92
	"INT Door2 Only  ",     // 93
	"    Feature     ",     // 94
	" Not Avalaible  ",     // 95
	"Auto WayOut Only",     // 96
	"Auto WayIn Only ",		// 97
	"             Off",		// 98
	"Auto      /  /  ",		// 99
	"  Low Battery!  ",		//100
	"Battery Backup! ",		//101
	"PC Bio  off-line",		//102
	"Man. WayOut Only",		//103
	"Man.  WayIn Only",		//104
	"Manual    /  /  ",		//105
	"PowerON   /  /  ",		//106
	"StandBy   /  /  ",		//107
	"SunMonTueWedThuE",		//108
	"FriSatAPDBioMD r",		//109
	"      Menu      ",		//110
	"  Key Function  ",		//111
	"     Portal     ",		//112
	"  Access Mode   ",		//113
	"    Date/Time   ",		//114
	"     Language:  ",		//115
	"     Italian    ",		//116
	"     English    ",		//117
	"      Date      ",		//118
	"      Time      ",		//119
	"BiometricWay Out",  	//120
	" Connection Box ",		//121
	"    Time Out    ",		//122
	"   15 seconds   ",		//123
	"   45 seconds   ",		//124
	" Only INT Door2 ",		//125
	" APD Reset Time ",		//126
	"    3 seconds   ",		//127
	"    6 seconds   ",		//128
	"APD Reset Freq. ",		//129
	"   15 minutes   ",		//130
	"   45 minutes   ",		//131
	"  240 minutes   ",		//132
	"AccessRequestRec",		//133
	" 1�Level Enable ",		//134
	" 2�Level Enable ",		//135
	"A.C.Mode Way In ",		//136
	"A.C.Mode Way Out",		//137	
	"A.P.D. 1st Entry",     //138
	"Biom Way In VIP ",  	//139
	"Biom Way Out VIP",     //140	
	"       No       ",		//141
	"    Message     ",		//142
	"  To Program    ",		//143
	"   EXT + INT    ",		//144
	"2 Zones Floormat",		//145
	" Panic Buttton  ",		//146	
	"     Backup     ",		//147
	"     Restore    ",		//148
	"      Done      ",		//149
	"      To Do     ",		//150 	
	"APD Reset Night ",		//151	
    "  Service Mode  ",     //152
    "    1 minute    ",     //153
    "    2 minutes   ",     //154
    "    3 minutes   ",     //155
    "    4 minutes   ",     //156
    "    5 minutes   ",     //157
    "AccContCrossover",     //158
    "MirrorPrePWRFail",     //159
    "DoorOpenBioCheck",     //160
    "CrossoverProxReq",     //161
    "D-Open TransitOk",     //162
    "       ON       ",     //163
    "       OFF      ",     //164
    "ObjectDtc Way In",     //165
    "StandByFireAlarm",     //166
    " Anti-Trap Mode ",     //167
    "   View Date    ",     //168
    "    DD/MM/YY    ",     //169
    "    MM/DD/YY    ",     //170
    " INPUT CN17.10  ",     //171
    " INPUT CN17.11  ",     //172
    "   P-EXT-MAN    ",     //173
    "   P-INT-MAN    ",     //174
    "    AntiBT 1    ",     //175
    "    AntiBT 2    ",     //176
    "  Visual  Exam. ",     //177
    "Visual Exam.Time",     //178
    "ReadyForAcc.Req.",		//179
    "DoorSabotagAlarm",     //180
    "TOutPresOpenDoor",     //181
    "OUTPUTS CN9.3-4 ",     //182
    "Intern.LED Signs",     //183
    " MB Transit OK  ",     //184	
	"DoorsClosing For",		//185
	"ObjDetecTimeSpan",		//186
	"ObjDetec. Checks",		//187
	"Power&BatteryOff",		//188
	"  Doors Locked  ",		//189
	"Not in Emergency",		//190
	"Emergency OnlyON",		//191
	"Emergency Always",		//192
	"Remain in Portal",		//193
	"Exit the Portal ",		//194
};

char *pDispStrg = (char*) StrgIta[0];


/* *** function definitions *** */

VS_VOID DisplayStatus(VS_VOID)
{
  if (DisTempCnt>0)
  {
    DisTempCnt--;
    if (DisTempCnt==0)
      LastMsgIdSys[Sys] = LogicMsgId;
  }
  if (DisplayMode == DISPLAY_MODE_DEBUG)
  {
    #ifdef MULTIPLE_LINE_LOG
      US_SendCharDbg((AT91PS_USART)DBGU_pt, 10);
    #endif
    US_SendCharDbg((AT91PS_USART)DBGU_pt, 13);
    DisplayValue(0, 45);    //'-'
  }
  else
  {
    LogicDisplay(LastMsgIdSys[Sys]);
    if (Sys==SysDisplay)
      LastMsgId = LastMsgIdSys[Sys];
    DisplayValue2(MaxUsersIn, 32);
    DisplayValue2(UsersIn, 32);
  }
}


void LogicManageInputs()
{
	//#define CLEAR_ID_DEBUG
#ifdef CLEAR_ID_DEBUG //cancella locazione EEPROM dove risiede l'ID
    unsigned char DummyIDKey[7]={0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};  //leone debug
#endif	
	// Debounce pulsante ON/OFF da consolle o remoto 
	if (( ON_OFFp != 0 ) || (INGRESSI[ON_OFFrem] != 0 ) ){
		catchON_OFF0 = MAX_DEBOUNCE;
		catchON_OFF1--;
	}
	else{
		catchON_OFF0--;
		catchON_OFF1=MAX_DEBOUNCE;
	}	
	
    if (catchON_OFF1 == 0)
    {
        catchON_OFF0=MAX_DEBOUNCE;
        catchON_OFF1=MAX_DEBOUNCE;
        if ((catchON_OFF_TOGGLE == 0) && (catchON_OFF) && (TOn == 0))
        {
            SEQ_AddEvent(eOnOff,Sys);
            catchON_OFF_TOGGLE = 1;
            catchON_OFF = 0;
        }
		
        if ((catchON_OFF_TOGGLE == 1) && (catchON_OFF) && (TOff == 0))
        {
            SEQ_AddEvent(eOnOff,Sys);
            catchON_OFF_TOGGLE = 0;
            catchON_OFF = 0;
        }
		
    }
	
    if (catchON_OFF0==0)
    {
        TOn = 50;
        TOff = 30;
        catchON_OFF0=MAX_DEBOUNCE;
        catchON_OFF1=MAX_DEBOUNCE;
        catchON_OFF = 1;
    }
	
    //  Debounce pulsante MAN_AUT
    if (MAN_AUTp != 0 )
    {
        catchMAN_AUT1--;
        if (catchMAN_AUT1==0)
        {
            catchMAN_AUT1=MAX_DEBOUNCE;
            if (catchMAN_AUT==0)
            {
                SEQ_AddEvent(eAuto,Sys);
                catchMAN_AUT=1;
            }
        }
    }
    else
    {
        catchMAN_AUT1=MAX_DEBOUNCE;
        catchMAN_AUT=0;
    }
	
    //  Debounce pulsante DIR da Consolle senza display
	
    if ( SEL_DIRp == 0 )
    {
        if (catchSEL_DIRp == 0)
        {
            catchSEL_DIRp = 1;
        }
    }
    else
    {
        if (catchSEL_DIRp == 1)
        {
            SEQ_AddEvent(eMode,Sys);
            catchSEL_DIRp = 0;
        }
    }
	
    //  Debounce pulsante COM_MANOUT
    if ( COM_MANOUTp == 0 )
    {
        if (catchCOM_MANOUT == 0)
        {
            catchCOM_MANOUT = 1;
        }
    }
    else
    {
        if (catchCOM_MANOUT == 1)
        {
            SEQ_AddEvent(eMBtn2,Sys);
            catchCOM_MANOUT = 0;
        }
    }
	
    //  Debounce pulsante COM_MANIN
    if ( COM_MANINp == 0 )
    {
        if (catchCOM_MANIN == 0)
        {
            catchCOM_MANIN = 1;
        }
    }
    else
    {
        if (catchCOM_MANIN == 1)
        {
            SEQ_AddEvent(eMBtn1,Sys);
            catchCOM_MANIN = 0;
        }
    }
	
    //  Debounce pulsante MAN_IN + MAN_OUT
    if ( (COM_MANINp == 0) && (COM_MANOUTp == 0) )
    {
        if (catchCOM_MANBOTH == 0)
        {
            catchCOM_MANBOTH = 1;
        }
    }
    else if((COM_MANINp == 1) && (COM_MANOUTp == 1))
    {
        if (catchCOM_MANBOTH == 1)
        {
            bDisDbg = 1;
            SysDisplay = 2;
            SEQ_AddEvent(eToProgOn,Sys);
            catchCOM_MANBOTH = 0;
        }
    }
	
    //  Debounce chiave AB_MET
    if ( INIB_METp == 0 )
    {
        catchINIB_METp1 = MAXDELAY;
        if (catchINIB_METp0 >0)
        {
            catchINIB_METp0--;
        }
        else
        {
            if (catchINIB_METp == 0)
            {
                bDisDbg = 0;
                SysDisplay = 2;
                SEQ_AddEvent(eMetalOn,Sys);
                catchINIB_METp = 1;
            }
        }
    }
    else
    {
        catchINIB_METp0 = MAXDELAY;
        if (catchINIB_METp1 >0)
        {
            catchINIB_METp1--;
        }
        else
        {
            if (catchINIB_METp == 1)
            {
                SEQ_AddEvent(eMetalOff,Sys);
                catchINIB_METp = 0;
            }
        }
    }
	
    //  Debounce reset RST_MDp
    if ( RST_MDp == 0 )
    {
        if (catchRST_MDp == 0)
        {
            catchRST_MDp = 1;
        }
    }
    else
    {
        if (catchRST_MDp == 1)
        {
            SEQ_AddEvent(eReset,Sys);
            catchRST_MDp = 0;
        }
    }
	
    //  Debounce ENTER
    if (EnablePCBioOnOff == 1)
    {
        if ( SEL_PERS_ENT != 0 )
        {
            catchEnter0=MAX_DEBOUNCE;
            catchEnter1--;
        }
        else
        {
            catchEnter0--;
            catchEnter1=MAX_DEBOUNCE;
        }
		
        if (catchEnter1==0)
        {
            catchEnter0=MAX_DEBOUNCE;
            catchEnter1=MAX_DEBOUNCE;
			
            if ((catchSEL_PERS_ENT) && (TBioOn == 0))
            {
                SEQ_AddEvent(ePCBioStart,Sys);
                catchSEL_PERS_ENT = 0;
            }
        }
		
        if (catchEnter0==0)
        {
            TBioOn  = 40;
            catchEnter0=MAX_DEBOUNCE;
            catchEnter1=MAX_DEBOUNCE;
			
            if (catchSEL_PERS_ENT==0)
            {
                SEQ_AddEvent(ePCBioStop,Sys);
                catchSEL_PERS_ENT = 1;
            }
        }
    }
    else
    {
        if ( SEL_PERS_ENT != 0 )
        {
            catchEnter0=MAX_DEBOUNCE;
            catchEnter1--;
        }
        else
        {
            catchEnter0--;
            catchEnter1=MAX_DEBOUNCE;
        }
		
        if (catchEnter1==0)
        {
            catchEnter0=MAX_DEBOUNCE;
            catchEnter1=MAX_DEBOUNCE;
            if (catchSEL_PERS_ENT == 0)
            {
                SEQ_AddEvent(eEnter,Sys);
                catchSEL_PERS_ENT = 1;
            }
        }
		
        if (catchEnter0==0)
        {
            catchEnter0=MAX_DEBOUNCE;
            catchEnter1=MAX_DEBOUNCE;
            if (catchSEL_PERS_ENT == 1)
            {
                catchSEL_PERS_ENT = 0;
            }
        }
    }
	
    //  NoDebounce pulsante funzione F1
    if ( AUTO_GST_F1 == 0 )
    {
        if (catchAUTO_GST_F1 == 0)
        {
            catchAUTO_GST_F1 = 1;
        }
    }
    else
    {
        if (catchAUTO_GST_F1 == 1)
        {
            SEQ_AddEvent(eF1,Sys);
            catchAUTO_GST_F1 = 0;
        }
    }
	
    //  NoDebounce pulsante funzione F2
    if ( CONF_F2 == 0 )
    {
        if (catchCONF_F2 == 0)
        {
            catchCONF_F2 = 1;
        }
    }
    else
    {
        if (catchCONF_F2 == 1)
        {
            SEQ_AddEvent(eF2,Sys);
            catchCONF_F2 = 0;
        }
    }
	
    //  NoDebounce pulsante funzione F3
    if ( SEL_UUEM_F3 == 0 )
    {
        if (catchSEL_UUEM_F3 == 0)
        {
            catchSEL_UUEM_F3 = 1;
        }
    }
    else
    {
        if (catchSEL_UUEM_F3 == 1)
        {
            SEQ_AddEvent(eF3,Sys);
            catchSEL_UUEM_F3 = 0;
        }
    }
	
    //  NoDebounce EMERGp
    if ( EMERGp == 0 )
    {
        if (catchEMERGp == 0)
        {
            SEQ_AddEvent(eNormal,Sys);
            catchEMERGp = 1;
        }
    }
    else
    {
        if (catchEMERGp == 1)
        {
            SEQ_AddEvent(eEmergency,Sys);
            catchEMERGp = 0;
        }
    }
	
    //  NoDebounce FIRE
    if ( INGRESSI[FIRE] == 0 )
    {
        if (catchFIRE == 0)
        {
            SEQ_AddEvent(eNormal,Sys);
            catchFIRE = 1;
        }
    }
    else
    {
        if (catchFIRE == 1)
        {
            SEQ_AddEvent(eFireAlm,Sys);
            catchFIRE = 0;
        }
    }
	
    //  NoDebounce ATR
    if(SimulPc==0)    //leone
    {
        if ( INGRESSI[ATR] == 0 )
        {
            USCITE[ATR_CB] = 0;
            if (catchATR == 0)
            {
                SEQ_AddEvent(eNotPresent,Sys);
                catchATR = 1;
            }
        }
        else
        {
            USCITE[ATR_CB] = 1;
            if (catchATR == 1)
            {
                SEQ_AddEvent(ePresent,Sys);
                catchATR = 0;
            }
        }
    }
    //  NoDebounce RETE
    if ( INGRESSI[RETE] == 0 ){
		
        if (catchRETE == 0){
            SEQ_AddEvent(eVacOff,Sys);
            catchRETE = 1;
        }
    }
    else{
        if (catchRETE == 1){
            SEQ_AddEvent(eVacOn,Sys);
            catchRETE = 0;
        }
    }
	
	
    //Gestione Uscita RETE_CB con Segnalazione Anta violata
    if(ViolateNtfy == 1){
        if (   USCITE[VIOLP1]==1 && USCITE[VIOLP2]==1 && INGRESSI[RETE]==1 ){
			
            USCITE[RETE_CB] = 1;
        }
        else{
            USCITE[RETE_CB] = 0;
        }
    }
    else{
        if (INGRESSI[RETE]==1 ){
			
            USCITE[RETE_CB] = 1;
        }
        else{
            USCITE[RETE_CB] = 0;
        }
    }
	
    //  NoDebounce BATTLOW
    if ( INGRESSI[BLOW] == 0 ){
        if (catchBLOW == 0){
            SEQ_AddEvent(eBattLow,Sys);
            catchBLOW = 1;
        }
    }
    else{
        if (catchBLOW == 1){
            catchBLOW = 0;
        }
    }
	
    //  NoDebounce ultima uscita UUSC
    if ( INGRESSI[UUSC] == 0 )
    {
        if (catchUUSC == 0)
        {
            catchUUSC = 1;
        }
    }
    else
    {
        if (catchUUSC == 1)
        {
            SEQ_AddEvent(eLastOut,Sys);
            catchUUSC = 0;
        }
    }
	
    //  NoDebounce primo ingresso PRING
    if ( INGRESSI[PRING] == 0 )
    {
        if (catchPRING == 0)
        {
            catchPRING = 1;
        }
    }
    else
    {
        if (catchPRING == 1)
        {
            SEQ_AddEvent(eFirstIn,Sys);
            catchPRING = 0;
        }
    }
	
    //  NoDebounce pulsante esterno PULS1
    if ( INGRESSI[PULS1] == 0 )
    {
        if (catchPULS1 == 0)
        {
            catchPULS1 = 1;
            //      D_SUOp = 0;
        }
    }
    else
    {
        if (catchPULS1 == 1)
        {
            SEQ_AddEvent(eBtn1,Sys);
            catchPULS1 = 0;
            /*
            if(D_MANUp == 1)
            {
            D_SUOp = 1;
        }
            */
        }
		
    }
	
    //  NoDebounce pulsante esterno PULS2
    if ( INGRESSI[PULS2] == 0 )
    {
        if (catchPULS2 == 0)
        {
            catchPULS2 = 1;
        }
    }
    else
    {
        if (catchPULS2 == 1)
        {
            SEQ_AddEvent(eBtn2,Sys);
            catchPULS2 = 0;
        }
    }
	
    //  NoDebounce METAL
    if(SimulPc == 0){
        if ( INGRESSI[METAL] == 0 )
        {
            if (catchMETAL == 0)
            {
                catchMETAL = 1;
            }
        }
        else
        {
            if (catchMETAL == 1)
            {
                SEQ_AddEvent(eMetal,Sys);
                catchMETAL = 0;
            }
        }
    }
	
    //  NoDebounce MORE
    if(SimulPc==0){   //leone  :Disabilita test segnale d'ingresso quando simulo da PC
        if ( INGRESSI[MORE] == 0 )
        {
            if (catchMORE == 0)
            {
                SEQ_AddEvent(eNotMore,Sys);
                catchMORE = 1;
            }
        }
        else
        {
            if (catchMORE == 1)
            {
                SEQ_AddEvent(eMore,Sys);
                catchMORE = 0;
            }
        }
    }
    //  NoDebounce OBJECT
    if(SimulPc==0){   //leone  :Disabilita test segnale d'ingresso quando simulo da PC
        if ( INGRESSI[OBJECT] == 0 )
        {
            if (catchOBJECT == 0)
            {
                catchOBJECT = 1;
            }
        }
        else
        {
            if (catchOBJECT == 1)
            {
                SEQ_AddEvent(eObject,Sys);
                catchOBJECT = 0;
            }
        }
    }
    //  NoDebounce PCON
	
    if ( INGRESSI[PCON] == 0 )
    {
        if (catchPCON == 0)
        {
            SEQ_AddEvent(ePCBioOff,Sys);
            catchPCON = 1;
        }
    }
    else
    {
        if (catchPCON == 1)
        {
            SEQ_AddEvent(ePCBioOn,Sys);
            catchPCON = 0;
        }
    }
	
    //  Debounce pulsante MAN_AUT remOTO
    if (INGRESSI[AMANrem] == 0 ){
        if (catchAMANrem == 0){			
            catchAMANrem = 1;
        }
    }
    else{
        if (catchAMANrem == 1){			
			SEQ_AddEvent(eAuto,Sys);					
            catchAMANrem = 0;
        }
    }
	
    //  Debounce pulsante RESet remOTO
    if (INGRESSI[RESrem] == 0 )
    {
        if (catchRESrem == 0)
        {
            catchRESrem = 1;
        }
    }
    else
    {
        if (catchRESrem == 1)
        {
            SEQ_AddEvent(eReset,Sys);
            catchRESrem = 0;
        }
    }
	
    //  Eventi da emergenza remota
    if ( INGRESSI[EMERGrem] == 0 ){
        if (catchEMERGrem == 0){
            SEQ_AddEvent(eNormal,Sys);
            catchEMERGrem = 1;
        }
    }
    else{
        if (catchEMERGrem == 1){
            SEQ_AddEvent(eEmergency,Sys);
            catchEMERGrem = 0;
        }
    }
	
    //  Eventi da comando P1 remoto
    if (( INGRESSI[P1rem] == 0) && (ABT1==0)){
        if (catchP1rem == 0){
            catchP1rem = 1;
        }
    }
    else if (( INGRESSI[P1rem] == 1) && (ABT1==0)){
        if (catchP1rem == 1){
            SEQ_AddEvent(eMBtn1,Sys);
            catchP1rem = 0;
        }
    }
	
    //  Eventi da comando P2 remoto
    if ( (INGRESSI[P2rem] == 0) && (ABT2==0) ){
        if (catchP2rem == 0){
            catchP2rem = 1;
        }
    }
    else if ( (INGRESSI[P2rem] == 1) && (ABT2==0) ){
        if (catchP2rem == 1){
            SEQ_AddEvent(eMBtn2,Sys);
            catchP2rem = 0;
        }
    }
	
    //  Debounce Sens Antisfondamento Porta 1
    if ((INGRESSI[ANTI_BT1] == 0) && (ABT1==1)){
        if (catchANTI_BT1 == 0){
            SEQ_AddEvent(eABT1Off,Sys);
            catchANTI_BT1 = 1;
        }
    }
    else if ((INGRESSI[ANTI_BT1] == 1) && (ABT1==1)){
        if (catchANTI_BT1 == 1){
            SEQ_AddEvent(eABT1On,Sys);
            catchANTI_BT1 = 0;
        }
    }
	
    //  Debounce Sens Antisfondamento Porta 2
    if ((INGRESSI[ANTI_BT2] == 0) && (ABT2==1)){
        if (catchANTI_BT2 == 0){
            SEQ_AddEvent(eABT2Off,Sys);
            catchANTI_BT2 = 1;
        }
    }
    else if ((INGRESSI[ANTI_BT2] == 1) && (ABT2==1)){
        if (catchANTI_BT2 == 1){
            SEQ_AddEvent(eABT2On,Sys);
            catchANTI_BT2 = 0;
        }
    }
	
    //  Debounce chiave SERVICE
    if (INGRESSI[SERVICE] == 0 ){
        catchSERVICE1 = MAX_DEBOUNCE;
        if (catchSERVICE0 >0){
            catchSERVICE0--;
        }
        else{
            if (catchSERVICE == 0){
                SEQ_AddEvent(eServiceOff,Sys);
                catchSERVICE = 1;
            }
        }
    }
    else{
        catchSERVICE0 = MAX_DEBOUNCE;
        if (catchSERVICE1 >0){
            catchSERVICE1--;
        }
        else{
            if (catchSERVICE == 1){
                SEQ_AddEvent(eServiceOn,Sys);
                catchSERVICE = 0;
            }
        }
    }
	
	
	
	
    //  Debounce PLAY Synt
    if ( INGRESSI[PLAY_MSG] == 0 ){
        if (catchPLAY_MSG == 0){
            catchPLAY_MSG = 1;
        }
    }
    else{
        if (catchPLAY_MSG == 1){
            SEQ_AddEvent(ePlayBtn,Sys);
            catchPLAY_MSG = 0;
        }
    }
	
    //  Debounce REC Synt
    if ( INGRESSI[REC_MSG] != 0 ){
        if (catchREC_MSG == 0){
            catchREC_MSG = 1;
        }
    }
    else{
        if (catchREC_MSG == 1){
            SEQ_AddEvent(eRecBtn,Sys);
            catchREC_MSG = 0;
        }
    }
	
    //  End play / record
	
    if ( INGRESSI[AYINT_SPI] == 1 ){
        if (catchINT_SPI == 0){
            catchINT_SPI = 1;
        }
    }
    else{
        if (catchINT_SPI == 1){
            SEQ_AddEvent(eEndPlay,Sys);
            catchINT_SPI = 0;
        }
    }
	
    //  Lettura da prossimit� Q5
	
    if ( INGRESSI[AYPROX] == 0 ){
        if (catchPROX == 0){
            catchPROX = 1;
        }
    }
    else{
        if (catchPROX == 1){
            SEQ_AddEvent(eQ5ReadOk,Sys);
            catchPROX = 0;
        }
    }
	
    //  Lettura da ingresso VIP
	
    if ( INGRESSI[VIP] == 0 ){
        if (catchVIP == 0){
            catchVIP = 1;
        }
    }
    else{
        if (catchVIP == 1){
            SEQ_AddEvent(eVip,Sys);
            catchVIP = 0;
        }
    }
	
    //  NoDebounce porta chiusa P1
    if ( USCITE[PC1] == 0 ){
        if (catchPC1 == 0){
            SEQ_AddEvent(eNotClosed1,Sys);
            catchPC1 = 1;
        }
    }
    else{
        if (catchPC1 == 1){
            SEQ_AddEvent(eClosed1,Sys);
            catchPC1 = 0;
        }
    }
	
    //  NoDebounce porta chiusa P2
    if ( USCITE[PC2] == 0 ){
        if (catchPC2 == 0){
            SEQ_AddEvent(eNotClosed2,Sys);
            catchPC2 = 1;
        }
    }
    else{
        if (catchPC2 == 1){
            SEQ_AddEvent(eClosed2,Sys);
            catchPC2 = 0;
        }
    }
	
    //  NoDebounce porta aperta P1
    if ( USCITE[PA1] == 0 ){
        if (catchPA1 == 0){
            catchPA1 = 1;
        }
    }
    else{
        if (catchPA1 == 1){
            SEQ_AddEvent(eOpened1,Sys);
            catchPA1 = 0;
        }
    }
	
    //  NoDebounce porta aperta P2
    if (USCITE[PA2] == 0){
        if (catchPA2 == 0){
            catchPA2 = 1;
        }
    }
    else{
        if (catchPA2 == 1){
            SEQ_AddEvent(eOpened2,Sys);
            catchPA2 = 0;
        }
    }
    //  NoDebounce violazione porta P1
    if (USCITE[VIOLP1] == 0){
        if (catchVIOLP1 == 0){
            SEQ_AddEvent(eViolateP1,Sys);
            catchVIOLP1 = 1;
        }
    }
    else{
        if (catchVIOLP1 == 1){
            catchVIOLP1 = 0;
        }
    }
    
	//  NoDebounce violazione porta P2
    if (USCITE[VIOLP2] == 0){
        if (catchVIOLP2 == 0){
            SEQ_AddEvent(eViolateP2,Sys);
            catchVIOLP2 = 1;
        }
    }
    else{
        if (catchVIOLP2 == 1){          
            catchVIOLP2 = 0;
        }
    }
    
    //  NoDebounce biometrico OK
    if (RxBiom[2] == 0x31){
        if (catchBIOOK == 0){
            catchBIOOK = 1;
            if (PCBioStatus!=0)
                SEQ_AddEvent(eBioOk,Sys);
        }
    }
    else  {
        if (catchBIOOK == 1){
            catchBIOOK = 0;
        }
    }
	
    //  NoDebounce biometrico NOT OK
    if (RxBiom[3] == 0x31){
        if (catchBIONOK == 0){
            catchBIONOK = 1;
            if (PCBioStatus!=0)
                SEQ_AddEvent(eBioNotOk,Sys);
        }
    }
    else{
        if (catchBIONOK == 1){
            catchBIONOK = 0;
        }
    }
	
    //  NoDebounce biometrico OK CB
    if (INGRESSI[BIOM_OK]){
        if (catchBioOkCB == 0){
            catchBioOkCB = 1;
            if (BioMode==4)
                SEQ_AddEvent(eBioOk,Sys);
        }
    }
    else{
        if (catchBioOkCB == 1){
            catchBioOkCB = 0;
        }
    }
	
    //  NoDebounce biometrico NOT OK CB
    if (INGRESSI[BIOM_NOTOK]){
        if (catchBioNotOkCB == 0){
            catchBioNotOkCB = 1;
            if (BioMode==4)
                USCITE[BIOM_NOK] = 1;	
            SEQ_AddEvent(eBioNotOk,Sys);
        }
    }
    else{
        if (catchBioNotOkCB == 1){
            USCITE[BIOM_NOK] = 0;
            catchBioNotOkCB = 0;
        }
    }
	
	
	
    //  NoDebounce accendi amplificatore
    if (RxBiom[4] == 0x31){
        if (catchBIOAMP == 0){
            catchBIOAMP = 1;
            SEQ_AddEvent(ePCBioAmpOn,Sys);
        }
    }
    else{
        if (catchBIOAMP == 1){
            catchBIOAMP = 0;
            SEQ_AddEvent(ePCBioAmpOff,Sys);
        }
    }
	
    //  NoDebounce PCBIO polling
    if (PCBioPolling == 1){
        PCBioPolling = 0;
        SEQ_AddEvent(ePCBioPolling,Sys);
    }
	
    //  NoDebounce BIO FingerPresent
    if (RxBiom[5] == 0x31){
        if (catchFING_IN == 0){
            catchFING_IN = 1;
            if (PCBioStatus!=0)
                SEQ_AddEvent(eFingPres,Sys);
        }
    }
    else{
        if (catchFING_IN == 1){
            catchFING_IN = 0;
            if (PCBioStatus!=0)
                SEQ_AddEvent(eFingNotPres,Sys);
        }
    }
	
	
    // -----------------------------------------------------------------------------
	
    // Ingressi da Connection Box
	
    // -----------------------------------------------------------------------------
	
    //  Lettura da ingresso PROX_NORM1
	
    if ( INGRESSI[PROX_NORM1] == 0 ){
        if (catchPROX_NORM1 == 0){
            catchPROX_NORM1 = 1;
        }
    }
    else{
        if (catchPROX_NORM1 == 1){
            SEQ_AddEvent(eProxNorm1,Sys);
            catchPROX_NORM1 = 0;
        }
    }
	
    //  Lettura da ingresso PROX_NORM2
	
    if ( INGRESSI[PROX_NORM2] == 0 ){
        if (catchPROX_NORM2 == 0){
            catchPROX_NORM2 = 1;
        }
    }
    else{
        if (catchPROX_NORM2 == 1){
            SEQ_AddEvent(eProxNorm2,Sys);
            catchPROX_NORM2 = 0;
        }
    }
	
    //  Lettura da ingresso PROX_DIS1
	
    if ( INGRESSI[PROX_DIS1] == 0 ){
        if (catchPROX_DIS1 == 0){
            catchPROX_DIS1 = 1;
        }
    }
    else{
        if (catchPROX_DIS1 == 1){
            SEQ_AddEvent(eProxDis1,Sys);
            catchPROX_DIS1 = 0;
        }
    }
	
    //  Lettura da ingresso PROX_DIS2
	
    if ( INGRESSI[PROX_DIS2] == 0 ){
        if (catchPROX_DIS2 == 0){
            catchPROX_DIS2 = 1;
        }
    }
    else{
        if (catchPROX_DIS2 == 1){
            SEQ_AddEvent(eProxDis2,Sys);
            catchPROX_DIS2 = 0;
        }
    }
	
    //  Lettura da ingresso ROT_EMERG
	
    if ( INGRESSI[ROT_EMERG] == 0 )
    {
        if (catchROT_EMERG == 0)
        {
            catchROT_EMERG = 1;
        }
    }
    else
    {
        if (catchROT_EMERG == 1)
        {
            SEQ_AddEvent(ePanicBtn,Sys);
            catchROT_EMERG = 0;
        }
    }
	
    //  Lettura da ingresso SW_PA3 //

#ifdef CLEAR_ID_DEBUG //cancella locazione EEPROM dove risiede l'ID
    if ( INGRESSI[SW_PA3] == 0 ){
        if (catchSW_PA3 == 0){
            catchSW_PA3 = 1;
        }
    }
    else{
        if (catchSW_PA3 == 1){
            TWI_Write ( AT91C_BASE_TWI, EEPROM_24AA256, ADDIDKEY,(unsigned char *)&DummyIDKey[0], 7);
            catchSW_PA3 = 0;
        }
    }
#else
	if ( INGRESSI[SW_AFT_LB_ST] == 0 ){
        if (catchSW_AF_LB_ST == 0){
			SEQ_AddEvent(eResAftLowBStat,Sys);
            catchSW_AF_LB_ST = 1;
        }
    }
    else{
        if (catchSW_AF_LB_ST == 1){   
			SEQ_AddEvent(eSetAftLowBStat,Sys);
            catchSW_AF_LB_ST = 0;
        }
    }

#endif
	
	
    // T200 Count
    if ( T200 == 0 )
    {
        if (catchT200 == 0)
        {
            NT200++;
            catchT200 = 1;
        }
    }
    else
    {
        if (catchT200 == 1)
        {
            NT200++;
            catchT200 = 0;
        }
    }
}

void DecodeCommand(void)
{
  unsigned char Buff[3] = {'0','0','0'}, ch=0;
  unsigned char TimeFreeze=0, i=0, Delay=0;

  for(i=0; i<3; ){

    TimeFreeze=SysTimeCount;

    Delay=(unsigned char)(SysTimeCount-TimeFreeze);
    while(!(AT91F_US_RxReady( (AT91PS_USART)DBGU_pt ))&&(Delay<3)){
      Delay=(unsigned char)(SysTimeCount-TimeFreeze);
    }
    if(Delay<3){
      ch = Mirror(AT91F_US_GetChar( (AT91PS_USART)DBGU_pt ));
#ifdef AM_KEY_VER
      if(ch != '!'){
#else
      if(ch != '~'){
#endif
        Buff[i] = ch;
        i++;
      }
    }
    else{
      Buff[i] = i;
      i++;
    }
  }
#ifdef AM_KEY_VER
  if(strncmp((const char *)Buff, (const char *)"%=^", 3) == 0){       //"KEY"
    SEQ_AddEvent(eCommKey,2);
  }
  else if(strncmp((const char *)Buff, (const char *)"_<$", 3) == 0){  //"RUN"
    SEQ_AddEvent(eCommRun,2);
  }
  else if(strncmp((const char *)Buff, (const char *)"{)(", 3) == 0){  //"INI"
    SEQ_AddEvent(eCommFrun,2);
  }
  else if(strncmp((const char *)Buff, (const char *)"]|>", 3) == 0){  //"CLR"
    SEQ_AddEvent(eCommClr,2);
  }
  else if(strncmp((const char *)Buff, (const char *)"[}?", 3) == 0){  //"FID"
    SEQ_AddEvent(eCommFid,2);
  }
  else if(strncmp((const char *)Buff, (const char *)"^$%", 3) == 0){  //"SID"
    SEQ_AddEvent(eCommSid,2);
  }
  else{
    //AT91F_US_PutChar((AT91PS_USART)DBGU_pt,Mirror('0'));  //leone debug
  }
#else
  if(strncmp((const char *)Buff, (const char *)"key", 3) == 0){
    SEQ_AddEvent(eCommKey,2);
  }
  else if(strncmp((const char *)Buff, (const char *)"run", 3) == 0){
    SEQ_AddEvent(eCommRun,2);
  }
  else if(strncmp((const char *)Buff, (const char *)"ini", 3) == 0){
    SEQ_AddEvent(eCommFrun,2);
  }
  else if(strncmp((const char *)Buff, (const char *)"clr", 3) == 0){
    SEQ_AddEvent(eCommClr,2);
  }
  else{
    //AT91F_US_PutChar((AT91PS_USART)DBGU_pt,Mirror('0'));  //leone debug
  }
#endif
}


void LogicManageCommands(void)
{
   char ch;
   static unsigned int NumCycles = 0;

   if(OpenedTransmission==0){
      if (AT91F_US_RxReady( (AT91PS_USART)DBGU_pt ))
         ch = Mirror(AT91F_US_GetChar( (AT91PS_USART)DBGU_pt ));
#ifdef AM_KEY_VER
      if(ch == '!'){ //se sta arrivando un comando
#else
      if(ch == '~'){
#endif
        DecodeCommand();
      }
   }
   else {
      if (NumCycles >0)
         NumCycles++;

      if (Sys==SysDisplay){
         if(AT91F_US_Error ( (AT91PS_USART)DBGU_pt ) & AT91C_US_OVRE){
            millisecondi(5);
            AT91F_US_GetChar( (AT91PS_USART)DBGU_pt );
            DBGU_pt -> DBGU_CR = AT91C_US_RSTSTA;
            millisecondi(1);
         }
         if (AT91F_US_RxReady( (AT91PS_USART)DBGU_pt )){
            ch = Mirror(AT91F_US_GetChar( (AT91PS_USART)DBGU_pt ));
            if (NumCycles >500)
               EscCount=0;
            switch (ch){

#ifdef AM_KEY_VER
             case '!':
#else
             case '~':
#endif
               DecodeCommand();
               break;

             case '3':
               bDisDbg = 0;
               SysDisplay = 2;
               EscCount = 0;
               US_SwitchDisplay();
               break;
             case '1':
             case '2':
               bDisDbg = 0;
               SysDisplay = ch-49;
               EscCount = 0;
               US_SwitchDisplay();
               break;
             case 'C':
             case 'c':
               bDisDbg = 1;
               SysDisplay = 2;
               US_SwitchDisplay();
               break;
             case 13: //enter
               SEQ_AddEvent(eEnter,Sys);
               break;
             case 'K':
             case 'k':
               bDisDbg = 0;
               SysDisplay = 2;
               SEQ_AddEvent(eMetalOn,Sys);
               EscCount = 0;
               break;
             case 27: //esc
               EscCount++;
               if ((EscCount > 1) && (bDisDbg == 0)){
                  bDisDbg = 1;
                  SysDisplay = 2;
                  EscCount = 0;
                  SEQ_AddEvent(eNight,Sys);
                  SEQ_AddEvent(eProgOn,Sys);
                  NumCycles = 0;
               }
               else{
                  SEQ_AddEvent(eReset,Sys);
                  NumCycles = 1;
               }
               break;
             case '+':
               SEQ_AddEvent(eMBtn1,Sys);
               break;
             case '-':
               SEQ_AddEvent(eMBtn2,Sys);
               break;
             case 'T':
             case 't':
               SEQ_AddEvent(eResetSod,Sys);
               break;
#ifdef CMD_DEBUG
             case 'N':
             case 'n':
               SEQ_AddEvent(eBioNotOk,Sys);
               break;
             case 'O':
             case 'o':
               SEQ_AddEvent(eBioOk,Sys);
               break;
             case 'Z':
             case 'z':
               SEQ_AddEvent(eOnOff,Sys);
               break;
             case 'H':
             case 'h':
               SEQ_AddEvent(eNight,Sys);
               break;
             case 'G':
             case 'g':
               SEQ_AddEvent(eMorning,Sys);
               break;
             case 'F':
             case 'f':
               SEQ_AddEvent(eFirstIn,Sys);
               break;
             case 'L':
             case 'l':
               SEQ_AddEvent(eLastOut,Sys);
               break;
             case 'V':
             case 'v':
               if (D_MANUp == 0){
                  if (AVerify == 1)
                     AVerify = 0;
                  else
                     AVerify = 1;
               }
               else{
                  if (MVerify == 1)
                     MVerify = 0;
                  else
                     MVerify = 1;
               }
               break;
             case 'W':
               if (AInNc == 1)
                  AInNc = 0;
               else
                  AInNc = 1;
               break;
             case 'w':
               if (AOutNc == 1)
                  AOutNc = 0;
               else
                  AOutNc = 1;
               break;
             case 'Y':
               if (MInNc == 1)
                  MInNc = 0;
               else
                  MInNc = 1;
               break;
             case 'y':
               if (MOutNc == 1)
                  MOutNc = 0;
               else
                  MOutNc = 1;
               break;
             case 'B':
               if (AInBio == 1)
                  AInBio = 0;
               else
                  AInBio = 1;
               break;
             case 'b':
               if (AOutBio == 1)
                  AOutBio = 0;
               else
                  AOutBio = 1;
               break;
             case 'M':
               if (AInMetal == 1)
                  AInMetal = 0;
               else
                  AInMetal = 1;
               break;
             case 'm':
               if (AOutMetal == 1)
                  AOutMetal = 0;
               else
                  AOutMetal = 1;
               break;
             case 'P':
               if (AInMore == 1)
                  AInMore = 0;
               else
                  AInMore = 1;
               break;
             case 'p':
               if (AOutMore == 1)
                  AOutMore = 0;
               else
                  AOutMore = 1;
               break;
             case 'X':
               if (AInChange == 1)
                  AInChange = 0;
               else
                  AInChange = 1;
               break;
             case 'x':
               if (AOutChange == 1)
                  AOutChange = 0;
               else
                  AOutChange = 1;
               break;
             case 'A':
             case 'a':
               if (MOutAuto == 1)
                  MOutAuto = 0;
               else
                  MOutAuto = 1;
               break;
             case 'U'://debug
             case 'u'://debug
               SEQ_AddEvent(eMode,Sys);
               break;
             case 'E'://debug
             case 'e'://debug
               if (EmeMode<2)
                  EmeMode++;
               else
                  EmeMode = 0;
               break;
             case 'S':
             case 's':
               SEQ_AddEvent(ePresent,Sys);
               SimulPc=1;
               break;
             case 'D':
             case 'd':
               SEQ_AddEvent(eNotPresent,Sys);
               //SimulPc=0;
               break;
             case 'R':
             case 'r':
               SEQ_AddEvent(eMetal,Sys);
               break;
             case 'I':
             case 'i':
               SEQ_AddEvent(eMore,Sys);
               SimulPcMoreFlag = 1;              //leone
               break;
             case 'J':
             case 'j':
               SEQ_AddEvent(eNotMore,Sys);
               SimulPcMoreFlag = 0;              //leone
               break;
             case 'Q':
             case 'q':
               SEQ_AddEvent(eObject,Sys);
               SimulPcObjectFlag = 1;            //leone
               break;
             case '*':
               SEQ_AddEvent(eBtn1,Sys);
               break;
             case '/':
               SEQ_AddEvent(eBtn2,Sys);
               break;
             case '4':
               SEQ_AddEvent(eProxDis1,Sys);
               break;
             case '5':
               SEQ_AddEvent(eProxNorm1,Sys);
               break;
             case '6':
               SEQ_AddEvent(eProxDis2,Sys);
               break;
             case '7':
               SEQ_AddEvent(eProxNorm2,Sys);
               break;
             case '8':
               SEQ_AddEvent(eVip,Sys);
               break;
             case '9':
               SEQ_AddEvent(ePanicBtn,Sys);
               break;
             case '0':
               SEQ_AddEvent(eEmergency,Sys);
               break;
             case '.':
               SEQ_AddEvent(eNormal,Sys);
               break;
#endif
            }
            ((AT91PS_USART)DBGU_pt)->US_CR = AT91C_US_RSTSTA;
         }
      }
   }
}

VS_VOID LogicDisplay (VS_INT MsgId)
{
    char DisTmp[19];

    if (DisplayMode == DISPLAY_MODE_DEBUG) {
        US_ClearDisplay((AT91PS_USART)DBGU_pt);
    }

    if (Language == 0)
        pDispStrg = (char*) StrgIta[0];
    else
        pDispStrg = (char*) StrgEng[0];	

    switch (MsgId) {
        case mIdle :
            DisTmp[0] = 10;
            DisTmp[1] = 13;
            DisTmp[2] = 'A';
            DisTmp[3] = 32;

            if (AIn){
                DisTmp[4] = 'I';
                if (AInMetal)
                    DisTmp[5] = 'm';
                else
                    DisTmp[5] = '-';
                if (AInMore)
                    DisTmp[6] = 'p';
                else
                    DisTmp[6] = '-';
                if (AInBio)
                    DisTmp[7] = 'b';
                else
                    DisTmp[7] = '-';
                if (AVerify)
                    DisTmp[8] = 'v';
                else
                    DisTmp[8] = '-';
                if (AInNc)
                    DisTmp[9] = 'c';
                else
                    DisTmp[9] = '-';
                if (AInChange)
                    DisTmp[10] = 'x';
                else
                    DisTmp[10] = '-';
            }
             else{                
				strncpy(&DisTmp[4],"        ", 7);
            }

            if (AOut) {
                DisTmp[11] = 'O';
                if (AOutMetal)
                    DisTmp[12] = 'm';
                else
                    DisTmp[12] = '-';
                if (AOutMore)
                    DisTmp[13] = 'p';
                else
                    DisTmp[13] = '-';
                if (AOutBio)
                    DisTmp[14] = 'b';
                else
                    DisTmp[14] = '-';
                if (AVerify)
                    DisTmp[15] = 'v';
                else
                    DisTmp[15] = '-';
                if (AOutNc)
                    DisTmp[16] = 'c';
                else
                    DisTmp[16] = '-';
                if (AOutChange)
                    DisTmp[17] = 'x';
                else
                    DisTmp[17] = '-';
            }
            else {              
				strncpy(&DisTmp[11],"        ", 7);
            }

            DisTmp[18] = 0;
            Dbg((AT91PS_USART)DBGU_pt, DisTmp);

            if (LastMsgIdSys[Sys] != MsgId)                      //clear buffer Riga2 @first idle
                sprintf(Riga2,pDispMask + (1*DISP_STRG_SIZE));   //"                "

            if (TReadClk > 490){
				//              TReadClk = 0;
				//              Clk_Read();
				
                if (((AInMore==0)&&(AOutMore==0)&&(MInMore==0)&&(MOutMore==0)) ||
                    ((BioMode>0)&&(AInBio==0)&&(AOutBio==0)&&(MInBio==0)&&(MOutBio==0)) ||
						((AInMetal==0)&&(MInMetal==0)&&(AOutMetal==0)&&(MOutMetal==0))) {
							
							sprintf(Riga2,(const char*) pDispStrg + (98*DISP_STRG_SIZE));		//"             Off"
							
							if ((AInMore==0)&&(AOutMore==0)&&(MInMore==0)&&(MOutMore==0)) {		
								if(Language == 0) {  //Italiano            	            
									strncpy(&Riga2[0],(char const*) &pDispStrg[(109*DISP_STRG_SIZE)+6], 2);	//+P
								}
								else {                            	
									strncpy(&Riga2[0],(char const*) &pDispStrg[(109*DISP_STRG_SIZE)+6], 3);	//APD
								}
							}
							if ((BioMode>0)&&(AInBio==0)&&(AOutBio==0)&&(MInBio==0)&&(MOutBio==0)) {
								if(Language == 0) {  //Italiano                           
									strncpy(&Riga2[3],(char const*) &pDispStrg[(109*DISP_STRG_SIZE)+8], 4);	//   Biom
								}
								else {                            
									strncpy(&Riga2[4],(char const*) &pDispStrg[(109*DISP_STRG_SIZE)+9], 3);	//    Bio
								}
							}
							if ((AInMetal==0)&&(MInMetal==0)&&(AOutMetal==0)&&(MOutMetal==0)) {
								if(Language == 0) {  //Italiano               	            
									strncpy(&Riga2[8],(char const*) &pDispStrg[(109*DISP_STRG_SIZE)+12], 3);	//        Met
								}
								else {                            
									strncpy(&Riga2[8],(char const*) &pDispStrg[(109*DISP_STRG_SIZE)+12], 2);	//        MD
								}
							}
						}
                else {
                    sprintf(Riga2,(const char*) pDispMask + (0*DISP_STRG_SIZE));		//"          :  :  "		
                    
					strncpy(&Riga2[0],(char const*) &DayName[0], 3);                    
					strncpy(&Riga2[8],(char const*) &HourPLL[0], 2);                    
					strncpy(&Riga2[11],(char const*) &MinPLL[0], 2);                    
					strncpy(&Riga2[14],(char const*) &SecPLL[0], 2);
                }
				
                
				if ((AIn==0)||(AOut==0)){
					if (AIn==0)
						sprintf(Riga1,(const char*) pDispStrg + (96*DISP_STRG_SIZE));		//Auto    Solo Usc
					else
						sprintf(Riga1,(const char*) pDispStrg + (97*DISP_STRG_SIZE));		//Auto    Solo Ing
				}
				else  {
					sprintf(Riga1,(const char*) pDispStrg + (99*DISP_STRG_SIZE));		//Auto      /  /  "
					if(ViewMode==0) { 		//ViewMode GG/MM/AA 		                       
						strncpy(&Riga1[8],(char const*) &DatePLL[0], 2); 			                  
						strncpy(&Riga1[11],(char const*) &MonthPLL[0], 2);
					}
					else {    	                       
						strncpy(&Riga1[8],(char const*) &MonthPLL[0], 2);     			               
						strncpy(&Riga1[11],(char const*) &DatePLL[0], 2);
					}                            
					strncpy(&Riga1[14],(char const*) &YearPLL[0], 2);
					break;                     
				}              
                
            }

            if (Vac==0) {
	            if (PassCount>=PassCountMax) {
	                sprintf(Riga1,(const char*) pDispStrg + (100*DISP_STRG_SIZE));		//Batteria bassa
	            }
	            else {
	                sprintf(Riga1,(const char*) pDispStrg + (101*DISP_STRG_SIZE));		//Assenza rete 230
	            }
	        }
            else {
	            if ((PCBioStatus==0)&&(BioMode>0)&&(BioMode<4)) {
	                sprintf(Riga1,(const char*) pDispStrg + (102*DISP_STRG_SIZE));		//PC Bio  off-line
	            }
	            if ((PCBioStatus==1)&&(BioMode>0)&&(BioMode<4)) {
	                sprintf(Riga1,(const char*) pDispStrg + (75*DISP_STRG_SIZE));		//PC  Biometrico
	                sprintf(Riga2,(const char*) pDispStrg + (89*DISP_STRG_SIZE));		//in  accensione	
	            }	
	        }
            Row1((AT91PS_USART)DBGU_pt, Riga1);
            Row2((AT91PS_USART)DBGU_pt, Riga2);

            if ((Orario[2] == BinBcd2(HHNight)) &&
                (Orario[1] == BinBcd2(MMNight)) &&
                (Orario[0] <= 0x05)) {

                if ((HHNight != 0) || (MMNight != 0)) {
                    SEQ_AddEvent(eNight,Sys);
                }
            }
        break;

        case mMIdle:
            DisTmp[0] = 10;
            DisTmp[1] = 13;
            DisTmp[2] = 'M';
            DisTmp[3] = 32;

            if (MIn){
                DisTmp[4] = 'I';
                if (MInMetal)
                    DisTmp[5] = 'm';
                else
                    DisTmp[5] = '-';
                if (MInMore)
                    DisTmp[6] = 'p';
                else
                    DisTmp[6] = '-';
                if (MInBio)
                    DisTmp[7] = 'b';
                else
                    DisTmp[7] = '-';
                if (MVerify)
                    DisTmp[8] = 'v';
                else
                    DisTmp[8] = '-';
                if (MInNc)
                    DisTmp[9] = 'c';
                else
                    DisTmp[9] = '-';
                if (MInChange)
                    DisTmp[10] = 'x';
                else
                  DisTmp[10] = '-';
            }
            else{                
				strncpy(&DisTmp[4],"        ", 7);
            }
            if (MOut) {
                if (MOutAuto == 0)
                    DisTmp[11] = 'O';
                else
                    DisTmp[11] = 'A';
                if (MOutMetal)
                  DisTmp[12] = 'm';
                else
                  DisTmp[12] = '-';
                if (MOutMore)
                  DisTmp[13] = 'p';
                else
                  DisTmp[13] = '-';
                if (MOutBio)
                  DisTmp[14] = 'b';
                else
                  DisTmp[14] = '-';
                if (MVerify)
                  DisTmp[15] = 'v';
                else
                  DisTmp[15] = '-';
                if (MOutNc)
                  DisTmp[16] = 'c';
                else
                  DisTmp[16] = '-';
                if (MOutChange)
                  DisTmp[17] = 'x';
                else
                  DisTmp[17] = '-';
            }
            else{
               
				strncpy(&DisTmp[11],"        ", 7);
            }
            DisTmp[18] = 0;
            Dbg((AT91PS_USART)DBGU_pt, DisTmp);

            if (LastMsgIdSys[Sys] != MsgId)                      //clear buffer Riga2 @first idle
                sprintf(Riga2,pDispMask + (1*DISP_STRG_SIZE));   //"                "
            if (TReadClk >490){
                if (((AInMore==0)&&(AOutMore==0)&&(MInMore==0)&&(MOutMore==0)) ||
                    ((BioMode>0)&&(AInBio==0)&&(AOutBio==0)&&(MInBio==0)&&(MOutBio==0)) ||
						((AInMetal==0)&&(MInMetal==0)&&(AOutMetal==0)&&(MOutMetal==0))) {
							sprintf(Riga2, (const char*) pDispStrg + (98*DISP_STRG_SIZE));		//"             Off"
							
							if ((AInMore==0)&&(AOutMore==0)&&(MInMore==0)&&(MOutMore==0)) {		
								if(Language == 0) {  //Italiano            	            
									strncpy(&Riga2[0],(char const*) &pDispStrg[(109*DISP_STRG_SIZE)+6], 2);	//+P
								}
								else {                            	
									strncpy(&Riga2[0],(char const*) &pDispStrg[(109*DISP_STRG_SIZE)+6], 3);	//APD
								}
							}
							if ((BioMode>0)&&(AInBio==0)&&(AOutBio==0)&&(MInBio==0)&&(MOutBio==0)) {
								if(Language == 0) {  //Italiano                           
									strncpy(&Riga2[3],(char const*) &pDispStrg[(109*DISP_STRG_SIZE)+8], 4);	//   Biom
								}
								else {                            
									strncpy(&Riga2[4],(char const*) &pDispStrg[(109*DISP_STRG_SIZE)+9], 3);	//    Bio
								}
							}
							if ((AInMetal==0)&&(MInMetal==0)&&(AOutMetal==0)&&(MOutMetal==0)) {
								if(Language == 0) {  //Italiano               	            
									strncpy(&Riga2[8],(char const*) &pDispStrg[(109*DISP_STRG_SIZE)+12], 3);	//        Met
								}
								else {                            
									strncpy(&Riga2[8],(char const*) &pDispStrg[(109*DISP_STRG_SIZE)+12], 2);	//        MD
								}
							}						
						}
                else {
                    sprintf(Riga2,(const char*) pDispMask + (0*DISP_STRG_SIZE));		//"          :  :  "
                    
					strncpy(&Riga2[0],(char const*) &DayName[0], 3);                   
					strncpy(&Riga2[8],(char const*) &HourPLL[0], 2);                    
					strncpy(&Riga2[11],(char const*) &MinPLL[0], 2);                    
					strncpy(&Riga2[14],(char const*) &SecPLL[0], 2);
                }
				if ((MIn==0)||(MOut==0)){
					if (MIn==0)
						sprintf(Riga1,(const char*) pDispStrg + (103*DISP_STRG_SIZE));		// Manuale Solo Usc
					else
						sprintf(Riga1,(const char*) pDispStrg + (104*DISP_STRG_SIZE));		//Manuale Solo Ing
				}
				else  {
					sprintf(Riga1,(const char*) pDispStrg + (105*DISP_STRG_SIZE));		//Manuale   /  /  
					if(ViewMode==0) { 		//ViewMode GG/MM/AA 		                       
						strncpy(&Riga1[8],(char const*) &DatePLL[0], 2); 			                  
						strncpy(&Riga1[11],(char const*) &MonthPLL[0], 2);
					}
					else {    	                       
						strncpy(&Riga1[8],(char const*) &MonthPLL[0], 2);     			               
						strncpy(&Riga1[11],(char const*) &DatePLL[0], 2);
					}                            
					strncpy(&Riga1[14],(char const*) &YearPLL[0], 2);
					break;                     
				}              
            }
            if (Vac==0) {
                if (PassCount>=PassCountMax) {
	                sprintf(Riga1, (const char*) pDispStrg + (100*DISP_STRG_SIZE));		//Batteria bassa
	            }
	            else {
	                sprintf(Riga1, (const char*) pDispStrg + (101*DISP_STRG_SIZE));		//Assenza rete 230
	            }
            }
            else {	
	            if ((PCBioStatus==0)&&(BioMode>0)&&(BioMode<4)) {
	                sprintf(Riga1,(const char*) pDispStrg + (102*DISP_STRG_SIZE));		//PC Bio  off-line
	            }
                if ((PCBioStatus==1)&&(BioMode>0)&&(BioMode<4)) {
	                sprintf(Riga1,(const char*) pDispStrg + (75*DISP_STRG_SIZE));		//PC  Biometrico
	                sprintf(Riga2,(const char*) pDispStrg + (89*DISP_STRG_SIZE));		//in  accensione	
	            }	
	        }	
            Row1((AT91PS_USART)DBGU_pt, Riga1);
            Row2((AT91PS_USART)DBGU_pt, Riga2);

            if ((Orario[2] == BinBcd2(HHNight)) &&
                (Orario[1] == BinBcd2(MMNight)) &&
                (Orario[0] <= 0x20)) {
                if ((HHNight != 0) || (MMNight != 0)) {
                    SEQ_AddEvent(eNight,Sys);
                }
            }
            break;
    case mInWait2         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (0*DISP_STRG_SIZE));     //Auto Ingresso
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (1*DISP_STRG_SIZE));   	//P. interna aperta
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rInWait2        ");
                           break;
    case mInOpen1         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (0*DISP_STRG_SIZE));     //Auto Ingresso
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (2*DISP_STRG_SIZE));     //P. esterna aperta
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rInOpen1        ");
                           break;
    case mInClose1        :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (0*DISP_STRG_SIZE));		//Auto Ingresso
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (3*DISP_STRG_SIZE));		//Pers. in  tunnel
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rInClose1       ");
                           break;
    case mInPause         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (0*DISP_STRG_SIZE));		//Auto Ingresso
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (4*DISP_STRG_SIZE));		//Controllo +P
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rInPause        ");
                           break;
    case mInControl       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (0*DISP_STRG_SIZE));		//Auto Ingresso
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (4*DISP_STRG_SIZE));		//Controllo +P
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rInControl      ");
                           break;
    case mInMoreAlarm     :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (0*DISP_STRG_SIZE));		//Auto Ingresso
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (5*DISP_STRG_SIZE));		//Allarme +P
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rInMoreAlarm    ");
                           break;
    case mInOpen2         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (0*DISP_STRG_SIZE));		//Auto Ingresso
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (6*DISP_STRG_SIZE));		//Pers.in ingresso
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rInOpen2        ");
                           break;
    case mInClose2        :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (0*DISP_STRG_SIZE));		//Auto Ingresso
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (1*DISP_STRG_SIZE));     //P. interna aperta
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rInClose2       ");
                           break;
    case mOutWait1        :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (7*DISP_STRG_SIZE));		//Auto      Uscita
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (2*DISP_STRG_SIZE));     //P. esterna aperta
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rOutWait1       ");
                           break;
    case mOutOpen2        :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (7*DISP_STRG_SIZE));		//Auto      Uscita
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (1*DISP_STRG_SIZE));     //P. interna aperta
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rOutOpen2       ");
                           break;
    case mOutClose2       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (7*DISP_STRG_SIZE));		//Auto      Uscita
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (3*DISP_STRG_SIZE));		//Pers. in  tunnel
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rOutClose2      ");
                           break;
    case mOutPause        :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (7*DISP_STRG_SIZE));		//Auto      Uscita
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (4*DISP_STRG_SIZE));		//Controllo +P
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rOutPause       ");
                           break;
    case mOutControl      :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (7*DISP_STRG_SIZE));		//Auto      Uscita
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (4*DISP_STRG_SIZE));		//Controllo +P
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rOutControl     ");
                           break;
    case mOutMoreAlarm    :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (7*DISP_STRG_SIZE));		//Auto      Uscita
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (5*DISP_STRG_SIZE));		//Allarme +P);
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rOutMoreAlarm   ");
                           break;
    case mOutOpen1        :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (7*DISP_STRG_SIZE));		//Auto      Uscita
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (8*DISP_STRG_SIZE));		//Pers.in uscita
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rOutOpen1       ");
                           break;
    case mOutClose1       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (7*DISP_STRG_SIZE));		//Auto      Uscita
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (2*DISP_STRG_SIZE));     //P. esterna aperta
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rOutClose1      ");
                           break;
    case mMetalPresent    :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (9*DISP_STRG_SIZE));		//Allarme  metal
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (10*DISP_STRG_SIZE));    //Tunnel  occupato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMetalPresent   ");
                           break;
    case mMetalNotPresent :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (9*DISP_STRG_SIZE));		//Allarme  metal
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (11*DISP_STRG_SIZE));    //Tunnel  libero
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMetalNotPresent");
                           break;
    case mMetalAlarm      :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (9*DISP_STRG_SIZE));	    //Allarme  metal
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (11*DISP_STRG_SIZE));    //Tunnel  libero
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMetalAlarm     ");
                           break;
	case mClosingBeforeObjChk:
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (185*DISP_STRG_SIZE));	//ChiusuraPortePer
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (12*DISP_STRG_SIZE));	//Verifica Oggetto
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rClsBeforeObjChk");
                           break;								   
    case mObjectCheck     :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (12*DISP_STRG_SIZE));    //Verifica Oggetto
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (11*DISP_STRG_SIZE));    //Tunnel  libero
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rObjectCheck    ");
                           break; 					   
    case mObjectAlarm     :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (13*DISP_STRG_SIZE));    //Allarme  oggetto
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (11*DISP_STRG_SIZE));    //Tunnel  libero
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rObjectAlarm    ");
                           break;
    case mBioCheckIn      :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (14*DISP_STRG_SIZE));    //Controllo  biom.
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (15*DISP_STRG_SIZE));    //in  ingresso
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rBioCheckIn     ");
                           break;
    case mBioCheckOut      :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (14*DISP_STRG_SIZE));    //Controllo  biom.
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (19*DISP_STRG_SIZE));    //in  uscita
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rBioCheckOut    ");
                           break;
    case mBioNok          :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (14*DISP_STRG_SIZE));    //Controllo  biom.
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (16*DISP_STRG_SIZE));    //negativo
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rBioNok         ");
                           break;
    case mInVisEx         :

                            sprintf(Riga2, pDispMask + (1*DISP_STRG_SIZE));   //"                "
                           if (VisExEn != 0){
                                BinBcdAscii((unsigned char) CountTVisEx);
                                Riga2[5] = BcdBuff[7];
                                Riga2[6] = BcdBuff[8];
                                Riga2[8] = 's'; Riga2[9] = 'e'; Riga2[10] = 'c';
                           }
                           else{
                                sprintf(Riga2, pDispStrg + (54*DISP_STRG_SIZE));    //Disabilitato
                           }

                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (177*DISP_STRG_SIZE));    //Controllo  Visivo
                           Row2((AT91PS_USART)DBGU_pt, Riga2);                          //count down
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rVisualExamIn   ");
                           break;

    case mInAbort         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (17*DISP_STRG_SIZE));    //Fine transito
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (15*DISP_STRG_SIZE));    //in  ingresso
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rInAbort        ");
                           break;
    case mInAbortPresent  :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (18*DISP_STRG_SIZE));    //Mancato transito
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (15*DISP_STRG_SIZE));    //in  ingresso
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rInAbortPresent ");
                           break;
    case mInAbortNotPresent:
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (0*DISP_STRG_SIZE));     //Auto Ingresso
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (2*DISP_STRG_SIZE));     //P. esterna aperta
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rInAbortNotPres ");
                           break;
    case mInAbortBioPresent:
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (14*DISP_STRG_SIZE));    //Controllo  biom.
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (16*DISP_STRG_SIZE));    //negativo
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rInAbortBioNok  ");
                           break;
    case mInAbortBioNotPresent:
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (0*DISP_STRG_SIZE));     //Auto Ingresso
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (2*DISP_STRG_SIZE));     //P. esterna aperta
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rInAbortBioNPres");
                           break;
    case mInAbortVisExPres:
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (177*DISP_STRG_SIZE));   //Controllo  visivo.
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (16*DISP_STRG_SIZE));    //negativo
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rInAbortVisExNok");
                           break;
    case mInAbortVisExNotPres:
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (0*DISP_STRG_SIZE));     //Auto Ingresso
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (2*DISP_STRG_SIZE));     //P. esterna aperta
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rInAbortVisExNPr");
                           break;
    case mOutAbort        :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (17*DISP_STRG_SIZE));    //Fine transito
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (19*DISP_STRG_SIZE));    //in  uscita
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rOutAbort       ");
                           break;
     case mOutAbortPresent  :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (18*DISP_STRG_SIZE));    //Mancato transito
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (19*DISP_STRG_SIZE));    //in  uscita
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rOutAbortPresent");
                           break;
    case mMInWait2        :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (20*DISP_STRG_SIZE));    //Manuale Ingresso
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (1*DISP_STRG_SIZE)); 	//P. interna aperta
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rManInWait2     ");
                           break;
    case mMInOpen1        :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (20*DISP_STRG_SIZE));    //Manuale Ingresso
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (2*DISP_STRG_SIZE));     //P. esterna aperta
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rManInOpen1     ");
                           break;
    case mMInClose1       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (20*DISP_STRG_SIZE));    //Manuale Ingresso
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (3*DISP_STRG_SIZE));     //Pers. in  tunnel
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rManInClose1    ");
                           break;
    case mMInPause        :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (20*DISP_STRG_SIZE));    //Manuale Ingresso
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (4*DISP_STRG_SIZE));     //Controllo +P
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rManInPause     ");
                           break;
    case mMInControl      :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (20*DISP_STRG_SIZE));    //Manuale Ingresso
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (4*DISP_STRG_SIZE));     //Controllo +P
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rManInControl   ");
                           break;
    case mMInMoreAlarm    :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (20*DISP_STRG_SIZE));    //Manuale Ingresso
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (5*DISP_STRG_SIZE));     //Allarme +P
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rManInMoreAlarm ");
                           break;
    case mMIn             :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (20*DISP_STRG_SIZE));    //Manuale Ingresso
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (3*DISP_STRG_SIZE));     //Pers. in  tunnel
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMIn            ");
                           break;
    case mMInOpen2        :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (20*DISP_STRG_SIZE));    //Manuale Ingresso
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (6*DISP_STRG_SIZE));     //Pers.in ingresso
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rManInOpen2     ");
                           break;
    case mMInClose2       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (20*DISP_STRG_SIZE));    //Manuale Ingresso
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (1*DISP_STRG_SIZE));     //P. interna aperta
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rManInClose2    ");
                           break;
    case mMOutWait1       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (21*DISP_STRG_SIZE));    //Manuale   Uscita
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (2*DISP_STRG_SIZE));     //P. esterna aperta
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rManOutWait1    ");
                           break;
    case mMOutOpen2       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (21*DISP_STRG_SIZE));    //Manuale   Uscita
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (1*DISP_STRG_SIZE));     //P. interna aperta
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rManOutOpen2    ");
                           break;
    case mMOutClose2      :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (21*DISP_STRG_SIZE));    //Manuale   Uscita
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (3*DISP_STRG_SIZE));     //Pers. in  tunnel
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rManOutClose2   ");
                           break;
    case mMOutPause       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (21*DISP_STRG_SIZE));    //Manuale   Uscita
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (4*DISP_STRG_SIZE));     //Controllo +P
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rManOutPause    ");
                           break;
    case mMOutControl     :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (21*DISP_STRG_SIZE));    //Manuale   Uscita
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (4*DISP_STRG_SIZE));     //Controllo +P
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rManOutControl  ");
                           break;
    case mMOutMoreAlarm   :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (21*DISP_STRG_SIZE));    //Manuale   Uscita
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (5*DISP_STRG_SIZE));     //Allarme +P
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rManOutMoreAlar ");
                           break;
    case mMOut		      :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (21*DISP_STRG_SIZE));    //Manuale   Uscita
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (3*DISP_STRG_SIZE));     //Pers. in  tunnel
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMOut           ");
                           break;
    case mMOutOpen1       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (21*DISP_STRG_SIZE));    //Manuale   Uscita
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (8*DISP_STRG_SIZE));     //Pers.in uscita
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rManOutOpen1    ");
                           break;
    case mMOutClose1      :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (21*DISP_STRG_SIZE));    //Manuale   Uscita
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (2*DISP_STRG_SIZE));     //P. esterna aperta
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rManOutClose1   ");
                           break;
    case mMChangeDir1     :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (21*DISP_STRG_SIZE));    //Manuale   Uscita
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (2*DISP_STRG_SIZE));     //P. esterna aperta
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rManChangeDir1  ");
                           break;
    case mMChangeDir2     :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (20*DISP_STRG_SIZE));    //Manuale Ingresso
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (1*DISP_STRG_SIZE));     //P. interna aperta
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rManChangeDir2  ");
                           break;

    case mMorningIdle     :
                             if (LastMsgIdSys[Sys] != MsgId)                      //clear buffer Riga2 @first idle
                                sprintf(Riga2,pDispMask + (1*DISP_STRG_SIZE));   //"                "
                           if (TReadClk >490)
                           {
//                            TReadClk= 0;
//                            Clk_Read();
                            LegalHour();
                            sprintf(Riga1,(const char*) pDispStrg + (106*DISP_STRG_SIZE));  //"Mattino   /  /  "
                            							
							if(ViewMode==0) { 		//ViewMode GG/MM/AA 		                       
								strncpy(&Riga1[8],(char const*) &DatePLL[0], 2); 			                   
								strncpy(&Riga1[11],(char const*) &MonthPLL[0], 2);
 		                    }
   		                    else {    	                       
								strncpy(&Riga1[8],(char const*) &MonthPLL[0], 2);     			               
								strncpy(&Riga1[11],(char const*) &DatePLL[0], 2);
 		                    }                           
							strncpy(&Riga1[14],(char const*) &YearPLL[0], 2);

                            sprintf(Riga2,(const char*) pDispMask + (0*DISP_STRG_SIZE));	//"          :  :  "

							
							strncpy(&Riga2[0],(char const*) &DayName[0], 3);                    		
							strncpy(&Riga2[8],(char const*) &HourPLL[0], 2);		                 
							strncpy(&Riga2[11],(char const*) &MinPLL[0], 2);		                   
							strncpy(&Riga2[14],(char const*) &SecPLL[0], 2);
                           }
                           Row1((AT91PS_USART)DBGU_pt, Riga1);
                           Row2((AT91PS_USART)DBGU_pt, Riga2);
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMorningIdle    ");
                           if ((Orario[2] == BinBcd2(HHNight)) &&
                              (Orario[1] == BinBcd2(MMNight)) &&
                              (Orario[0] <= 0x20))
                           {
                            if ((HHNight != 0) || (MMNight != 0))
                            {
                              SEQ_AddEvent(eNight,Sys);
                            }
                           }
                           break;
    case mMorningPresent  :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (22*DISP_STRG_SIZE));    //Porte chiuse
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (3*DISP_STRG_SIZE));     //Pers. in  tunnel
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMorningPresent ");
                           break;
    case mMorningError    :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (22*DISP_STRG_SIZE));    //Porte chiuse
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (3*DISP_STRG_SIZE));     //Pers. in  tunnel
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMorningError   ");
                           break;
    case mEmergency       :
                           if(EmeMode==1)
                           	Row1((AT91PS_USART)DBGU_pt, pDispStrg + (2*DISP_STRG_SIZE));    //P. esterna aperta
                           else if (EmeMode==2)	
                           	Row1((AT91PS_USART)DBGU_pt, pDispStrg + (1*DISP_STRG_SIZE));    //P. interna aperta
                           else
                           	Row1((AT91PS_USART)DBGU_pt, pDispStrg + (23*DISP_STRG_SIZE));   //Porte aperte	
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (24*DISP_STRG_SIZE));    //in emergenza
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rEmergency      ");
                           break;
    case mSodCheck       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (25*DISP_STRG_SIZE));    //Reset
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (26*DISP_STRG_SIZE));    //impianto
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rAPDCheck       ");
                           break;
    case mSodPause        :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (27*DISP_STRG_SIZE));    //Pausa prereset
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (26*DISP_STRG_SIZE));    //impianto
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rAPDPause       ");
                           break;
    case mSodReset        :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (25*DISP_STRG_SIZE));    //Reset
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (26*DISP_STRG_SIZE));    //impianto
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rAPDReset       ");
                           break;

    case mNightIdle       :
                            if (LastMsgIdSys[Sys] != MsgId)                      //clear buffer Riga2 @first idle
                            sprintf(Riga2,pDispMask + (1*DISP_STRG_SIZE));   //"                "
                            if (TReadClk >490)
                           {
//                            TReadClk= 0;
//                            Clk_Read();
                            LegalHour();
                            sprintf(Riga1, (const char*) pDispStrg + (107*DISP_STRG_SIZE)); //"Notte     /  /  "
                           if(ViewMode==0) { 		//ViewMode GG/MM/AA 		                       
								strncpy(&Riga1[8],(char const*) &DatePLL[0], 2); 			                   
								strncpy(&Riga1[11],(char const*) &MonthPLL[0], 2);
 		                    }
   		                    else {    	                        
								strncpy(&Riga1[8],(char const*) &MonthPLL[0], 2);     			                
								strncpy(&Riga1[11],(char const*) &DatePLL[0], 2);
 		                    }                            
							strncpy(&Riga1[14],(char const*) &YearPLL[0], 2);

                            sprintf(Riga2,(const char*) pDispMask + (0*DISP_STRG_SIZE));    //"          :  :  "
                            
							strncpy(&Riga2[0],(char const*) &DayName[0], 3);		                   
							strncpy(&Riga2[8],(char const*) &HourPLL[0], 2);		                  
							strncpy(&Riga2[11],(char const*) &MinPLL[0], 2);		                   
							strncpy(&Riga2[14],(char const*) &SecPLL[0], 2);

                            if ((BinBcd2(HHDay) == Orario[2]) &&
                                (BinBcd2(MMDay) == Orario[1]) &&
                                (Orario[0] <= 0x20))
                            {
                            	if ((HHDay != 0) || (MMDay != 0))
                            	{
	                              CfgDay = CfgR(aSun+Orario[3]-1);
	                              if (CfgDay  == 1)
	                              {
	                                SEQ_AddEvent(eMorning,Sys);
	                              }
                              }
                            }
                           }

                           Row1((AT91PS_USART)DBGU_pt, Riga1);
                           Row2((AT91PS_USART)DBGU_pt, Riga2);
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rNightIdle      ");
                           break;
    case mNightClose2     :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (28*DISP_STRG_SIZE));    //Notte
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (1*DISP_STRG_SIZE));     //P. interna aperta
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rNightClose2    ");
                           break;
    case mNightClose1     :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (28*DISP_STRG_SIZE));    //Notte
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (2*DISP_STRG_SIZE));     //P. esterna aperta
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rNightClose1    ");
                           break;
    case mNightChkObject  :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (28*DISP_STRG_SIZE));    //Notte
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (12*DISP_STRG_SIZE));    //Verifica Oggetto
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rNightChkObject ");
                           break;
    case mNightOpenAll    :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (28*DISP_STRG_SIZE));    //Notte
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (23*DISP_STRG_SIZE));    //Porte aperte
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rNightOpenAll   ");
                           break;
    case mNightPresent    :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (22*DISP_STRG_SIZE));    //Porte chiuse
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (3*DISP_STRG_SIZE));     //Pers. in  tunnel
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rNightPresent   ");
                           break;
    case mNightError      :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (22*DISP_STRG_SIZE));    //Porte chiuse
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (3*DISP_STRG_SIZE));     //Pers. in  tunnel
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rNightError     ");
                           break;
    case mDoorsForced     :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (29*DISP_STRG_SIZE));    //Alluser
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (30*DISP_STRG_SIZE));    // Industrie PD
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rDoorsForced    ");
                           break;
    case mServiceMode     :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (152*DISP_STRG_SIZE));   //Service Mode
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (1*DISP_STRG_SIZE));     //P.interna aperta
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rServiceMode    ");
                           break;
    case mProgOn1         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (31*DISP_STRG_SIZE));    //PLL v.1.8.3
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (32*DISP_STRG_SIZE));    //Opzioni
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rProgOn         ");
                           break;
    case mFuncBtn         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (110*DISP_STRG_SIZE));   //Menu
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (111*DISP_STRG_SIZE));   //Pulsanti Func
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rFunctionButton ");
                           break;
    case mBiometric       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (110*DISP_STRG_SIZE)); 	//Menu
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (35*DISP_STRG_SIZE));    //Biometrico
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rBiometric      ");
                           break;
    case mAccessMode      :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (110*DISP_STRG_SIZE));   //Menu
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (113*DISP_STRG_SIZE));   //Modo Transito
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rAccessMode     ");
                           break;
    case mDateTime        :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (110*DISP_STRG_SIZE));   //Menu
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (114*DISP_STRG_SIZE));   //Data/Ora
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rDateTime       ");
                           break;
    case mDateT           :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (110*DISP_STRG_SIZE));   //Menu
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (118*DISP_STRG_SIZE));   //Data
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rDateT          ");
                           break;
    case mTime            :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (110*DISP_STRG_SIZE));   //Menu
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (119*DISP_STRG_SIZE));   //Ora
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rTime           ");
                           break;
    case mTunnel          :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (110*DISP_STRG_SIZE));   //Menu
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (112*DISP_STRG_SIZE));   //Tunnel
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rTunnel         ");
                           break;
    case mEnglish         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (115*DISP_STRG_SIZE));           //Lingua:
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (117*DISP_STRG_SIZE),FlashOn);   //Inglese
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rLanguage       ");
                           break;
    case mItaliano        :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (115*DISP_STRG_SIZE));           //Lingua:
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (116*DISP_STRG_SIZE),FlashOn);   //Italiano
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rLanguage       ");
                           break;
    case mBackUpCfg       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (110*DISP_STRG_SIZE));   //Menu
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (147*DISP_STRG_SIZE));   //Backup
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rBackup         ");
                           break;                                                                                                                                                                      		
    case mF1Off           :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (33*DISP_STRG_SIZE));            //Pulsante F1
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (34*DISP_STRG_SIZE),FlashOn);    //Nessuna
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rF1 Off         ");
                           break;
    case mF1Bio           :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (33*DISP_STRG_SIZE));            //Pulsante F1
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (35*DISP_STRG_SIZE),FlashOn);    //Biometrico
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rF1 Biometric   ");
                           break;
    case mF1Emergency     :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (33*DISP_STRG_SIZE));            //Pulsante F1
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (36*DISP_STRG_SIZE),FlashOn);    //Emergenza
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rF1 Emergency   ");
                           break;
    case mF1In            :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (33*DISP_STRG_SIZE));            //Pulsante F1
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (37*DISP_STRG_SIZE),FlashOn);    //Solo uscita
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rF1 Only out    ");
                           break;
    case mF1Metal         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (33*DISP_STRG_SIZE));            //Pulsante F1
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (38*DISP_STRG_SIZE),FlashOn);    //Metal
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rF1 Metal       ");
                           break;
    case mF1More          :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (33*DISP_STRG_SIZE));            //Pulsante F1
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (39*DISP_STRG_SIZE),FlashOn);    //+ Persone
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rF1 More        ");
                           break;
    case mF1VisEx         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (33*DISP_STRG_SIZE));            //Pulsante F1
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (177*DISP_STRG_SIZE),FlashOn);   //Controllo Visivo
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rF1 Visual Exam.");
                           break;
    case mF2Off           :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (40*DISP_STRG_SIZE));            //Pulsante F2
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (34*DISP_STRG_SIZE),FlashOn);    //Nessuna
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rF2 Off         ");
                           break;
    case mF2Bio           :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (40*DISP_STRG_SIZE));            //Pulsante F2
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (35*DISP_STRG_SIZE),FlashOn);    //Biometrico
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rF2 Biometric   ");
                           break;
    case mF2Emergency     :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (40*DISP_STRG_SIZE));            //Pulsante F2
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (36*DISP_STRG_SIZE),FlashOn);    //Emergenza
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rF2 Emergency   ");
                           break;
    case mF2In            :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (40*DISP_STRG_SIZE));            //Pulsante F2
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (37*DISP_STRG_SIZE),FlashOn);    //Solo uscita
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rF2 Only out    ");
                           break;
    case mF2Metal         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (40*DISP_STRG_SIZE));            //Pulsante F2
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (38*DISP_STRG_SIZE),FlashOn);    //Metal
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rF2 Metal       ");
                           break;
    case mF2More          :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (40*DISP_STRG_SIZE));            //Pulsante F2
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (39*DISP_STRG_SIZE),FlashOn);    //+ Persone
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rF2 More        ");
                           break;
    case mF2VisEx         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (40*DISP_STRG_SIZE));            //Pulsante F2
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (177*DISP_STRG_SIZE),FlashOn);   //Controllo Visivo
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rF2 Visual Exam.");
                           break;
    case mF3Off           :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (41*DISP_STRG_SIZE));            //Pulsante F3
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (34*DISP_STRG_SIZE),FlashOn);    //Nessuna
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rF3 Off         ");
                           break;
    case mF3Bio           :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (41*DISP_STRG_SIZE));            //Pulsante F3
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (35*DISP_STRG_SIZE),FlashOn);    //Biometrico
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rF3 Biometric   ");
                           break;
    case mF3Emergency     :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (41*DISP_STRG_SIZE));            //Pulsante F3
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (36*DISP_STRG_SIZE),FlashOn);    //Emergenza
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rF3 Emergency   ");
                           break;
    case mF3In            :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (41*DISP_STRG_SIZE));            //Pulsante F3
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (37*DISP_STRG_SIZE),FlashOn);    //Solo uscita
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rF3 Only out    ");
                           break;
    case mF3Metal         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (41*DISP_STRG_SIZE));            //Pulsante F3
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (38*DISP_STRG_SIZE),FlashOn);    //Metal
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rF3 Metal       ");
                           break;
    case mF3More          :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (41*DISP_STRG_SIZE));            //Pulsante F3
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (39*DISP_STRG_SIZE),FlashOn);    //+ Persone
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rF3 More        ");
                           break;
    case mF3VisEx         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (41*DISP_STRG_SIZE));            //Pulsante F3
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (177*DISP_STRG_SIZE),FlashOn);   //Controllo Visivo
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rF3 Visual Exam.");
                           break;
    case mBid             :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (42*DISP_STRG_SIZE));            //Tipo Tunnel
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (43*DISP_STRG_SIZE),FlashOn);    //Bidirezionale
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rBidirectional  ");
                           break;
    case mOnlyIn          :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (42*DISP_STRG_SIZE));            //Tipo Tunnel
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (44*DISP_STRG_SIZE),FlashOn);    //Solo Ingresso
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rOnlyIn         ");
                           break;
    case mOnlyOut         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (42*DISP_STRG_SIZE));            //Tipo Tunnel
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (45*DISP_STRG_SIZE),FlashOn);    //Solo Uscita
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rOnlyOut        ");
                           break;
    case mDoorsClsd       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (46*DISP_STRG_SIZE));            //Porte aperte:
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (47*DISP_STRG_SIZE),FlashOn);    //Nessuna
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rDayClosed      ");
                           break;
    case mDoorOpnP1       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (46*DISP_STRG_SIZE));            //Porte aperte:
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (48*DISP_STRG_SIZE),FlashOn);    //Solo Porta Ext.
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rDayOpen P2     ");
                           break;
    case mDoorOpnP2       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (46*DISP_STRG_SIZE));            //Porte aperte:
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (125*DISP_STRG_SIZE),FlashOn);   //Solo Porta Int.
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rDayOpen P1     ");
                           break;

    case mNightNc         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (49*DISP_STRG_SIZE));            //Porta interna
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (50*DISP_STRG_SIZE),FlashOn);    //Chiusa di notte
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rNightClosed    ");
                           break;
    case mDoorOpnN        :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (49*DISP_STRG_SIZE));            //Porta interna
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (51*DISP_STRG_SIZE),FlashOn);    //Aperta di notte
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rNightOpen      ");
                           break;
    case mMoreInOn        :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (52*DISP_STRG_SIZE));            //+P in ingresso
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (53*DISP_STRG_SIZE),FlashOn);    //Abilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMoreInOn       ");
                           break;
    case mMoreInOff       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (52*DISP_STRG_SIZE));            //+P in ingresso
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (54*DISP_STRG_SIZE),FlashOn);    //Disabilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMoreInOff      ");
                           break;
    case mMoreOutOn       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (55*DISP_STRG_SIZE));            //+P  in  uscita
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (53*DISP_STRG_SIZE),FlashOn);    //Abilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMoreOutOn      ");
                           break;
    case mMoreOutOff      :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (55*DISP_STRG_SIZE));            //+P  in  uscita
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (54*DISP_STRG_SIZE),FlashOn);    //Disabilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMoreOutOff     ");
                           break;

    case mEnSynHallOn 	 :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (145*DISP_STRG_SIZE));           //Tappeto 2 Zone
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (80*DISP_STRG_SIZE),FlashOn);    //Abilitata
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rEnSynHallOn   ");
                           break;
    case mEnSynHallOff	 :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (145*DISP_STRG_SIZE));           //Tappeto 2 Zone
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (58*DISP_STRG_SIZE),FlashOn);    //Disabilitata
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rEnSynHallOff  ");
                           break;
    case mFInMetalOn      :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (56*DISP_STRG_SIZE));            //Metal in pr.ing.
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (53*DISP_STRG_SIZE),FlashOn);    //Abilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rFInMetalOn     ");
                           break;
    case mFInMetalOff     :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (56*DISP_STRG_SIZE));            //Metal in pr.ing.
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (54*DISP_STRG_SIZE),FlashOn);    //Disabilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rFInMetalOff    ");
                           break;
    case mFInMoreOn       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (138*DISP_STRG_SIZE));           //+P in pr.ing.
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (53*DISP_STRG_SIZE),FlashOn);    //Abilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rFInMetalOn     ");
                           break;
    case mFInMoreOff      :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (138*DISP_STRG_SIZE));           //+P in pr.ing.
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (54*DISP_STRG_SIZE),FlashOn);    //Disabilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rFInMetalOff    ");
                           break;
    case mLOutEmOn        :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (57*DISP_STRG_SIZE));            //Ult.uscita in em
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (80*DISP_STRG_SIZE),FlashOn);    //Abilitata
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rLOutEmOn       ");
                           break;
    case mLOutEmOff       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (57*DISP_STRG_SIZE));            //Ult.uscita in em
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (58*DISP_STRG_SIZE),FlashOn);    //Disabilitata
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rLOutEmOff      ");
                           break;
    case mBlkAlarmRedOn   :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (59*DISP_STRG_SIZE));            //Bloc. in pr.ing.
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (53*DISP_STRG_SIZE),FlashOn);    //Abilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rBlkAlarmRedOn  ");
                           break;
    case mBlkAlarmRedOff  :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (59*DISP_STRG_SIZE));            //Bloc. in pr.ing.
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (54*DISP_STRG_SIZE),FlashOn);    //Disabilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rBlkAlarmRedOff ");
                           break;
    case mYear            :
                           sprintf(Riga2,(const char*) pDispMask + (1*DISP_STRG_SIZE));     //"                "
                           Orario[0]=BinBcd2(CfgV);
                           Riga2[7] = ((Orario[0] & 0xF0) >> 4) | 0x30;
                           Riga2[8] = (Orario[0] & 0x0F)       | 0x30;
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (60*DISP_STRG_SIZE));    //Anno
                           Fls2((AT91PS_USART)DBGU_pt, Riga2,FlashOn);
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rYear           ");
                           break;
    case mMonth           :
                           sprintf(Riga2,(const char*) pDispMask + (1*DISP_STRG_SIZE));     //"                "
                           Orario[0]=BinBcd2(CfgV);
                           Riga2[7] = ((Orario[0] & 0xF0) >> 4) | 0x30;
                           Riga2[8] = (Orario[0] & 0x0F)       | 0x30;
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (61*DISP_STRG_SIZE));    //Mese
                           Fls2((AT91PS_USART)DBGU_pt, Riga2,FlashOn);
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMonth          ");
                           break;
    case mDate            :
                           sprintf(Riga2,(const char*) pDispMask + (1*DISP_STRG_SIZE));     //"                "
                           Orario[0]=BinBcd2(CfgV);
                           Riga2[7] = ((Orario[0] & 0xF0) >> 4) | 0x30;
                           Riga2[8] = (Orario[0] & 0x0F)       | 0x30;
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (62*DISP_STRG_SIZE));    //Giorno  (1-31)
                           Fls2((AT91PS_USART)DBGU_pt, Riga2,FlashOn);
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rDate           ");
                           break;
    case mDay             :
                           sprintf(Riga2,(const char*) pDispMask + (1*DISP_STRG_SIZE));     //"                "
                           Orario[0]=BinBcd2(CfgV);
                           switch (CfgV)
                           {
                             case  1:							   
							   strncpy(&Riga2[6], &pDispStrg[(108*DISP_STRG_SIZE)+0], 3);	//Dom
							   break;
                             case  2:							   
							   strncpy(&Riga2[6], &pDispStrg[(108*DISP_STRG_SIZE)+3], 3);	//Lun
							   break;
                             case  3:							                                   
							   strncpy(&Riga2[6], &pDispStrg[(108*DISP_STRG_SIZE)+6], 3);	//Mar
							   break;
                             case  4:
							   strncpy(&Riga2[6], &pDispStrg[(108*DISP_STRG_SIZE)+9], 3);	//Mer
							   break;
                             case  5:							                                  
							   strncpy(&Riga2[6], &pDispStrg[(108*DISP_STRG_SIZE)+12], 3);	//Gio
							   break;
                             case  6:                                 
							   strncpy(&Riga2[6], &pDispStrg[(109*DISP_STRG_SIZE)+0], 3);	//Ven
							   break;
                             case  7:                                 
							   strncpy(&Riga2[6], &pDispStrg[(109*DISP_STRG_SIZE)+3], 3);	//Sab
							   break;
							 default:
							   
							   strncpy(&Riga2[6],"Err", 3);
                           }
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (63*DISP_STRG_SIZE));    //Giorno (Lun-Dom)
                           Fls2((AT91PS_USART)DBGU_pt, Riga2,FlashOn);
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rDay            ");
                           break;
   case mViewModeEU      :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (168*DISP_STRG_SIZE));           //Visualizza Data
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (169*DISP_STRG_SIZE),FlashOn);   //GG/MM/AA
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rViewDateEU     ");
                           break;
   case mViewModeUK      :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (168*DISP_STRG_SIZE));           //Visualizza Data
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (170*DISP_STRG_SIZE),FlashOn);    //MM/GG/AA
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rViewDateUK     ");
                           break;

    case mHour            :
                           sprintf(Riga2,(const char*) pDispMask + (1*DISP_STRG_SIZE));     //"                "
                           Orario[0]=BinBcd2(CfgV);
                           Riga2[7] = ((Orario[0] & 0xF0) >> 4) | 0x30;
                           Riga2[8] = (Orario[0] & 0x0F)       | 0x30;
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (64*DISP_STRG_SIZE));    //Ore
                           Fls2((AT91PS_USART)DBGU_pt, Riga2,FlashOn);
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rHour           ");
                           break;
    case mMin             :
                           sprintf(Riga2,(const char*) pDispMask + (1*DISP_STRG_SIZE));     //"                "
                           Orario[0]=BinBcd2(CfgV);
                           Riga2[7] = ((Orario[0] & 0xF0) >> 4) | 0x30;
                           Riga2[8] = (Orario[0] & 0x0F)       | 0x30;
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (65*DISP_STRG_SIZE));    //Minuti
                           Fls2((AT91PS_USART)DBGU_pt, Riga2,FlashOn);
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMin            ");
                           break;
  case mSec               :
                           sprintf(Riga2,(const char*) pDispMask + (1*DISP_STRG_SIZE));     //"                "
                           Orario[0]=BinBcd2(CfgV);
                           Riga2[7] = ((Orario[0] & 0xF0) >> 4) | 0x30;
                           Riga2[8] = (Orario[0] & 0x0F)       | 0x30;
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (66*DISP_STRG_SIZE));    //Secondi
                           Fls2((AT91PS_USART)DBGU_pt, Riga2,FlashOn);
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rSec            ");
                           break;
  case mHHMMNight         :
                           sprintf(Riga2,(const char*) pDispMask + (2*DISP_STRG_SIZE));     //"       :        "
                           BinBcdAscii2(HHNight);                           				
                           strncpy(&Riga2[5],(const char*) &BcdBuff[0], 2);
						   BinBcdAscii2(MMNight);                         
						    strncpy(&Riga2[8],(const char*) &BcdBuff[0], 2);
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (67*DISP_STRG_SIZE));    //Inizio Notte
                           Row2((AT91PS_USART)DBGU_pt, Riga2);
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rNight          ");
                           break;
  case mHHNight           :
                           sprintf(Riga2,(const char*) pDispMask + (2*DISP_STRG_SIZE));     //"       :        "
                           BinBcdAscii2(HHNight);
                           if (T400)
                           {                             
							 strncpy(&Riga2[5],(const char*) &BcdBuff[0], 2);
                           }
                           else
                           {                             
							  strncpy(&Riga2[5],(const char*) pDispMask + (2*DISP_STRG_SIZE), 2);
                           }
                           BinBcdAscii2(MMNight);                          
						   strncpy(&Riga2[8],(char const*)&BcdBuff[0], 2);
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (67*DISP_STRG_SIZE));    //Inizio Notte
                           Row2((AT91PS_USART)DBGU_pt, Riga2);
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rNight hh       ");
                           break;
  case mMMNight           :
                           sprintf(Riga2,(const char*) pDispMask + (2*DISP_STRG_SIZE));     //"       :        "
                           BinBcdAscii2(HHNight);                           
						   strncpy(&Riga2[5],(char const*)&BcdBuff[0], 2);
                           BinBcdAscii2(MMNight);
                           if (T400)
                           {                             
							 strncpy(&Riga2[8],(char const*)&BcdBuff[0], 2);
                           }
                           else
                           {                           
							 strncpy(&Riga2[5],(const char*) pDispMask + (2*DISP_STRG_SIZE), 2);
                           }
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (67*DISP_STRG_SIZE));    //Inizio Notte
                           Row2((AT91PS_USART)DBGU_pt, Riga2);
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rNight mm       ");
                           break;
  case mHHMMDay           :
                           sprintf(Riga2,(const char*) pDispMask + (2*DISP_STRG_SIZE));     //"       :        "
                           BinBcdAscii2(HHDay);                           
						   strncpy(&Riga2[5],(char const*)&BcdBuff[0], 2);
                           BinBcdAscii2(MMDay);                           
						   strncpy(&Riga2[8],(char const*)&BcdBuff[0], 2);
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (68*DISP_STRG_SIZE));    //Inizio Mattino
                           Row2((AT91PS_USART)DBGU_pt, Riga2);
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMorning        ");
                           break;
  case mHHDay             :
                           sprintf(Riga2,(const char*) pDispMask + (2*DISP_STRG_SIZE));     //"       :        "
                           BinBcdAscii2(HHDay);
                           if (T400)
                           {                            
							 strncpy(&Riga2[5],(char const*)&BcdBuff[0], 2);
                           }
                           else
                           {                            
							 strncpy(&Riga2[5],(const char*) pDispMask + (2*DISP_STRG_SIZE), 2);
                           }
                           BinBcdAscii2(MMDay);                           
						   strncpy(&Riga2[8],(char const*)&BcdBuff[0], 2);
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (68*DISP_STRG_SIZE));    //Inizio Mattino
                           Row2((AT91PS_USART)DBGU_pt, Riga2);
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMorning hh     ");
                           break;
  case mMMDay             :
                           sprintf(Riga2,(const char*) pDispMask + (2*DISP_STRG_SIZE));     //"       :        "
                           BinBcdAscii2(HHDay);
                          
						   strncpy(&Riga2[5],(char const*)&BcdBuff[0], 2);
                           BinBcdAscii2(MMDay);
                           if (T400)
                           {                             
							 strncpy(&Riga2[8],(char const*)&BcdBuff[0], 2);
                           }
                           else
                           {                            
							 strncpy(&Riga2[5],(const char*) pDispMask + (2*DISP_STRG_SIZE), 2);
                           }
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (68*DISP_STRG_SIZE));    //Inizio Mattino
                           Row2((AT91PS_USART)DBGU_pt, Riga2);
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMorning mm     ");
                           break;
  case mCAM               :
                           sprintf(Riga2,pDispMask + (1*DISP_STRG_SIZE));   //"                "
                           BinBcdAscii(CfgCAM);
                           Riga2[7] = BcdBuff[7];
                           Riga2[8] = BcdBuff[8];
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (69*DISP_STRG_SIZE));    //Numero CAM
                           Fls2((AT91PS_USART)DBGU_pt, Riga2,FlashOn);
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rCAM            ");
                           break;
  case mWeek              :
                           sprintf(Riga2,pDispMask + (1*DISP_STRG_SIZE));   //"                "
                           Riga2[1] = CfgMon;
                           Riga2[3] = CfgTue;
                           Riga2[5] = CfgWed;
                           Riga2[7] = CfgThu;
                           Riga2[9] = CfgFri;
                           Riga2[11] = CfgSat;
                           Riga2[13] = CfgSun;
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (70*DISP_STRG_SIZE));    // L M M G V S D
                           Row2((AT91PS_USART)DBGU_pt, Riga2);
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rWeek           ");
                           break;
  case mMon               :
                           sprintf(Riga2,pDispMask + (1*DISP_STRG_SIZE));   //"                "
                           if (T400)
                           {
                            Riga2[1] = CfgMon;
                           }
                           else
                           {
                            Riga2[1] = ' ';
                           }
                           Riga2[3] = CfgTue;
                           Riga2[5] = CfgWed;
                           Riga2[7] = CfgThu;
                           Riga2[9] = CfgFri;
                           Riga2[11] = CfgSat;
                           Riga2[13] = CfgSun;
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (70*DISP_STRG_SIZE));    // L M M G V S D
                           Row2((AT91PS_USART)DBGU_pt, Riga2);
                           break;
  case mTue               :
                           sprintf(Riga2,pDispMask + (1*DISP_STRG_SIZE));   //"                "
                           Riga2[1] = CfgMon;
                           if (T400)
                           {
                            Riga2[3] = CfgTue;
                           }
                           else
                           {
                            Riga2[3] = ' ';
                           }
                           Riga2[5] = CfgWed;
                           Riga2[7] = CfgThu;
                           Riga2[9] = CfgFri;
                           Riga2[11] = CfgSat;
                           Riga2[13] = CfgSun;
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (70*DISP_STRG_SIZE));    // L M M G V S D
                           Row2((AT91PS_USART)DBGU_pt, Riga2);
                           break;
    case mWed             :
                           sprintf(Riga2,pDispMask + (1*DISP_STRG_SIZE));   //"                "
                           Riga2[1] = CfgMon;
                           Riga2[3] = CfgTue;
                           if (T400)
                           {
                            Riga2[5] = CfgWed;
                           }
                           else
                           {
                            Riga2[5] = ' ';
                           }
                           Riga2[7] = CfgThu;
                           Riga2[9] = CfgFri;
                           Riga2[11] = CfgSat;
                           Riga2[13] = CfgSun;
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (70*DISP_STRG_SIZE));    // L M M G V S D
                           Row2((AT91PS_USART)DBGU_pt, Riga2);
                           break;
    case mThu             :
                           sprintf(Riga2,pDispMask + (1*DISP_STRG_SIZE));   //"                "
                           Riga2[1] = CfgMon;
                           Riga2[3] = CfgTue;
                           Riga2[5] = CfgWed;
                           if (T400)
                           {
                            Riga2[7] = CfgThu;
                           }
                           else
                           {
                            Riga2[7] = ' ';
                           }
                           Riga2[9] = CfgFri;
                           Riga2[11] = CfgSat;
                           Riga2[13] = CfgSun;
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (70*DISP_STRG_SIZE));    // L M M G V S D
                           Row2((AT91PS_USART)DBGU_pt, Riga2);
                           break;
    case mFri             :
                           sprintf(Riga2,pDispMask + (1*DISP_STRG_SIZE));   //"                "
                           Riga2[1] = CfgMon;
                           Riga2[3] = CfgTue;
                           Riga2[5] = CfgWed;
                           Riga2[7] = CfgThu;
                           if (T400)
                           {
                            Riga2[9] = CfgFri;
                           }
                           else
                           {
                            Riga2[9] = ' ';
                           }
                           Riga2[11] = CfgSat;
                           Riga2[13] = CfgSun;
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (70*DISP_STRG_SIZE));    // L M M G V S D
                           Row2((AT91PS_USART)DBGU_pt, Riga2);
                           break;
    case mSat             :
                           sprintf(Riga2,pDispMask + (1*DISP_STRG_SIZE));   //"                "
                           Riga2[1] = CfgMon;
                           Riga2[3] = CfgTue;
                           Riga2[5] = CfgWed;
                           Riga2[7] = CfgThu;
                           Riga2[9] = CfgFri;
                           if (T400)
                           {
                            Riga2[11] = CfgSat;
                           }
                           else
                           {
                            Riga2[11] = ' ';
                           }
                           Riga2[13] = CfgSun;
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (70*DISP_STRG_SIZE));    // L M M G V S D
                           Row2((AT91PS_USART)DBGU_pt, Riga2);
                           break;
    case mSun             :
                           sprintf(Riga2,pDispMask + (1*DISP_STRG_SIZE));   //"                "
                           Riga2[1] = CfgMon;
                           Riga2[3] = CfgTue;
                           Riga2[5] = CfgWed;
                           Riga2[7] = CfgThu;
                           Riga2[9] = CfgFri;
                           Riga2[11] = CfgSat;
                           if (T400)
                           {
                            Riga2[13] = CfgSun;
                           }
                           else
                           {
                            Riga2[13 ] = ' ';
                           }
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (70*DISP_STRG_SIZE));    // L M M G V S D
                           Row2((AT91PS_USART)DBGU_pt, Riga2);
                           break;
    case mBioInNone       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (71*DISP_STRG_SIZE));            //Biometrico ingr.
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (72*DISP_STRG_SIZE),FlashOn);    //Assente
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rBioInOff       ");
                           break;
    case mBioInOff        :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (71*DISP_STRG_SIZE));            //Biometrico ingr.
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (54*DISP_STRG_SIZE),FlashOn);    //Disabilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rBioInOff       ");
                           break;
    case mBioInOn         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (71*DISP_STRG_SIZE));            //Biometrico ingr.
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (53*DISP_STRG_SIZE),FlashOn);    //Abilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rBioInOn        ");
                           break;
    case mBioInDisOff     :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (139*DISP_STRG_SIZE));			//Biometrico ingr. Dis. off
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (54*DISP_STRG_SIZE),FlashOn);	//Disabilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rBioInOff       ");
                           break;
    case mBioOutNone      :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (120*DISP_STRG_SIZE));			//Biometrico usc.
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (72*DISP_STRG_SIZE),FlashOn);	//Assente
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rBioInOff       ");
                           break;
    case mBioOutOff        :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (120*DISP_STRG_SIZE));			//Biometrico usc.
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (54*DISP_STRG_SIZE),FlashOn);	//Disabilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rBioOutOff       ");
                           break;
    case mBioOutOn         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (120*DISP_STRG_SIZE));			//Biometrico usc.
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (53*DISP_STRG_SIZE),FlashOn);	//Abilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rBioOutOn        ");
                           break;
    case mBioOutDisOff     :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (140*DISP_STRG_SIZE));			//Biometrico usc. Dis. off
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (54*DISP_STRG_SIZE),FlashOn);	//Disabilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rBioOutOff       ");
                           break;
    case mXChgInOff       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (73*DISP_STRG_SIZE));			    //Cambio pers. ing
                           if(Language==0) 		//Italiano
                            {
                            	Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (54*DISP_STRG_SIZE),FlashOn);	//Disabilitato
                            }
                            else
                            {
                            	Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (53*DISP_STRG_SIZE),FlashOn);   //Enabled
                            }
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rChangeInOff    ");
                           break;
    case mXChgInOn        :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (73*DISP_STRG_SIZE));			//Cambio pers. ing
                           if(Language==0) 		//Italiano
                            {
                            	Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (53*DISP_STRG_SIZE),FlashOn);	//Abilitato
                            }
                            else
                            {
                            	Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (54*DISP_STRG_SIZE),FlashOn);   //Disabled
                            }
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rChangeInOn     ");
                           break;
    case mXChgOutOff      :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (74*DISP_STRG_SIZE));			//Cambio pers. usc
                           if(Language==0) 		//Italiano
                            {
                            	Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (54*DISP_STRG_SIZE),FlashOn);	//Disabilitato
                            }
                            else
                            {
                            	Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (53*DISP_STRG_SIZE),FlashOn);   //Enabled
                            }
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rChangeOutOff   ");
                           break;
    case mXChgOutOn       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (74*DISP_STRG_SIZE));			//Cambio pers. usc
                           if(Language==0){ 		//Italiano
                               Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (53*DISP_STRG_SIZE),FlashOn);	//Abilitato
                           }
                           else{
                               Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (54*DISP_STRG_SIZE),FlashOn);   //Disabled
                           }
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rChangeOutOn    ");
                           break;
    case mBioNone         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (75*DISP_STRG_SIZE));			//PC  Biometrico
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (72*DISP_STRG_SIZE),FlashOn);	//Assente
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rBioNone        ");
                           break;
    case mBioFing         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (75*DISP_STRG_SIZE));			//PC  Biometrico
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (76*DISP_STRG_SIZE),FlashOn);	//Finger
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rBioFing        ");
                           break;
    case mBioFace         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (75*DISP_STRG_SIZE));			//PC  Biometrico
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (77*DISP_STRG_SIZE),FlashOn);	//Face
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rBioFace        ");
                           break;
    case mBioFingFace     :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (75*DISP_STRG_SIZE));			//PC  Biometrico
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (78*DISP_STRG_SIZE),FlashOn);	//Finger + Face
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rBioFingFace    ");
                           break;
    case mBioCnb          :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (75*DISP_STRG_SIZE));			//PC  Biometrico
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (121*DISP_STRG_SIZE),FlashOn);	//Connection Box
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rBio Conn. Box  ");
                           break;
    case mBioCnbTOut15    :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (122*DISP_STRG_SIZE));			//Tempo espulsione
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (123*DISP_STRG_SIZE),FlashOn);	//15 sec
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rTimeOutBio 15 s");
                           break;
    case mBioCnbTOut45    :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (122*DISP_STRG_SIZE));			//Tempo Espulsione
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (124*DISP_STRG_SIZE),FlashOn);	//45 sec
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rTimeOutBio 45 s");
                           break;
    case mBioCnbTOut1m    :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (122*DISP_STRG_SIZE));			//Tempo Espulsione
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (153*DISP_STRG_SIZE),FlashOn);	//1 min
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rTimeOutBio 1 m ");
                           break;
    case mBioCnbTOut2m    :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (122*DISP_STRG_SIZE));			//Tempo Espulsione
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (154*DISP_STRG_SIZE),FlashOn);	//2 min
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rTimeOutBio 2 m ");
                           break;
    case mBioCnbTOut3m    :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (122*DISP_STRG_SIZE));			//Tempo Espulsione
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (155*DISP_STRG_SIZE),FlashOn);	//3 min
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rTimeOutBio 3 m ");
                           break;
    case mBioCnbTOut4m    :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (122*DISP_STRG_SIZE));			//Tempo Espulsione
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (156*DISP_STRG_SIZE),FlashOn);	//4 min
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rTimeOutBio 4 m ");
                           break;
    case mBioCnbTOut5m    :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (122*DISP_STRG_SIZE));			//Tempo Espulsione
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (157*DISP_STRG_SIZE),FlashOn);	//5 min
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rTimeOutBio 5 m ");
                           break;
    case mLampOff         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (79*DISP_STRG_SIZE));			//Lampada accesa
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (58*DISP_STRG_SIZE),FlashOn);	//Disabilitata
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rLampOff        ");
                           break;
    case mLampOn          :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (79*DISP_STRG_SIZE));		    //Lampada accesa
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (80*DISP_STRG_SIZE),FlashOn);    //Abilitata
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rLampOn         ");
                           break;
    case mPanicBtnOff       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (146*DISP_STRG_SIZE));           //Puls. Antipanico
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (54*DISP_STRG_SIZE),FlashOn);    //Disabilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rPanic ButtonOff");
                           break;
    case mPanicBtnOn        :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (146*DISP_STRG_SIZE));           //Puls. Antipanico
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (53*DISP_STRG_SIZE),FlashOn);    //Abilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rPanic Button On");
                           break;
    case mSOD3            :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (126*DISP_STRG_SIZE));           //Durata ResetAPD
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (127*DISP_STRG_SIZE),FlashOn);   //3 sec
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rTresetAPD      ");
                           break;
    case mSOD6            :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (126*DISP_STRG_SIZE));           //Durata ResetAPD
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (128*DISP_STRG_SIZE),FlashOn);   //6 sec
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rTresetAPD      ");
                           break;
    case mIntraSOD15      :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (129*DISP_STRG_SIZE));           //Intra ResetAPD
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (130*DISP_STRG_SIZE),FlashOn);   //15 min
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rPeriodResetAPD ");
                           break;
    case mIntraSOD45      :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (129*DISP_STRG_SIZE));           //Intra ResetAPD
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (131*DISP_STRG_SIZE),FlashOn);   //45 min
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rPeriodResetAPD ");
                           break;
    case mIntraSOD240     :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (129*DISP_STRG_SIZE));           //Intra ResetAPD
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (132*DISP_STRG_SIZE),FlashOn);   //240 min
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rPeriodResetAPD ");
                           break;
    case mMemoOff         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (133*DISP_STRG_SIZE));           //Mem. Rich. Tr.
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (58*DISP_STRG_SIZE),FlashOn);    //Disabilitata
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMemoRequestOff ");
                           break;
    case mMemoOn1         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (133*DISP_STRG_SIZE));           //Mem. Rich. Tr.
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (134*DISP_STRG_SIZE),FlashOn);   //Abilitata 1Liv
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMemoRequest 1L ");
                           break;
    case mMemoOn2         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (133*DISP_STRG_SIZE));           //Mem. Rich. Tr.
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (135*DISP_STRG_SIZE),FlashOn);   //Abilitata 2Liv
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMemoRequest 2L ");
                           break;
    case mEnReadyOff      :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (179*DISP_STRG_SIZE));           //Ready X Rich.Tr.
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (58*DISP_STRG_SIZE),FlashOn);    //Disabilitata
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rEnReadyOff     ");
                           break;
    case mEnReadyOn       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (179*DISP_STRG_SIZE));           //Ready X Rich.Tr.
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (80*DISP_STRG_SIZE),FlashOn);    //Abilitata
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rEnReadyOn      ");
                           break;
    case mProxInOff       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (136*DISP_STRG_SIZE));           //Prox In
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (54*DISP_STRG_SIZE),FlashOn);    //Disabilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rInProx         ");
                           break;
    case mProxInOn        :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (136*DISP_STRG_SIZE));           //Prox In
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (53*DISP_STRG_SIZE),FlashOn);    //Abilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rInProx         ");
                           break;
    case mProxOutOff      :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (137*DISP_STRG_SIZE));           //Prox Out
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (54*DISP_STRG_SIZE),FlashOn);    //Disabilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rOutProxOff     ");
                           break;
    case mProxOutOn       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (137*DISP_STRG_SIZE));           //Prox Out
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (53*DISP_STRG_SIZE),FlashOn);    //Abilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rOutProxOn      ");
                           break;
    case mVfyOff          :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (81*DISP_STRG_SIZE));			//Verifica  autom.
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (58*DISP_STRG_SIZE),FlashOn);	//Disabilitata
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rVerifyOff      ");
                           break;
    case mVfyOn           :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (81*DISP_STRG_SIZE));			//Verifica  autom.
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (80*DISP_STRG_SIZE),FlashOn);	//Abilitata
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rVerifyOn       ");
                           break;

    case mVfyInOff       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (165*DISP_STRG_SIZE));			//Verif. Ogg. Ing.
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (58*DISP_STRG_SIZE),FlashOn);	//Disabilitata
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rVerifyInOff    ");
                           break;
    case mVfyInOn        :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (165*DISP_STRG_SIZE));			//Verif. Ogg. ing.
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (80*DISP_STRG_SIZE),FlashOn);	//Abilitata
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rVerifyInOn     ");
                           break;
    case mVfyOutOff       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (82*DISP_STRG_SIZE));			//Verif. Ogg. Usc.
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (58*DISP_STRG_SIZE),FlashOn);	//Disabilitata
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rVerifyOutOff   ");
                           break;
    case mVfyOutOn        :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (82*DISP_STRG_SIZE));			//Verif. Ogg. Usc.
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (80*DISP_STRG_SIZE),FlashOn);	//Abilitata
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rVerifyOutOn    ");
                           break;
    case mMailOff         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (83*DISP_STRG_SIZE));            //Funzione Posta
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (58*DISP_STRG_SIZE),FlashOn);    //Disabilitata
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMailOff        ");
                           break;
    case mMailOn          :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (83*DISP_STRG_SIZE));            //Funzione Posta
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (80*DISP_STRG_SIZE),FlashOn);    //Abilitata
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMailOn         ");
                           break;
    case mMOutAutoOff     :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (84*DISP_STRG_SIZE));			//Usc.automatica
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (58*DISP_STRG_SIZE),FlashOn);	//Disabilitata
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMOutAutoOff    ");
                           break;
    case mMOutAutoOn      :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (84*DISP_STRG_SIZE));            //Usc.automatica
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (80*DISP_STRG_SIZE),FlashOn);    //Abilitata
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMOutAutoOn     ");
                           break;
    case mPCBioOn         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (75*DISP_STRG_SIZE));    //PC  Biometrico
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (85*DISP_STRG_SIZE));	//acceso
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rPCBioOn        ");
                           break;
  /*  case mPCBioOff        :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (75*DISP_STRG_SIZE));		//PC  Biometrico
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (86*DISP_STRG_SIZE));		//spento
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rPCBioOff       ");
                           break;
	*/
    case mPCBioOnLine     :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (75*DISP_STRG_SIZE));    //PC  Biometrico
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (87*DISP_STRG_SIZE));    //on-line
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rPCBioOnLine    ");
                           break;
    case mPCBioOffLine    :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (75*DISP_STRG_SIZE));    //PC  Biometrico
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (88*DISP_STRG_SIZE));    //off-line
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rPCBioOffLine   ");
                           break;
    case mPCBioWaitOn     :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (75*DISP_STRG_SIZE));    //PC  Biometrico
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (89*DISP_STRG_SIZE));    //in  accensione
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rPCBioWaitOn    ");
                           break;
    case mPCBioWaitOff    :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (75*DISP_STRG_SIZE));    //PC  Biometrico
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (90*DISP_STRG_SIZE));    //in spegnimento
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rPCBioWaitOff   ");
                           break;
    case mMetalOff        :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (38*DISP_STRG_SIZE));    //Metal
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (54*DISP_STRG_SIZE));    //Disabilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMetalOff       ");
                           break;
    case mMetalOn         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (38*DISP_STRG_SIZE));    //Metal
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (53*DISP_STRG_SIZE));    //Abilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMetalOn        ");
                           break;
    case mBioOff          :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (14*DISP_STRG_SIZE));    //Controllo  biom
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (54*DISP_STRG_SIZE));    //Disabilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rControlBio Off ");
                           break;
    case mBioOn           :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (14*DISP_STRG_SIZE));    //Controllo  biom
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (53*DISP_STRG_SIZE));    //Abilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rControlBio On  ");
                           break;
    case mMoreOff         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (4*DISP_STRG_SIZE));     //Controllo +P
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (54*DISP_STRG_SIZE));    //Disabilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMoreOff        ");
                           break;
    case mMoreOn          :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (4*DISP_STRG_SIZE));     //Controllo +P
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (53*DISP_STRG_SIZE));    //Abilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMoreOn         ");
                           break;
    case mEmeP1P2         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (36*DISP_STRG_SIZE));            //Emergenza
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (91*DISP_STRG_SIZE),FlashOn);    //P1 P2
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rEmeP1P2        ");
                           break;
    case mEmeP1           :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (36*DISP_STRG_SIZE));            //Emergenza
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (92*DISP_STRG_SIZE),FlashOn);    //P1
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rEmeP1          ");
                           break;
    case mEmeP2           :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (36*DISP_STRG_SIZE));            //Emergenza
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (93*DISP_STRG_SIZE),FlashOn);    //P2
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rEmeP2          ");
                           break;
    case mEnProgOn		  :
    					   Row1((AT91PS_USART)DBGU_pt, pDispStrg + (143*DISP_STRG_SIZE));   //In Program
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (144*DISP_STRG_SIZE));   //EXT + INT
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rIn Program     ");
                           break;
    case mRestoredCfg     :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (148*DISP_STRG_SIZE));   //Restore
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (149*DISP_STRG_SIZE));   //Eseguito
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rRestored       ");
                           break;
    case mRestoreCfg       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (148*DISP_STRG_SIZE));           //Restore
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (150*DISP_STRG_SIZE),FlashOn);   //Esegui
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rRestore        ");
                           break;
    case mSavedCfg        :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (147*DISP_STRG_SIZE));   //Backup
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (149*DISP_STRG_SIZE));   //Eseguito
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rSaved          ");
                           break;
    case mSaveCfg         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (147*DISP_STRG_SIZE));           //Backup
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (150*DISP_STRG_SIZE),FlashOn);   //Esegui
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rSave           ");
                           break;                            	
    case mNightRSODOff    :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (151*DISP_STRG_SIZE));           //Reset APD Notte
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (54*DISP_STRG_SIZE),FlashOn);    //Disabilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rNightResAPDOff ");
                           break;
    case mNightRSODOn     :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (151*DISP_STRG_SIZE));           //Reset APD Notte
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (53*DISP_STRG_SIZE),FlashOn);    //Abilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rNightResAPDOn  ");
                           break;
    case mNhtFireAlmOff   :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (166*DISP_STRG_SIZE));           //FireAlarm Notte
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (54*DISP_STRG_SIZE),FlashOn);    //Disabilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rNightFireAlmOff");
                           break;
    case mNhtFireAlmOn    :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (166*DISP_STRG_SIZE));           //FireAlarm Notte
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (53*DISP_STRG_SIZE),FlashOn);    //Abilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rNightFireAlmOn ");
                           break;
    case mServiceCfg      :
                           sprintf(Riga2, pDispMask + (1*DISP_STRG_SIZE));   //"                "
                           if (ServMode != 0){
                                BinBcdAscii(ServMode);                                
								strncpy(&Riga2[5],(char const*)&BcdBuff[7], 2);                                
								strncpy(&Riga2[8],"min", 3);
                           }
                           else{
                                sprintf(Riga2, pDispStrg + (54*DISP_STRG_SIZE));    //Disabilitato
                           }
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (152*DISP_STRG_SIZE));    //Service Mode
                           Fls2((AT91PS_USART)DBGU_pt, Riga2,FlashOn);
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rServiceCfg     ");
                           break;

  case  mTOutOpDrCfg     :
                           sprintf(Riga2, pDispMask + (1*DISP_STRG_SIZE));   //"                "
                           BinBcdAscii(TOutOpDrCfg);                           
                           strncpy(&Riga2[5],(char const*)&BcdBuff[7], 2);						  
						   strncpy(&Riga2[8],"min", 3);
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (181*DISP_STRG_SIZE));    //ScadPresP.Aperta
                           Fls2((AT91PS_USART)DBGU_pt, Riga2,FlashOn);
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rTOutOpDrCfg    ");
                           break;
    case mAccessCtrlOff   :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (158*DISP_STRG_SIZE));           //CambPersContrAcc
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (54*DISP_STRG_SIZE),FlashOn);    //Disabilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rAccessContrOff ");
                           break;
    case mAccessCtrlOn    :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (158*DISP_STRG_SIZE));           //CambPersContrAcc
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (53*DISP_STRG_SIZE),FlashOn);    //Abilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rAccessContrOn  ");
                           break;
    case mRestoreModeOff  :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (159*DISP_STRG_SIZE));           //Riavvio
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (164*DISP_STRG_SIZE),FlashOn);    //Notte/Disable
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rRestoreModeOff  ");
                           break;
   case mRestoreModeOn   :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (159*DISP_STRG_SIZE));           //Riavvio
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (163*DISP_STRG_SIZE),FlashOn);    //Stesso Stato/Enable
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rRestoreModeOn  ");
                           break;
   case mPresentModeOff  :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (167*DISP_STRG_SIZE));           //Anti Blocco Pers
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (54*DISP_STRG_SIZE),FlashOn);    //Disabilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rSensAtrioPChOff");
                           break;
   case mPresentModeOn   :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (167*DISP_STRG_SIZE));           //Anti Blocco Pers
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (53*DISP_STRG_SIZE),FlashOn);    //Abilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rSensAtrioPChOn ");
                           break;
    case mMemBioResOff    :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (160*DISP_STRG_SIZE));           //Mem. Bio Result
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (54*DISP_STRG_SIZE),FlashOn);    //Disabilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rRestoreModeOff ");
                           break;
    case mMemBioResOn     :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (160*DISP_STRG_SIZE));           //Mem. Bio Result
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (53*DISP_STRG_SIZE),FlashOn);    //Abilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rRestoreModeOn  ");
                           break;
    case mEasyTransOff    :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (161*DISP_STRG_SIZE));           //Mem Cambio Prox
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (54*DISP_STRG_SIZE),FlashOn);    //Disabilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rEasyTransitOff ");
                           break;
    case mEasyTransOn     :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (161*DISP_STRG_SIZE));           //Mem Cambio Prox
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (53*DISP_STRG_SIZE),FlashOn);    //Abilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rEasyTransitOn  ");
                           break;
    case mAdvanceConfOff  :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (162*DISP_STRG_SIZE));           //Conf.Anticipata
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (54*DISP_STRG_SIZE),FlashOn);    //Disabilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rAdvanceConfOff ");
                           break;
    case mAdvanceConfOn   :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (162*DISP_STRG_SIZE));           //Conf.Anticipata
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (53*DISP_STRG_SIZE),FlashOn);    //Abilitato
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rAdvanceConfOn  ");
                           break;
    case mABT1Off         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (171*DISP_STRG_SIZE));           //INPUT CN17.10
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (173*DISP_STRG_SIZE),FlashOn);   //P-EXT-MAN
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rCN17.10P-EXT-M ");
                           break;
    case mABT1On          :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (171*DISP_STRG_SIZE));           //INPUT CN17.10
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (175*DISP_STRG_SIZE),FlashOn);   //AntiSF 1
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rCN17.10ASF1    ");
                           break;
    case mABT2Off         :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (172*DISP_STRG_SIZE));           //INPUT CN17.11
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (174*DISP_STRG_SIZE),FlashOn);   //P-INT-MAN
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rCN17.11P-INT-M ");
                           break;
    case mABT2On          :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (172*DISP_STRG_SIZE));           //INPUT CN17.11
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (176*DISP_STRG_SIZE),FlashOn);   //AntiSF 2
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rCN17.11ASF2    ");
                           break;
    case mDelayVisEx      :
                           sprintf(Riga2, pDispMask + (1*DISP_STRG_SIZE));   //"                "
                           BinBcdAscii(DelayVisEx);
                           strncpy(&Riga2[5],(char const*)&BcdBuff[7], 2);                           
						   strncpy(&Riga2[8],"sec", 3);

                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (178*DISP_STRG_SIZE));    //Tempo Contr.Vis.
                           Fls2((AT91PS_USART)DBGU_pt, Riga2,FlashOn);
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rDelayVisualExam");
                           break;
    case mViolateNtfyOff      :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (180*DISP_STRG_SIZE));           //Alarm Sabot.Ante
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (58*DISP_STRG_SIZE),FlashOn);    //Disabilitata
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rViolateNtfyOff ");
                           break;
    case mViolateNtfyOn       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (180*DISP_STRG_SIZE));           //Alarm Sabot.Ante
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (80*DISP_STRG_SIZE),FlashOn);    //Abilitata
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rViolateNtfyOn  ");
                           break;
    case mCN93_4AsTransOk  :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (182*DISP_STRG_SIZE));           //OUTPUTS CN9.3-4
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (184*DISP_STRG_SIZE),FlashOn);    //Transito OK MB
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rCN9.3-4AsTranOk");
                           break;
    case mCN93_4AsIntTrLight   :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (182*DISP_STRG_SIZE));           //OUTPUTS CN9.3-4
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (183*DISP_STRG_SIZE),FlashOn);    //LED Semaforo Aux
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rCN9.3-4AsIntTrL");
                           break;		   
	case mTOutObjVfyCfg   :
                           sprintf(Riga2, pDispMask + (1*DISP_STRG_SIZE));   //"                "
                           BinBcdAscii(TOutObjVfyCfg);
						   if (BcdBuff[8]!='0'){
							   if (BcdBuff[7]==' '){
								   Riga2[4] = '0';
							   }
							   else{
								   Riga2[4] = BcdBuff[7];
							   }
							Riga2[5] = ',';
							Riga2[6] = BcdBuff[8];
						   }
						   else{                           
                           Riga2[6] = BcdBuff[7];
                           }
						   Riga2[8] = 's'; Riga2[9] = 'e'; Riga2[10] = 'c';

                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (186*DISP_STRG_SIZE));    //DurataVerif.Ogg.
                           Fls2((AT91PS_USART)DBGU_pt, Riga2,FlashOn);
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rVerify Obj.Time");
                           break;							   
	case mVfyRepeat   :
                           sprintf(Riga2, pDispMask + (1*DISP_STRG_SIZE));   //"                "
                           BinBcdAscii(VfyRepeat);						   
                           Riga2[6] = BcdBuff[8];
						   
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (187*DISP_STRG_SIZE));    // Prove Verif.Ogg.
                           Fls2((AT91PS_USART)DBGU_pt, Riga2,FlashOn);
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rVerifyObj.Count");
                           break;		
	case mAfterLowBatStatForced  :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (188*DISP_STRG_SIZE));			// Ass.Rete+Batt. 	
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (189*DISP_STRG_SIZE),FlashOn);	//Ante Bloccate   
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rAftLowBStForced");
                           break;						   
    case mAfterLowBatStatEme       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (188*DISP_STRG_SIZE));			//Ass.Rete+Batt. 
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (191*DISP_STRG_SIZE),FlashOn);    //Emergenza SoloON
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rAftLowBStEmerON");
                           break;					
	case mAlertChangAftLowBStat       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (188*DISP_STRG_SIZE));           // Ass.Rete+Batt. 
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (190*DISP_STRG_SIZE),FlashOn);	//"Non in Emergenza
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rModifyAftLowBS ");
                           break;						   
	case mAfterLowBatStatEmeAlw       :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (188*DISP_STRG_SIZE));           // Ass.Rete+Batt. 
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (192*DISP_STRG_SIZE),FlashOn);	//Emergenza Sempre
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rAftLowBStEmeAlw");
                           break;							   
	
	case mMoreModeRewind  :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (5*DISP_STRG_SIZE));				// Allarme +P  
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (193*DISP_STRG_SIZE),FlashOn);	//RipetiDopoAllarm
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMreModeRewind  ");
                           break;	
						   
	case mMoreModeStopStart:
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (5*DISP_STRG_SIZE));				// Controllo +P  
                           Fls2((AT91PS_USART)DBGU_pt, pDispStrg + (194*DISP_STRG_SIZE),FlashOn);	//EspelliDopoAllar
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rMreModStopStart");
                           break;							   	
	case mUnavailable     :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (94*DISP_STRG_SIZE));    //Funzione
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (95*DISP_STRG_SIZE));    //non  disponibile
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rUnavailable    ");
                           break;

    default               :
                           Row1((AT91PS_USART)DBGU_pt, pDispStrg + (141*DISP_STRG_SIZE));   //Nessun
                           Row2((AT91PS_USART)DBGU_pt, pDispStrg + (142*DISP_STRG_SIZE));   //messaggio
                           Dbg((AT91PS_USART)DBGU_pt, "\n\rNo message     ");
                           break;
  }
}


unsigned char LogicInstance(SEM_CONTEXT *Context, SEM_EVENT_TYPE EventNo,
      SEM_INSTANCE_TYPE InstanceNo)
{
  /* Define completion code variable. */
  unsigned char cc;
  SEM_ACTION_EXPRESSION_TYPE ActionExpress;

        /* Set active instance */
      if ((cc = SMP_SetInstance(Context, InstanceNo)) != SES_OKAY)
      {
        return(cc);
      }
        /* Deduct the event */
      if ((cc = VSLogicSMP_Deduct(Context, EventNo)) != SES_OKAY) //versione per gestione corretta var bufferizzate
 //     if ((cc = SMP_Deduct(Context, EventNo)) != SES_OKAY)
      {
        return(cc);
      }
        /* Get resulting action expressions and execute them. */
      while ((cc = SMP_GetOutput(Context, &ActionExpress)) == SES_FOUND)
        SMP_TableAction(Context, VSLogicVSAction, ActionExpress);
        /* Check for error. */
      if (cc != SES_OKAY)
      {
        return(cc);
      }
        /* Change the next state vector. */
      if ((cc = SMP_NextState(Context)) != SES_OKAY)
      {
        return(cc);
      }
      return(cc);
}

unsigned char LogicInit(SEM_CONTEXT VS_TQ_CONTEXT * * Context)
{
  unsigned char cc;

  US1_init();

  /* Initialize the Logic VS System. */
  if ((cc = VSLogicSMP_InitAll(Context)) != SES_OKAY)
    return(cc);
  return(cc);
}

unsigned char LogicMain(SEM_CONTEXT *pSEMCont)
{
    unsigned char cc=SES_OKAY;

    SEM_EVENT_TYPE eventNo;

    if (DisplayMode != DISPLAY_MODE_DEBUG){
        DisplayStatus();
        if (bDisDbg == 0)
            US_UpdateDisplay((AT91PS_USART)DBGU_pt);
        else
            US_UpdateDisplay2((AT91PS_USART)DBGU_pt);
    }
    SerialInOut();

    BattLow = INGRESSI[BLOW];		
    Vac = INGRESSI[RETE];
	SwAfterLowBatSt = INGRESSI[SW_AFT_LB_ST];
    Present = INGRESSI[ATR];
    Closed1 = USCITE[PC1];
    Closed2 = USCITE[PC2];
    Opened1 = USCITE[PA1];
    Opened2 = USCITE[PA2];

    USCITE[P1CH] = USCITE[PC1];
    USCITE[P2CH] = USCITE[PC2];

    if ((Closed1==0) && (Closed2==0)){
        USCITE[P1P2AP] = 1;
    }
    else{
        USCITE[P1P2AP] = 0;
    }

    if ((Closed1==1) && (Closed2==1)){
        USCITE[PCHIUSE]=1;
    }
    else{
        USCITE[PCHIUSE]=0;
    }

    if (BeepRun == 0){
        if (LedsConsoleVisible==1){
            D_CICp   = INGRESSI[METAL];
        }
        else{
            D_CICp   = 0;
        }
        NT200 = 0;
    }
    else{
        switch(NT200){
          case 0:
            D_CICp = 0;
            break;
          case 1:
            D_CICp = 1;
            break;
          case 2:
            D_CICp = 0;
            break;
          case 3:
            if(SlowBeep == 0)
                D_CICp = 1;
            break;
          case 4:
            if ((D_EMERGp == 0)&&(SlowBeep == 0)){
                NT200 = 0;
            }
            D_CICp = 0;
            break;
          case 5:
          case 6:
          case 7:
            D_CICp = 0;
            break;
          case 8:
            NT200 = 0;
            break;
          default :
            D_CICp = 0;
            NT200 = 0;
            break;
        }
    }

    if (LedsConsoleVisible==1){
        D_SINGRp = USCITE[LAMP_IN];
        D_SUSCIp = USCITE[LAMP_OUT];
        D_RETEp = INGRESSI[RETE];
        if ((PassCount>=PassCountMax) && (INGRESSI[RETE] == 0)){
            if (T200 == 1){
                D_BATTp = 1;
            }
            else{
                D_BATTp = 0;
            }
        }
        else{
            if (INGRESSI[RETE] == 0){
                D_BATTp = 1;
            }
            else{
                D_BATTp = 0;
            }
        }
        D_SWINTp = USCITE[PC2];
        D_SWEXTp = USCITE[PC1];
        D_ATRp   = INGRESSI[ATR];
        D_PUEXTp = INGRESSI[PULS1];
        D_PUINTp = INGRESSI[PULS2];

        if (LedMetRun == 0){
            D_ALLp   = INGRESSI[METAL];
        }
        else{
            if (T200 == 0){
                D_ALLp   = 0;
            }
            else{
                D_ALLp   = 1;
            }
        }


        D_MDISBp = INIB_METp;//con tasti funzione usare flag per abilitare metal
    }
    else{
        D_SINGRp = 0;
        D_SUSCIp = 0;
        D_RETEp  = 0;
        D_BATTp  = 0;    	
        D_SWINTp = 0;
        D_SWEXTp = 0;
        D_ATRp   = 0;
        D_PUEXTp = 0;
        D_PUINTp = 0;
        D_ALLp   = 0;
        D_MDISBp = 0;
    }

    ConsolleCom();

    LogicManageInputs();
    LogicManageCommands();
    if(SEQ_RetrieveEvent(&eventNo,Sys) != UCC_QUEUE_EMPTY) {
        switch (Sys){
          case 0:
          case 1:
            break;
          case 2:
            if ((cc = LogicInstance(pSEMCont, eventNo, 0)) != SES_OKAY)
                return(cc);
            break;
        }
    }
    return(cc);
}


