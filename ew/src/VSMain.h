/*****************************************************************************
* IAR visualSTATE Main Loop Header File
*
* The file contains an implementation for a main loop using the visualSTATE
* basic API.
*
* The code uses a simple queue for storing events. The functions for inter-
* facing to the queue are described in the sample code file
* simpleEventHandler.h.
*****************************************************************************/



#ifndef _VSMAIN_H
#define _VSMAIN_H


/* *** include directives *** */

#include "simpleEventHandler.h"
#include "SEMLibE.h"

#define FLASH_MODE

#ifdef FLASH_MODE
#define FLASH_FACTOR 120/100
#else
#define FLASH_FACTOR 1
#endif

#define MOT_SYSTEMS 2

/* *** variable declarations *** */

extern unsigned int Sys;
extern unsigned int SysDisplay;
extern unsigned int LastMsgId;
extern unsigned int LastMsgIdSys[EVENT_QUEUE_SYSTEMS];
extern VS_BOOL bDisDbg;

extern unsigned int Cp[MOT_SYSTEMS];
extern unsigned int EscCount;

/* *** function declarations *** */

void TIMER_SW_start(SEM_EVENT_TYPE event, unsigned int ticks, VS_UINT8 system, VS_UINT8 timerid);

void TIMER_SW_stop(VS_UINT8 system, VS_UINT8 timerid);

void TIMER_SW_tick(VS_UINT8 system, VS_UINT8 timerid);

void HandleError(unsigned char cc);

#endif
