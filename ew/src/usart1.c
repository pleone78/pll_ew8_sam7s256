// -----------------------------------------------------------------------------
//
// Note tecniche sul protocollo di dialogo in RS485 con la consolle attuale
//
// -----------------------------------------------------------------------------
//
// Accensione
//
// All'accensione il master invia 2 byte uguali contenenti l'indirizzo di slave
// e ripete l'operazione per 31 volte, una per ciascun indirizzo di slave al
// fine di identificare gli slave collegati: infatti quelli presenti rispondono
// alla ricezione del proprio indirizzo con un pacchetto di 8byte caratteristici
// ed un byte di check xor degli 8 byte precedenti.
//
// A regime
//
// Ad ogni ciclo di programma il master comunica con tutti gli slave che hanno
// risposto alla scansione iniziale: in questo caso ai 2 byte iniziali fa
// seguire i byte di uscita verso lo slave che subito dopo risponde con i byte
// di uscita verso il master.
//
// Protocollo
//
// Il formato del protocollo per ogni byte �:
//  - 1 bit 0 di start;
//  - 8 bit di dati a partire dal LSB;
//  - nessuna parit�;
//  - 1 bit di distinzione tra dati (0) e comandi (1) (9� bit);
//  - 2 bit 1 di stop;
// per un totale di 12 bit per ogni byte trasmesso con un Tbit = 5.36 �sec
//
// All'accensione il master trasmette l'indirizzo mettendolo in due byte
// consecutivi uguali con i 3 bit pi� alti a 1 e con il 9�bit a 1 nel primo.
// Nel nostro caso addslave=02 quindi l'intero pacchetto �:
//
//  0 0100 0111 1 11 0 0100 0111 0 11
//  S L       M 9 ss S L       M 9 ss
//
//  S= Start L=LSB M=MSB 9=9�bit s=stop
//
// La composizione dei byte caratteristici �:
//
//  1 - numero byte di uscita da master a slave              (nel ns. caso 0x02)
//  2 - numero byte di ingresso da slave a master            (nel ns. caso 0x02)
//  3 - puntatore uscita slave area RAM master               (nel ns. caso 0x08)
//  4 - puntatore ingresso slave area RAM master             (nel ns. caso 0x00)
//  5 - slavecode0 byte 0 di codice identificatore slave     (nel ns. caso 0x20)
//  6 - slavecode1 byte 1 di codice identificatore slave     (nel ns. caso 0x02)
//  7 - slavecode2 byte 2 di codice identificatore slave     (nel ns. caso 0x00)
//  8 - slavecode3 byte 3 di codice identificatore slave     (nel ns. caso 0x00)
//  9 - byte di checksum xor dei byte precedenti             (nel ns. caso 0x2A)
//
// Nella scansione a regime il master invia due volte l'indirizzo come prima ma
// con i 3 bit pi� alti a 0.
//
// La composizione dei byte caratteristici per la consolle S.Paolo �:
//
//  1 - numero byte di uscita da master a slave              (0x0B)
//  2 - numero byte di ingresso da slave a master            (0x02)
//  3 - puntatore uscita slave area RAM master               (0x08)
//  4 - puntatore ingresso slave area RAM master             (0x00)
//  5 - slavecode0 byte 0 di codice identificatore slave     (0x20)
//  6 - slavecode1 byte 1 di codice identificatore slave     (0x02)
//  7 - slavecode2 byte 2 di codice identificatore slave     (0x02)
//  8 - slavecode3 byte 3 di codice identificatore slave     (0x00)
//  9 - byte di checksum xor dei byte precedenti             (0x21)
//
// -----------------------------------------------------------------------------
//
// Note tecniche per la costruzione del driver di comunicazione con gli slave
// in RS485
//
// -----------------------------------------------------------------------------

// All'accensione il driver deve rilevare gli indirizzi delle consolle presenti
// ed il tipo che si pu� ricavare dal codice identificativo nei byte 5,6,7,8:
//
//  - standard : 20 02 00 00
//  - S.Paolo  : 20 02 02 00
//
// il tipo di consolle definisce anche i byte dati da trasmettere e da ricevere:
//
//  - standard : TX  2  RX 2
//  - S.Paolo  : TX 11  RX 2
//
// attualmente gli indirizzi disponibili vanno da 1 a 3
//

// Composizione byte TX (uscite logiche) per consolle standard indirizzo 2
//
//  byte 1:
//          0 = D_RETEp   Led presenza rete
//          1 = D_BATTp   Led battery
//          2 = D_MDISBp  Led metal disabilitato
//          3 = D_EMERGp  Led emergenza attiva
//          4 = D_SINGRp  Led solo ingresso
//          5 = D_SUSCIp  Led solo uscita
//          6 = D_MANUp   Led manuale
//          7 = D_PUINTp  Led pulsante INT
//  byte 2:
//          0 = D_SWINTp  Led SWITCH PORTA INT
//          1 = D_SWEXTp  Led SWITCH PORTA EXT
//          2 = D_PUEXTp  Led pulsante ext
//          3 = D_CICp    Cicalino
//          4 = D_ATRp    Led ATRIO
//          5 = D_AUTOp   Led automatico
//          6 = D_ALLp    Led ALLARME
//          7 = D_SUOp    Suoneria

// Composizione byte RX (ingressi logici) per consolle standard
//
//  byte 1:
//          0 = dipsw 0: CONFp conferma comando
//          1 = dipsw 1:
//          2 = dipsw 2:
//          3 = dipsw 3:
//          4 = dipsw 4:
//          5 = dipsw 5:
//          6 = dipsw 6:
//          7 = dipsw 7:
//  byte 2:
//          0 = dipsw 0: CONFp conferma comando
//          1 = dipsw 1:
//          2 = dipsw 2:
//          3 = dipsw 3:
//          4 = dipsw 4:
//          5 = dipsw 5:
//          6 = dipsw 6:
//          7 = dipsw 7:

// -----------------------------------------------------------------------------
//
// Headers
//
// -----------------------------------------------------------------------------

//#include "simpleEventHandler.h"
//#include "SEMLibE.h"
#include "VSMain.h"
#include "VSLogicData.h"
#include "usart.h"
#include "usart1.h"
#include "deviceSetup.h"

// -----------------------------------------------------------------------------
//
// Variabili
//
// -----------------------------------------------------------------------------
#define CNSDEB  10

unsigned char char_rx_US1 = 0;

unsigned char Nbb = 0,/*Chks = 0,*/ CnsBusy = 0, DspRow = 0;

unsigned char NByteRx = 0;
unsigned char RxCns[4];

unsigned char NByteTx=0, TxNByte[3];

unsigned char BeepRun=0, SlowBeep=0,LedMetRun=0;

//unsigned int NumCnsOk=0,NumCnsNok=0,NumCnsTx=0;

union byte4bit32 USCITECNS[3];
union byte4bit32 USCITECNS_NODISP[3];
union byte4bit32 INGRESSICNS[3];
union byte4bit32 INGRESSICNS_NODISP[3];
union txdsp TxCns[3];

// -----------------------------------------------------------------------------
//
// Costanti
//
// -----------------------------------------------------------------------------

AT91PS_USART US1_pt = AT91C_BASE_US1;

const unsigned int CnsAdd0 = 0x1E1;     // Indirizzo base slave per test
const unsigned int CnsAdd1 = 0x101;     // Indirizzo base slave per lavoro

// -----------------------------------------------------------------------------
//
//   Gestione interrupt da porta seriale US1
//
// -----------------------------------------------------------------------------

__ramfunc void US1_irq_handler( void )
{
	unsigned int status_1;
	//  get Usart status register
	
	status_1 = US1_pt->US_CSR;
	
	if ( status_1 & AT91C_US_RXRDY )
	{
		char_rx_US1 = US1_pt->US_RHR & 0x1FF;		
		US1_pt->US_CR = AT91C_US_RSTSTA;		
		RxCns[3] ^= char_rx_US1;		
		RxCns[NByteRx++] = char_rx_US1;
	} 
}

// -----------------------------------------------------------------------------
//
//   Inizializzazione porta US1 verso la CONSOLLE
//
// -----------------------------------------------------------------------------

void US1_init ( void )
{
  //  Configure PIO controllers to periph mode

  AT91F_PIO_CfgPeriph( AT91C_BASE_PIOA,
                     ( AT91C_PA21_RXD1 ) |
                     ( AT91C_PA22_TXD1 ) |
                     ( AT91C_PA24_RTS1 ),
                                     0);     // Peripheral B
  // Enable the clock of the PIOA

  AT91F_PMC_EnablePeriphClock ( AT91C_BASE_PMC, 1 << AT91C_ID_US1 );

  // Usart Configure

  // Disable interrupts

  US1_pt->US_IDR = 0xFFFFFFFF;// (unsigned int) -1;

  // Reset receiver and transmitter

  US1_pt->US_CR =AT91C_US_RSTRX | AT91C_US_RSTTX | AT91C_US_RXDIS | AT91C_US_TXDIS;

  // Define the USART mode

//  US1_pt->US_MR =AT91C_US_USMODE_NORMAL |
  US1_pt->US_MR =AT91C_US_USMODE_RS485 |
                 AT91C_US_CLKS_CLOCK |
                 AT91C_US_CHRL_8_BITS |
                 AT91C_US_PAR_NONE |
                 AT91C_US_NBSTOP_2_BIT |
                 AT91C_US_MODE9;

  // Define the baud rate divisor register

  US1_pt->US_BRGR = US1_CLKDIV;

  // Enable usart

  US1_pt->US_CR = AT91C_US_RXEN | AT91C_US_TXEN;

  // Enable USART IT error and RXRDY

  AT91F_US_EnableIt(US1_pt,AT91C_US_TIMEOUT | AT91C_US_FRAME | AT91C_US_OVRE | AT91C_US_RXRDY);

  // open Usart  interrupt

  AT91F_AIC_ConfigureIt ( AT91C_BASE_AIC,
                          AT91C_ID_US1,
                          US1_INTERRUPT_LEVEL,
                          AT91C_AIC_SRCTYPE_INT_HIGH_LEVEL,
                          US1_irq_handler);

  AT91F_AIC_EnableIt (AT91C_BASE_AIC, AT91C_ID_US1);
}

void NoDispToDispInput(unsigned char cnsNum)
{
  //tutti i dip devono essere off
/*
  CONF_F2     = 0;
  AUTO_GST_F1 = 0;
  SEL_UUEM_F3 = 0;
  SEL_OPC     = 0;
  SEL_VAUTO   = 0;
  SEL_MTLAB   = 0;
  SEL_BLK     = 0;
  SEL_PERS_ENT= 0;//AUX=ENT

  ON_OFFp     = ON_OFFp_nd;
  EMERGp      = EMERGp_nd;
  INIB_METp   = INIB_METp_nd;
  MAN_AUTp    = MAN_AUTp_nd;
  if (CONFp_nd !=0)
  {
    SEL_DIRp    = SEL_DIRp_nd;
  }
  COM_MANOUTp = COM_MANOUTp_nd;
  RST_MDp     = RST_MDp_nd;
  COM_MANINp  = COM_MANINp_nd;
*/
  INGRESSICNS[cnsNum].bb32.b5      = INGRESSICNS_NODISP[cnsNum].bb32.b8;
  INGRESSICNS[cnsNum].bb32.b10     = INGRESSICNS_NODISP[cnsNum].bb32.b9;
  INGRESSICNS[cnsNum].bb32.b9      = INGRESSICNS_NODISP[cnsNum].bb32.b10;
  INGRESSICNS[cnsNum].bb32.b2      = INGRESSICNS_NODISP[cnsNum].bb32.b11;

  if (INGRESSICNS_NODISP[cnsNum].bb32.b0 !=0)
  {
    INGRESSICNS[cnsNum].bb32.b11    = INGRESSICNS_NODISP[cnsNum].bb32.b12;
  }

  INGRESSICNS[cnsNum].bb32.b6      = INGRESSICNS_NODISP[cnsNum].bb32.b13;
  INGRESSICNS[cnsNum].bb32.b3      = INGRESSICNS_NODISP[cnsNum].bb32.b14;
  INGRESSICNS[cnsNum].bb32.b4      = INGRESSICNS_NODISP[cnsNum].bb32.b15;
}

void DispToNoDispOutput(void)
{
  D_RETEp_nd    =  D_RETEp;
  D_BATTp_nd    =  D_BATTp;
  D_MDISBp_nd   =  D_MDISBp;
  D_EMERGp_nd   =  D_EMERGp;
  D_SINGRp_nd   =  D_SINGRp;
  D_SUSCIp_nd   =  D_SUSCIp;
  D_MANUp_nd    =  D_MANUp;
  D_PUINTp_nd   =  D_PUINTp;

  D_SWINTp_nd   =  D_SWINTp;
  D_SWEXTp_nd   =  D_SWEXTp;
  D_PUEXTp_nd   =  D_PUEXTp;
  D_CICp_nd     =  D_CICp;
  D_ATRp_nd     =  D_ATRp;
  D_AUTOp_nd    =  D_AUTOp;
  D_ALLp_nd     =  D_ALLp;
  D_SUOp_nd     =  D_SUOp;
}


// -----------------------------------------------------------------------------
//
// Comunicazione alle consolle presenti
//
// La funzione invia i dati di uscita alle consolle presenti che rispondono con
// i propri dati di ingresso
//
// -----------------------------------------------------------------------------
extern unsigned char USCITE[];

void ConsolleCom(void)
{
#define MAX_CNS_COUNT  10
#define ADJ_LOOP_PERIOD 425

    unsigned int kk,CnsAdd;
    unsigned char Chks;
    static unsigned int Count1 = 0,Count2 = 0,Count3 = 0;
    static unsigned char ToSend1 = 1,ToSend2 = 1,ToSend3 = 1;
    static unsigned char key1 = 0, key2 = 0, key3 = 0;
    static unsigned char RxCnsOld1[2] = {0,0},RxCnsOld2[2] = {0,0},RxCnsOld3[2] = {0,0};
    static unsigned char RxCnsDeb1 = CNSDEB,RxCnsDeb2 = CNSDEB,RxCnsDeb3 = CNSDEB;
    static unsigned char CnsCom = 1;


    switch(CnsCom)
    {
      case 1:

        if (Cns1On==1){

            //se si usa if dopo ottimizzazione elimina parte da if (ToSend==0) nel caso Cns1Disp==0 !!
            switch (Cns1Disp){
              case 1:
                if (( NByteRx == 3 ) && ( RxCns[3] == 0)){
                    SEQ_AddEvent(eCns1Ok,Sys);
                    if ((RxCnsOld1[0] == RxCns[0]) && (RxCnsOld1[1] == RxCns[1])){
                        INGRESSICNS[1].BB[0] = RxCns[0];
                        INGRESSICNS[1].BB[1] = RxCns[1];
                        if(Cns1On!=1){
                            INGRESSICNS[1].BB[0] = 0x00;
                            INGRESSICNS[1].BB[1] = 0x00;
                        }
                        RxCnsDeb1 = CNSDEB;
                    }
                    else{
                        if (RxCnsDeb1 >0){
                            RxCnsDeb1--;
                        }
                        else{
                            RxCnsOld1[0] = RxCns[0];
                            RxCnsOld1[1] = RxCns[1];
                        }
                    }

                    NByteRx = 0;
                    RxCns[3] = 0;
                    ToSend1 = 1;
                    CnsCom = ChangeCns (CnsCom);
                }
                else{
                    if (ToSend1 == 0){
                        if (--Count1 == 0){
                            SEQ_AddEvent(eCns1NotOk,Sys);
                            NByteRx = 0;
                            RxCns[3] = 0;
                            ToSend1 = 1;
                            CnsCom = ChangeCns (CnsCom);
                        }
                    }
                    else{
                        NByteRx = 0;
                        RxCns[3] = 0;
                        Count1 = MAX_CNS_COUNT;
                        ToSend1 = 0;
                        CnsAdd = CnsAdd1 + 1;
                        US1_pt->US_THR = CnsAdd;                      // Invio indirizzo 1� byte
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        US1_pt->US_THR = CnsAdd & 0x000000FF;         // Invio indirizzo 2� byte
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        Chks = 0;
                        for (kk = 0; kk < 2; kk++){
                            TxCns[1].txdspcns[kk] = USCITECNS[1].BB[kk];
                            US1_pt->US_THR = TxCns[1].txdspcns[kk];
                            Chks ^= TxCns[1].txdspcns[kk];
                            while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        }
                        US1_pt->US_THR = DisplayBuff[key1];//48 + key1;//225
                        Chks ^= DisplayBuff[key1];//48 + key1;//225;
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        US1_pt->US_THR = 0;
                        Chks ^= 0;
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        US1_pt->US_THR = 1 + key1;
                        Chks ^= 1 + key1;
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        key1++;
                        if (key1>31)
                            key1=0;
                        for (kk = 0; kk < 6; kk++){
                            US1_pt->US_THR = 0;
                            Chks ^= 0;
                            while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        }
                        US1_pt->US_THR = Chks;
                        while (!(US1_pt->US_CSR & AT91C_US_TXEMPTY));
                    }
                }
                break;
              case 0:
                if (( NByteRx == 3 ) && ( RxCns[3] == 0)){
                    SEQ_AddEvent(eCns1Ok,Sys);

                    if ((RxCnsOld1[0] == RxCns[0]) && (RxCnsOld1[1] == RxCns[1])){
                        INGRESSICNS_NODISP[1].BB[0] = RxCns[0];
                        INGRESSICNS_NODISP[1].BB[1] = RxCns[1];
                        if(Cns1On!=1){
                            INGRESSICNS[1].BB[0] = 0x00;
                            INGRESSICNS[1].BB[1] = 0x00;
                            INGRESSICNS_NODISP[1].BB[0] = 0x00;
                            INGRESSICNS_NODISP[1].BB[1] = 0x00;
                        }
                        NoDispToDispInput(1);
                        RxCnsDeb1 = CNSDEB;
                    }
                    else {
                        if (RxCnsDeb1 >0){
                            RxCnsDeb1--;
                        }
                        else {
                            RxCnsOld1[0] = RxCns[0];
                            RxCnsOld1[1] = RxCns[1];
                        }
                    }

                    NByteRx = 0;
                    RxCns[3] = 0;
                    ToSend1 = 1;
                    CnsCom = ChangeCns (CnsCom);
                }
                else{
                    if (ToSend1 == 0){
                        if (--Count1 == 0) {
                            SEQ_AddEvent(eCns1NotOk,Sys);
                            NByteRx = 0;
                            RxCns[3] = 0;
                            ToSend1 = 1;
                            CnsCom = ChangeCns (CnsCom);	
                        }
                    }
                    else{
                        NByteRx = 0;
                        RxCns[3] = 0;
                        Count1 = MAX_CNS_COUNT;
                        ToSend1 = 0;
                        // Invio x ritardo di
                        // allineamento nodisplay
                        CnsAdd = CnsAdd1 + 5;
                        US1_pt->US_THR = CnsAdd;                      // Invio indirizzo 1� byte
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        US1_pt->US_THR = CnsAdd & 0x000000FF;         // Invio indirizzo 2� byte
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        Chks = 0;
                        for (kk = 0; kk < 2; kk++){
                            TxCns[1].txdspcns[kk] = USCITECNS_NODISP[1].BB[kk];
                            US1_pt->US_THR = TxCns[1].txdspcns[kk];
                            Chks ^= TxCns[1].txdspcns[kk];
                            while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        }
                        US1_pt->US_THR = Chks;
                        while (!(US1_pt->US_CSR & AT91C_US_TXEMPTY));

                        CnsAdd = CnsAdd1 + 5;
                        US1_pt->US_THR = CnsAdd;                      // Invio indirizzo 1� byte
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        US1_pt->US_THR = CnsAdd & 0x000000FF;         // Invio indirizzo 2� byte
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        Chks = 0;
                        for (kk = 0; kk < 2; kk++){
                            TxCns[1].txdspcns[kk] = USCITECNS_NODISP[1].BB[kk];
                            US1_pt->US_THR = TxCns[1].txdspcns[kk];
                            Chks ^= TxCns[1].txdspcns[kk];
                            while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        }
                        US1_pt->US_THR = Chks;
                        while (!(US1_pt->US_CSR & AT91C_US_TXEMPTY));
                        // Fine Invio x ritardo di
                        // allineamento nodisplay
                        //          USCITE[AT]=1;     // Debug
                        CnsAdd = CnsAdd1 + 1;
                        US1_pt->US_THR = CnsAdd;                      // Invio indirizzo 1� byte
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        US1_pt->US_THR = CnsAdd & 0x000000FF;         // Invio indirizzo 2� byte
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        Chks = 0;
                        DispToNoDispOutput();
                        for (kk = 0; kk < 2; kk++){
                            TxCns[1].txdspcns[kk] = USCITECNS_NODISP[1].BB[kk];
                            US1_pt->US_THR = TxCns[1].txdspcns[kk];
                            Chks ^= TxCns[1].txdspcns[kk];
                            while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        }
                        US1_pt->US_THR = Chks;
                        while (!(US1_pt->US_CSR & AT91C_US_TXEMPTY));
                    }
                }
                break;
              default:
                Cns1Disp = 1;
            }
        }
        else{
            CnsCom = ChangeCns (CnsCom);
            microsecondi(ADJ_LOOP_PERIOD);
        }		
        break;

      case 2:

        if (Cns2On==1){
            //se si usa if dopo ottimizzazione elimina parte da if (ToSend==0) nel caso Cns1Disp==0 !!
            switch (Cns2Disp){
              case 1:
                if (( NByteRx == 3 ) && ( RxCns[3] == 0)){
                    SEQ_AddEvent(eCns2Ok,Sys);

                    if ((RxCnsOld2[0] == RxCns[0]) && (RxCnsOld2[1] == RxCns[1])){
                        INGRESSICNS[2].BB[0] = RxCns[0];
                        INGRESSICNS[2].BB[1] = RxCns[1];
                        if(Cns1On!=1){
                            INGRESSICNS[1].BB[0] = 0x00;
                            INGRESSICNS[1].BB[1] = 0x00;
                        }
                        RxCnsDeb2 = CNSDEB;
                    }
                    else{
                        if (RxCnsDeb2 >0){
                            RxCnsDeb2--;
                        }
                        else{
                            RxCnsOld2[0] = RxCns[0];
                            RxCnsOld2[1] = RxCns[1];
                        }
                    }

                    NByteRx = 0;
                    RxCns[3] = 0;
                    ToSend2 = 1;
                    CnsCom = ChangeCns (CnsCom);	
                }
                else{
                    if (ToSend2 == 0){
                        if (--Count2 == 0){
                            SEQ_AddEvent(eCns2NotOk,Sys);
                            NByteRx = 0;
                            RxCns[3] = 0;
                            ToSend2 = 1;
                            CnsCom = ChangeCns (CnsCom);
                        }
                    }
                    else{
                        NByteRx = 0;
                        RxCns[3] = 0;
                        Count2 = MAX_CNS_COUNT;
                        ToSend2 = 0;
                        CnsAdd = CnsAdd1 + 2;
                        US1_pt->US_THR = CnsAdd;                      // Invio indirizzo 1� byte
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        US1_pt->US_THR = CnsAdd & 0x000000FF;         // Invio indirizzo 2� byte
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        Chks = 0;
                        for (kk = 0; kk < 2; kk++){
                            TxCns[2].txdspcns[kk] = USCITECNS[1].BB[kk];
                            US1_pt->US_THR = TxCns[2].txdspcns[kk];
                            Chks ^= TxCns[2].txdspcns[kk];
                            while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        }
                        US1_pt->US_THR = DisplayBuff[key2];//48 + key2;//225
                        Chks ^= DisplayBuff[key2];//48 + key2;//225;
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        US1_pt->US_THR = 0;
                        Chks ^= 0;
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        US1_pt->US_THR = 1 + key2;
                        Chks ^= 1 + key2;
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        key2++;
                        if (key2>31)
                            key2=0;
                        for (kk = 0; kk < 6; kk++){
                            US1_pt->US_THR = 0;
                            Chks ^= 0;
                            while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        }
                        US1_pt->US_THR = Chks;
                        while (!(US1_pt->US_CSR & AT91C_US_TXEMPTY));
                    }
                }
                break;
              case 0:
                if (( NByteRx == 3 ) && ( RxCns[3] == 0)){
                    SEQ_AddEvent(eCns2Ok,Sys);
                    if ((RxCnsOld2[0] == RxCns[0]) && (RxCnsOld2[1] == RxCns[1])){
                        INGRESSICNS_NODISP[2].BB[0] = RxCns[0];
                        INGRESSICNS_NODISP[2].BB[1] = RxCns[1];
                        if(Cns1On!=1){
                            INGRESSICNS[1].BB[0] = 0x00;
                            INGRESSICNS[1].BB[1] = 0x00;
                            INGRESSICNS_NODISP[1].BB[0] = 0x00;
                            INGRESSICNS_NODISP[1].BB[1] = 0x00;
                        }
                        NoDispToDispInput(2);
                        RxCnsDeb2 = CNSDEB;
                    }
                    else{
                        if (RxCnsDeb2 >0){
                            RxCnsDeb2--;
                        }
                        else{
                            RxCnsOld2[0] = RxCns[0];
                            RxCnsOld2[1] = RxCns[1];
                        }
                    }

                    NByteRx = 0;
                    RxCns[3] = 0;
                    ToSend2 = 1;
                    CnsCom = ChangeCns (CnsCom);
                }
                else{
                    if (ToSend2 == 0){
                        if (--Count2 == 0){
                            SEQ_AddEvent(eCns2NotOk,Sys);
                            NByteRx = 0;
                            RxCns[3] = 0;
                            ToSend2 = 1;
                            CnsCom = ChangeCns (CnsCom);
                        }
                    }
                    else{
                        NByteRx = 0;
                        RxCns[3] = 0;
                        Count2 = MAX_CNS_COUNT;
                        ToSend2 = 0;
                        // Invio x ritardo di
                        // allineamento nodisplay
                        CnsAdd = CnsAdd1 + 5;
                        US1_pt->US_THR = CnsAdd;                      // Invio indirizzo 1� byte
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        US1_pt->US_THR = CnsAdd & 0x000000FF;         // Invio indirizzo 2� byte
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        Chks = 0;
                        for (kk = 0; kk < 2; kk++){
                            TxCns[2].txdspcns[kk] = USCITECNS_NODISP[2].BB[kk];
                            US1_pt->US_THR = TxCns[2].txdspcns[kk];
                            Chks ^= TxCns[2].txdspcns[kk];
                            while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        }
                        US1_pt->US_THR = Chks;
                        while (!(US1_pt->US_CSR & AT91C_US_TXEMPTY));

                        CnsAdd = CnsAdd1 + 5;
                        US1_pt->US_THR = CnsAdd;                      // Invio indirizzo 1� byte
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        US1_pt->US_THR = CnsAdd & 0x000000FF;         // Invio indirizzo 2� byte
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        Chks = 0;
                        for (kk = 0; kk < 2; kk++){
                            TxCns[2].txdspcns[kk] = USCITECNS_NODISP[2].BB[kk];
                            US1_pt->US_THR = TxCns[2].txdspcns[kk];
                            Chks ^= TxCns[2].txdspcns[kk];
                            while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        }
                        US1_pt->US_THR = Chks;
                        while (!(US1_pt->US_CSR & AT91C_US_TXEMPTY));
                        // Fine Invio x ritardo di
                        // allineamento nodisplay
                        //          USCITE[AT]=1;     // Debug
                        CnsAdd = CnsAdd1 + 2;
                        US1_pt->US_THR = CnsAdd;                      // Invio indirizzo 1� byte
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        US1_pt->US_THR = CnsAdd & 0x000000FF;         // Invio indirizzo 2� byte
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        Chks = 0;
                        DispToNoDispOutput();
                        for (kk = 0; kk < 2; kk++){
                            TxCns[2].txdspcns[kk] = USCITECNS_NODISP[1].BB[kk];
                            US1_pt->US_THR = TxCns[2].txdspcns[kk];
                            Chks ^= TxCns[2].txdspcns[kk];
                            while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        }
                        US1_pt->US_THR = Chks;
                        while (!(US1_pt->US_CSR & AT91C_US_TXEMPTY));
                    }
                }
                break;
              default:
                Cns2Disp = 1;
            }
        }
        else{
            CnsCom = ChangeCns (CnsCom);
            microsecondi(ADJ_LOOP_PERIOD);
        }
        break;
      case 3:

        if (Cns3On==1){
            //se si usa if dopo ottimizzazione elimina parte da if (ToSend==0) nel caso Cns1Disp==0 !!
            switch (Cns3Disp){
              case 1:
                if (( NByteRx == 3 ) && ( RxCns[3] == 0)){
                    SEQ_AddEvent(eCns3Ok,Sys);
                    if ((RxCnsOld3[0] == RxCns[0]) && (RxCnsOld3[1] == RxCns[1])){
                        INGRESSICNS[0].BB[0] = RxCns[0];
                        INGRESSICNS[0].BB[1] = RxCns[1];
                        RxCnsDeb3 = CNSDEB;
                    }
                    else{
                        if (RxCnsDeb3 >0){
                            RxCnsDeb3--;
                        }
                        else{
                            RxCnsOld3[0] = RxCns[0];
                            RxCnsOld3[1] = RxCns[1];
                        }
                    }

                    NByteRx = 0;
                    RxCns[3] = 0;
                    ToSend3 = 1;
                    CnsCom = ChangeCns (CnsCom);
                }
                else{
                    if (ToSend3 == 0){
                        if (--Count3 == 0) {
                            SEQ_AddEvent(eCns3NotOk,Sys);
                            NByteRx = 0;
                            RxCns[3] = 0;
                            ToSend3 = 1;
                            CnsCom = ChangeCns (CnsCom);
                        }
                    }
                    else{
                        NByteRx = 0;
                        RxCns[3] = 0;
                        Count3 = MAX_CNS_COUNT;
                        ToSend3 = 0;
                        CnsAdd = CnsAdd1;
                        US1_pt->US_THR = CnsAdd;                      // Invio indirizzo 1� byte
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        US1_pt->US_THR = CnsAdd & 0x000000FF;         // Invio indirizzo 2� byte
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        Chks = 0;
                        for (kk = 0; kk < 2; kk++){
                            TxCns[0].txdspcns[kk] = USCITECNS[1].BB[kk];
                            US1_pt->US_THR = TxCns[0].txdspcns[kk];
                            Chks ^= TxCns[0].txdspcns[kk];
                            while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        }
                        US1_pt->US_THR = DisplayBuff[key3];//48 + key3;//225
                        Chks ^= DisplayBuff[key3];//48 + key3;//225;
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        US1_pt->US_THR = 0;
                        Chks ^= 0;
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        US1_pt->US_THR = 1 + key3;
                        Chks ^= 1 + key3;
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        key3++;
                        if (key3>31)
                            key3=0;
                        for (kk = 0; kk < 6; kk++){
                            US1_pt->US_THR = 0;
                            Chks ^= 0;
                            while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        }
                        US1_pt->US_THR = Chks;
                        while (!(US1_pt->US_CSR & AT91C_US_TXEMPTY));
                    }
                }
                break;
              case 0:
                if (( NByteRx == 3 ) && ( RxCns[3] == 0)){
                    SEQ_AddEvent(eCns3Ok,Sys);
                    if ((RxCnsOld3[0] == RxCns[0]) && (RxCnsOld3[1] == RxCns[1])){
                        INGRESSICNS_NODISP[0].BB[0] = RxCns[0];
                        INGRESSICNS_NODISP[0].BB[1] = RxCns[1];
                        NoDispToDispInput(0);
                        RxCnsDeb3 = CNSDEB;
                    }
                    else{
                        if (RxCnsDeb3 >0){
                            RxCnsDeb3--;
                        }
                        else{
                            RxCnsOld3[0] = RxCns[0];
                            RxCnsOld3[1] = RxCns[1];
                        }
                    }

                    NByteRx = 0;
                    RxCns[3] = 0;
                    ToSend3 = 1;
                    CnsCom = ChangeCns (CnsCom);
                }
                else{
                    if (ToSend3 == 0){
                        if (--Count3 == 0){
                            SEQ_AddEvent(eCns3NotOk,Sys);
                            NByteRx = 0;
                            RxCns[3] = 0;
                            ToSend3 = 1;
                            CnsCom = ChangeCns (CnsCom);
                        }
                    }
                    else{
                        NByteRx = 0;
                        RxCns[3] = 0;
                        Count3 = MAX_CNS_COUNT;
                        ToSend3 = 0;
                        // Invio x ritardo di
                        // allineamento nodisplay
                        CnsAdd = CnsAdd1 + 5;
                        US1_pt->US_THR = CnsAdd;                      // Invio indirizzo 1� byte
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        US1_pt->US_THR = CnsAdd & 0x000000FF;         // Invio indirizzo 2� byte
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        Chks = 0;
                        for (kk = 0; kk < 2; kk++){
                            TxCns[0].txdspcns[kk] = USCITECNS_NODISP[0].BB[kk];
                            US1_pt->US_THR = TxCns[0].txdspcns[kk];
                            Chks ^= TxCns[0].txdspcns[kk];
                            while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        }
                        US1_pt->US_THR = Chks;
                        while (!(US1_pt->US_CSR & AT91C_US_TXEMPTY));

                        CnsAdd = CnsAdd1 + 5;
                        US1_pt->US_THR = CnsAdd;                      // Invio indirizzo 1� byte
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        US1_pt->US_THR = CnsAdd & 0x000000FF;         // Invio indirizzo 2� byte
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        Chks = 0;
                        for (kk = 0; kk < 2; kk++){
                            TxCns[0].txdspcns[kk] = USCITECNS_NODISP[0].BB[kk];
                            US1_pt->US_THR = TxCns[0].txdspcns[kk];
                            Chks ^= TxCns[0].txdspcns[kk];
                            while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        }
                        US1_pt->US_THR = Chks;
                        while (!(US1_pt->US_CSR & AT91C_US_TXEMPTY));
                        // Fine Invio x ritardo di
                        // allineamento nodisplay
                        //          USCITE[AT]=1;     // Debug
                        CnsAdd = CnsAdd1 + 0;
                        US1_pt->US_THR = CnsAdd;                      // Invio indirizzo 1� byte
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        US1_pt->US_THR = CnsAdd & 0x000000FF;         // Invio indirizzo 2� byte
                        while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        Chks = 0;
                        DispToNoDispOutput();
                        for (kk = 0; kk < 2; kk++){
                            TxCns[0].txdspcns[kk] = USCITECNS_NODISP[1].BB[kk];
                            US1_pt->US_THR = TxCns[0].txdspcns[kk];
                            Chks ^= TxCns[0].txdspcns[kk];
                            while (!(US1_pt->US_CSR & AT91C_US_TXRDY));
                        }
                        US1_pt->US_THR = Chks;
                        while (!(US1_pt->US_CSR & AT91C_US_TXEMPTY));
                    }
                }
                break;
              default:
                Cns3Disp = 1;
            }
        }
        else{
            CnsCom = ChangeCns (CnsCom);
            microsecondi(ADJ_LOOP_PERIOD);
        }
        break;
      default:
        CnsCom = 1;
    }

    INGRESSICNS[1].BB[0] |= INGRESSICNS[2].BB[0]|INGRESSICNS[0].BB[0];
    INGRESSICNS[1].BB[1] |= INGRESSICNS[2].BB[1]|INGRESSICNS[0].BB[1];



}

// ------------------------------------------------------
// ChangeCns
// ------------------------------------------------------------

unsigned char ChangeCns(unsigned char NCns )
{ 		
    switch(NCns)
    {
      case 1:
        if(Cns2On==1)
            NCns = 2;
     	
        else if(Cns3On==1)     	
            NCns = 3;     			
     	
        else NCns = NCns;     		
        break;

      case 2:
        if(Cns3On==1)
            NCns = 3;

        else if(Cns1On==1)
            NCns = 1;     			

        else NCns = NCns;
        break;     		

      case 3:
        if(Cns1On==1)
            NCns = 1;

        else if(Cns2On==1)
            NCns = 2;     			

        else NCns = NCns;
        break;
    }
    return (NCns);
}




