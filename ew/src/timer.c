

#include "AT91SAM7S256.h"
#define __inline inline
#include "lib_AT91SAM7S256.h"

#include "timer.h"
#include "usart1.h"
#include "deviceSetup.h"
#include "inout.h"


#define TIMER0_INTERRUPT_LEVEL		7
#define TIMER1_INTERRUPT_LEVEL		6
#define TIMER2_INTERRUPT_LEVEL		3

#define TC_CLKS_MCK2             0x0
#define TC_CLKS_MCK8             0x1
#define TC_CLKS_MCK32            0x2
#define TC_CLKS_MCK128           0x3
#define TC_CLKS_MCK1024          0x4


// -----------------------------------------------------------------------------
//
// Variabili
//
// -----------------------------------------------------------------------------

unsigned char SyntTimeCount = 0,  Dlyms = 0;

unsigned char T200 = 0, T300 = 0, T400 = 0, TOn = 0, TOff = 0, TBioOn = 0;

volatile unsigned char  SysTimeCount=0;

// -----------------------------------------------------------------------------
//
// Costanti
//
// -----------------------------------------------------------------------------

AT91PS_TC TC0_pt = AT91C_BASE_TC0;

AT91PS_TC TC1_pt = AT91C_BASE_TC1;

AT91PS_TC TC2_pt = AT91C_BASE_TC2;

AT91PS_PIO PIO_pt = AT91C_BASE_PIOA;


// -----------------------------------------------------------------------------
//
//   Gestione interrupt da TIMER TC1
//
// -----------------------------------------------------------------------------

__ramfunc void TC1_irq_handler( void )
{
  unsigned int dummy;

  dummy = TC1_pt->TC_SR;
  dummy = dummy;

  TC1_pt->TC_RC = 0xB500;
}

// -----------------------------------------------------------------------------
//
//   Gestione interrupt da TIMER TC2
//
// -----------------------------------------------------------------------------


__ramfunc void TC2_irq_handler( void )
{
    unsigned int dummy;
    static unsigned char Cnt2 = 0,Cnt4 = 0;
    unsigned char xx;

    dummy = TC2_pt->TC_SR;
    dummy = dummy;

    TC2_pt->TC_RC = 0x1248;
    T200 = !T200;                           //zuck

    if (Cnt4 >= 3) {
        Cnt4 = 0;
        T400 = !T400;
    }
    else {
        Cnt4 ++;
    }

    if (Cnt2 >= 1) {
        Cnt2 = 0;
        T300 = !T300;                         //zuck
    }
    else {
        Cnt2 ++;
    }

    for (xx = 1; xx <= OUT_ACT; xx++) {  //Leone aggiunto = a <
        if (UscitePuls[xx] != 0) {
            UscitePuls[xx] --;
            if (UscitePuls[xx] == 0 ) {
                if (USCITE[xx] != 0)  {
                     USCITE[xx] = 0;
                }
                else {
                    USCITE[xx] = 1;
                }
            }
        }
    }
    if (TOn > 0) {
        TOn--;
    }
    if (TOff>0) {
        TOff--;
    }
    if (TBioOn>0) {
        TBioOn--;
    }
    SyntTimeCount++;
    SysTimeCount++;
}


// -----------------------------------------------------------------------------
//
//   Inizializzazione timer TC0
//
// -----------------------------------------------------------------------------
/*
void TC0_init ( void )
{
  unsigned int dummy;

  // Enable the clock of the TIMER

  AT91F_PMC_EnablePeriphClock ( AT91C_BASE_PMC, 1 << AT91C_ID_TC0 );

  // Disable the clock and the interrupts

 	TC0_pt->TC_CCR = AT91C_TC_CLKDIS;
	TC0_pt->TC_IDR = 0xFFFFFFFF;

  //  Clear status bit

  dummy = TC0_pt->TC_SR;
  dummy = dummy;

  // Set the Mode of the Timer Counter 200 ms

  // Set the Mode of the Timer Counter 50�sec

  TC0_pt->TC_CMR = TC_CLKS_MCK2 | AT91C_TC_WAVE | AT91C_TC_WAVESEL_UP_AUTO;

  // Set the counter to 1248 x 41 nsec = 52.4 �sec

  TC0_pt->TC_RC = 0x04E0;				// 52.4 �sec


  // Enable the clock

  TC0_pt->TC_CCR = AT91C_TC_CLKEN ;

	AT91F_AIC_ConfigureIt ( AT91C_BASE_AIC,
	                        AT91C_ID_TC0,
	                        TIMER0_INTERRUPT_LEVEL,
	                        AT91C_AIC_SRCTYPE_INT_HIGH_LEVEL,
	                        TC0_irq_handler );

  //  IRQ enable CPC
	
	TC0_pt->TC_IER = AT91C_TC_CPCS;

	AT91F_AIC_EnableIt (AT91C_BASE_AIC, AT91C_ID_TC0);

  AT91F_AIC_Trig (AT91C_BASE_AIC,AT91C_ID_TC0) ;

  AT91C_BASE_TC0->TC_CCR = AT91C_TC_SWTRG ;
}
*/
// -----------------------------------------------------------------------------
//
//   Inizializzazione timer TC1
//
// -----------------------------------------------------------------------------

void TC1_init ( void )
{
  unsigned int dummy;

  // Enable the clock of the TIMER

  AT91F_PMC_EnablePeriphClock ( AT91C_BASE_PMC, 1 << AT91C_ID_TC1 );

  // Disable the clock and the interrupts

	TC1_pt->TC_CCR = AT91C_TC_CLKDIS ;
	TC1_pt->TC_IDR = 0xFFFFFFFF ;

  //  Clear status bit

  dummy = TC1_pt->TC_SR ;
  dummy = dummy ;

  // Set the Mode of the Timer Counter

  TC1_pt->TC_CMR = TC_CLKS_MCK1024| AT91C_TC_WAVE | AT91C_TC_WAVESEL_UP_AUTO;

//  TC1_pt->TC_RC = 0xAFFF;
  TC1_pt->TC_RC = 0xB500;

  // Enable the clock

  TC1_pt->TC_CCR = AT91C_TC_CLKEN ;

	AT91F_AIC_ConfigureIt ( AT91C_BASE_AIC,
	                        AT91C_ID_TC1,
	                        TIMER0_INTERRUPT_LEVEL,
	                        AT91C_AIC_SRCTYPE_INT_HIGH_LEVEL,
	                        TC1_irq_handler );

  //  IRQ enable CPC
	
	TC1_pt->TC_IER = AT91C_TC_CPCS;
	
	AT91F_AIC_EnableIt (AT91C_BASE_AIC, AT91C_ID_TC1);

  AT91F_AIC_Trig (AT91C_BASE_AIC,AT91C_ID_TC1) ;

  AT91C_BASE_TC1->TC_CCR = AT91C_TC_SWTRG ;
}

// -----------------------------------------------------------------------------
//
//   Inizializzazione timer TC2
//
// -----------------------------------------------------------------------------

void TC2_init ( void )
{
 unsigned int dummy;

  // Enable the clock of the TIMER

  AT91F_PMC_EnablePeriphClock ( AT91C_BASE_PMC, 1 << AT91C_ID_TC2 );

  // Disable the clock and the interrupts

	TC2_pt->TC_CCR = AT91C_TC_CLKDIS ;
	TC2_pt->TC_IDR = 0xFFFFFFFF ;

  //  Clear status bit

  dummy = TC2_pt->TC_SR ;
  dummy = dummy ;

  // Set the Mode of the Timer Counter

  TC2_pt->TC_CMR = TC_CLKS_MCK1024 | AT91C_TC_CPCTRG;

  TC2_pt->TC_RC = 0x1248;

  // Enable the clock

  TC2_pt->TC_CCR = AT91C_TC_CLKEN ;

	AT91F_AIC_ConfigureIt ( AT91C_BASE_AIC,
	                        AT91C_ID_TC2,
	                        TIMER2_INTERRUPT_LEVEL,
	                        AT91C_AIC_SRCTYPE_INT_HIGH_LEVEL,
	                        TC2_irq_handler );

  //  IRQ enable CPC
	
	TC2_pt->TC_IER = AT91C_TC_CPCS;
	
	AT91F_AIC_EnableIt (AT91C_BASE_AIC, AT91C_ID_TC2);

	AT91F_AIC_Trig (AT91C_BASE_AIC,AT91C_ID_TC2) ;

	AT91C_BASE_TC2->TC_CCR = AT91C_TC_SWTRG ;
}

