/*****************************************************************************
* IAR visualSTATE Device driver for handling action functions to outputs
* headerfile.
*****************************************************************************/

#ifndef _VSDOORSOUTPUT_H
#define _VSDOORSOUTPUT_H

#include "SEMLibE.h"
#include "VSDoorsMain.h"
#include "deviceSetup.h"

/* *** macros *** */

#define drive_mask 0xF0   /*use high nibble as drive status*/
//#define floor_mask 0x0F   /*use low nibble as floor indicator*/
//#define drive_up 0x80
//#define drive_stop 0x40
#define drive_down 0x20

#ifdef AM_KEY_VER
#define EEPROM_CFG_ADDRESS  0x0000 //massimo 32 parametri di 4 bytes (compreso versione)
#define EEPROM_MASK_ADDRESS 0x0080

#else

#define EEPROM_CFG_ADDRESS  0x0200 //massimo 32 parametri di 4 bytes (compreso versione)
#define EEPROM_MASK_ADDRESS 0x0280
#define EEPROM_OLD_OFFSET   0x0200
#endif

#define EEPROM_MASK_RATIO 64 // (16 valori (1 valore ogni EEPROM_MASK_RATIO su MaskSize) * 4 bytes) = 64 bytes
//#define EEPROM_MASK_ADDRESS 512
//#define EEPROM_MASK_RATIO 16 // (64 valori (1 valore ogni EEPROM_MASK_RATIO su MaskSize) * 4 bytes) = 512 bytes
// produce sovrascrittura: sulle maschere nei primi campioni sono riportati i parametri e maschera 2 sovrascrive 1


#define UPDATE_POS_SAVE_COUNT 10
#define UPDATE_POS_FILTER 4  //deve essere >1; 2 corrisponde ad un peso del vecchio valore del 50%; 3=67%

#define CHR_OPEN    43 //'+'
#define CHR_CLOSE   45 //'-'
#define CHR_MASK    109 //'m'
#define CHR_PWM     112 //'p'
#define CHR_CUR     99 //'c'

#define PAR_NUM_MAX 26

extern const char MSG_16[];

VS_VOID UpdateSwitch (VS_VOID);

void DisplayValues(void);

struct tMotSys {
  //to save start ..variabili di elaborazione condivise con VS
  VS_UINT _CurStep;
  VS_UINT _CurThrOverClose;
  VS_UINT _CurThrOverOpen;
  VS_UINT _ParNumCounter;
  VS_UINT _ParSel;
  VS_UINT _Pos;
  VS_UINT _PosClose;
  VS_UINT _PosInvClose;
  VS_UINT _PosInvOpen;
  VS_UINT _PosInvOpenShk;		//aggiunto in v. 2.29
  VS_UINT _PosMaxClose;
  VS_UINT _PosMinOpen;
  VS_UINT _PosMinOpenShk;		//aggiunto in v. 2.29
  VS_UINT _PosNearClose;
  VS_UINT _PosNearOpen;
  VS_UINT _PosNearOpenShk;		//aggiunto in v. 2.29
  VS_UINT _PosOpen;
  VS_UINT _PosOpenShk;			//aggiunto in v. 2.29
  VS_UINT _PosOffsetAO;
  VS_UINT _PosOffsetAC;
  VS_UINT _MovCount;
  VS_BOOL _bParReread;
  VS_BOOL _bEnExtProt;
  VS_BOOL _bEmergency;
  VS_BOOL _bPassiveMov;
  VS_BOOL _bLowBatt;
  //to save end
  VS_UINT PosMinClose;
  VS_UINT AccClose;
  VS_UINT AccOpen;
  VS_UINT Counter;
  VS_UINT CurThrOpen;
  VS_UINT CurThrClose;
  VS_UINT CurThrSlope;
  VS_UINT DecClose;
  VS_UINT DecOpen;
  VS_UINT Dir;
  VS_UINT PosAccClose;
  VS_UINT PosAccOpen;
  VS_UINT PosAccOpenShk;
  VS_UINT PosCur;
  VS_UINT PosDecClose;
  VS_UINT PosDecOpen;
  VS_UINT PosDecOpenShk;
  VS_UINT PosDelta;
  VS_UINT PwmAccClose;
  VS_UINT PwmAccOpen;
  VS_UINT PwmActive;
  VS_UINT PwmChannel;
  VS_UINT PwmDecClose;
  VS_UINT PwmDecOpen;
  VS_UINT PwmFindClose;
  VS_UINT PwmFindOpen;
  VS_UINT PwmInitClose;
  VS_UINT PwmInitOpen;
  VS_UINT PwmNearClose;
  VS_UINT PwmNearOpen;
  VS_UINT PwmPosClose;
  VS_UINT PwmPosOpen;
  VS_UINT PwmStartClose;
  VS_UINT PwmStartOpen;
  VS_UINT PwmTarget;
  VS_UINT Version;
  VS_BOOL bCloseDetected;
  VS_BOOL bCloseEnabled;
  VS_UINT bManual;
  VS_BOOL bOpen;
  VS_BOOL bOpenDetected;
  VS_BOOL bOpenEnabled;
  VS_BOOL bOverCurDetected;
  VS_BOOL bOverCurEnabled;
  VS_BOOL bPosNotifyEnabled;
  VS_UINT bProg;
  VS_BOOL bUnderCurDetected;
  VS_BOOL bUnderCurEnabled;
  VS_BOOL bUpdateMaskEnabled;
  VS_UINT Cur;
  VS_UINT CurPos;
  VS_UINT MovDelta;
  VS_UINT EncoderCCWCfg;
  VS_UINT StrokeShk;
  //process
  VS_UINT Sys;
  VS_UINT Mot;
  VS_UINT RacCount;
  VS_UINT ToWriteOpened;
  VS_UINT OverCountMax;
  VS_UINT Toggle;
  VS_UINT CountDown;
  VS_UINT EntryPwmDecClose;
  VS_UINT EntryPwmDecOpen;
  VS_UINT EntryPosDecClose;
  VS_UINT EntryPosDecOpen;
  VS_UINT OverCount;
  VS_UINT OldPos;
  VS_UINT LastPosMovClose;
  VS_UINT LastPosMovOpen;
  VS_UINT FirstUpdate;
  VS_UINT PosCh;
  VS_UINT CurCh;
  VS_UINT PwmCh;
  VS_UINT PwmChId;
  VS_UINT PwmPh;
  unsigned char CpOk;
  unsigned char CpNok;
  unsigned char Gom;
  unsigned char CatchGom;
  unsigned char Pc;
  unsigned char Pa;
  unsigned char Err;
  unsigned char Prg;
  unsigned char Frn;
  unsigned char ViolP;
  unsigned char CatchRun;
  unsigned char CatchSS;
  unsigned char CatchSP;
  //to save start ..variabili di configurazione condivise con VS
  VS_UINT  _BrakeOn;
  VS_UINT  _Motor;
  VS_UINT  _SSCfg;
  VS_UINT  _SPCfg;
  VS_UINT  _STCfg;
  VS_UINT  _CloseOn;

  //to save stop
  VS_UINT  Run;
  VS_UINT  Dip1;
  VS_UINT  Dip2;
  VS_UINT  Dip5;
  unsigned char CatchDip5;
  VS_UINT  Dip6;
  unsigned char ExtProtEnabled;
  #ifdef CUR_CONTROL
  unsigned short CurMaskOpen[MASK_SIZE];
  unsigned short CurMaskClose[MASK_SIZE];
  unsigned short CurLastOpen[MASK_SIZE];
  unsigned short CurLastClose[MASK_SIZE];
  unsigned short PwmMaskOpen[MASK_SIZE];
  unsigned short PwmMaskClose[MASK_SIZE];
  unsigned char CurMaskCloseOk;
  unsigned char CurMaskOpenOk;
  VS_BOOL bAcqMaxCurADC;
  unsigned short MaxCurADC[11];
  #endif
  unsigned int ParValues[PAR_NUM_MAX];
};
typedef struct tMotSys tMotSys;

extern tMotSys MotSys[];
extern tMotSys * pm;

void RestoreMotSys(tMotSys * pmot);
void SaveMotSys(tMotSys * pmot);

#endif
