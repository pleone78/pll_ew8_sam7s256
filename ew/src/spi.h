// -----------------------------------------------------------------------------
//
// Headers
//
// -----------------------------------------------------------------------------

#ifndef spi_h
#define spi_h

#include "AT91SAM7S256.h"
#define __inline inline
#include "lib_AT91SAM7S256.h"

// -----------------------------------------------------------------------------
// 
//  COMANDI PER ISD4002
// 
// -----------------------------------------------------------------------------

#define     ISD_POWERUP		0x0004       			// power up
#define     ISD_SETPLAY 	0x0007        		// play from address
#define     ISD_PLAY    	0x000F        		// play from current address
#define     ISD_SETREC  	0x0005        		// rec  from address
#define     ISD_REC     	0x000D        		// rec from current address
#define     ISD_SETMC   	0x0017        		// set message cueing from address
#define     ISD_MC      	0x001F        		// message cueing
#define     ISD_STOP    	0x000C        		// stop
#define     ISD_STOPPWRDN 0x0008      			// stop and stand by 
#define     ISD_RINT    	0x000C            // leggi INT

// -----------------------------------------------------------------------------
// 
//  Prototipi
// 
// -----------------------------------------------------------------------------


void SPI_init(void);

unsigned short MirrorShort(unsigned short inval);

void PlayMsg(unsigned short nmsg);

void RecMsg(unsigned short nmsg);

void StopMsg(void);

#endif
