
#ifndef timer_h
#define timer_h

//extern unsigned char BeepRun; 



// -----------------------------------------------------------------------------
// 
//  Prototipi timers
// 
// -----------------------------------------------------------------------------

__ramfunc void TC0_irq_handler( void );

__ramfunc void TC1_irq_handler( void );

__ramfunc void TC2_irq_handler( void );

void TC0_init ( void );
void TC1_init ( void );
void TC2_init ( void );

#endif



