// -----------------------------------------------------------------------------
// 
//  Inclusione headers
// 
// -----------------------------------------------------------------------------

#include "deviceSetup.h"
#include "spi.h"
#include "usart.h"

// -----------------------------------------------------------------------------
// 
//  FUNZIONE PLAY
// 
//  Premo pulsante PLAY ed ascolto messaggio oppure ripremo PLAY per passare a
//  quello successivo oppure se conosco la posizione del messaggio premo PLAY
//  tante volte quant'� il numero del messaggio oppure tengo premuto PLAY a 
//  lungo per ricominciare
// 
//  FUNZIONE REC
// 
//  Premo pulsante REC e registro messaggio fino alla ulteriore pressione di REC
//  e cos� via fino alla fine dei 120sec
//  Se ascolto i messaggi con PLAY posso accodare all'ultimo messaggio ascoltato
//  uno nuovo ma perdo quelli eventualmente memorizzati di seguito in precedenza
// 
// -----------------------------------------------------------------------------


#define NPCS0 0x00




// AT91C_SPI_SCBR600 = MCK / SPCK = 47923200 / 600000 = 80 = 0x50

#define AT91C_SPI_SCBR600 ((unsigned int) 0x50 <<  8)  

#define Tpud 25

extern unsigned char SyntTimeCount;

// -----------------------------------------------------------------------------
// 
//  Inizializzazione SPI
// 
// -----------------------------------------------------------------------------

void SPI_init(void)
{
  AT91PS_SPI p_SPI = AT91C_BASE_SPI;        // Indirizzo base registri SPI

  AT91F_PIO_CfgPeriph( AT91C_BASE_PIOA,     // PIO controller base address
                       AT91C_PA13_MOSI |    // MOSI PA13 - pin 22
                       AT91C_PA14_SPCK |    // SPCK PA14 - pin 21
                       AT91C_PA11_NPCS0 |   // NPCS0 PA11 - pin 11
                       AT91C_PA12_MISO,     // MISO PA12 - pin 27 Periph. A
                       0);                  // Nessuna PA su Peripheral B

  // Abilita clock su Periph. A
 
  AT91F_PMC_EnablePeriphClock( AT91C_BASE_PMC, 1 << AT91C_ID_SPI );

  // Cfg Master: MSTR=1,PS=0,PCSDEC=0,MODFDIS=0,LLB=0,PCS=0000,DLYBCS=0x00
  
  p_SPI->SPI_MR = AT91C_SPI_MSTR ; 

  // Cfg Chip Select: CPOL=0,NCPHA=1,CSAAT=0,BITS=1000=16,SCBR600=0x50,
  //                  DLYBS = 0, DLYBCT = 0;

  *(p_SPI->SPI_CSR) = AT91C_SPI_NCPHA | AT91C_SPI_BITS_16 | AT91C_SPI_SCBR600;    

  // Abilita SPI
  
  p_SPI->SPI_CR = AT91C_SPI_SPIEN;
  
  p_SPI->SPI_TDR = ISD_POWERUP;
}

// -----------------------------------------------------------------------------
// 
//  Funzione specchio a 2 byte 
// 
// -----------------------------------------------------------------------------

unsigned short MirrorShort(unsigned short inval)
{
  unsigned short retval,bit0,ii;
   
  for( ii=0 ; ii<16 ; ii++)
  {
    bit0 = inval & 0x0001;
    inval = inval >> 1;
    retval = retval << 1;
    retval  = retval | bit0;  
  }

  return retval;    
}

// -----------------------------------------------------------------------------
// 
//  Funzione di ascolto messaggi: n.1 = Allarme METAL    n.2 = Allarme +P
// 
// -----------------------------------------------------------------------------

void PlayMsg(unsigned short nmsg)
{
  unsigned short Nummsg = 0;
  
  AT91PS_SPI p_SPI = AT91C_BASE_SPI;
  
//  p_SPI->SPI_TDR = ISD_POWERUP;  
    
//  millisecondi(Tpud);  

  AT91F_PIO_SetOutput( AT91C_BASE_PIOA,V_SAMP);

  Nummsg = nmsg - 1;
  
//  millisecondi(100);

  p_SPI->SPI_TDR = ISD_SETPLAY | MirrorShort(Nummsg * 75);  
}

// -----------------------------------------------------------------------------
// 
//  Funzione di registrazione messaggi
// 
// -----------------------------------------------------------------------------

void RecMsg(unsigned short nmsg)
{
  unsigned short Nummsg = 0;
  
  AT91PS_SPI p_SPI = AT91C_BASE_SPI;
  
  Nummsg = nmsg - 1;

  AT91F_PIO_SetOutput( AT91C_BASE_PIOA,V_SAMP);

//  millisecondi(100);
  
  p_SPI->SPI_TDR = ISD_SETREC | MirrorShort(Nummsg * 75);
  
//  SyntTimeCount = 0;  
//  millisecondi(500);

//  while (SyntTimeCount < 150)
//  {

//  }
  
//  p_SPI->SPI_TDR = ISD_STOP;
}

// -----------------------------------------------------------------------------
// 
//  Funzione di stop sintesi
// 
// -----------------------------------------------------------------------------

void StopMsg(void)
{  
  AT91PS_SPI p_SPI = AT91C_BASE_SPI;  
  p_SPI->SPI_TDR = ISD_STOP;
}

