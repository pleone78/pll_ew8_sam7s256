// -----------------------------------------------------------------------------
//
//  Headers
//
// -----------------------------------------------------------------------------
#include "VSLogicData.h"
#include "twi.h"
#include "deviceSetup.h"
#include "usart.h"

//#define TWI_PAGE_WRITE
//#define TWI_POLL_READ
#define MAX_LOOP_COUNT 20000;

unsigned int TWI_ReadError = 0;
unsigned int TWI_WriteError = 0;

unsigned char SecPLL[2], MinPLL[2], HourPLL[2], DayPLL[2], DatePLL[2];
unsigned char MonthPLL[2], YearPLL[2], DayName[3]; 
unsigned char Orario[7],DataOra[7], Trickle = 0xA5, Tricklev;
unsigned int /*sizeW, sizeWd, */sizenew, addrnew;

// -----------------------------------------------------------------------------
//
//  Impostazione clock TWI
//
// -----------------------------------------------------------------------------

void  Cfg_Clk_TWI ( AT91PS_TWI pTWI,
                  unsigned int CKDIV,
                  unsigned int CHDIV,
                  unsigned int CLDIV )
{
  CHDIV = CHDIV << 8;
  CKDIV = CKDIV << 16;
  pTWI->TWI_CWGR = CKDIV | CHDIV | CLDIV;
}


void TWI_init ( void )
{
//  AT91F_PMC_EnablePeriphClock ( AT91C_BASE_PMC, (1 << AT91C_ID_PIOA));

// Definisci puntatore ai registri PIOA

//	AT91PS_PIO pPio = AT91C_BASE_PIOA;
	
// Definisci puntatore ai registri TWI

	AT91PS_TWI pTWI = AT91C_BASE_TWI;

// Abilita clock sulla porta TWI

  AT91F_PMC_EnablePeriphClock ( AT91C_BASE_PMC, (1 << AT91C_ID_TWI) );

// Assegna la funzione TWI alle porte: PA3 = TWD; PA4 = TWCK (peripheral A)

  AT91F_PIO_CfgPeriph(AT91C_BASE_PIOA, AT91C_PA3_TWD | AT91C_PA4_TWCK, 0);

// Elimina pull-up dalle porte TWD e TWCK
// non disabilitare se non strettamente necessario
//(al resetARM li ha attivi e se si disabilitano si alterano tutti)

  AT91F_PIO_CfgPullup(AT91C_BASE_PIOA, ~(AT91C_PA3_TWD | AT91C_PA4_TWCK));

// Configura porta TWD in open drain per la bidirezionalitÓ

	AT91F_PIO_CfgOpendrain(AT91C_BASE_PIOA, AT91C_PA3_TWD);

// Configura porta clock


	pTWI->TWI_IDR = (unsigned) -1;
	pTWI->TWI_CR = AT91C_TWI_SWRST;
	pTWI->TWI_CR = AT91C_TWI_MSEN | AT91C_TWI_SVDIS;

  
    
  //Cfg_Clk_TWI ( AT91C_BASE_TWI, 1, 55, 65 );    //debug leone   THigh=2775ns, TLow=2350ns   195 121 Hz
  //Cfg_Clk_TWI ( AT91C_BASE_TWI, 1, 50, 50 );    //debug leone   THigh=2149ns, TLow=2149ns   232 666 Hz
  Cfg_Clk_TWI ( AT91C_BASE_TWI, 1, 35, 45 );   //debug leone   THigh=1520ns,  TLow=1940ns   289 017 Hz
  // Cfg_Clk_TWI ( AT91C_BASE_TWI, 1, 20, 35 );   //debug leone   THigh=897ns,  TLow=1520ns   413 736 Hz 
  
  millisecondi(20);
  TWI_Write ( AT91C_BASE_TWI, DS1339, 0x10,(unsigned char *)&Trickle , 1);  
}


// -----------------------------------------------------------------------------
//
//  Scrivi TWI/I2C EEPROM_24AA08 DS1339 EEPROM_24AA256
//
// -----------------------------------------------------------------------------

void TWI_Write ( AT91PS_TWI pTwi,          // Registri TWI
               unsigned int TYPE_TWI,      // Tipo slave
               unsigned int address,       // Indirizzo 1░ dato su slave
               unsigned char *dataw,       // 1░ dato da scrivere
               unsigned int size )         // Numero byte da scrivere
{
  unsigned int sizeW,sizeWd;  
  unsigned int LoopCount = MAX_LOOP_COUNT; 
  unsigned int status;
  
  pTwi->TWI_MMR = TYPE_TWI & ~AT91C_TWI_MREAD;  // TWI mode: master mode, write    
  while (size !=0)
  {
    if (size <= (EEPROM_PAGE - (address%EEPROM_PAGE)))
    {
      sizeW = size;
      size = 0;
    }
    else if (size > (EEPROM_PAGE - (address%EEPROM_PAGE)))
    {
      sizeW = EEPROM_PAGE - (address%EEPROM_PAGE);
      sizeWd = sizeW;
      size -= (EEPROM_PAGE - (address%EEPROM_PAGE));
    }

    pTwi->TWI_IADR = address;           // Indirizzo dove inizia la scrittura
    status = pTwi->TWI_SR;    
    DisAllInt();   
    pTwi->TWI_THR = *( dataw++ );       // Carica registro di scrittura
    EnAllInt();
    pTwi->TWI_CR = AT91C_TWI_START;     // Avvia la scrittura
      
    LoopCount = MAX_LOOP_COUNT;         // Controllo ACK
    while ((!(pTwi->TWI_SR & AT91C_TWI_TXRDY & ~AT91C_TWI_NACK)) && (LoopCount > 0))
    {
      ResetWatchdog();
      if (LoopCount > 0)
      {
        LoopCount--;
      }  
      pTwi->TWI_CR = AT91C_TWI_START;   // Ripeti avvio scrittura se NACK
    } 
    if (LoopCount == 0)
    {
      TWI_WriteError = 3;
    }

    while (sizeW-- >1)                  // Ciclo scritture multiple
    {      
      LoopCount = MAX_LOOP_COUNT;       // Attesa fine scrittura
      while ((!(pTwi->TWI_SR & AT91C_TWI_TXRDY)) && (LoopCount > 0))
      {
        ResetWatchdog();
        if (LoopCount > 0)
        {
          LoopCount--;
        }
      }
      if (LoopCount == 0)
      {
        TWI_WriteError = 1;
      }      
      if (pTwi->TWI_SR & AT91C_TWI_UNRE)
      {
        TWI_WriteError = 4;
      }      
      DisAllInt();   
      pTwi->TWI_THR = *( dataw++ );     // Carica il byte successivo      
      EnAllInt();  
    }
  
    pTwi->TWI_CR = AT91C_TWI_STOP;      // Ferma la scrittura 
    status = pTwi->TWI_SR;
      
    LoopCount = MAX_LOOP_COUNT;         // Attesa completamento stop scrittura
    while ((!(pTwi->TWI_SR & AT91C_TWI_TXCOMP)) && (LoopCount > 0))
    {
      ResetWatchdog();
      if (LoopCount > 0)
      {
        LoopCount--;
      }
    }
    if (LoopCount == 0)
    {
      TWI_WriteError = 2;
    }    
    status  = status;                   // Evita warning su status    
    address += sizeWd;  
    millisecondi(6);
  }
}


// -----------------------------------------------------------------------------
//
//  Leggi TWI/I2C EEPROM_24AA08 DS1339 EEPROM_24AA256
//
// -----------------------------------------------------------------------------

void TWI_Read ( AT91PS_TWI pTwi,          // Registri TWI

               unsigned int TYPE_TWI,     // Tipo slave

               unsigned int address,      // Indirizzo 1░ dato su slave

               unsigned char *datar,      // 1░ dato da leggere

               unsigned int size )        // Numero byte da leggere
{

  unsigned int LoopCount = MAX_LOOP_COUNT;
  unsigned int status,ghostraed;

  pTwi->TWI_MMR = TYPE_TWI | AT91C_TWI_MREAD; // TWI mode : master mode read
  pTwi->TWI_IADR = address;                   // Indirizzo dove inizia la lettura  
  pTwi->TWI_CR = AT91C_TWI_START;             // Avvia la lettura

  LoopCount = MAX_LOOP_COUNT;                 // Controllo ACK
  while ((!(pTwi->TWI_SR & AT91C_TWI_RXRDY & ~AT91C_TWI_NACK)) && (LoopCount > 0))
  {
    ResetWatchdog();
    if (LoopCount > 0)
    {
      LoopCount--;
    }        
    pTwi->TWI_CR = AT91C_TWI_START;           // Ripeti avvio scrittura se NACK  
  }
  if (LoopCount == 0)
  {
    TWI_ReadError = 3;
  }      
  if (pTwi->TWI_SR & AT91C_TWI_OVRE)
  {
    TWI_ReadError = 4;
  }
  DisAllInt();   
  *(datar++) = pTwi->TWI_RHR;
  EnAllInt();   
  while (size-- >1)                           // Ciclo di letture multiple
  {
    LoopCount = MAX_LOOP_COUNT;               // Attesa fine lettura 
    while ((!(pTwi->TWI_SR & AT91C_TWI_RXRDY)) && (LoopCount > 0))
    {
      ResetWatchdog();
      if (LoopCount > 0)
      {
        LoopCount--;
      }
    }
    if (LoopCount == 0)
    {
      TWI_ReadError = 1;
    }
    DisAllInt(); 
    *(datar++) = pTwi->TWI_RHR;               // Copia nel buffer il dato letto
    EnAllInt(); 
  }
  
  pTwi->TWI_CR = AT91C_TWI_STOP;              // Ferma la lettura
  status = pTwi->TWI_SR;

  LoopCount = MAX_LOOP_COUNT;                 // Completamento stop lettura
  while ((!(pTwi->TWI_SR & AT91C_TWI_TXCOMP)) && (LoopCount > 0))
  {
    ResetWatchdog();
    if (LoopCount > 0)
    {
      LoopCount--;
    }
  }
  if (LoopCount == 0)
  {
    TWI_ReadError = 2;
  }
  status  = status;                           // Evita warning su status  
  ghostraed = pTwi->TWI_RHR;                  // Pulizia registro di lettura
  ghostraed = ghostraed;                      // Evita warning su ghostread
}


// -------------------------------------------------------------------2----------
// 
//  Leggi orologio DS1339
// 
// -----------------------------------------------------------------------------

void Clk_Read( void )
{
#ifndef ORSINI
  TWI_Read ( AT91C_BASE_TWI, DS1339 , 0x00,(unsigned char *)&Orario, 7);
  
  DatePLL[0]  = ((Orario[4] & 0xF0) >> 4) | 0x30;
  DatePLL[1]  =  (Orario[4] & 0x0F)       | 0x30;

  MonthPLL[0] = ((Orario[5] & 0xF0) >> 4) | 0x30;
  MonthPLL[1] =  (Orario[5] & 0x0F)       | 0x30;

  YearPLL[0]  = ((Orario[6] & 0xF0) >> 4) | 0x30;
  YearPLL[1]  =  (Orario[6] & 0x0F)       | 0x30;

  HourPLL[0]  = ((Orario[2] & 0xF0) >> 4) | 0x30;
  HourPLL[1]  =  (Orario[2] & 0x0F)       | 0x30;
  
  MinPLL[0]   = ((Orario[1] & 0xF0) >> 4) | 0x30;
  MinPLL[1]   =  (Orario[1] & 0x0F)       | 0x30;

  SecPLL[0]   = ((Orario[0] & 0xF0) >> 4) | 0x30;
  SecPLL[1]   =  (Orario[0] & 0x0F)       | 0x30;

  DayPLL[0]   = ((Orario[3] & 0xF0) >> 4) | 0x30;
  DayPLL[1]   =  (Orario[3] & 0x0F)       | 0x30;
  
  switch (DayPLL[1])
  {
    case  0x31:
    			 if(Language == 0){    			 	
                	DayName[0] = 'D';
                	DayName[1] = 'o';
                	DayName[2] = 'm';
                }
                else{
                	DayName[0] = 'S';
                	DayName[1] = 'u';
                	DayName[2] = 'n'; 
                }
                break;
    case  0x32:
                if(Language == 0){    			 	
                	DayName[0] = 'L';
                	DayName[1] = 'u';
                	DayName[2] = 'n';
                }
                else{
                	DayName[0] = 'M';
                	DayName[1] = 'o';
                	DayName[2] = 'n'; 
                }
                break;
    case  0x33:
                if(Language == 0){    			 	
                	DayName[0] = 'M';
                	DayName[1] = 'a';
                	DayName[2] = 'r';
                }
                else{
                	DayName[0] = 'T';
                	DayName[1] = 'u';
                	DayName[2] = 'e'; 
                }
                break;
    case  0x34:
                if(Language == 0){    			 	
                	DayName[0] = 'M';
                	DayName[1] = 'e';
                	DayName[2] = 'r';
                }
                else{
                	DayName[0] = 'W';
                	DayName[1] = 'e';
                	DayName[2] = 'd'; 
                }
                break;
    case  0x35:
                if(Language == 0){    			 	
                	DayName[0] = 'G';
                	DayName[1] = 'i';
                	DayName[2] = 'o';
                }
                else{
                	DayName[0] = 'T';
                	DayName[1] = 'h';
                	DayName[2] = 'u'; 
                }
                break;
    case  0x36:
                if(Language == 0){    			 	
                	DayName[0] = 'V';
                	DayName[1] = 'e';
                	DayName[2] = 'n';
                }
                else{
                	DayName[0] = 'F';
                	DayName[1] = 'r';
                	DayName[2] = 'i'; 
                }
                break;
    case  0x37:
                if(Language == 0){    			 	
                	DayName[0] = 'S';
                	DayName[1] = 'a';
                	DayName[2] = 'b';
                }
                else{
                	DayName[0] = 'S';
                	DayName[1] = 'a';
                	DayName[2] = 't'; 
                }
                break;
  }
#endif  
}

// -------------------------------------------------------------------2----------
// 
//  Leggi singolo parametro dell'orologio DS1339 
// 
// -----------------------------------------------------------------------------

unsigned char Clk_ReadSingle(unsigned char Pos)
{
  TWI_Read ( AT91C_BASE_TWI, DS1339 , Pos,(unsigned char *)&Orario, 1);
  
  DatePLL[0]  = ((Orario[0] & 0xF0) >> 4) | 0x30;
  DatePLL[1]  =  (Orario[0] & 0x0F)       | 0x30;
  return (Orario[0]&0x0F)+((Orario[0]>>4)*0x0A);
}

// -------------------------------------------------------------------2----------
// 
//  Aggiorna singolo parametro dell'orologio DS1339 
// 
// -----------------------------------------------------------------------------

void Clk_WriteSingle(unsigned char Pos)
{
  Orario[0]=BinBcd2(CfgV);
  TWI_Write ( AT91C_BASE_TWI, DS1339 , Pos,(unsigned char *)&Orario, 1);
}

// -----------------------------------------------------------------------------
// 
//  Ora legale
// 
// -----------------------------------------------------------------------------

void LegalHour( void )
{
  static unsigned char Ottobre=0;
  
  if (Orario[5] != 0x10)
  {
    Ottobre = 0;
  }
   
  if ((Orario[5] == 0x03) &&       // Se mese di marzo (03)
      (Orario[4] >  0x24) &&       // se ultima
      (Orario[3] == 0x01) &&       // domenica del mese
      (Orario[2] == 0x01) &&       // se ora 01
      (Orario[1] == 0x59))         // se minuto 59     
  {    
    DataOra[2] = 0x02;             // l'ora viene messa avanti di 1
  
    TWI_Write ( AT91C_BASE_TWI, DS1339, 0x02,(unsigned char *)&DataOra[2], 1);   
  }

  if ((Orario[5] == 0x10) &&       // Se mese di ottobre (10)
      (Orario[4] >  0x24) &&       // se ultima
      (Orario[3] == 0x01) &&       // domenica del mese
      (Orario[2] == 0x02) &&       // se ora 02
      (Orario[1] == 0x59) &&       // se minuto 59
      (Ottobre == 0))                
  {    
    DataOra[2] = 0x01;             // l'ora viene messa indietro di 1
  
    TWI_Write ( AT91C_BASE_TWI, DS1339, 0x02,(unsigned char *)&DataOra[2], 1);
    
    Ottobre = 1;                   // flag che impedisce la ripetizione   
  }

}

/*-----------------------------------------------------------------------------
Function: AT91F_AT24C_MemoryReset(void)

Arguments:
- <const AT91PS_PIO pPio> : Pointer to a PIO structure,
- <int PioID> :  PIO peripheral ID
- <int Twck> : PIO/TWI clock pin
- <int Twd> : PIO/TWI data pin

Comments:
This function sends a "bus recovery" sequence" compatible with I2C(tm)
MEMORY RESET: After an interruption in protocol, power loss or system reset,
any two-wire part can be reset by following these steps:
1. Clock up to 9 cycles.
2. Look for SDA high in each cycle while SCL is high.
3. Create a start condition.
Since the TWI Port does not have the embedded "bus recovery" sequence, it
is managed in PIO mode. The clock peiod is set to 400KHz.

Return Value: None
-----------------------------------------------------------------------------*/
void AT91F_AT24C_MemoryReset (const AT91PS_PIO pPio,unsigned int PioID, unsigned int Twck, unsigned int Twd)
{
  unsigned int ClockPulse;

  /* First, enable the clock of the PIO */
  AT91F_PMC_EnablePeriphClock ( AT91C_BASE_PMC, 1 << PioID ) ;

  /* Set the TWI pins controlled by the PIO */
  AT91F_PIO_Enable(pPio,Twck|Twd ) ;

  /* then, we configure the PIO Lines corresponding to TWD & TWCK
     to be outputs. */
  AT91F_PIO_CfgOutput (pPio, Twck|Twd ) ;

  /* Enable open drain on TWCK / TWD PIO lines */
  pPio->PIO_MDER = (Twck|Twd) ;

  /* Disable the pull up on the TWI line */
  pPio->PIO_PPUDR = (Twck|Twd) ;

  /* Set TWCK and TWD to high level */
  AT91F_PIO_SetOutput(pPio, Twck|Twd ) ;
  microsecondi(2);//AT91F_AT24C_WaitMicroSecond(2);

  /* Perform the bus recovery sequence */

  /* 1. Clock up to 9 cycles */
  for (ClockPulse=0 ; ClockPulse < 9 ; ClockPulse++)
  {
     /* Toggle the clock */
     AT91F_PIO_SetOutput(pPio, Twck );
     microsecondi(2);//AT91F_AT24C_WaitMicroSecond(2);
     AT91F_PIO_ClearOutput(pPio, Twck );
     microsecondi(2);//AT91F_AT24C_WaitMicroSecond(2);
     AT91F_PIO_SetOutput(pPio, Twck );
     microsecondi(2);//AT91F_AT24C_WaitMicroSecond(2);

     /* 2. Look for SDA high in each cycle while SCL is high */
     AT91F_PIO_CfgInput(pPio,Twd );
     
     if ( (AT91F_PIO_GetInput(pPio)&Twd) == Twd)
      ClockPulse = 10;
  }
  
  AT91F_PIO_SetOutput(pPio, Twck|Twd ) ;
  AT91F_PIO_CfgOutput (pPio, Twd ) ;
  /* Set the TWCK and TWD to send the next start */
  AT91F_PIO_ClearOutput(pPio, Twd );
  AT91F_PIO_SetOutput(pPio, Twck);
  microsecondi(4);//AT91F_AT24C_WaitMicroSecond(2);

  /* 3. Create a stop condition */
  AT91F_PIO_SetOutput(pPio, Twd );
  microsecondi(2);//AT91F_AT24C_WaitMicroSecond(2);
  AT91F_PIO_SetOutput(pPio, Twck );
  microsecondi(2);//AT91F_AT24C_WaitMicroSecond(2);


}



