/*****************************************************************************
* IAR visualSTATE Main Loop Header File
*
* The file contains an implementation for a main loop using the visualSTATE
* basic API.
*
* The code uses a simple queue for storing events. The functions for inter-
* facing to the queue are described in the sample code file
* simpleEventHandler.h.
*****************************************************************************/

#ifndef _VSLOGICMAIN_H
#define _VSLOGICMAIN_H

/* *** include directives *** */

#include "VSMain.h"

#define FMWV1   (unsigned char) 0x01      // Versione EEPROM
//#define FMWV2   (unsigned char) 0x01      // Versione EEPROM
#define FMWV2   (unsigned char) 0x01      // Versione EEPROM da 1.11.8.9

#define CfgAdd  0x7F00    // Indirizzo di partenza dati di configurazione       
#define ADDTSESSION 0x7E00   //Indirizzo dove viene congelato periodicamente il tempo di intervento rimasto
#define ADDIDKEY    0x7E04

/* *** variable declarations *** */

extern unsigned char catchATR0;
extern unsigned char catchATR1;
extern unsigned char TOn,TOff,TBioOn;
extern unsigned char T400;
extern unsigned char LedsConsoleVisible;

extern char catchATR;
extern char catchPULS1;
extern char catchPULS2;
extern char catchPC1;
extern char catchPC2;
extern char catchPA1;
extern char catchPA2;
extern char catchCOM_MANOUT;
extern char catchCOM_MANIN;   
extern char catchCOM_MANBOTH;
extern char catchOBJECT;
extern char catchMORE;
extern char catchINIB_METp;
extern char catchEMERGp;
extern char catchEMERGrem;
extern char catchPCON;


/* *** function declarations *** */

unsigned char LogicInit(SEM_CONTEXT VS_TQ_CONTEXT * * Context);

unsigned char LogicMain(SEM_CONTEXT *pSEMCont);   

unsigned char IsCommKey(void);


VS_VOID DisplayStatus(VS_VOID);
VS_VOID LogicDisplay (VS_INT MsgId);

#endif
