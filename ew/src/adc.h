// -----------------------------------------------------------------------------

// Headers

// -----------------------------------------------------------------------------

#ifndef adc_h
#define adc_h

// -----------------------------------------------------------------------------

// ADC field definition for the Mode Register

/* TRGEN    => Select bewteen Software or hardware start of conversion

   TRGSEL   => Relevant if previous field is set on hardware triggering

               0x0 << 1 (ADC) Selected TRGSEL = TIAO0
               0x1 << 1 (ADC) Selected TRGSEL = TIAO1
               0x2 << 1 (ADC) Selected TRGSEL = TIAO2

   LOWRES   => 10-bit result if ths bit is cleared 0

   SLEEP    => normal mode if ths is cleared

   PRESCAL  => ADCclock = MCK / [(PRESCAL + 1)*2]

   STARTUP  => Startup Time = [(STARTUP + 1)*8] / ADCclock

   SHTIM    => Tracking time = (SHTIM + 1) / ADCclock
 */
// -----------------------------------------------------------------------------

#define   TRGEN     (0x1)   // Hardware triggering
#define   TRGSEL    (0x0)   // Use a Timer output signal (on rising edge) from 
                            // TIOA0 (for this example)
#define   LOWRES    (0x0)   // 10-bit result output
#define   SLEEP     (0x0)   // Normal Mode
#define   PRESCAL   (0x4)   // Max value
#define   STARTUP   (0xC)   // This time period must be higher than 20 �s and 
                            // not 20 ms
#define   SHTIM     (0x2)    // Must be higher than 3 ADC clock cycles but 
                             // depends on output impedance of the analog driver
                             // to the ADC input

#define   ADC_MODE ((SHTIM << 24) | (STARTUP << 16) | (PRESCAL << 8) | (SLEEP << 5) | (LOWRES <<4) | (TRGSEL << 1) | (TRGEN ))

#define   CHANNEL0  ((unsigned int) 0x1 <<  0) // Canale ADC 0     
#define   CHANNEL1  ((unsigned int) 0x1 <<  1) // Canale ADC 1     
#define   CHANNEL2  ((unsigned int) 0x1 <<  2) // Canale ADC 2     
#define   CHANNEL3  ((unsigned int) 0x1 <<  3) // Canale ADC 3     
#define   CHANNEL4  ((unsigned int) 0x1 <<  4) // Canale ADC 4     
#define   CHANNEL5  ((unsigned int) 0x1 <<  5) // Canale ADC 5     
#define   CHANNEL6  ((unsigned int) 0x1 <<  6) // Canale ADC 6     
#define   CHANNEL7  ((unsigned int) 0x1 <<  7) // Canale ADC 7     

#define   NLETT  16 // Numero letture 
     

// -----------------------------------------------------------------------------
//
// Variabili
//
// -----------------------------------------------------------------------------

extern unsigned char BcdBuff[];
                    
// -----------------------------------------------------------------------------
//
// Prototipi
//
// -----------------------------------------------------------------------------

void ADC_init ( void );
unsigned int ADC_read ( unsigned int channel, unsigned char n_read );

#endif
